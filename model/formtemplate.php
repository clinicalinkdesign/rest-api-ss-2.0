<?php

if(!defined('MyConst')) {
    die('Direct access not permitted');
}

include_once '../../functions/globalCUD.php';

class formtemplate{

    //database connection and table name
    private $conn;
    private $tableName = "formtemplate";

    //object properties
    public $id;
    public $name;
    public $studyid;
    public $createdDate;
    public $lastModifiedDate;
    public $isRemovedFlag;
    
    private $globalCUD;

    //constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
        $this->globalCUD = new globalCUD();
    }

    public function getConn(){
        return $this->conn;
    }

    //read quotes
    function read($where){

        $query = '';
        
        //only id provided
        if($where != "")
        {
            $query = 'SELECT id, name, studyid, createdDate, lastModifiedDate, isRemovedFlag 
                        FROM formtemplate 
                        WHERE '.$where.'
                        ORDER BY studyid asc, name asc';
        }
        //select all query 
        else{
            $query = 'SELECT id, name, studyid, createdDate, lastModifiedDate, isRemovedFlag 
                        FROM formtemplate 
                        ORDER BY studyid asc, name asc';                                  
        }

        //prepare query statement
        $stmt = $this->conn->prepare($query);

        //execute query
        $stmt->execute();
        $num = $stmt->rowCount();

        //check if more than 0 record found
        if($num>0){
            //if $id is specified we also need to return a list of studies associated with it
            //other wise, just return the entire list of formtemplates
            if(!empty($_GET)){
                //formtemplate array
                $output_arr = array();

                //retrive formtemplate table conents
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    
                    //extract row
                    extract($row);
                    $item = array(
                        "id" => $id,
                        "name" => $name,
                        "studyid" => $studyid,
                        "createdDate" => $createdDate,
                        "lastModifiedDate" => $lastModifiedDate,
                        "isRemovedFlag" => $isRemovedFlag,
                        "sectionTemplates" => $this->sectionTemplateAssoc($id)
                    );

                    array_push($output_arr, $item);
                }
            }
            else{
                
                return $stmt->fetchAll(\PDO::FETCH_ASSOC);
            }

            return $output_arr;
        }
        else {
            return null;
        }
    }

    function create($array){

        //Check that your study actually exist!
        if(array_key_exists('studyid', $array) && !id_Exists($array['studyid'], "study", $this->getConn())){ 
                
            $currRet['success'] = "false";
            $currRet['message'] = "studyid Not Found";

            return $currRet;

        }
        else{        
            return $this->globalCUD->CreateGlobal($array, $this->conn, $this->tableName, $this->id);
        }
        
    }

    function update($array){

        $this->id = $array['id'];

        //Check that your study actually exist!
        if(array_key_exists('studyid', $array) && !id_Exists($array['studyid'], "study", $this->getConn())){ 
                
            $currRet['success'] = "false";
            $currRet['message'] = "studyid Not Found";

            return $currRet;

        }
        else{      
            return $this->globalCUD->UpdateGlobal($array, $this->conn, $this->tableName);
        }
    }

    function delete($array){

        $this->id = $array['id'];

        return $this->globalCUD->DeleteGlobal($array, $this->conn, $this->tableName);

    }

    function sectionTemplateAssoc($formtemplateid)
    {
        $query =  'SELECT sectiontemplate.id, 
                            sectiontemplate.name, 
                            sectiontempformlink.orderInForm, 
                            sectiontempformlink.id AS "sectiontempformlinkid",
                            sectiontempformlink.isRemovedFlag AS "isRemovedFlag"
                            FROM sectiontempformlink
                            JOIN sectiontemplate ON sectiontemplate.id = sectiontempformlink.sectionTemplateid
                            WHERE formtemplateid = "'.$formtemplateid.'"
                            ORDER by sectiontempformlink.orderInForm asc';

        //prepare query statement
        $stmt = $this->conn->prepare($query);

        //execute query
        $stmt->execute();

        if($stmt->rowCount() > 0){
            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        }
        else{
            return null;
        }
    }
}

?>