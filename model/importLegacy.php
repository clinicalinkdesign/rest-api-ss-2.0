<?php 

if(!defined('MyConst')) {
    die('Direct access not permitted');
}

include_once '../../functions/importCUD.php';

class importLegacy{

    //database connection and table name
    private $conn;
    private $history;
    private $importCUD;
    private $adminLegacyImport;

    //constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
        $this->importCUD = new importCUD();
        $this->history = new History($this->conn);
        $this->adminLegacyImport = $this->importAdminid();
    }

    public function getConn(){
        return $this->conn;
    }

    function importCrossForm(&$array) {

        $study = $this->quickQuery('ciStudyProjectCode = '.$this->conn->quote($_GET['ciStudyProjectCode']).' AND (isRemovedFlag IS NULL OR isRemovedFlag <> "1")', 'study');

        if($study != null) {

            //Data pulled from DB
            $arms = $this->quickQuery('studyid = '.$this->conn->quote($study[0]['id']).' AND (isRemovedFlag IS NULL OR isRemovedFlag <> "1")', 'arm');

            $crossForm = array();
            $newCrossForm = array();

            //Start at 1, not 0, first row is the headers in the CSV
            for($i = 1; $i < sizeof($array); $i++) {

                //Clean each input of ending spaces using rtrim
                foreach($array[$i] as &$piece) {
                    $piece = rtrim($piece);
                }
                
                //First determine which arm this falls in (you need this for the proper id, not the name)
                //If the arm in the file is Default or Scheduled and the study has 1 arm, move on, youre good
                //If the study has more than 1 arm, the arms in filemaker MUST match the arms in the portal report
                //Otherwise, you cannot import the triggers OR the cross form

                //Another thought, search for the arm value from this report as a index of in arm names from filemaker
                /*
                    Example from OTS0035
                    in FM: OTS0035 Otsuka 331-201-00176 - Screening
                    in portal: Screening
                */

                if($arms != null) {
                    //Determine which arm to use: parent / child
                    if(strtolower($array[$i]['ParentArm']) == "default" || strtolower($array[$i]['ParentArm']) == "scheduled") {
                        $parentArmid = $arms[0]['id'];
                    }
                    else {
                        foreach($arms as $item) {
                            if(strpos(strtolower($item['name']), strtolower($array[$i]['ParentArm']))) {
                                $parentArmid = $item['id'];
                                //print_r($parentArmid);
                                break;
                            }
                        }
                    }

                    
                    if(strtolower($array[$i]['ChildArm']) == "default" || strtolower($array[$i]['ChildArm']) == "scheduled") {
                        $childArmid = $arms[0]['id'];
                    }
                    else {
                        foreach($arms as $item) {
                            if(strpos(strtolower($item['name']), strtolower($array[$i]['ChildArm']))) {
                                $childArmid = $item['id'];
                                //print_r($childArmid);
                                break;
                            }
                        }
                    }
                }
                else {
                    //set response code - 400 bad request
                    http_response_code(400);
                    
                    //tell the user
                    echo json_encode(array("message" => "No arms available for requested study. Have you imported the visit matrix?"));

                    exit();
                }
                
                //Time to look up Visits and forms per arm above: parent / child
                $parentArmFormVisit = $this->visitFormLookup($parentArmid);
                $childArmFormVisit = $this->visitFormLookup($childArmid);
                
                if($parentArmFormVisit != null && $childArmFormVisit != null) {
                    //Find the Child Visit / Form Combo
                    //Note: Child visit names can be null, this is because they are add ons
                    $childFormid = "";
                    if($array[$i]['ChildVisit'] == "AdverseEvents" || $array[$i]['ChildVisit'] == "ConMeds" || $array[$i]['ChildVisit'] == "MedHx") {
                        foreach($childArmFormVisit as $visitForm) {
                            if(strpos(strtolower($visitForm['visitName']), "application") !== FALSE && $visitForm['formName'] == $array[$i]['ChildForm']) {
                                $childFormid = $visitForm['formid'];
                                break;
                            }
                        }
                    }
                    else if($array[$i]['ChildVisit'] != "" && $array[$i]['ChildVisit'] != "NULL") {
                        foreach($childArmFormVisit as $visitForm) {
                            if($visitForm['visitName'] == $array[$i]['ChildVisit'] && $visitForm['formName'] == $array[$i]['ChildForm']) {
                                $childFormid = $visitForm['formid'];
                                break;
                            }
                        }
                    }
                    else {
                        //Assume that null here means that it is an add on
                        //Find the "add on visit" for this study

                        $addOnit = array(
                            "addon",
                            "add on",
                            "add-on",
                            "add_on"
                        );

                        foreach($childArmFormVisit as $visitForm) {
                            $visitName = strtolower($visitForm['visitName']);

                            if((strpos($visitName, $addOnit[0]) !== false || strpos($visitName, $addOnit[1]) !== false || 
                               strpos($visitName, $addOnit[2]) !== false || strpos($visitName, $addOnit[3]) !== false) &&
                               $visitForm['formName'] == $array[$i]['ChildForm']) {
                                $childFormid = $visitForm['formid'];
                                break;
                            }
                        }
                    }

                    //Find the Child Field / Friendly Name Combo
                    $childFieldVerid = "";
                    if($array[$i]['ChildField'] != "") {
                        $childFieldVerid = $this->studySRCFieldLookup($array[$i]['ChildField'], $childFormid, $study[0]['id']);
                    }

                    //Find the Parent Visit / Form Combo
                    $parentFormid = "";
                    foreach($parentArmFormVisit as $visitForm) {
                        if($visitForm['visitName'] == $array[$i]['ParentVisit'] && $visitForm['formName'] == $array[$i]['ParentForm']) {
                            $parentFormid = $visitForm['formid'];
                            break;
                        }
                    }

                    //Find the Parent Field / Friendly Name Combo
                    $parentFieldVerid = "";
                    if($array[$i]['ParentField'] != "") {
                        $parentFieldVerid = $this->studySRCFieldLookup($array[$i]['ParentField'], $parentFormid, $study[0]['id']);
                    }

                    //Wrap it all up and make it look pretty
                    $newCrossForm = array(
                        "id" => get_UUID($this->conn),
                        "studyid" => $study[0]['id'],
                        "childFormid" => $childFormid,
                        "childFieldVerid" => $childFieldVerid,
                        "parentFormid" => $parentFormid,
                        "parentFieldVerid" => $parentFieldVerid,
                        "stuff" => $array[$i]
                    );

                    array_push($crossForm, $newCrossForm);
                }
                else {
                    //set response code - 400 bad request
                    http_response_code(400);
                    
                    //tell the user
                    echo json_encode(array("message" => "Arm was unable to return any Visit/Form relationships. Verify a visit matrix exists."));
                    
                    exit();
                }
            }

            //echo(json_encode($arms));
        
            $this->importCrossFormQuery($crossForm);
        }
        else {
            //set response code - 400 bad request
            http_response_code(400);
            
            //tell the user
            echo json_encode(array("message" => "Requested study does not exist. Create study in database prior to import."));
            
            exit();
        }
    }

    function importCrossFormQuery(&$crossForm) {
        $outputArray = array(
            "crossformlinking" => array()
        );

        $this->runQueries($outputArray, $crossForm, 'crossformlinking');

        echo json_encode($outputArray);
    }

    function importTriggers(&$array) {

        $study = $this->quickQuery('ciStudyProjectCode = '.$this->conn->quote($_GET['ciStudyProjectCode']).' AND (isRemovedFlag IS NULL OR isRemovedFlag <> "1")', 'study');

        if($study != null) {

            //Data pulled from DB
            $arms = $this->quickQuery('studyid = '.$this->conn->quote($study[0]['id']).' AND (isRemovedFlag IS NULL OR isRemovedFlag <> "1")', 'arm');

            $visitTriggers = array();
            $newVisitTrigger = array();
            $formTriggers = array();
            $newFormTrigger = array();

            //Start at 1, not 0, first row is the headers in the CSV
            for($i = 1; $i < sizeof($array); $i++) {

                //Clean each input of ending spaces using rtrim
                foreach($array[$i] as &$piece) {
                    $piece = rtrim($piece);
                }
                
                //First determine which arm this falls in (you need this for the proper id, not the name)
                //If the arm in the file is Default or Scheduled and the study has 1 arm, move on, youre good
                //If the study has more than 1 arm, the arms in filemaker MUST match the arms in the portal report
                //Otherwise, you cannot import the triggers OR the cross form

                //Another thought, search for the arm value from this report as a index of in arm names from filemaker
                /*
                    Example from OTS0035
                    in FM: OTS0035 Otsuka 331-201-00176 - Screening
                    in portal: Screening
                */

                if($arms != null) {
                    //Determine which arm to use: parent / child
                    if(strtolower($array[$i]['ParentArm']) == "default" || strtolower($array[$i]['ParentArm']) == "scheduled") {
                        $parentArmid = $arms[0]['id'];
                    }
                    else {
                        foreach($arms as $item) {
                            if(strpos(strtolower($item['name']), strtolower($array[$i]['ParentArm'])) !== FALSE) {
                                $parentArmid = $item['id'];
                                //print_r($parentArmid);
                                break;
                            }
                        }
                    }

                    
                    if(strtolower($array[$i]['ChildArm']) == "default" || strtolower($array[$i]['ChildArm']) == "scheduled") {
                        $childArmid = $arms[0]['id'];
                    }
                    else {
                        foreach($arms as $item) {
                            if(strpos(strtolower($item['name']), strtolower($array[$i]['ChildArm'])) !== FALSE) {
                                $childArmid = $item['id'];
                                //print_r($childArmid);
                                break;
                            }
                        }
                    }
                }
                else {
                    //set response code - 400 bad request
                    http_response_code(400);
                    
                    //tell the user
                    echo json_encode(array("message" => "No arms available for requested study. Have you imported the visit matrix?"));

                    exit();
                }

                //Time to look up Visits and forms per arm above: parent / child
                $parentArmFormVisit = $this->visitFormLookup($parentArmid);
                $childArmFormVisit = $this->visitFormLookup($childArmid);

                if($parentArmFormVisit != null && $childArmFormVisit != null) {
                    //Now map it all
                    if($array[$i]['TriggerType'] == "Visit") {

                        //Search for source form
                        $sourceFormid = "";
                        foreach($parentArmFormVisit as $visitForm) {
                            if($visitForm['visitName'] == $array[$i]['ParentVisit'] && $visitForm['formName'] == $array[$i]['TriggerForm']) {
                                $sourceFormid = $visitForm['formid'];
                                break;
                            }
                        }

                        //Search for the target VISIT
                        $targetVisitid = "";
                        foreach($childArmFormVisit as $visit) {
                            if($visit['visitName'] == $array[$i]['ChildVisit']) {
                                $targetVisitid = $visit['visitid'];
                                break;
                            }
                        }

                        //Search for the fieldid
                        $sourceFieldid = "";
                        if($array[$i]['Trigger'] != "" && $array[$i]['Trigger'] != "Form Save") {
                            $sourceFieldid = $this->studySRCFieldLookup($array[$i]['Trigger'], $sourceFormid, $study[0]['id']);
                        }
                        
                        $newVisitTrigger = array(
                            "id" => get_UUID($this->conn),
                            "studyid" => $study[0]['id'],
                            "triggerSourceType" => ($array[$i]['Trigger'] == "Form Save") ? ("form") : ("field"),
                            "sourceFormVisitLinkid" => $sourceFormid, 
                            "triggerVisitid" => $targetVisitid,
                            "sourceTriggerFieldid" => $sourceFieldid
                        );

                        array_push($visitTriggers, $newVisitTrigger);
                    }
                    else if($array[$i]['TriggerType'] == "Conditional Form") {

                        //Search for source form
                        $sourceFormid = "";
                        foreach($parentArmFormVisit as $visitForm) {
                            if($visitForm['visitName'] == $array[$i]['ParentVisit'] && $visitForm['formName'] == $array[$i]['TriggerForm']) {
                                $sourceFormid = $visitForm['formid'];
                                break;
                            }
                        }

                        //Search for the target VISIT
                        $targetFormid = "";
                        foreach($childArmFormVisit as $visit) {
                            if($visit['visitName'] == $array[$i]['ChildVisit'] && $visit['formName'] == $array[$i]['ConditionalForm']) {
                                $targetFormid = $visit['formid'];
                                break;
                            }
                        }

                        //Search for the fieldid
                        $sourceFieldid = "";
                        if($array[$i]['Trigger'] != "" && $array[$i]['Trigger'] != "Form Save") {
                            $sourceFieldid = $this->studySRCFieldLookup($array[$i]['Trigger'], $sourceFormid, $study[0]['id']);
                        }
                        
                        $newFormTrigger = array(
                            "id" => get_UUID($this->conn),
                            "studyid" => $study[0]['id'],
                            "triggerSourceType" => ($array[$i]['Trigger'] == "Form Save") ? ("form") : ("field"),
                            "sourceFormVisitLinkid" => $sourceFormid, 
                            "triggerFormid" => $targetFormid,
                            "sourceTriggerFieldid" => $sourceFieldid
                        );

                        array_push($formTriggers, $newFormTrigger);
                    }
                }
                else {
                    //set response code - 400 bad request
                    http_response_code(400);
                    
                    //tell the user
                    echo json_encode(array("message" => "Arm was unable to return any Visit/Form relationships. Verify a visit matrix exists."));
                    
                    exit();
                }
            }

            //echo(json_encode($array));
        
            $this->importTriggersQuery($formTriggers, $visitTriggers);
        }
        else {
            //set response code - 400 bad request
            http_response_code(400);
            
            //tell the user
            echo json_encode(array("message" => "Requested study does not exist. Create study in database prior to import."));
            
            exit();
        }
    }

    function importTriggersQuery(&$formTriggers, &$visitTriggers) {
        $outputArray = array(
            "visittrigger" => array(),
            "formtrigger" => array()
        );

        $this->runQueries($outputArray, $visitTriggers, 'visittrigger');
        $this->runQueries($outputArray, $formTriggers, 'formtrigger');

        echo json_encode($outputArray);
    }

    function importVisitMatrix(&$array) {
        //0. check left most column in all rows for empty and report the empty rows, then bail out (this is because section order must be present to import)
        //1. look up the study in the DB (need the id)
        //2. map out all the arms
        //3. map out all the visits
        //4. map out all the form templates
        //5. map out all the forms
        //6. map out all the section template form links

        //if there are errors program will terminate and warn the user
        $this->visitMatrixErrorCheck($array);        

        $study = $this->quickQuery('ciStudyProjectCode = '.$this->conn->quote(substr($array[0]['Study_Name'], 0, 7)).' AND (isRemovedFlag IS NULL OR isRemovedFlag <> "1")', 'study');
        $sectionTemplates = $this->sectionTemplates($study[0]['id']);

        if($study != null) {
            //ARM
            $arms = array();
            $tempArmNames = array();
            $armCount = 1;

            //VISIT
            $visits = array();
            $tempVisitName = array();

            //FORM TEMPLATES
            $formTemplates = array();
            $tempFormTNames = array();

            //FORMS
            $forms = array();
            $tempFormNames = array();

            //SECTION TEMPLATE FORM LINK
            $stFormLinks = array();
            $tempStFormLinks = array();

            $addOnit = array(
                "addon",
                "add on",
                "add-on",
                "add_on"
            );

            for($i = 0; $i < sizeof($array); $i++) {

                //Clean each input of ending spaces using rtrim
                foreach($array[$i] as &$item) {
                    $item = rtrim($item);
                }

                $test = $array[$i]['Section_Order'] != "" &&
                        $array[$i]['Section_Name'] != "" &&
                        $array[$i]['Section_Template'] != "" &&
                        $array[$i]['Arm_Name'] != "" &&
                        $array[$i]['Visit_Name'] != "" &&
                        $array[$i]['Visit_Order'] != "" &&
                        $array[$i]['Form_Template_Name'] != "" &&
                        $array[$i]['Form_Name'] != "" &&
                        $array[$i]['Form_Order'] != "" &&
                        $array[$i]['Study_Name'] != "";

                if($test) {
                    //ARM CREATION FOR EACH ROW IN CSV
                    if(!array_key_exists($array[$i]['Arm_Name'], $tempArmNames) && $array[$i]['Arm_Name'] != "") {
                        $newArm = array(
                            "id" => get_UUID($this->conn),
                            "name" => $array[$i]['Arm_Name'],
                            "studyid" => $study[0]['id'],
                            "armOrder" => $armCount
                        );

                        array_push($arms, $newArm);
                        $tempArmNames = array_merge($tempArmNames, array($newArm['name'] => $newArm['id']));

                        $armCount++;
                    }

                    //VISIT CREATION FOR EACH ROW IN CSV
                    if(!array_key_exists($array[$i]['Arm_Name'].$array[$i]['Visit_Name'], $tempVisitName) && $array[$i]['Visit_Name'] != "") {

                        //Determine what type of visit for visitType field
                        // 0 - normal visit
                        // 1 - add on forms
                        // 2 - application level
                        // 3 - unscheduled visit

                        $visitType = null;
                        $testVisitName = strtolower($array[$i]['Visit_Name']);

                        //Add on first
                        //"addon","add on","add-on","add_on"
                        if(strpos($testVisitName, $addOnit[0]) !== false || strpos($testVisitName, $addOnit[1]) !== false || 
                           strpos($testVisitName, $addOnit[2]) !== false || strpos($testVisitName, $addOnit[3]) !== false)
                        {
                            $visitType = "1";
                        }
                        //unscheduled
                        else if(strpos($testVisitName, "unscheduled") !== false)
                        {
                            $visitType = "3";
                        }
                        //application level
                        else if(strpos($testVisitName, "application") !== false)
                        {
                            $visitType = "2";
                        }
                        else
                        {
                            $visitType = "0";
                        }

                        $newVisit = array(
                            "id" => get_UUID($this->conn),
                            "name" => $array[$i]['Visit_Name'],
                            "armid" => $tempArmNames[$array[$i]['Arm_Name']],
                            "visitOrder" => $array[$i]['Visit_Order'],
                            "visitType" => $visitType,
                            "filemakerVisitNote" => $array[$i]['Notes']
                        );

                        array_push($visits, $newVisit);
                        $tempVisitName = array_merge($tempVisitName, array($array[$i]['Arm_Name'].$array[$i]['Visit_Name'] => $newVisit['id']));
                    }

                    //FORM TEMPLATE CREATION FOR EACH ROW IN CSV
                    if(!array_key_exists($array[$i]['Form_Template_Name'], $tempFormTNames) && $array[$i]['Form_Template_Name'] != "") {
                        $newFormT = array(
                            "id" => get_UUID($this->conn),
                            "name" => $array[$i]['Form_Template_Name'],
                            "studyid" => $study[0]['id']
                        );

                        array_push($formTemplates, $newFormT);
                        $tempFormTNames = array_merge($tempFormTNames, array($newFormT['name'] => $newFormT['id']));
                    }

                    //FORM CREATION
                    if(!array_key_exists($array[$i]['Arm_Name'].$array[$i]['Visit_Name'].$array[$i]['Form_Name'], $tempFormNames) && $array[$i]['Form_Name'] != "") {
                        $newForm = array(
                            "id" => get_UUID($this->conn),
                            "name" => $array[$i]['Form_Name'],
                            "visitid" => $tempVisitName[$array[$i]['Arm_Name'].$array[$i]['Visit_Name']],
                            "orderInVisit" => $array[$i]['Form_Order'],
                            "formTemplateid" => $tempFormTNames[$array[$i]['Form_Template_Name']]
                        );

                        array_push($forms, $newForm);
                        $tempFormNames = array_merge($tempFormNames, array($array[$i]['Arm_Name'].$array[$i]['Visit_Name'].$array[$i]['Form_Name'] => $newForm['id']));
                    }

                    //SECTION TEMPLATE FORM LINK CREATION FOR EACH ROW IN CSV
                    $sectionTemplateid = "";

                    foreach($sectionTemplates as $template) {
                        if(strtolower($template['name']) === htmlspecialchars(strtolower($array[$i]['Section_Template']))) {
                            $sectionTemplateid = $template['id'];

                            break;
                        }
                    }

                    //THIS CAN BE USED FOR TROUBLESHOOTING EMPTY LINKS
                    /*
                    if($sectionTemplateid == "") {
                        print_r(json_encode($array[$i]));
                        //echo(json_encode($sectionTemplates));
                    }*/

                    if($array[$i]['Form_Template_Name'] != "" && !array_key_exists($sectionTemplateid.$tempFormTNames[$array[$i]['Form_Template_Name']], $tempStFormLinks) && $array[$i]['Section_Template'] != "") {
                        $newSTLink = array(
                            "id" => get_UUID($this->conn),
                            "orderInForm" => $array[$i]['Section_Order'],
                            "sectionTemplateid" => $sectionTemplateid,
                            "formTemplateid" => $tempFormTNames[$array[$i]['Form_Template_Name']]
                        );

                        array_push($stFormLinks, $newSTLink);
                        $tempStFormLinks = array_merge($tempStFormLinks, array($sectionTemplateid.$tempFormTNames[$array[$i]['Form_Template_Name']] => $newSTLink['id']));
                    }
                }
            }

            //print_r($stFormLinks);
            //echo json_encode($array);

            $this->importVisitMatrixQuery($arms, $visits, $formTemplates, $forms, $stFormLinks);
        }
        else {
            //set response code - 400 bad request
            http_response_code(400);
            
            //tell the user
            echo json_encode(array("message" => "Requested study does not exist. Create study in database prior to import."));
        }
    }

    function importVisitMatrixQuery(&$arms, &$visits, &$formTemplates, &$forms, &$stFormLinks) {
        
        $outputArray = array(
            "arm" => array(),
            "visit" => array(),
            "formtemplate" => array(),
            "form" => array(),
            "sectiontempformlink" => array(),
        );

        $this->runQueries($outputArray, $arms, 'arm');
        $this->runQueries($outputArray, $visits, 'visit');
        $this->runQueries($outputArray, $formTemplates, 'formtemplate');
        $this->runQueries($outputArray, $forms, 'form');
        $this->runQueries($outputArray, $stFormLinks, 'sectiontempformlink');

        echo json_encode($outputArray);
    }

    function importFields(&$array) {
        //0. look up the study in the DB (need the id)
        //1. map out all section groups
        //2. map out all section templates
        //3. map out all fields
        //4. map out all fieldversions

        $name = substr($array[0]['studyName'], 0, 7);

        $study = $this->quickQuery('ciStudyProjectCode = '.$this->conn->quote($name).' AND (isRemovedFlag IS NULL OR isRemovedFlag <> "1")', 'study');
        
        //echo json_encode($study);

        if($study != null) {
            //SECTION GROUP
            $sectionGroups = array();
            $tempSGNames = array();

            //SECTION TEMPLATE AND SECTION GROUP LINK
            $sectionTemplates = array();
            $tempSTNames = array();
            $sgGroupLink = array();

            //FIELD AND FIELD VERSION
            $fields = array();
            $tempFieldsList = array();
            $fieldVersions = array();

            for($i = 0; $i < sizeof($array); $i++) {

                //Clean each input of ending spaces using rtrim
                foreach($array[$i] as &$item) {
                    $item = rtrim($item);
                }

                //SECTION GROUP CREATION FOR EACH ROW IN CSV
                if(!array_key_exists($array[$i]['sectionGroupName'], $tempSGNames)) {
                    $newSG = array(
                        "id" => get_UUID($this->conn),
                        "name" => $array[$i]['sectionGroupName'],
                        "studyid" => $study[0]['id'],
                        "sectionGroupPrefix" => "_legacy_"
                    );

                    array_push($sectionGroups, $newSG);
                    $tempSGNames = array_merge($tempSGNames, array($newSG['name'] => $newSG['id']));
                }

                //SECTION TEMPLATE AND SECTION GROUP LINK CREATION FOR EACH ROW IN CSV
                if(!array_key_exists($array[$i]['sectionTemplateName'], $tempSTNames)) {
                    $newST = array(
                        "id" => get_UUID($this->conn),
                        "name" => $array[$i]['sectionTemplateName'],
                        "uniqueSectionHeader" => $array[$i]['sectionTemplateNameUnique']
                    );

                    $newSGgroupLink = array(
                        "id" => get_UUID($this->conn),
                        "sectionTemplateid" => $newST['id'], 
                        "sectionGroupid" => $tempSGNames[$array[$i]['sectionGroupName']]
                    );
                    
                    array_push($sectionTemplates, $newST);
                    $tempSTNames = array_merge($tempSTNames, array($newST['name'] => $newST['id']));
                    array_push($sgGroupLink, $newSGgroupLink);
                }

                //FIELD
                if(!array_key_exists($array[$i]['fileMakerFieldidLink'], $tempFieldsList)) {
                    $newField = array(
                        "id" => get_UUID($this->conn),
                        "fileMakerFieldidLink" => $array[$i]['fileMakerFieldidLink'],
                        "sectionTemplateid" => $tempSTNames[$array[$i]['sectionTemplateName']]
                    );

                    array_push($fields, $newField);
                    $tempFieldsList = array_merge($tempFieldsList, array($newField['fileMakerFieldidLink'] => $newField['id']));
                }
                
                //FIELD VERSION
                //TOOL TIP FLAG
                $toolTipFlag = "0";
                if($array[$i]['Range'] != "") {
                    $toolTipFlag = "1";
                }

                //IS REQUIRED FLAG
                $requiredFlag = "0";
                if(strpos($array[$i]['css_formatted_error_message'], "Required:") > 0) {
                    $requiredFlag = "1";
                }

                $newFieldVersion = array(
                    "id" => get_UUID($this->conn),
                    "ciFieldName" => $array[$i]['CI_Field_Name'],
                    "versionNum" => $array[$i]['Version'],
                    "ciFieldType" => $array[$i]['Field_Type'], 
                    "createdByAppUserid" => $this->adminLegacyImport, 
                    "lastModifiedByAppUserid" => $this->adminLegacyImport,
                    "ciFriendlyName" => $array[$i]['css_formatted_friendly_name'],
                    "ciLabelText" => $array[$i]['css_formatted_label'], 
                    "ciValues" => $array[$i]['css_formatted_values'], 
                    "ciRange" => $array[$i]['Range'], 
                    "ciLogic" => $array[$i]['css_formatted_logic'], 
                    "isRequiredFlag" => $requiredFlag, 
                    "ciValidation" => $array[$i]['Validation'], 
                    "hasToolTipFlag" => $toolTipFlag, 
                    "ciValidationMessage" => $array[$i]['Error_Message'], 
                    "cidesignInstructions" => $array[$i]['Design_Instructions'], 
                    "cidMInstructions" => $array[$i]['DM_Instructions'],    
                    "ciCustomField1" => $this->get_Custom($array[$i]),            
                    "version" => $array[$i]['Version'],
                    "fieldid" => $tempFieldsList[$array[$i]['fileMakerFieldidLink']],
                    "latestFlag" => $array[$i]['Latest_Flag']
                );

                //IS LATEST FLAG
                if ($array[$i]['Latest_Flag'] != "") {
                    $found_key = array_search($newFieldVersion["fieldid"], array_column($fields, "id"));

                    $removed = $array[$i]['Removed_Flag'];
                    $order = $array[$i]['Order'];

                    if ($removed != "") {
                        $fields[$found_key]['isRemovedFlag'] = $removed;
                    }

                    if ($order != "") {
                        $fields[$found_key]['ciFieldOrderNum'] = $order;
                    }
                    else {
                        //$fields[$found_key]['ciFieldOrderNum'] = "0";
                    }
                }

                if(!array_key_exists($tempFieldsList[$array[$i]['fileMakerFieldidLink']], $fieldVersions)) {                 
                    $fieldVersions = array_merge($fieldVersions, array($newFieldVersion['fieldid'] => array($newFieldVersion['version'] => $newFieldVersion)));
                }
                else {
                    array_push($fieldVersions[$newFieldVersion['fieldid']], $newFieldVersion);
                }
            }

            $this->orphanFieldCleanUp($fields, $fieldVersions);

            //TIME TO ACTUALLY IMPORT!
            $this->importFieldsQuery($sectionGroups, $sectionTemplates, $sgGroupLink, $fields, $fieldVersions);

            //print_r($tempSGNames);
            //echo json_encode($fields);
        }
        else {
            //set response code - 400 bad request
            http_response_code(400);
            
            //tell the user
            echo json_encode(array("message" => "Requested study (".$name.") does not exist. Create study in database prior to import."));
        }
    }

    function get_Custom(&$field) {

        $customarr = array();

        for ($i = 1; $i <= 9; $i++) {
            $key = $field['gCustom_Field_'.$i];
            $val = $field['Custom_Field_Value_'.$i];

            if ($val === null || trim($val) === '') {
                continue;
            }
                
            $customarr[$key] = $val;
        };

        $customarr['FM_id'] = $field['fileMakerFieldidLink'];
        
        return json_encode($customarr);
    }

    function orphanFieldCleanUp(&$fields, &$fieldVersions) {

        //This method takes the fields and fieldversions from import fields
        //It essentially creates an orphanage for all oprhaned fields within a section as a FIELD
        //Then it assigned all orphaned FIELDVERSIONS to that field and removes their previous parent fields from the array.
        //echo json_encode($fieldVersions);
        //$fieldsToRemove = array();

        $sectionTempList = array(); //sectionTempid => orphanGroupid
        foreach($fieldVersions as &$field) {

            // Determine if the field is an orphan (no FV has latestFlag='1')
            $isOrphan = true;
            foreach ($field as &$fv)
            {
                if ($fv['latestFlag'] == "1") {
                    $isOrphan = false;
                    break;
                }
            }

            if ($isOrphan && count($field) > 0) {

                $myFieldid = $field[key($field)]['fieldid'];
                $sectionTemplateid = "";
                $newFieldid = "";

                //Search for the section template id
                for($i = 0; $i < sizeof($fields); $i++) {
                    if($myFieldid == $fields[$i]['id']) {
                        $sectionTemplateid = $fields[$i]['sectionTemplateid'];
                        
                        // remove field
                        array_splice($fields, $i, 1);
                        break;
                    }
                }

                if ($sectionTemplateid == "") {
                    continue;
                }

                //Need to determine if the given section has an orphan list, if not, create it
                if(!array_key_exists($sectionTemplateid, $sectionTempList)) {
                    //Create the new field that houses orphans for this section template
                    $newField = array(
                        "id" => get_UUID($this->conn),
                        "sectionTemplateid" => $sectionTemplateid,
                        "ciFieldOrderNum" => '0'
                    );

                    array_push($fields, $newField);
                    $sectionTempList = array_merge($sectionTempList, array($sectionTemplateid => $newField['id']));   
                    $newFieldid = $newField['id'];
                }
                else {
                    $newFieldid = $sectionTempList[$sectionTemplateid];
                }

                foreach ($field as &$fieldVersion) {
                    $fieldVersion['fieldid'] = $newFieldid;
                }
            }
        }

        //echo json_encode($sectionTempList);
    }

    function importFieldsQuery(&$sectionGroups, &$sectionTemplates, &$sgGroupLink, &$fields, &$fieldVersions) {        
                
        $outputArray = array(
            "sectiongroup" => array(),
            "sectiontemplate" => array(),
            "sectiongrouplink" => array(),
            "field" => array(),
            "fieldversion" => array(),
        );

        $this->runQueries($outputArray, $sectionGroups, 'sectiongroup');
        $this->runQueries($outputArray, $sectionTemplates, 'sectiontemplate');
        $this->runQueries($outputArray, $sgGroupLink, 'sectiongrouplink');
        $this->runQueries($outputArray, $fields, 'field');
        
        foreach($fieldVersions as $field) {
            if(sizeof($field) > 1) {
                asort($field);
            }
            $this->runQueries($outputArray, $field, 'fieldversion');
        }

        echo json_encode($outputArray);
    }

    function runQueries(&$outputArray, $array, $tableName) {

        $list = array();

        //WRITE TO TABLE
        foreach($array as $item) {
            if($item != null) {
                array_push($outputArray[$tableName], $this->importCUD->CreateGlobal($item, $this->conn, $tableName));

                array_push($list, $item['id']);
            }
        }

        //WRITE TO HISTORY
        if($tableName != "fieldversion") {
            foreach($list as $item) {
		
                $this->history->write("IMPORT", $item, $tableName, $this->adminLegacyImport);
            }
        }
    }

    function quickQuery($where, $table) {
        $query = 'SELECT *
                    FROM '.$table.'
                    WHERE '.$where.'';

        //prepare query statement
        $stmt = $this->conn->prepare($query);

        //execute query
        $stmt->execute();

        if($stmt->rowCount() > 0) {
            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        }
        else {
            return null;
        }
    }

    function sectionTemplates($studyid) {

        $query = 'SELECT sectiontemplate.id, sectiontemplate.name
                    FROM sectiongroup
                        JOIN sectiongrouplink ON sectiongroup.id = sectiongrouplink.sectionGroupid
                        JOIN sectiontemplate ON sectiongrouplink.sectionTemplateid = sectiontemplate.id
                    WHERE sectiongroup.studyid = "'.$studyid.'"
                    ORDER BY sectiontemplate.name asc';
        
        //prepare query statement
        $stmt = $this->conn->prepare($query);

        //execute query
        $stmt->execute();

        if($stmt->rowCount() > 0) {
            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        }
        else {
            return null;
        }
    }

    function importSponsors($array) {

        $outputArray = array(
            "sponsor" => array()
        );

        for($i = 0; $i < sizeof($array); $i++) {
            $this->runQueries($outputArray, array(array("id" => get_UUID($this->conn),"name" => rtrim($array[$i]['0']))), 'sponsor');
        }

        echo json_encode($outputArray);
    }

    function importStudies($array) {

        $outputArray = array(
            "study" => array()
        );

        $studies = array();
        $sponsorid = "";

        for($i = 0; $i < sizeof($array); $i++) {

            $sponsor = $this->quickQuery('name = '.$this->conn->quote(htmlspecialchars(rtrim($array[$i]['sponsor']))).' AND (isRemovedFlag IS NULL OR isRemovedFlag <> "1")', 'sponsor');

            if($sponsor != null) {
                $sponsorid = $sponsor[0]['id'];
            }

            $newStudy = array(
                "id" => get_UUID($this->conn),
                "ciStudyProjectCode" => substr($array[$i]['name'], 0, 7), 
                "protocolName" => substr($array[$i]['name'], 8),
                "sponsorid" => $sponsorid,
                "isLegacyFlag" => "1",
                "proviedSponName" => htmlspecialchars(rtrim($array[$i]['sponsor'])),
                "foundSponName" => $sponsor[0]['name']
            );

            array_push($studies, $newStudy);
        }

        $this->runQueries($outputArray, $studies, 'study');

        echo json_encode($outputArray);
    }

    function visitFormLookup($armid) {
        $query = 'SELECT visit.armid AS armid,
                            visit.id AS visitid,
                            visit.name AS visitName,
                            t_form.id AS formid,
                            t_form.name AS formName
                    FROM visit
                        RIGHT JOIN form AS t_form on visit.id = t_form.visitid
                    WHERE armid = "'.$armid.'"
                    AND (visit.isRemovedFlag IS NULL OR visit.isRemovedFlag <> "1")
                    AND (t_form.isRemovedFlag IS NULL OR t_form.isRemovedFlag <> "1")
                    ORDER BY visitOrder asc';

        //prepare query statement
        $stmt = $this->conn->prepare($query);

        //execute query
        $stmt->execute();

        if($stmt->rowCount() > 0) {
            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        }
        else {
            return null;
        }
    }

    function studySRCFieldLookup($fieldName, $formid, $studyid) {
        //Takes the field name and study id and returns the id of the FIELD returned
        $query = 'SELECT sectiontempformlink.sectionTemplateid
        FROM form 
            JOIN formtemplate ON formtemplate.id = form.formTemplateid 
            JOIN sectiontempformlink ON sectiontempformlink.formTemplateid = formtemplate.id 
        WHERE form.id = "'.$formid.'"; ';

        //prepare query statement
        $stmt = $this->conn->prepare($query);

        //execute query
        $stmt->execute();

        if($stmt->rowCount() > 0) {
            $stmt = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        }
        else {
            return null;
        }

        $sectionTemplateIds = array();

        foreach($stmt as &$row) {
            array_push($sectionTemplateIds, $row['sectionTemplateid']);
        }

        $query = 'SELECT fieldversion.id 
        FROM fieldversion
            JOIN field AS t_field ON fieldversion.fieldid = t_field.id
            JOIN sectiontemplate AS t_sectiontemplate ON t_field.sectionTemplateid = t_sectiontemplate.id
            JOIN sectiongrouplink AS t_SGL ON t_SGL.sectionTemplateid = t_sectiontemplate.id
            JOIN sectiongroup AS t_SG ON t_SGL.sectionGroupid = t_SG.id
        WHERE fieldversion.ciFieldName = '.$this->conn->quote($fieldName).' AND t_SG.studyid = "'.$studyid.'" AND t_sectiontemplate.id IN ("'.implode('", "', $sectionTemplateIds).'")
        ORDER BY fieldversion.versionNum desc';

        //logCodeItem($query);

        //prepare query statement
        $stmt = $this->conn->prepare($query);

        //execute query
        $stmt->execute();

        if($stmt->rowCount() > 0) {
            $stmt = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            return $stmt[0]['id'];
        }
        else {
            return null;
        }
    }

    function countOrphanedFields($array) {

        //This is an optional helper function that counts the number of orphaned fields for a study
        //Not meant to be used regularly, just for troubleshooting

        $study = $this->quickQuery('ciStudyProjectCode = '.$this->conn->quote(substr($array[0]['studyName'], 0, 7)), 'study');

        //echo json_encode($study);

        if($study != null) {
            //SECTION GROUP
            $sectionGroups = array();
            $tempSGNames = array();

            //SECTION TEMPLATE AND SECTION GROUP LINK
            $sectionTemplates = array();
            $tempSTNames = array();
            $sgGroupLink = array();

            //FIELD AND FIELD VERSION
            $fields = array();
            $tempFieldsList = array();
            $fieldVersions = array();

            for($i = 0; $i < sizeof($array); $i++) {

                //Clean each input of ending spaces using rtrim
                foreach($array[$i] as &$item) {
                    $item = rtrim($item);
                }

                //SECTION GROUP CREATION FOR EACH ROW IN CSV
                if(!array_key_exists($array[$i]['sectionGroupName'], $tempSGNames)) {
                    $newSG = array(
                        "id" => get_UUID($this->conn),
                        "name" => $array[$i]['sectionGroupName'],
                        "studyid" => $study[0]['id'],
                        "sectionGroupPrefix" => "_legacy_"
                    );

                    array_push($sectionGroups, $newSG);
                    $tempSGNames = array_merge($tempSGNames, array($newSG['name'] => $newSG['id']));
                }

                //SECTION TEMPLATE AND SECTION GROUP LINK CREATION FOR EACH ROW IN CSV
                if(!array_key_exists($array[$i]['sectionTemplateName'], $tempSTNames)) {
                    $newST = array(
                        "id" => get_UUID($this->conn),
                        "name" => $array[$i]['sectionTemplateName']
                    );

                    $newSGgroupLink = array(
                        "id" => get_UUID($this->conn),
                        "sectionTemplateid" => $newST['id'], 
                        "sectionGroupid" => $tempSGNames[$array[$i]['sectionGroupName']]
                    );
                    
                    array_push($sectionTemplates, $newST);
                    $tempSTNames = array_merge($tempSTNames, array($newST['name'] => $newST['id']));
                    array_push($sgGroupLink, $newSGgroupLink);
                }

                //FIELD
                if(!array_key_exists($array[$i]['fileMakerFieldidLink'], $tempFieldsList)) {
                    $newField = array(
                        "id" => get_UUID($this->conn),
                        "fileMakerFieldidLink" => $array[$i]['fileMakerFieldidLink'],
                        "sectionTemplateid" => $tempSTNames[$array[$i]['sectionTemplateName']],
                        "ciFieldOrderNum" => $array[$i]['Order']
                    );

                    array_push($fields, $newField);
                    $tempFieldsList = array_merge($tempFieldsList, array($newField['fileMakerFieldidLink'] => $newField['id']));
                }
                
                //FIELD VERSION
                //TOOL TIP FLAG
                $toolTipFlag = "0";
                if($array[$i]['Range'] != "") {
                    $toolTipFlag = "1";
                }

                //IS REQUIRED FLAG
                $requiredFlag = "0";
                if(strpos($array[$i]['css_formatted_error_message'], "Required:") > 0) {
                    $requiredFlag = "1";
                }

                $newFieldVersion = array(
                    "id" => get_UUID($this->conn),
                    "ciFieldName" => $array[$i]['CI_Field_Name'],
                    "versionNum" => $array[$i]['Version'],
                    "ciFieldType" => $array[$i]['Field_Type'], 
                    "ciFriendlyName" => $array[$i]['css_formatted_friendly_name'],
                    "ciLabelText" => $array[$i]['css_formatted_label'], 
                    "ciValues" => $array[$i]['css_formatted_values'], 
                    "ciRange" => $array[$i]['Range'], 
                    "ciLogic" => $array[$i]['css_formatted_logic'], 
                    "isRequiredFlag" => $requiredFlag, 
                    "ciValidation" => $array[$i]['Validation'], 
                    "hasToolTipFlag" => $toolTipFlag, 
                    "ciValidationMessage" => $array[$i]['Error_Message'], 
                    "cidesignInstructions" => $array[$i]['Design_Instructions'], 
                    "cidMInstructions" => $array[$i]['DM_Instructions'],
                    "isRemovedFlag" => $array[$i]['Removed_Flag'],
                    "version" => $array[$i]['Version'],
                    "fieldid" => $tempFieldsList[$array[$i]['fileMakerFieldidLink']],
                    "latestFlag" => $array[$i]['Latest_Flag'],
                    "fmlinkid" => $array[$i]['fileMakerFieldidLink']
                );

                if(!array_key_exists($tempFieldsList[$array[$i]['fileMakerFieldidLink']], $fieldVersions)) {
                    
                    $fieldVersions = array_merge($fieldVersions, array($newFieldVersion['fieldid'] => array($newFieldVersion['version'] => $newFieldVersion)));
                }
                else {

                    array_push($fieldVersions[$newFieldVersion['fieldid']], $newFieldVersion);
                }
            }

            $count = 0;
            $countEmpty = 0;
            $list = array();
            foreach($fieldVersions as $field) {
                if(sizeof($field) == 1) {

                    foreach($field as $fieldVersion) {
                        if($fieldVersion['latestFlag'] == "") {
                            $count++;

                            array_push($list, $fieldVersion['fmlinkid']);
                        }
                    }

                }

                if(sizeof($field) == 0) {
                    $countEmpty++;
                }
            }

            //TIME TO ACTUALLY IMPORT!
            //$this->importFieldsQuery($sectionGroups, $sectionTemplates, $sgGroupLink, $fields, $fieldVersions);

            //print_r($count);
            echo json_encode(array("maroonedFields" => $count, "emptyFields" => $countEmpty, "filemakerIds" => $list));
        }
        else {
            //set response code - 400 bad request
            http_response_code(400);
            
            //tell the user
            echo json_encode(array("message" => "Requested study does not exist. Create study in database prior to import."));
        }

    }

    function visitMatrixErrorCheck(&$array) {
        //Check for errors in report, these orders missing cause issues with linking everything else downstream
        //These must be reconciled in surestart and then re-exported to continue
        $badOutput = array ();
        foreach($array as $item) {
            if($item['Section_Order'] === "" && $item['Arm_Name'] !== "") {
                $tempArrayItem = array (
                    "ERROR WITH VISIT MATRIX INPUT" => "SECTION ORDER BLANK",
                    "DATA" => $item
                );

                array_push($badOutput, $tempArrayItem);
            }
            else if($item['Form_Order'] === "" && $item['Arm_Name'] !== "") {
                $tempArrayItem = array (
                    "ERROR WITH VISIT MATRIX INPUT" => "FORM ORDER BLANK",
                    "DATA" => $item
                );

                array_push($badOutput, $tempArrayItem);
            }
            else if($item['Visit_Order'] === "" && $item['Arm_Name'] !== "") {
                $tempArrayItem = array (
                    "ERROR WITH VISIT MATRIX INPUT" => "VISIT ORDER BLANK",
                    "DATA" => $item
                );

                array_push($badOutput, $tempArrayItem);
            }
        }
        //Exit import if any errors found
        if(!empty($badOutput)) {
            echo(json_encode($badOutput));
            exit;
        }
    }

    private function importAdminid(){
        //Query database and return the import admin id from the system
        //This is used for writing to the base tables AND the history tables

        $data = $this->quickQuery('name = "Import Admin"', "appusers");

        if($data != null){
            return $data[0]['id'];
        }
        else {
            return null;
        }
    }
}