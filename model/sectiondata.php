<?php

if(!defined('MyConst')) {
    die('Direct access not permitted');
}
    
include_once '../../model/general.php';

class sectiondata{

    //database connection and table name
    private $conn;
    private $history;

    //constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    public function getConn(){
        return $this->conn;
    }

    //read quotes
    function read($studyid, $includeHistory = "false"){
        //Reminder, we are returning the formatted array

        $this->history = $includeHistory; //Need this for when we get to field versions

        //Pull all section groups
            //For each section group pull all section templates
                //For all section templates pull all fields
                    //For each field pull all field versions, if history is true include, otherwise only current versions

        $output = array($this->sectionGroups(($studyid)));

        return $output;
    }

    function sectionGroups($studyid){

        $stmt = $this->runQuery('SELECT *
                                 FROM sectiongroup
                                 WHERE studyid = "'.$studyid.'"
                                 ORDER BY name asc');

        $num = $stmt->rowCount();

        if($num > 0){

            $output_arr = array();

            //retrieve section group contents
            while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                
                //extract row
                extract($row);
                $item = array(
                    "id" => $id,
                    "name" => $name,
                    "sectionGroupPrefix" => $sectionGroupPrefix,
                    "isRemovedFlag" => $isRemovedFlag,
                    "sectionTemplates" => $this->sectionTemplates($id)
                );

                array_push($output_arr, $item);

            }

            if(sizeof($output_arr) > 0){
                return $output_arr;
            }
            else{
                return null;
            }
        }
        else {
            return null;
        }
    }

    function sectionTemplates($sectionGroupid){

        $query = 'SELECT sectiongrouplink.id AS SGLid,  
                            sectiontemplate.id AS STid,
                            sectiontemplate.name AS STname,
                            sectiontemplate.hasCrossFormFlag AS STcf,
                            sectiontemplate.hasTriggerFlag AS STtf,
                            sectiontemplate.createdDate AS STcreatedDate,
                            sectiontemplate.lastModifiedDate AS STlastModifiedDate,
                            sectiontemplate.isRemovedFlag AS STremoved,
                            sectiontemplate.uniqueFieldPrefix AS STprefix,
                            sectiontemplate.uniqueSectionHeader AS STheader
                    FROM sectiongrouplink
                    JOIN sectiontemplate ON sectiongrouplink.sectionTemplateid = sectiontemplate.id
                    WHERE sectiongrouplink.sectiongroupid = "'.$sectionGroupid.'"';

        //prepare query statement
        $stmt = $this->conn->prepare($query);

        //execute query
        $stmt->execute();
        $num = $stmt->rowCount();
        
        if($num > 0) {
            $output_arr = array();

            //retrive study table conents
            while($row2 = $stmt->fetch(PDO::FETCH_ASSOC)){
                //extract row
                extract($row2);
                $item = array(
                    "id" => $STid,
                    "name" => $STname,
                    "uniqueFieldPrefix" => $STprefix,
                    "uniqueSectionHeader" => $STheader,
                    "hasCrossFormFlag" => $STcf,
                    "hasTriggerFlag" => $STtf,
                    "createdDate" => $STcreatedDate,
                    "lastModifiedDate" => $STlastModifiedDate,
                    "isRemovedFlag" => $STremoved,
                    "sectiongrouplinkid" => $SGLid,
                    "fields" => $this->fields($STid)
                );

                array_push($output_arr, $item);
            }

            if(sizeof($output_arr) > 0){
                return $output_arr;
            }
            else{
                return null;
            }
        }
        else{
            return null;
        }
    }

    function fields($sectionTempid){

        $query = 'SELECT * 
                    FROM field 
                    WHERE sectionTemplateid = "'.$sectionTempid.'"
                    ORDER BY ciFieldOrderNum asc';
        //prepare query statement
        $stmt = $this->conn->prepare($query);

        //execute query
        $stmt->execute();
        $num = $stmt->rowCount();

        //check if more than 0 record found
        if($num>0){
            //field array
            $output_arr = array();

            //retrive field table conents
            while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                
                //extract row
                extract($row);
                $item = array(
                    "id" => $id,
                    "sectionTemplateid" => $sectionTemplateid,
                    "ciFieldOrderNum" => $ciFieldOrderNum,
                    "createdDate" => $createdDate,
                    "lastModifiedDate" => $lastModifiedDate,
                    "isIntegration" => $isIntegration,
                    "isRemovedFlag" => $isRemovedFlag,
                    "libraryStatus" => $libraryStatus,
                    "fieldVersions" => $this->fieldVerAssoc($id)
                );

                array_push($output_arr, $item);
            }

            if(sizeof($output_arr) > 0){
                return $output_arr;
            }
            else{
                return null;
            }
        }
        else {
            return null;
        }
    }

    function fieldVerAssoc($id)
    {
        if($this->history == "true") {
            $query =  'SELECT *
                        FROM fieldversion
                        WHERE fieldid = "'.$id.'"
                        ORDER by versionNum asc';
        }
        else {
            $query =  'SELECT *
                        FROM fieldversion
                        WHERE fieldid = "'.$id.'" AND latestFlag = "1"
                        ORDER by versionNum asc';
        }

        //prepare query statement
        $stmt = $this->conn->prepare($query);

        //execute query
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        
    }

    function runQuery($query)
    {
        //prepare query statement
        $stmt = $this->conn->prepare($query);

        //execute query
        $stmt->execute();

        return $stmt;
    }
}

?>