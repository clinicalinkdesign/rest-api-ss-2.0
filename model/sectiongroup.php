<?php

if(!defined('MyConst')) {
    die('Direct access not permitted');
}

include_once '../../functions/globalCUD.php';

class sectiongroup{

    //database connection and table name
    private $conn;
    private $tableName = "sectiongroup";

    //object properties
    public $id;
    public $name;
    public $studyid;
    public $sectionGroupPrefix;
    public $createdDate;
    public $lastModifiedDate;
    public $isRemovedFlag;
    
    private $globalCUD;

    //constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
        $this->globalCUD = new globalCUD();
    }

    public function getConn(){
        return $this->conn;
    }

    //read quotes
    function read($where){

        $query = '';
        
        //only id provided
        if($where != "")
        {
            $query = 'SELECT id, name, studyid, sectionGroupPrefix, createdDate, lastModifiedDate, isRemovedFlag 
                        FROM sectiongroup 
                        WHERE '.$where.'
                        ORDER BY studyid asc, name asc';
        }
        //select all query 
        else{
            $query = 'SELECT id, name, studyid, sectionGroupPrefix, createdDate, lastModifiedDate, isRemovedFlag 
                        FROM sectiongroup 
                        ORDER BY studyid asc, name asc';                                 
        }

        //prepare query statement
        $stmt = $this->conn->prepare($query);

        //execute query
        $stmt->execute();
        $num = $stmt->rowCount();

        //check if more than 0 record found
        if($num>0){
            //if $id is specified we also need to return a list of studies associated with it
            //other wise, just return the entire list of sectiongroups
            if(!empty($_GET)){
                //sectiongroup array
                $output_arr = array();

                //retrive sectiongroup table conents
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    
                    //extract row
                    extract($row);
                    $item = array(
                        "id" => $id,
                        "name" => $name,
                        "studyid" => $studyid,
                        "sectionGroupPrefix" => $sectionGroupPrefix,
                        "createdDate" => $createdDate,
                        "lastModifiedDate" => $lastModifiedDate,
                        "isRemovedFlag" => $isRemovedFlag,
                        "sectionTemplates" => $this->sectionTemplateAssoc($id)
                    );

                    array_push($output_arr, $item);
                }
            }
            else{
                
                return $stmt->fetchAll(\PDO::FETCH_ASSOC);

            }

            return $output_arr;
        }
        else {
            return null;
        }
    }

    function create($array){

        //Check that your study actually exist!
        if(array_key_exists('studyid', $array) && !id_Exists($array['studyid'], "study", $this->getConn())){ 
                
            $currRet['success'] = "false";
            $currRet['message'] = "studyid Not Found";

            return $currRet;

        }
        else{
            return $this->globalCUD->CreateGlobal($array, $this->conn, $this->tableName, $this->id);
        }
        
    }

    function update($array){

        //Check that your study actually exist!
        if(array_key_exists('studyid', $array) && !id_Exists($array['studyid'], "study", $this->getConn())){ 
                
            $currRet['success'] = "false";
            $currRet['message'] = "studyid Not Found";

            return $currRet;

        }
        else{

            $this->id = $array['id'];
            
            return $this->globalCUD->UpdateGlobal($array, $this->conn, $this->tableName);
        }
    }

    function delete($array){

        $this->id = $array['id'];

        return $this->globalCUD->DeleteGlobal($array, $this->conn, $this->tableName);

    }

    function sectionTemplateAssoc($id)
    {
        $query = '';

        $query = 'SELECT sectiongrouplink.id AS SGLid,  
                            sectiontemplate.id AS STid,
                            sectiontemplate.name AS STname,
                            sectiontemplate.hasCrossFormFlag AS STcf,
                            sectiontemplate.hasTriggerFlag AS STtf,
                            sectiontemplate.createdDate AS STcreatedDate,
                            sectiontemplate.lastModifiedDate AS STlastModifiedDate,
                            sectiontemplate.isRemovedFlag AS STremoved,
                            sectiontemplate.uniqueFieldPrefix AS STprefix,
                            sectiontemplate.uniqueSectionHeader AS STheader
                    FROM sectiongrouplink
                    JOIN sectiontemplate ON sectiongrouplink.sectionTemplateid = sectiontemplate.id
                    WHERE sectiongrouplink.sectiongroupid = "'.$id.'"';

        //prepare query statement
        $stmt = $this->conn->prepare($query);

        //execute query
        $stmt->execute();
        $num = $stmt->rowCount();
        
        $output_arr = array();

        //retrive study table conents
        while($row2 = $stmt->fetch(PDO::FETCH_ASSOC)){
            //extract row
            extract($row2);
            $item = array(
                "id" => $STid,
                "name" => $STname,
                "uniqueFieldPrefix" => $STprefix,
                "uniqueSectionHeader" => $STheader,
                "hasCrossFormFlag" => $STcf,
                "hasTriggerFlag" => $STtf,
                "createdDate" => $STcreatedDate,
                "lastModifiedDate" => $STlastModifiedDate,
                "isRemovedFlag" => $STremoved,
                "sectiongrouplinkid" => $SGLid
            );

            array_push($output_arr, $item);
        }

        if(sizeof($output_arr) > 0){
            return $output_arr;
        }
        else{
            return null;
        }
    }
}

?>