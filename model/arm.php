<?php

if(!defined('MyConst')) {
    die('Direct access not permitted');
}

include_once '../../functions/globalCUD.php';

class arm{

    //database connection and table name
    private $conn;
    private $tableName = "arm";

    //object properties
    public $id;
    public $name;
    public $studyid;
    public $createdDate;
    public $lastModifiedDate;
    public $isRemovedFlag;

    private $globalCUD;

    //constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
        $this->globalCUD = new globalCUD();
    }

    public function getConn(){
        return $this->conn;
    }

    //read quotes
    function read($where){
        
        $query = '';
        
        //only id provided
        if($where != "")
        {
            $query = 'SELECT id, name, studyid, armOrder, createdDate, lastModifiedDate, isRemovedFlag 
                        FROM arm 
                        WHERE '.$where.'
                        ORDER BY name asc';
        }
        //select all query 
        else{
            $query = 'SELECT id, name, studyid, armOrder, createdDate, lastModifiedDate, isRemovedFlag  
                        FROM arm 
                        ORDER BY name asc';                                  
        }

        //prepare query statement
        $stmt = $this->conn->prepare($query);

        //execute query
        $stmt->execute();
        $num = $stmt->rowCount();

        //check if more than 0 record found
        if($num>0){
            //if $id is specified we also need to return a list of studies associated with it
            //other wise, just return the entire list of arms
            if(!empty($_GET)){
                //arm array
                $output_arr = array();

                //retrive arm table conents
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    
                    //extract row
                    extract($row);
                    $item = array(
                        "id" => $id,
                        "name" => $name,
                        "studyid" => $studyid,
                        "createdDate" => $createdDate,
                        "lastModifiedDate" => $lastModifiedDate,
                        "isRemovedFlag" => $isRemovedFlag,
                        "visits" => $this->visitAssoc($id)
                    );

                    array_push($output_arr, $item);
                }

                return $output_arr;
            }
            else{

                return $stmt->fetchAll(\PDO::FETCH_ASSOC);
            }
        }
        else {
            return null;
        }
    }

    function create($array){

        //Check that your study actually exist!
        if(array_key_exists('studyid', $array) && !id_Exists($array['studyid'], "study", $this->getConn())){ 
                
            $currRet['success'] = "false";
            $currRet['message'] = "studyid Not Found";

            return $currRet;

        }
        else{
            return $this->globalCUD->CreateGlobal($array, $this->conn, $this->tableName, $this->id);
        }
    }

    function update($array){

        //Check that your study actually exist!
        if(array_key_exists('studyid', $array) && !id_Exists($array['studyid'], "study", $this->getConn())){ 
                
            $currRet['success'] = "false";
            $currRet['message'] = "studyid Not Found";

            return $currRet;

        }
        else{

            $this->id = $array['id'];
            
            return $this->globalCUD->UpdateGlobal($array, $this->conn, $this->tableName);
        }
    }

    function delete($array){

        $this->id = $array['id'];
        
        return $this->globalCUD->DeleteGlobal($array, $this->conn, $this->tableName);

    }

    //Used for finding if name id is used in the visit table
    function id_used()
    {
        $query = 'SELECT armid FROM visit WHERE armid = "'.$this->id.'"';

        //prepare query statement
        $stmt = $this->conn->prepare($query);

        //execute query
        $stmt->execute();

        if($stmt->rowCount() > 0){
            return true;
        }
        else{
            return false;
        }
    }

    function visitAssoc($id)
    {
        $query = '';
       
        $query = 'SELECT id, name, visitOrder, isRemovedFlag 
                    FROM visit 
                    WHERE armid = "'.$id.'"
                    ORDER BY visitOrder asc';

        //prepare query statement
        $stmt = $this->conn->prepare($query);

        //execute query
        $stmt->execute();

        if($stmt->rowCount() > 0){
            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        }
        else{
            return null;
        }
    }
}

?>