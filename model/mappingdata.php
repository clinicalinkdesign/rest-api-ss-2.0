<?php

if(!defined('MyConst')) {
    die('Direct access not permitted');
}
    
include_once '../../model/general.php';
include_once 'visitmatrix.php';
include_once 'sectiondata.php';

class mappingdata{

    //database connection and table name
    private $conn;
    private $visitmatrix;
    private $sectiondata;

    //constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
        $this->visitmatrix = new visitmatrix($db);
        $this->sectiondata = new sectiondata($db);
    }

    public function getConn(){
        return $this->conn;
    }

    //read quotes
    function read($intstudyid){
        //Study id > section groups > section template > fields > field history

        //1. Look up study GUID from internal study id
        $studyid = $this->lookUpStudyGUID($intstudyid);

        //2. Build the report
        /*
            Structure
            Study Information
            Section groups
                Section templates
                    Fields
                        Field versions (history not included)
            Visit matrix
                Arm
                    Visits
                        Forms
                            Form template
                                Section templates
        */

        $output = array (
            "id" => $studyid[0]['id'],
            "ciStudyProjectCode" => $intstudyid, 
            "section groups" => $this->sectiondata->read($studyid[0]['id']),
            "visit matrix" => $this->visitmatrix->read($studyid[0]['id'])
        );

        return $output;
    }

    function lookUpStudyGUID($intstudyid){
        $query = 'SELECT id FROM study WHERE ciStudyProjectCode = '.$this->conn->quote($intstudyid);

        $stmt = $this->conn->prepare($query);

        //execute query
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }
}

?>