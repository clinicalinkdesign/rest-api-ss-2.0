<?php

if(!defined('MyConst')) {
    die('Direct access not permitted');
}

include_once '../../functions/globalCUD.php';

class sectiontempformlink{

    //database connection and table name
    private $conn;
    private $tableName = "sectiontempformlink";

    //object properties
    public $id;
    public $orderInForm;
    public $sectionTemplateid;
    public $formTemplateid;
    public $createdDate;
    public $lastModifiedDate;
    public $isRemovedFlag;
    
    private $globalCUD;

    //constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
        $this->globalCUD = new globalCUD();
    }

    public function getConn(){
        return $this->conn;
    }

    //read quotes
    function read($where){

        $query = '';
        
        //only id provided
        if($where != "")
        {
            $query = 'SELECT id, orderInForm, sectionTemplateid, formTemplateid, createdDate, lastModifiedDate, isRemovedFlag 
                        FROM sectiontempformlink 
                        WHERE '.$where.'
                        ORDER BY formTemplateid asc, sectionTemplateid asc';
        }
        //select all query 
        else{
            $query = 'SELECT id, orderInForm, sectionTemplateid, formTemplateid, createdDate, lastModifiedDate, isRemovedFlag 
                        FROM sectiontempformlink 
                        ORDER BY formTemplateid asc, sectionTemplateid asc';                                  
        }

        //prepare query statement
        $stmt = $this->conn->prepare($query);

        //execute query
        $stmt->execute();
        $num = $stmt->rowCount();

        //check if more than 0 record found
        if($num>0){
            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        }
        else {
            return null;
        }
    }

    function create($array){

        //Check that your study actually exist!
        if(array_key_exists('sectionTemplateid', $array) && !id_Exists($array['sectionTemplateid'], "sectiontemplate", $this->getConn())){ 
                
            $currRet['success'] = "false";
            $currRet['message'] = "sectionTemplateid Not Found";

            return $currRet;

        }
        else if(array_key_exists('formTemplateid', $array) && !id_Exists($array['formTemplateid'], "formtemplate", $this->getConn())){ 
                
            $currRet['success'] = "false";
            $currRet['message'] = "formTemplateid Not Found";

            return $currRet;

        }
        else{
            return $this->globalCUD->CreateGlobal($array, $this->conn, $this->tableName, $this->id);
        }
        
    }

    function update($array){

        //Check that your study actually exist!
        if(array_key_exists('sectionTemplateid', $array) && !id_Exists($array['sectionTemplateid'], "sectiontemplate", $this->getConn())){ 
                
            $currRet['success'] = "false";
            $currRet['message'] = "sectionTemplateid Not Found";

            return $currRet;

        }
        else if(array_key_exists('formTemplateid', $array) && !id_Exists($array['formTemplateid'], "formtemplate", $this->getConn())){ 
                
            $currRet['success'] = "false";
            $currRet['message'] = "formTemplateid Not Found";

            return $currRet;

        }
        else{

            $this->id = $array['id'];
            
            return $this->globalCUD->UpdateGlobal($array, $this->conn, $this->tableName);
        }
    }

    function delete($array){

        $this->id = $array['id'];

        return $this->globalCUD->DeleteGlobal($array, $this->conn, $this->tableName);

    }
}

?>