<?php

if(!defined('MyConst')) {
    die('Direct access not permitted');
}

include_once '../../functions/globalCUD.php';

class field{

    //database connection and table name
    private $conn;
    private $tableName = "field";

    //object properties
    public $id;
    public $sectionTemplateid;
    public $ciFieldOrderNum;
    public $createdDate;
    public $lastModifiedDate;
    public $isRemovedFlag;
    public $libraryStatus;
    
    private $globalCUD;

    //constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
        $this->globalCUD = new globalCUD();
    }

    public function getConn(){
        return $this->conn;
    }

    //read quotes
    function read($where){

        $query = '';
        
        //only id provided
        if($where != "")
        {
            $query = 'SELECT * 
                        FROM field 
                        WHERE '.$where.'
                        ORDER BY sectionTemplateid asc, ciFieldOrderNum asc';
        }
        //select all query 
        else{
            $query = 'SELECT *
                        FROM field 
                        ORDER BY sectionTemplateid asc, ciFieldOrderNum asc';                                  
        }

        //prepare query statement
        $stmt = $this->conn->prepare($query);

        //execute query
        $stmt->execute();
        $num = $stmt->rowCount();

        //check if more than 0 record found
        if($num>0){
            //if $id is specified we also need to return a list of studies associated with it
            //other wise, just return the entire list of fields
            if(!empty($_GET)){
                //field array
                $output_arr = array();

                //retrive field table conents
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    
                    //extract row
                    extract($row);
                    $item = array(
                        "id" => $id,
                        "sectionTemplateid" => $sectionTemplateid,
                        "ciFieldOrderNum" => $ciFieldOrderNum,
                        "createdDate" => $createdDate,
                        "lastModifiedDate" => $lastModifiedDate,
                        "isIntegration" => $isIntegration,
                        "isRemovedFlag" => $isRemovedFlag,
                        "libraryStatus" => $libraryStatus,
                        "fieldVersions" => $this->fieldVerAssoc($id)
                    );

                    array_push($output_arr, $item);
                }
            }
            else{
                
                return $stmt->fetchAll(\PDO::FETCH_ASSOC);

            }

            return $output_arr;
        }
        else {
            return null;
        }
    }

    function create($array){

        //Check that your study actually exist!
        if(array_key_exists('sectionTemplateid', $array) && !id_Exists($array['sectionTemplateid'], "sectiontemplate", $this->getConn())){ 
                
            $currRet['success'] = "false";
            $currRet['message'] = "sectionTemplateid Not Found";

            return $currRet;

        }
        else{      
            return $this->globalCUD->CreateGlobal($array, $this->conn, $this->tableName, $this->id);
        }
        
    }

    function update($array){

        $this->id = $array['id'];

        //Check that your study actually exist!
        if(array_key_exists('sectionTemplateid', $array) && !id_Exists($array['sectionTemplateid'], "sectiontemplate", $this->getConn())){ 
                
            $currRet['success'] = "false";
            $currRet['message'] = "sectionTemplateid Not Found";

            return $currRet;

        }
        else{   
            return $this->globalCUD->UpdateGlobal($array, $this->conn, $this->tableName);
        }
    }

    function delete($array){

        $this->id = $array['id'];

        return $this->globalCUD->DeleteGlobal($array, $this->conn, $this->tableName);

    }

    function fieldVerAssoc($id)
    {

        $query =  'SELECT *
                    FROM fieldversion
                    WHERE fieldid = "'.$id.'"
                    ORDER by versionNum asc';

        //prepare query statement
        $stmt = $this->conn->prepare($query);

        //execute query
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        
    }
}

?>