<?php

if(!defined('MyConst')) {
    die('Direct access not permitted');
}

include_once '../../functions/globalCUD.php';

class approles{

    //database connection and table name
    private $conn;
    private $tableName = "approles";

    //object properties
    public $id;
    public $userRoleNum;
    public $roleName;
    public $editUsers_01;
    public $exportRecords_02;
    public $editRequirements_03;
    public $studyMilestones_04;
    public $studyMilestonesQC_05;
    public $editReqQCMilestone_06;
    public $viewUserNamesHistory_07;
    public $bypassStatusWorkflow_08;
    public $editQueue_09;
    public $createdDate;
    public $lastModifiedDate;

    private $globalCUD;

    //constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
        $this->globalCUD = new globalCUD();
    }

    public function getConn(){
        return $this->conn;
    }

    function read($where){
        
        $query = '';
        
        //only id provided
        if($where != "")
        {
            $query = 'SELECT * 
                        FROM approles 
                        WHERE '.$where.'
                        ORDER BY userRole asc';
        }
        //select all query 
        else{
            $query = 'SELECT *  
                        FROM approles 
                        ORDER BY userRole asc';                                  
        }

        //prepare query statement
        $stmt = $this->conn->prepare($query);

        //execute query
        $stmt->execute();
        $num = $stmt->rowCount();

        //check if more than 0 record found
        if($num>0){
            //if $id is specified we also need to return a list of studies associated with it
            //other wise, just return the entire list of arms
            if(!empty($_GET)){
                //arm array
                $output_arr = array();

                //retrive arm table conents
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    
                    //extract row
                    extract($row);
                    $item = array(
                        "id" => $id,
                        "userRole" => $userRole,
                        "roleName" => $roleName,
                        "editUsers_01" => $editUsers_01,
                        "exportRecords_02" => $exportRecords_02,
                        "editRequirements_03" => $editRequirements_03,
                        "studyMilestones_04" => $studyMilestones_04,
                        "studyMilestonesQC_05" => $studyMilestonesQC_05,
                        "editReqQCMilestone_06" => $editReqQCMilestone_06,
                        "viewUserNamesHistory_07" => $viewUserNamesHistory_07,
                        "bypassStatusWorkflow_08" => $bypassStatusWorkflow_08,
                        "editQueue_09" => $editQueue_09,
                        "createdDate" => $createdDate,
                        "lastModifiedDate" => $lastModifiedDate
                    );

                    array_push($output_arr, $item);
                }

                return $output_arr;
            }
            else{

                return $stmt->fetchAll(\PDO::FETCH_ASSOC);
            }
        }
        else {
            return null;
        }
    }
}

