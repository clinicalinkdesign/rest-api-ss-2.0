<?php

if(!defined('MyConst')) {
    die('Direct access not permitted');
}

include_once '../../functions/globalCUD.php';

class General{

    //database connection and table name
    private $conn;

    //constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    public function getConn(){
        return $this->conn;
    }

    //read quotes
    function read($where, $table, $test = null){

        $query = '';
        
        //only id provided
        if($where != "")
        {
            $query = 'SELECT * 
                        FROM '.$table.' 
                        WHERE '.$where.'
                        ORDER BY lastModifiedDate asc';
        }
        //select all query 
        else{
            $query = 'SELECT * 
                        FROM '.$table.'
                        ORDER BY lastModifiedDate asc';                              
        }

        //prepare query statement
        $stmt = $this->conn->prepare($query);

        //execute query
        $stmt->execute();

        if($test != null) {
            return array($stmt->fetch(\PDO::FETCH_ASSOC));
        }
        else {
            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        }
    }
}

?>