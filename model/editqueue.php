<?php

if(!defined('MyConst')) {
    die('Direct access not permitted');
}

include_once '../../functions/globalCUD.php';

class editqueue{

    //database connection and table name
    private $conn;
    private $tableName = "editqueue";

    //object properties
    public $id;

    private $globalCUD;

    //constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
        $this->globalCUD = new globalCUD();
    }

    public function getConn(){
        return $this->conn;
    }

    //read quotes
    function read($where){
        
        $query = '';
        
        //only id provided
        if($where != "")
        {
            $query = 'SELECT * 
                        FROM editqueue 
                        WHERE '.$where.'
                        ORDER BY id asc, createdDate asc';
        }
        //select all query 
        else{
            $query = 'SELECT * 
                        FROM editqueue 
                        ORDER BY id asc, createdDate asc';                              
        }

        //prepare query statement
        $stmt = $this->conn->prepare($query);

        //execute query
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        
    }

    function lock($array){

        //Check if the field provided is already being editted, if yes tell the app no, otherwise, add to queue

        if($this->checkQueue($array)) {
            return $this->globalCUD->CreateGlobal($array, $this->conn, $this->tableName, $this->id);
        }
        else {
            
            $currRet = array(
                "success" => "false",
                "message" => "Table / id combination already being edited.",
                "data" => $array
            );

            return $currRet;
        }
    }

    function unlock($array){

        $this->id = $array['id'];
        
        return $this->globalCUD->DeleteGlobal($array, $this->conn, $this->tableName);
        
    }

    function checkQueue($array){
        //returns boolean value

        $query = 'SELECT id
                    FROM editqueue
                    WHERE tableName = '.$this->conn->quote($array['tableName']).' AND rowid = '.$this->conn->quote($array['rowid']);

        //prepare query statement
        $stmt = $this->conn->prepare($query);

        //execute query
        $stmt->execute();

        if($stmt->rowCount() > 0) {
            return false;
        }
        else {
            return true;
        }
    }
}

?>