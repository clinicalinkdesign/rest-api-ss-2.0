<?php

if(!defined('MyConst')) {
    die('Direct access not permitted');
}

include_once '../../functions/globalCUD.php';

class study{

    //database connection and table name
    private $conn;
    private $tableName = "study";

    //object properties
    public $id;
    public $ciStudyProjectCode;
    public $protocolName;
    public $sponsorid;
    public $croid;
    public $libraryFlag;
    public $createdDate;
    public $lastModifiedDate;
    public $studyType;
    public $isRemovedFlag;

    private $globalCUD;

    //constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
        $this->globalCUD = new globalCUD();
    }

    public function getConn(){
        return $this->conn;
    }

    //read quotes
    function read($where){
        $query = '';
        
        //only id provided
        if($where != "")
        {
            $query = 'SELECT study.id, 
                                study.ciStudyProjectCode, 
                                study.protocolName, 
                                study.sponsorid, 
                                t_sponsor.name AS sponsorName,
                                t_sponsor.createdDate AS sponsorCD,
                                t_sponsor.lastModifiedDate AS sponsorLMD,
                                t_sponsor.isRemovedFlag AS sponsorIRM,
                                study.croid, 
                                t_cro.name AS croName,
                                t_cro.createdDate AS croCD,
                                t_cro.lastModifiedDate AS croLMD,
                                t_cro.isRemovedFlag AS croIRM,
                                study.libraryFlag, 
                                study.createdDate, 
                                study.lastModifiedDate, 
                                study.studyType, 
                                study.studyPhaseFlag,
                                study.isRemovedFlag,  
                                study.isLegacyFlag  
                        FROM study 
                            LEFT JOIN sponsor AS t_sponsor on study.sponsorid = t_sponsor.id
                            LEFT JOIN cro AS t_cro ON study.croid = t_cro.id
                        WHERE '.$where.'
                        ORDER BY ciStudyProjectCode asc';
        }
        //select all query 
        else{
            $query = 'SELECT study.id, 
                                study.ciStudyProjectCode, 
                                study.protocolName, 
                                study.sponsorid, 
                                t_sponsor.name AS sponsorName,
                                t_sponsor.createdDate AS sponsorCD,
                                t_sponsor.lastModifiedDate AS sponsorLMD,
                                t_sponsor.isRemovedFlag AS sponsorIRM,
                                study.croid, 
                                t_cro.name AS croName,
                                t_cro.createdDate AS croCD,
                                t_cro.lastModifiedDate AS croLMD,
                                t_cro.isRemovedFlag AS croIRM,
                                study.libraryFlag, 
                                study.createdDate, 
                                study.lastModifiedDate, 
                                study.studyType, 
                                study.studyPhaseFlag,
                                study.isRemovedFlag,  
                                study.isLegacyFlag 
                        FROM study 
                            LEFT JOIN sponsor AS t_sponsor on study.sponsorid = t_sponsor.id
                            LEFT JOIN cro AS t_cro ON study.croid = t_cro.id
                        ORDER BY ciStudyProjectCode asc';                                  
        }

        //prepare query statement
        $stmt = $this->conn->prepare($query);

        //execute query
        $stmt->execute();
        $num = $stmt->rowCount();

        //study array
        $output_arr = array();

        //check if more than 0 record found
        if($num>0){
            //retrive table conents
            while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                //extract row
                extract($row);
                $item = array(
                    "id" => $id,
                    "ciStudyProjectCode" => $ciStudyProjectCode,
                    "protocolName" => $protocolName,
                    "sponsor" => array(
                        "id" => $sponsorid,
                        "name" => $sponsorName,
                        "createdDate" => $sponsorCD,
                        "lastModifiedDate" => $sponsorLMD,
                        "isRemovedFlag" => $sponsorIRM
                    ),
                    "cro" => array(
                        "id" => $croid,
                        "name" => $croName,
                        "createdDate" => $croCD,
                        "lastModifiedDate" => $croLMD,
                        "isRemovedFlag" => $croIRM
                    ),
                    "libraryFlag" => $libraryFlag,
                    "createdDate" => $createdDate,
                    "lastModifiedDate" => $lastModifiedDate,
                    "studyType" => $studyType,
                    "studyPhaseFlag" => $studyPhaseFlag,
                    "isRemovedFlag" => $isRemovedFlag,
                    "isLegacyFlag" => $isLegacyFlag
                );

                array_push($output_arr, $item);
            }

            return $output_arr;
        }
        else {
            return null;
        }
    }

    function create($array){
        
        //Check that your sponsor and cro actually exist!
        if(array_key_exists('sponsorid', $array) && !id_Exists($array['sponsorid'], "sponsor", $this->getConn())){ 
                
            $currRet['success'] = "false";
            $currRet['message'] = "sponsorid Not Found";

            return $currRet;

        }
        else if(array_key_exists('croid', $array) && !id_Exists($array['croid'], "cro", $this->getConn())){ 
            
            $currRet['success'] = "false";
            $currRet['message'] = "croid Not Found";

            return $currRet;

        }
        else{
            return $this->globalCUD->CreateGlobal($array, $this->conn, $this->tableName, $this->id);
        }
    }

    function update($array){
        
        //Check that your sponsor and cro actually exist!
        if(array_key_exists('sponsorid', $array) && !id_Exists($array['sponsorid'], "sponsor", $this->getConn())){ 
                
            $currRet['success'] = "false";
            $currRet['message'] = "sponsorid Not Found";

            return $currRet;

        }
        else if(array_key_exists('croid', $array) && !id_Exists($array['croid'], "cro", $this->getConn())){ 
            
            $currRet['success'] = "false";
            $currRet['message'] = "croid Not Found";

            return $currRet;

        }
        else{

            $this->id = $array['id'];
            
            return $this->globalCUD->UpdateGlobal($array, $this->conn, $this->tableName);
        }
    }

    function delete($array){

        $this->id = $array['id'];
        
        return $this->globalCUD->DeleteGlobal($array, $this->conn, $this->tableName);

    }
}

?>