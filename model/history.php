<?php

if(!defined('MyConst')) {
    die('Direct access not permitted');
}

include_once '../../functions/globalCUD.php';
    
include_once '../../model/general.php';

class History{

    //database connection and table name
    private $conn;

    //constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    public function getConn(){
        return $this->conn;
    }

    //read quotes
    function read($where, $table, $studyid = null){

        $query = '';
        
        if($studyid == null)
        {
            //only id provided
            if($where != "")
            {
                $query = 'SELECT * 
                            FROM '.$table.'_history 
                            WHERE '.$where.'
                            ORDER BY '.$table.'id asc, lastModifiedDate asc';
            }
            //select all query 
            else{
                $query = 'SELECT * 
                            FROM '.$table.'_history 
                            ORDER BY '.$table.'id asc, lastModifiedDate asc';                              
            }
        }
        else
        {
            //Code for writing history queries to return all history data for a table based on its study
            //Note, not all history tables have study id, this will require custom queries per table

            $query = $this->customHistoryQuery($table, $studyid);
        }

        //prepare query statement
        $stmt = $this->conn->prepare($query);

        //execute query
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    function write($action, $itemid, $table, $globalAppUserid = null, $copiedFromid = null) // <- action is the CRUD operator update, delete, create
    {
        date_default_timezone_set('America/New_York'); // on deployment this needs to change to UTC-0

        $general = new General($this->conn);

        //We are taking the previous value modified in the table and just writing a copy to the version history table
        //PULL DOWN THE DATA YOU JUST PUT IN AND MAP IT TO THE NEW TABLE
        $list = $general->read("id = '$itemid'", $table);

        //$list['id'] and so on for accessing parts of the k,v array
        $historyid = get_UUid($this->conn);

        //logCodeItem(json_encode($list));

	    $modifiedDate = null;

        //Action is : update, create, delete
        if($list != null)
        {
            if($action == "DELETE"){
                $modifiedDate = date('Y-m-d H:i:s');
            }
            else if($action == "POST"){
                $modifiedDate = "0000-00-00 00:00:00";
            }
            else if($action == "IMPORT"){
                $modifiedDate = "0000-00-00 00:00:00";
            }
            else{
                $modifiedDate = $list[0]['lastModifiedDate'];
            }
    
            //Write query pieces
            $queryPieces = $this->createClause($list, $table, $modifiedDate);

            try {

                if($action !== "IMPORT") {
                    if (is_null($copiedFromid)) {
                        $query = 'INSERT INTO '.$table.'_history
                                (id, action, modifiedByAppUserid, '.$queryPieces[0].')
                            VALUES
                                ('.$this->conn->quote($historyid).', '.$this->conn->quote($action).', '.$this->conn->quote($globalAppUserid).', '.$queryPieces[1].')';
                    }
                    else {
                        $query = 'INSERT INTO '.$table.'_history
                                (id, action, modifiedByAppUserid, copiedFromid, '.$queryPieces[0].')
                            VALUES
                                ('.$this->conn->quote($historyid).', '.$this->conn->quote($action).', '.$this->conn->quote($globalAppUserid).', '.$this->conn->quote($copiedFromid).', '.$queryPieces[1].')';
                    }
                }
                else{
                    if (is_null($copiedFromid)) {
                        $query = 'INSERT INTO '.$table.'_history
                                (id, action, modifiedByAppUserid, '.$queryPieces[0].')
                            VALUES
                                ('.$this->conn->quote($historyid).', '.$this->conn->quote($action).', "26d0c045-9803-11ed-979a-244bfe7dd4fe", '.$queryPieces[1].')';
                    }
                    else {
                        $query = 'INSERT INTO '.$table.'_history
                                (id, action, modifiedByAppUserid, copiedFromid, '.$queryPieces[0].')
                            VALUES
                                ('.$this->conn->quote($historyid).', '.$this->conn->quote($action).', "26d0c045-9803-11ed-979a-244bfe7dd4fe", '.$this->conn->quote($copiedFromid).', '.$queryPieces[1].')';
                    }
                }

                //prepare query statement
                $stmt = $this->conn->prepare($query);

                //execute query
                $stmt->execute();

            } catch (Exception $e) {

                logQueryError($query, $e->getMessage());

            }
        }
        else {

            logQueryError("UNABLE TO LOG!!", "REQUEST FOR ".$action." EMPTY PARAMETERS PROVIDED, TABLE: ".$table.", ID: ".$itemid);
        }
    }

    function createClause($data, $table, $modifiedDate)
    {
        //Returns a formatted array for the two parts necessary for an insert query.
        //Used for the history table only

        // [0] will be: item 1, ..., item n
        // [1] will be similar but the actual value related to the above

        $args0 = array();
        $args1 = array();
        $output = array();

        foreach ($data[0] as $key => $value)
        {
            if($value != "") {
                if($key == "lastModifiedDate"){
                    array_push($args0, $key);
                    array_push($args1, $this->conn->quote($modifiedDate));
                }
                else if($key == "dateTimeCopied"){
                    array_push($args0, $key);
                    array_push($args1, '"0000-00-00 00:00:00"');
                }
                else if($key != "id"){ 
                    array_push($args0, $key);
                    array_push($args1, $this->conn->quote($value));
                }
                else{
                    array_push($args0, "$table"."id");
                    array_push($args1, $this->conn->quote($value));
                }
            }        
        }

        array_push($output, implode(', ', $args0));
        array_push($output, implode(', ', $args1));

        return $output;
    }

    function customHistoryQuery ($table, $studyid)
    {
        $tempTable = strtolower($table);
        //arm - crossformlinking - formtemplate - formtrigger - sectiongroup - visittrigger
        if($tempTable == "arm" || $tempTable == "crossformlinking" || 
           $tempTable == "formtemplate" || $tempTable == "formtrigger" ||
           $tempTable == "sectiongroup" || $tempTable == "visittrigger")
        {
            $query = 'SELECT * 
                        FROM '.$table.'_history 
                        WHERE studyid = "'.$studyid.'"
                        ORDER BY lastModifiedDate asc';
        }
        //field
        else if($tempTable == "field")
        {
            $query = 'SELECT field_history.*
                        FROM field_history
                            JOIN sectiontemplate AS t_sectiontemplate ON field_history.sectionTemplateid = t_sectiontemplate.id
                            JOIN sectiongrouplink AS t_SGL ON t_SGL.sectionTemplateid = t_sectiontemplate.id
                            JOIN sectiongroup AS t_SG ON t_SGL.sectionGroupid = t_SG.id
                        WHERE t_SG.studyid = "'.$studyid.'"
                        ORDER BY field_history.lastModifiedDate DESC';
        }
        //form
        else if($tempTable == "form")
        {
            $query = 'SELECT form_history.*
                        FROM form_history
                            JOIN visit as t_visit ON form_history.visitid = t_visit.id
                            JOIN arm as t_arm ON t_visit.armid = t_arm.id
                        WHERE t_arm.studyid = "'.$studyid.'"
                        ORDER BY form_history.lastModifiedDate DESC';
        }
        //qualityfinding
        else if($tempTable == "qualityfinding")
        {
            $query = 'SELECT qualityfinding_history.*
                        FROM qualityfinding_history
                            JOIN field AS t_field ON qualityfinding_history.fieldid = t_field.id
                            JOIN sectiontemplate AS t_sectiontemplate ON t_field.sectionTemplateid = t_sectiontemplate.id
                            JOIN sectiongrouplink AS t_SGL ON t_SGL.sectionTemplateid = t_sectiontemplate.id
                            JOIN sectiongroup AS t_SG ON t_SGL.sectionGroupid = t_SG.id
                        WHERE t_SG.studyid = "'.$studyid.'"
                        ORDER BY qualityfinding_history.lastModifiedDate DESC';
        }
        //sectiongrouplink
        else if($tempTable == "sectiongrouplink")
        {
            $query = 'SELECT sectiongrouplink_history.*
                        FROM sectiongrouplink_history
                            JOIN sectiongroup AS t_SG ON sectiongrouplink_history.sectionGroupid = t_SG.id
                        WHERE t_SG.studyid = "'.$studyid.'"
                        ORDER BY sectiongrouplink_history.lastModifiedDate DESC';
        }
        //sectiontempformlink
        else if($tempTable == "sectiontempformlink")
        {
            $query = 'SELECT sectiontempformlink_history.*
                        FROM sectiontempformlink_history
                            JOIN formtemplate AS t_FT ON sectiontempformlink_history.formTemplateid = t_FT.id
                        WHERE t_FT.studyid = "'.$studyid.'"
                        ORDER BY sectiontempformlink_history.lastModifiedDate DESC';
        }
        //sectiontemplate
        else if($tempTable == "sectiontemplate")
        {
            $query = 'SELECT sectiontemplate_history.*
                        FROM sectiontemplate_history
                            JOIN sectiongrouplink AS t_SGL ON t_SGL.sectionTemplateid = sectiontemplate_history.sectionTemplateid
                            JOIN sectiongroup AS t_SG ON t_SGL.sectionGroupid = t_SG.id
                        WHERE t_SG.studyid = "'.$studyid.'"
                        ORDER BY sectiontemplate_history.lastModifiedDate DESC';
        }
        else if($tempTable == "visit")
        {
            $query = 'SELECT visit_history.*
                        FROM visit_history
                            JOIN arm as t_arm ON visit_history.armid = t_arm.id
                        WHERE t_arm.studyid = "'.$studyid.'"
                        ORDER BY visit_history.lastModifiedDate DESC';
        }
        else if($tempTable == "fieldversion")
        {
            $query = 'SELECT fieldversion.*
                        FROM fieldversion
                            JOIN field AS t_field ON fieldversion.fieldid = t_field.id
                            JOIN sectiontemplate AS t_sectiontemplate ON t_field.sectionTemplateid = t_sectiontemplate.id
                            JOIN sectiongrouplink AS t_SGL ON t_SGL.sectionTemplateid = t_sectiontemplate.id
                            JOIN sectiongroup AS t_SG ON t_SGL.sectionGroupid = t_SG.id
                        WHERE t_SG.studyid = "'.$studyid.'"
                        ORDER BY fieldversion.lastModifiedDate DESC';
        }

        return $query;
    }
}

?>