<?php

if(!defined('MyConst')) {
    die('Direct access not permitted');
}

include_once '../../functions/globalCUD.php';

class crossformlinking{

    //database connection and table name
    private $conn;
    private $tableName = "crossformlinking";

    //object properties
    public $id;
    public $studyid;
    public $parentFormid;
    public $parentFieldVerid;
    public $childFormid;
    public $childFieldVerid;
    public $createdDate;
    public $lastModifiedDate;
    public $isRemovedFlag;

    private $globalCUD;

    //constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
        $this->globalCUD = new globalCUD();
    }

    public function getConn(){
        return $this->conn;
    }

    //read quotes
    function read($where){

        $query = '';
        
        //only id provided
        if($where != "")
        {
            $query = 'SELECT crossformlinking.id, 
                                crossformlinking.studyid, 
                                t_Study.ciStudyProjectCode, 
                                t_ParForm.visitid AS "ParentVisitid", 
                                t_ParVisit.name AS "ParentVisitName",
                                crossformlinking.parentFormid AS "ParentFormid", 
                                t_ParForm.name AS "ParentFormName", 
                                t_ParForm.formTemplateid AS "ParentFormTemplateid",
                                t_ParFormTemp.name AS "ParentFormTemplateName", 
                                crossformlinking.parentFieldVerid,
                                t_ParField.ciFieldName AS "ParentCIFieldName",
                                t_ChiForm.visitid AS "ChildVisitid", 
                                t_ChiVisit.name AS "ChildVisitName",
                                crossformlinking.childFormid AS "ChildFormid", 
                                t_ChiForm.name AS "ChildFormName", 
                                t_ChiForm.formTemplateid AS "ChildFormTemplateid",
                                t_ChiFormTemp.name AS "ChildFormTemplateName",
                                crossformlinking.childFieldVerid,
                                t_ChiField.ciFieldName AS "ChildCIFieldName",
                                crossformlinking.createdDate,
                                crossformlinking.lastModifiedDate,
                                crossformlinking.isRemovedFlag
                            FROM crossformlinking 
                                LEFT JOIN study AS t_Study ON crossformlinking.studyid = t_Study.id
                                LEFT JOIN form AS t_ParForm ON crossformlinking.parentFormid = t_ParForm.id
                                LEFT JOIN visit AS t_ParVisit ON t_ParForm.visitid = t_ParVisit.id
                                LEFT JOIN formtemplate AS t_ParFormTemp ON t_ParForm.formTemplateid = t_ParFormTemp.id
                                LEFT JOIN fieldversion AS t_ParField ON crossformlinking.parentFieldVerid = t_ParField.id
                                LEFT JOIN form AS t_ChiForm ON crossformlinking.childFormid = t_ChiForm.id
                                LEFT JOIN visit AS t_ChiVisit ON t_ChiForm.visitid = t_ChiVisit.id 
                                LEFT JOIN formtemplate AS t_ChiFormTemp ON t_ChiForm.formtemplateid = t_ChiFormTemp.id
                                LEFT JOIN fieldversion AS t_ChiField ON crossformlinking.childFieldVerid = t_ChiField.id
                            WHERE '.$where.'
                            ORDER BY crossformlinking.studyid asc, ParentVisitName asc';
        }
        //select all query 
        else{
            $query = 'SELECT crossformlinking.id, 
                                crossformlinking.studyid, 
                                t_Study.ciStudyProjectCode, 
                                t_ParForm.visitid AS "ParentVisitid", 
                                t_ParVisit.name AS "ParentVisitName",
                                crossformlinking.parentFormid AS "ParentFormid", 
                                t_ParForm.name AS "ParentFormName", 
                                t_ParForm.formTemplateid AS "ParentFormTemplateid",
                                t_ParFormTemp.name AS "ParentFormTemplateName", 
                                crossformlinking.parentFieldVerid,
                                t_ParField.ciFieldName AS "ParentCIFieldName",
                                t_ChiForm.visitid AS "ChildVisitid", 
                                t_ChiVisit.name AS "ChildVisitName",
                                crossformlinking.childFormid AS "ChildFormid", 
                                t_ChiForm.name AS "ChildFormName", 
                                t_ChiForm.formTemplateid AS "ChildFormTemplateid",
                                t_ChiFormTemp.name AS "ChildFormTemplateName",
                                crossformlinking.childFieldVerid,
                                t_ChiField.ciFieldName AS "ChildCIFieldName",
                                crossformlinking.createdDate,
                                crossformlinking.lastModifiedDate,
                                crossformlinking.isRemovedFlag
                            FROM crossformlinking 
                                LEFT JOIN study AS t_Study ON crossformlinking.studyid = t_Study.id
                                LEFT JOIN form AS t_ParForm ON crossformlinking.parentFormid = t_ParForm.id
                                LEFT JOIN visit AS t_ParVisit ON t_ParForm.visitid = t_ParVisit.id
                                LEFT JOIN formtemplate AS t_ParFormTemp ON t_ParForm.formTemplateid = t_ParFormTemp.id
                                LEFT JOIN fieldversion AS t_ParField ON crossformlinking.parentFieldVerid = t_ParField.id
                                LEFT JOIN form AS t_ChiForm ON crossformlinking.childFormid = t_ChiForm.id
                                LEFT JOIN visit AS t_ChiVisit ON t_ChiForm.visitid = t_ChiVisit.id 
                                LEFT JOIN formtemplate AS t_ChiFormTemp ON t_ChiForm.formtemplateid = t_ChiFormTemp.id
                                LEFT JOIN fieldversion AS t_ChiField ON crossformlinking.childFieldVerid = t_ChiField.id
                            ORDER BY crossformlinking.studyid asc, ParentVisitName asc';                                  
        }

        //prepare query statement
        $stmt = $this->conn->prepare($query);

        //execute query
        $stmt->execute();
        $num = $stmt->rowCount();

        //check if more than 0 record found
        if($num>0){

            $output_arr = array();

            //retrive cro table conents
            while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    
                //extract row
                extract($row);
                $item = array(
                    "id" => $id,
                    "study" => is_null($studyid) ?
                    null :
                    array(
                        "id" => $studyid,
                        "ciStudyProjectCode" => $ciStudyProjectCode
                    ),
                    "parentVisit" => is_null($ParentVisitid) ?
                    null :
                    array(
                        "id" => $ParentVisitid,
                        "name" => $ParentVisitName
                    ),
                    "parentForm" => is_null($ParentFormid) ? 
                    null :    
                    array(
                        "id" => $ParentFormid,
                        "name" => $ParentFormName
                    ),
                    "parentFormTemplate" => is_null($ParentFormTemplateid) ?
                    null :
                    array(
                        "id" => $ParentFormTemplateid,
                        "name" => $ParentFormTemplateName
                    ),
                    "parentField" => is_null($parentFieldVerid) ?
                    null :
                    array(
                        "id" => $parentFieldVerid,
                        "ciFieldName" => $ParentCIFieldName
                    ),
                    "childVisit" => is_null($ChildVisitid) ?
                    null :
                    array(
                        "id" => $ChildVisitid,
                        "name" => $ChildVisitName
                    ),
                    "childForm" => is_null($ChildFormid) ?
                    null :
                    array(
                        "id" => $ChildFormid,
                        "name" => $ChildFormName
                    ),
                    "childFormTemplate" => is_null($ChildFormTemplateid) ?
                    null :
                    array(
                        "id" => $ChildFormTemplateid,
                        "name" => $ChildFormTemplateName
                    ),
                    "childField" => is_null($childFieldVerid) ?
                    null :
                    array(
                        "id" => $childFieldVerid,
                        "ciFieldName" => $ChildCIFieldName
                    ),
                    "createdDate" => $createdDate,
                    "lastModifiedDate" => $lastModifiedDate,
                    "isRemovedFlag" => $isRemovedFlag
                );

                array_push($output_arr, $item);
            }

            return $output_arr;
        }
        else {
            return null;
        }
    }

    function create($array){

        //Check that your study actually exist!
        if(array_key_exists('studyid', $array) && !id_Exists($array['studyid'], "study", $this->getConn())){ 
                
            $currRet['success'] = "false";
            $currRet['message'] = "studyid Not Found";

            return $currRet;

        }
        else if(array_key_exists('parentFormid', $array) && !id_Exists($array['parentFormid'], "form", $this->getConn())){ 
                
            $currRet['success'] = "false";
            $currRet['message'] = "parentFormid Not Found";

            return $currRet;

        }
        else if(array_key_exists('parentFieldVerid', $array) && !id_Exists($array['parentFieldVerid'], "fieldversion", $this->getConn())){ 
                
            $currRet['success'] = "false";
            $currRet['message'] = "parentFieldVerid Not Found";

            return $currRet;

        }
        else if(array_key_exists('childFormid', $array) && !id_Exists($array['childFormid'], "form", $this->getConn())){ 
                
            $currRet['success'] = "false";
            $currRet['message'] = "childFormid Not Found";

            return $currRet;

        }
        else if(array_key_exists('childFieldVerid', $array) && !id_Exists($array['childFieldVerid'], "fieldversion", $this->getConn())){ 
                
            $currRet['success'] = "false";
            $currRet['message'] = "childFieldVerid Not Found";

            return $currRet;

        }
        else{
            return $this->globalCUD->CreateGlobal($array, $this->conn, $this->tableName, $this->id);
        }
        
    }

    function update($array){

        //Check that your study actually exist!
        if(array_key_exists('studyid', $array) && !id_Exists($array['studyid'], "study", $this->getConn())){ 
                
            $currRet['success'] = "false";
            $currRet['message'] = "studyid Not Found";

            return $currRet;

        }
        else if(array_key_exists('parentFormid', $array) && !id_Exists($array['parentFormid'], "form", $this->getConn())){ 
                
            $currRet['success'] = "false";
            $currRet['message'] = "parentFormid Not Found";

            return $currRet;

        }
        else if(array_key_exists('parentFieldVerid', $array) && !id_Exists($array['parentFieldVerid'], "fieldversion", $this->getConn())){ 
                
            $currRet['success'] = "false";
            $currRet['message'] = "parentFieldVerid Not Found";

            return $currRet;

        }
        else if(array_key_exists('childFormid', $array) && !id_Exists($array['childFormid'], "form", $this->getConn())){ 
                
            $currRet['success'] = "false";
            $currRet['message'] = "childFormid Not Found";

            return $currRet;

        }
        else if(array_key_exists('childFieldVerid', $array) && !id_Exists($array['childFieldVerid'], "fieldversion", $this->getConn())){ 
                
            $currRet['success'] = "false";
            $currRet['message'] = "childFieldVerid Not Found";

            return $currRet;

        }
        else{

            $this->id = $array['id'];
            
            return $this->globalCUD->UpdateGlobal($array, $this->conn, $this->tableName);
        }
    }

    function delete($array){

        $this->id = $array['id'];

        return $this->globalCUD->DeleteGlobal($array, $this->conn, $this->tableName);

    }
}

?>