<?php

if(!defined('MyConst')) {
    die('Direct access not permitted');
}

include_once '../../functions/globalCUD.php';

class formtrigger{

    //database connection and table name
    private $conn;
    private $tableName = "formtrigger";

    //object properties
    public $id;
    public $studyid;
    public $triggerSourceType; //Field vs Form
    public $sourceFormVisitLinkid;
    public $sourceTriggerFieldid;
    public $triggerFormid;
    public $createdDate;
    public $lastModifiedDate;
    public $isRemovedFlag;

    private $globalCUD;

    //constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
        $this->globalCUD = new globalCUD();
    }

    public function getConn(){
        return $this->conn;
    }

    //read quotes
    function read($where){

        $query = '';
        
        //only id provided
        if($where != "")
        {
            $query = 'SELECT formtrigger.id,
                                formtrigger.studyid,
                                t_Study.ciStudyProjectCode, 
                                formtrigger.triggerSourceType,
                                t_srcVisit.id AS "SourceVisitid",
                                t_srcVisit.name AS "SourceVisitName",
                                formtrigger.sourceFormVisitLinkid AS "SourceFormid",
                                t_srcForm.name AS "SourceFormName",
                                formtrigger.sourceTriggerFieldid AS "SourceFieldid",
                                t_srcField.ciFieldName AS "SourceFieldName",
                                t_srcField.ciLogic AS "SourceFieldLogic",
                                t_tarVisit.id AS "TargetVisitid",
                                t_tarVisit.name AS "TargetVisitName",
                                t_tarForm.id AS "TargetFormid",
                                t_tarForm.name AS "TargetFormName",
                                formtrigger.createdDate,
                                formtrigger.lastModifiedDate,
                                formtrigger.isRemovedFlag
                        FROM formtrigger
                            LEFT JOIN study AS t_Study ON formtrigger.studyid = t_Study.id
                            LEFT JOIN form AS t_srcForm ON formtrigger.sourceFormVisitLinkid = t_srcForm.id
                            LEFT JOIN visit AS t_srcVisit ON t_srcForm.visitid = t_srcVisit.id
                            LEFT JOIN fieldversion AS t_srcField ON formtrigger.sourceTriggerFieldid = t_srcField.id
                            LEFT JOIN form AS t_tarForm ON formtrigger.triggerFormid = t_tarForm.id
                            LEFT JOIN visit AS t_tarVisit ON t_tarForm.visitid = t_tarVisit.id
                            WHERE '.$where.'
                        ORDER BY formtrigger.studyid asc, t_srcVisit.name asc';
        }
        //select all query 
        else{
            $query = 'SELECT formtrigger.id,
                                formtrigger.studyid,
                                t_Study.ciStudyProjectCode, 
                                formtrigger.triggerSourceType,
                                t_srcVisit.id AS "SourceVisitid",
                                t_srcVisit.name AS "SourceVisitName",
                                formtrigger.sourceFormVisitLinkid AS "SourceFormid",
                                t_srcForm.name AS "SourceFormName",
                                formtrigger.sourceTriggerFieldid AS "SourceFieldid",
                                t_srcField.ciFieldName AS "SourceFieldName",
                                t_srcField.ciLogic AS "SourceFieldLogic",
                                t_tarVisit.id AS "TargetVisitid",
                                t_tarVisit.name AS "TargetVisitName",
                                t_tarForm.id AS "TargetFormid",
                                t_tarForm.name AS "TargetFormName",
                                formtrigger.createdDate,
                                formtrigger.lastModifiedDate,
                                formtrigger.isRemovedFlag
                                
                        FROM formtrigger
                            LEFT JOIN study AS t_Study ON formtrigger.studyid = t_Study.id
                            LEFT JOIN form AS t_srcForm ON formtrigger.sourceFormVisitLinkid = t_srcForm.id
                            LEFT JOIN visit AS t_srcVisit ON t_srcForm.visitid = t_srcVisit.id
                            LEFT JOIN fieldversion AS t_srcField ON formtrigger.sourceTriggerFieldid = t_srcField.id
                            LEFT JOIN form AS t_tarForm ON formtrigger.triggerFormid = t_tarForm.id
                            LEFT JOIN visit AS t_tarVisit ON t_tarForm.visitid = t_tarVisit.id
                        
                        ORDER BY formtrigger.studyid asc, t_srcVisit.name asc';                                  
        }

        //prepare query statement
        $stmt = $this->conn->prepare($query);

        //execute query
        $stmt->execute();
        $num = $stmt->rowCount();

        //check if more than 0 record found
        if($num>0){

            $output_arr = array();

            //retrive cro table conents
            while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    
                //extract row
                extract($row);
                $item = array(
                    "id" => $id,
                    "triggerSourceType" => $triggerSourceType,
                    "study" => is_null($studyid) ?
                    null :
                    array(
                        "id" => $studyid,
                        "ciStudyProjectCode" => $ciStudyProjectCode
                    ),
                    "sourceVisit" => is_null($SourceVisitid) ?
                    null :
                    array(
                        "id" => $SourceVisitid,
                        "name" => $SourceVisitName
                    ),
                    "sourceForm" => is_null($SourceFormid) ?
                    null :
                    array(
                        "id" => $SourceFormid,
                        "name" => $SourceFormName
                    ),
                    "sourceField" => is_null($SourceFieldid) ?
                    null :
                    array(
                        "id" => $SourceFieldid,
                        "ciFieldName" => $SourceFieldName,
                        "ciLogic" => $SourceFieldLogic
                    ),
                    "targetVisit" => is_null($TargetVisitid) ?
                    null :
                    array(
                        "id" => $TargetVisitid,
                        "name" => $TargetVisitName
                    ),
                    "targetForm" => is_null($TargetFormid) ?
                    null : 
                    array(
                        "id" => $TargetFormid,
                        "name" => $TargetFormName
                    ),
                    "createdDate" => $createdDate,
                    "lastModifiedDate" => $lastModifiedDate,
                    "isRemovedFlag" => $isRemovedFlag
                );

                array_push($output_arr, $item);
            }

            return $output_arr;
        }
        else {
            return null;
        }
    }

    function create($array){

        //Check that your study actually exist!
        if(array_key_exists('studyid', $array) && !id_Exists($array['studyid'], "study", $this->getConn())){ 
                
            $currRet['success'] = "false";
            $currRet['message'] = "studyid Not Found";

            return $currRet;

        }
        else if(array_key_exists('sourceFormVisitLinkid', $array) && !id_Exists($array['sourceFormVisitLinkid'], "form", $this->getConn())){ 
                
            $currRet['success'] = "false";
            $currRet['message'] = "sourceFormVisitLinkid Not Found";

            return $currRet;

        }
        else if(array_key_exists('sourceTriggerFieldid ', $array) && !id_Exists($array['sourceTriggerFieldid '], "fieldversion", $this->getConn())){ 
                
            $currRet['success'] = "false";
            $currRet['message'] = "sourceTriggerFieldid  Not Found";

            return $currRet;

        }
        else if(array_key_exists('triggerFormid ', $array) && !id_Exists($array['triggerFormid '], "form", $this->getConn())){ 
                
            $currRet['success'] = "false";
            $currRet['message'] = "triggerFormid  Not Found";

            return $currRet;

        }
        else{
            return $this->globalCUD->CreateGlobal($array, $this->conn, $this->tableName, $this->id);
        }
        
    }

    function update($array){

        //Check that your study actually exist!
        if(array_key_exists('studyid', $array) && !id_Exists($array['studyid'], "study", $this->getConn())){ 
                
            $currRet['success'] = "false";
            $currRet['message'] = "studyid Not Found";

            return $currRet;

        }
        else if(array_key_exists('sourceFormVisitLinkid', $array) && !id_Exists($array['sourceFormVisitLinkid'], "form", $this->getConn())){ 
                
            $currRet['success'] = "false";
            $currRet['message'] = "sourceFormVisitLinkid Not Found";

            return $currRet;

        }
        else if(array_key_exists('sourceTriggerFieldid ', $array) && !id_Exists($array['sourceTriggerFieldid '], "fieldversion", $this->getConn())){ 
                
            $currRet['success'] = "false";
            $currRet['message'] = "sourceTriggerFieldid  Not Found";

            return $currRet;

        }
        else if(array_key_exists('triggerFormid ', $array) && !id_Exists($array['triggerFormid '], "form", $this->getConn())){ 
                
            $currRet['success'] = "false";
            $currRet['message'] = "triggerFormid  Not Found";

            return $currRet;

        }
        else{

            $this->id = $array['id'];
            
            return $this->globalCUD->UpdateGlobal($array, $this->conn, $this->tableName);
        }
    }

    function delete($array){

        $this->id = $array['id'];

        return $this->globalCUD->DeleteGlobal($array, $this->conn, $this->tableName);

    }
}

?>