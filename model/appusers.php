<?php

if(!defined('MyConst')) {
    die('Direct access not permitted');
}

include_once '../../functions/globalCUD.php';

class appusers{

    //database connection and table name
    private $conn;
    private $tableName = "appusers";

    //object properties
    public $id;
    public $name;
    public $email;
    public $isActiveFlag;
    public $readOnlyFlag;
    public $userRole;
    public $createdDate;
    public $lastModifiedDate;

    private $globalCUD;

    //constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
        $this->globalCUD = new globalCUD();
    }

    public function getConn(){
        return $this->conn;
    }

    function read($where){
        
        $query = '';
        
        //only id provided
        if($where != "")
        {
            $query = 'SELECT * 
                        FROM appusers 
                        WHERE '.$where.'
                        ORDER BY email asc';
        }
        //select all query 
        else{
            $query = 'SELECT *  
                        FROM appusers 
                        ORDER BY email asc';                                  
        }

        //prepare query statement
        $stmt = $this->conn->prepare($query);

        //execute query
        $stmt->execute();
        $num = $stmt->rowCount();

        //check if more than 0 record found
        if($num>0){
            //if $id is specified we also need to return a list of studies associated with it
            //other wise, just return the entire list of arms
            if(!empty($_GET)){
                //arm array
                $output_arr = array();

                //retrive arm table conents
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    
                    //extract row
                    extract($row);
                    $item = array(
                        "id" => $id,
                        "name" => $name,
                        "email" => $email,
                        "isActiveFlag" => $isActiveFlag,
                        "readOnlyFlag" => $readOnlyFlag,
                        "userRole" => $this->roleAssoc($userRole),
                        "createdDate" => $createdDate,
                        "lastModifiedDate" => $lastModifiedDate
                    );

                    array_push($output_arr, $item);
                }

                return $output_arr;
            }
            else{

                return $stmt->fetchAll(\PDO::FETCH_ASSOC);
            }
        }
        else {
            return null;
        }
    }

    function create($array){

        //Check that your user already exist!
        if(array_key_exists('email', $array) && $this->userExists($array['email'])){ 
                
            $currRet['success'] = "false";
            $currRet['message'] = "User already exists. Cannot create duplicate users.";

            return $currRet;

        }
        else{
            return $this->globalCUD->CreateGlobal($array, $this->conn, $this->tableName, $this->id);
        }
    }

    function update($array){
        //Check that your user actually exist!
        if(array_key_exists('email', $array) && !id_Exists($array['id'], "appusers", $this->getConn())){ 
                
            $currRet['success'] = "false";
            $currRet['message'] = "userid Not found.";

            return $currRet;

        }
        else{

            $this->id = $array['id'];
            
            return $this->globalCUD->UpdateGlobal($array, $this->conn, $this->tableName);
        }
    }

    function delete($array){

        $this->id = $array['id'];
        
        return $this->globalCUD->DeleteGlobal($array, $this->conn, $this->tableName);

    }

    private function userExists($email){
        $query = 'SELECT * 
                    FROM appusers 
                    WHERE email = '.$this->conn->quote($email);
    
        //prepare query statement
        $stmt = $this->conn->prepare($query);
    
        //execute query
        $stmt->execute();
        $num = $stmt->rowCount();
    
        if($num >0) {
            return true;
        }
        else {
            return false;
        }
    }

    private function roleAssoc($roleNumber){
        $output_arr = array();

        $query = 'SELECT *
                    FROM approles
                    WHERE userRole = '.$this->conn->quote($roleNumber);
    
        //prepare query statement
        $stmt = $this->conn->prepare($query);

        //execute query
        $stmt->execute();

        //retrive study table conents
        while($row2 = $stmt->fetch(PDO::FETCH_ASSOC)){
            //extract row
            extract($row2);
            $item = array(
                "id" => $id,
                "userRole" => $userRole,
                "roleName" => $roleName,
                "editUsers_01" => $editUsers_01,
                "exportRecords_02" => $exportRecords_02,
                "editRequirements_03" => $editRequirements_03,
                "studyMilestones_04" => $studyMilestones_04,
                "studyMilestonesQC_05" => $studyMilestonesQC_05,
                "editReqQCMilestone_06" => $editReqQCMilestone_06,
                "viewUserNamesHistory_07" => $viewUserNamesHistory_07,
                "bypassStatusWorkflow_08" => $bypassStatusWorkflow_08
            );

            array_push($output_arr, $item);
        }

        if(sizeof($output_arr) > 0){
            return $output_arr;
        }
        else{
            return null;
        }
    }
}