<?php

if(!defined('MyConst')) {
    die('Direct access not permitted');
}

include_once '../../functions/globalCUD.php';

class form{

    //database connection and table name
    private $conn;
    private $tableName = "form";

    //object properties
    public $id;
    public $name;
    public $visitid;
    public $orderInVisit;
    public $formTemplateid;
    public $createdDate;
    public $lastModifiedDate;
    public $isRemovedFlag;
    
    private $globalCUD;

    //constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
        $this->globalCUD = new globalCUD();
    }

    public function getConn(){
        return $this->conn;
    }

    //read quotes
    function read($where){

        $query = '';
        
        //only id provided
        if($where != "")
        {
            $query = 'SELECT id, name, visitid, orderInVisit, formTemplateid, createdDate, lastModifiedDate, isRemovedFlag 
                        FROM form 
                        WHERE '.$where.'
                        ORDER BY name asc, orderInVisit asc';
        }
        //select all query 
        else{
            $query = 'SELECT id, name, visitid, orderInVisit, formTemplateid, createdDate, lastModifiedDate, isRemovedFlag 
                        FROM form 
                        ORDER BY name asc, orderInVisit asc';                                  
        }

        //prepare query statement
        $stmt = $this->conn->prepare($query);

        //execute query
        $stmt->execute();
        $num = $stmt->rowCount();

        //check if more than 0 record found
        if($num>0){
            //if $id is specified we also need to return a list of studies associated with it
            //other wise, just return the entire list of forms
            if(!empty($_GET)){
                //form array
                $output_arr = array();

                //retrive form table conents
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    
                    //extract row
                    extract($row);
                    $item = array(
                        "id" => $id,
                        "name" => $name,
                        "visitid" => $visitid,
                        "orderInVisit" => $orderInVisit,
                        "formTemplateid" => $formTemplateid,
                        "createdDate" => $createdDate,
                        "lastModifiedDate" => $lastModifiedDate,
                        "isRemovedFlag" => $isRemovedFlag,
                        "formTemplate" => $this->formTempAssoc($formTemplateid)
                    );

                    array_push($output_arr, $item);
                }
            }
            else{
                
                return $stmt->fetchAll(\PDO::FETCH_ASSOC);
            }

            return $output_arr;
        }
        else {
            return null;
        }
    }

    function create($array){

        //Check that your study actually exist!
        if(array_key_exists('visitid', $array) && !id_Exists($array['visitid'], "visit", $this->getConn())){ 
                
            $currRet['success'] = "false";
            $currRet['message'] = "visitid Not Found";

            return $currRet;

        }
        else if(array_key_exists('formTemplateid', $array) && !id_Exists($array['formTemplateid'], "formtemplate", $this->getConn())){ 
                
            $currRet['success'] = "false";
            $currRet['message'] = "formTemplateid Not Found";

            return $currRet;

        }
        else{
            return $this->globalCUD->CreateGlobal($array, $this->conn, $this->tableName, $this->id);
        }
        
    }

    function update($array){

        //Check that your study actually exist!
        if(array_key_exists('visitid', $array) && !id_Exists($array['visitid'], "visit", $this->getConn())){ 
                
            $currRet['success'] = "false";
            $currRet['message'] = "visitid Not Found";

            return $currRet;

        }
        else if(array_key_exists('formTemplateid', $array) && !id_Exists($array['formTemplateid'], "formtemplate", $this->getConn())){ 
                
            $currRet['success'] = "false";
            $currRet['message'] = "formTemplateid Not Found";

            return $currRet;

        }
        else{

            $this->id = $array['id'];
            
            return $this->globalCUD->UpdateGlobal($array, $this->conn, $this->tableName);
        }
    }

    function delete($array){

        $this->id = $array['id'];

        return $this->globalCUD->DeleteGlobal($array, $this->conn, $this->tableName);

    }

    function formTempAssoc($id)
    {
        $query = '';
       
        $query = 'SELECT id, name 
                    FROM formtemplate 
                    WHERE id = "'.$id.'"';

        //prepare query statement
        $stmt = $this->conn->prepare($query);

        //execute query
        $stmt->execute();

        if($stmt->rowCount() > 0){
            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        }
        else{
            return null;
        }
    }
}

?>