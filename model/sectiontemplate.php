<?php

if(!defined('MyConst')) {
    die('Direct access not permitted');
}

include_once '../../functions/globalCUD.php';

class sectiontemplate{

    //database connection and table name
    private $conn;
    private $tableName = "sectiontemplate";

    //object properties
    public $id;
    public $name;
    public $copiedFlag;
    public $copiedUserid;
    public $copiedFromStudyid;
    public $copiedFromSectionGroupid;
    public $copiedFromSectionTemplateid;
    public $templateModifiedFlag;
    public $dateTimeCopied;
    public $sectionGeneratorConfig;
    public $uniqueFieldPrefix;
    public $isRemovedFlag;
    public $libraryStatus;

    private $globalCUD;

    //constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
        $this->globalCUD = new globalCUD();
    }

    public function getConn(){
        return $this->conn;
    }

    //read quotes
    function read($where){

        $query = '';
        
        //only id provided
        if($where != "")
        {
            $query = 'SELECT sectiontemplate.id, 
                             sectiontemplate.name, 
                             sectiontemplate.uniqueSectionHeader,
                             sectiontemplate.copiedFlag, 
                             sectiontemplate.copiedUserid, 
                             sectiontemplate.copiedFromStudyid, 
                             sectiontemplate.copiedFromSectionGroupid, 
                             sectiontemplate.copiedFromSectionTemplateid, 
                             sectiontemplate.templateModifiedFlag, 
                             sectiontemplate.dateTimeCopied, 
                             sectiontemplate.sectionGeneratorConfig, 
                             sectiontemplate.uniqueFieldPrefix, 
                             sectiontemplate.createdDate, 
                             sectiontemplate.lastModifiedDate, 
                             sectiontemplate.hasCrossFormFlag,
                             sectiontemplate.hasTriggerFlag,
                             sectiontemplate.isRemovedFlag, 
                             sectiontemplate.libraryStatus, 
                             sectiontemplate.salesforceRecordid, 
                             t_SGL.id AS sectionGroupLinkid, 
                             t_Sgroup.id AS sectionGroupid, 
                             t_Sgroup.studyid AS studyid
                        FROM sectiontemplate 
                            LEFT JOIN sectiongrouplink As t_SGL ON t_SGL.sectionTemplateid = sectiontemplate.id
                            LEFT JOIN sectiongroup AS t_Sgroup ON t_Sgroup.id = t_SGL.sectionGroupid
                        WHERE '.$where.'
                        ORDER BY t_SGL.id asc, name asc';
        }
        //select all query 
        else{
            $query = 'SELECT sectiontemplate.id, 
                             sectiontemplate.name, 
                             sectiontemplate.uniqueSectionHeader,
                             sectiontemplate.copiedFlag, 
                             sectiontemplate.copiedUserid, 
                             sectiontemplate.copiedFromStudyid, 
                             sectiontemplate.copiedFromSectionGroupid, 
                             sectiontemplate.copiedFromSectionTemplateid, 
                             sectiontemplate.templateModifiedFlag, 
                             sectiontemplate.dateTimeCopied, 
                             sectiontemplate.sectionGeneratorConfig, 
                             sectiontemplate.uniqueFieldPrefix, 
                             sectiontemplate.createdDate, 
                             sectiontemplate.lastModifiedDate, 
                             sectiontemplate.hasCrossFormFlag,
                             sectiontemplate.hasTriggerFlag,
                             sectiontemplate.isRemovedFlag, 
                             sectiontemplate.libraryStatus, 
                             sectiontemplate.salesforceRecordid, 
                             t_SGL.id AS sectionGroupLinkid, 
                             t_Sgroup.id AS sectionGroupid, 
                             t_Sgroup.studyid AS studyid
                        FROM sectiontemplate 
                            LEFT JOIN sectiongrouplink As t_SGL ON t_SGL.sectionTemplateid = sectiontemplate.id
                            LEFT JOIN sectiongroup AS t_Sgroup ON t_Sgroup.id = t_SGL.sectionGroupid
                        ORDER BY t_SGL.id asc, name asc';                                  
        }

        //prepare query statement
        $stmt = $this->conn->prepare($query);

        //execute query
        $stmt->execute();
        
        if($stmt->rowCount() > 0){
            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        }
        else {
            return null;
        }
    }

    function create($array){

        //Check that your study actually exist!
        if(array_key_exists('sectionGroupid', $array) && !id_Exists($array['sectionGroupid'], "sectiongroup", $this->getConn())){ 
                
            $currRet['success'] = "false";
            $currRet['message'] = "sectionGroupid Not Found";

            return $currRet;

        }
        else if(array_key_exists('copiedUserid', $array) && !id_Exists($array['copiedUserid'], "users", $this->getConn())){ 
                
            $currRet['success'] = "false";
            $currRet['message'] = "userid Not Found";

            return $currRet;

        }
        else{
            return $this->globalCUD->CreateGlobal($array, $this->conn, $this->tableName, $this->id);
        }
        
    }

    function update($array){

        //Check that your study actually exist!
        if(array_key_exists('sectionGroupid', $array) && !id_Exists($array['sectionGroupid'], "sectiongroup", $this->getConn())){ 
                
            $currRet['success'] = "false";
            $currRet['message'] = "sectionGroupid Not Found";

            return $currRet;

        }
        else if(array_key_exists('copiedUserid', $array) && !id_Exists($array['copiedUserid'], "users", $this->getConn())){ 
                
            $currRet['success'] = "false";
            $currRet['message'] = "userid Not Found";

            return $currRet;

        }
        else{

            $this->id = $array['id'];
            
            return $this->globalCUD->UpdateGlobal($array, $this->conn, $this->tableName);
        }
    }

    function delete($array){

        $this->id = $array['id'];

        return $this->globalCUD->DeleteGlobal($array, $this->conn, $this->tableName);

    }
}

?>