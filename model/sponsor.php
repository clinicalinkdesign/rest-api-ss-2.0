<?php

if(!defined('MyConst')) {
    die('Direct access not permitted');
}

include_once '../../functions/globalCUD.php';

class sponsor{

    //database connection and table name
    private $conn;
    private $tableName = "sponsor";

    //object properties
    public $id;
    public $name;
    public $createdDate;
    public $lastModifiedDate;
    public $isRemovedFlag;

    private $globalCUD;

    //constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
        $this->globalCUD = new globalCUD();
    }

    public function getConn(){
        return $this->conn;
    }

    //read quotes
    function read($where){
        
        $query = '';
        
        //only id provided
        if($where != "")
        {
            $query = 'SELECT id, name, createdDate, lastModifiedDate, isRemovedFlag 
                        FROM sponsor 
                        WHERE '.$where.'
                        ORDER BY name asc';
        }
        //select all query 
        else{
            $query = 'SELECT sponsor.id, sponsor.name, sponsor.createdDate, sponsor.lastModifiedDate, sponsor.isRemovedFlag  
                        FROM sponsor 
                        ORDER BY name asc';                                  
        }

        //prepare query statement
        $stmt = $this->conn->prepare($query);

        //execute query
        $stmt->execute();
        $num = $stmt->rowCount();

        //check if more than 0 record found
        if($num>0){
            //if $id is specified we also need to return a list of studies associated with it
            //other wise, just return the entire list of sponsors
            if(!empty($_GET)){
                //sponsor array
                $output_arr = array();
                

                //retrive sponsor table conents
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    
                    //extract row
                    extract($row);
                    $item = array(
                        "id" => $id,
                        "name" => $name,
                        "createdDate" => $createdDate,
                        "lastModifiedDate" => $lastModifiedDate,
                        "isRemovedFlag" => $isRemovedFlag,
                        "studies" => $this->studyAssoc($id)
                    );

                    array_push($output_arr, $item);
                }

                return $output_arr;
            }
            else{

                return $stmt->fetchAll(\PDO::FETCH_ASSOC);
            }
        }
        else {
            return null;
        }
    }

    function create($array){

        return $this->globalCUD->CreateGlobal($array, $this->conn, $this->tableName, $this->id);
        
    }

    function update($array){

        $this->id = $array['id'];
        
        return $this->globalCUD->UpdateGlobal($array, $this->conn, $this->tableName);

    }

    function delete($array){

        $this->id = $array['id'];
        
        return $this->globalCUD->DeleteGlobal($array, $this->conn, $this->tableName);

    }

    function studyAssoc($id)
    {
        $query = '';
       
        $query = 'SELECT id, ciStudyProjectCode, protocolName, isRemovedFlag 
                    FROM study 
                    WHERE sponsorid = "'.$id.'"
                    ORDER BY ciStudyProjectCode asc';

        //prepare query statement
        $stmt = $this->conn->prepare($query);

        //execute query
        $stmt->execute();

        if($stmt->rowCount() > 0){
            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        }
        else{
            return null;
        }
    }
}

?>