<?php

if(!defined('MyConst')) {
    die('Direct access not permitted');
}

include_once '../../functions/globalCUD.php';

class visit{

    //database connection and table name
    private $conn;
    private $tableName = "visit";

    //object properties
    public $id;
    public $name;
    public $armid;
    public $createdDate;
    public $lastModifiedDate;
    public $visitOrder;
    public $isRemovedFlag;
    
    private $globalCUD;

    //constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
        $this->globalCUD = new globalCUD();
    }

    public function getConn(){
        return $this->conn;
    }

    //read quotes
    function read($where){

        $query = '';
        
        //only id provided
        if($where != "")
        {
            $query = 'SELECT id, name, armid, createdDate, lastModifiedDate, visitOrder, visitType, isRemovedFlag 
                        FROM visit 
                        WHERE '.$where.'
                        ORDER BY armid asc, visitOrder asc';
        }
        //select all query 
        else{
            $query = 'SELECT id, name, armid, createdDate, lastModifiedDate, visitOrder, visitType, isRemovedFlag  
                        FROM visit 
                        ORDER BY name asc, visitOrder asc';                                  
        }

        //prepare query statement
        $stmt = $this->conn->prepare($query);

        //execute query
        $stmt->execute();
        $num = $stmt->rowCount();

        //check if more than 0 record found
        if($num>0){
            //if $id is specified we also need to return a list of studies associated with it
            //other wise, just return the entire list of visits
            if(!empty($_GET)){
                //visit array
                $output_arr = array();

                //retrive visit table conents
                while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    
                    //extract row
                    extract($row);
                    $item = array(
                        "id" => $id,
                        "name" => $name,
                        "armid" => $armid,
                        "createdDate" => $createdDate,
                        "lastModifiedDate" => $lastModifiedDate,
                        "isRemovedFlag" => $isRemovedFlag,
                        "visitOrder" => $visitOrder,
                        "visitType" => $visitType,
                        "Forms" => $this->formAssoc($id)
                    );

                    array_push($output_arr, $item);
                }
            }
            else{
                
                return $stmt->fetchAll(\PDO::FETCH_ASSOC);

            }

            return $output_arr;
        }
        else {
            return null;
        }
    }

    function create($array){

        //Check that your study actually exist!
        if(array_key_exists('armid', $array) && !id_Exists($array['armid'], "arm", $this->getConn())){ 
                
            $currRet['success'] = "false";
            $currRet['message'] = "armid Not Found";

            return $currRet;

        }
        else{
            return $this->globalCUD->CreateGlobal($array, $this->conn, $this->tableName, $this->id);
        }
        
    }

    function update($array){

        //Check that your study actually exist!
        if(array_key_exists('armid', $array) && !id_Exists($array['armid'], "arm", $this->getConn())){ 
                
            $currRet['success'] = "false";
            $currRet['message'] = "armid Not Found";

            return $currRet;

        }
        else{

            $this->id = $array['id'];
            
            return $this->globalCUD->UpdateGlobal($array, $this->conn, $this->tableName);
        }
    }

    function delete($array){

        $this->id = $array['id'];

        return $this->globalCUD->DeleteGlobal($array, $this->conn, $this->tableName);

    }

    //Used for finding if name id is used in the visit table
    function id_used()
    {
        $query = 'SELECT visitid FROM form WHERE visitid = "'.$this->id.'"';

        //prepare query statement
        $stmt = $this->conn->prepare($query);

        //execute query
        $stmt->execute();

        if($stmt->rowCount() > 0){
            return true;
        }
        else{
            return false;
        }
    }

    function formAssoc($id)
    {
        //This function pulls all FORMS that fall within a visit,
        //It then calls formtemplateassoc for the same purpsoe but for the form TEMPLATE
        $query = '';
       
        $query = 'SELECT id, name, orderInVisit, formTemplateid, isRemovedFlag 
                    FROM form 
                    WHERE visitid = "'.$id.'"
                    ORDER BY orderInVisit asc';

        //prepare query statement
        $stmt = $this->conn->prepare($query);

        //execute query
        $stmt->execute();
        
        $output_arr = array();

        //retrive study table conents
        while($row2 = $stmt->fetch(PDO::FETCH_ASSOC)){
            //extract row
            extract($row2);
            $item = array(
                "id" => $id,
                "name" => $name,
                "orderInVisit" => $orderInVisit,
                "formTemplate" => $this->formTempAssoc($formTemplateid)[0],
                "isRemovedFlag" => $isRemovedFlag
            );

            array_push($output_arr, $item);
        }

        if(sizeof($output_arr) > 0){
            return $output_arr;
        }
        else{
            return null;
        }
    }

    function formTempAssoc($id)
    {
        $query = '';
       
        $query = 'SELECT id, name 
                    FROM formtemplate 
                    WHERE id = "'.$id.'"';

        //prepare query statement
        $stmt = $this->conn->prepare($query);

        //execute query
        $stmt->execute();

        if($stmt->rowCount() > 0){
            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        }
        else{
            return null;
        }
    }

    function cleanInputs()
    {
        $this->name = strip_tags($this->name);
        $this->armid = strip_tags($this->armid);
        $this->visitOrder = strip_tags($this->visitOrder);
    }
}

?>