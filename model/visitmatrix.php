<?php

if(!defined('MyConst')) {
    die('Direct access not permitted');
}

include_once '../../functions/globalCUD.php';
    
include_once '../../model/general.php';

class visitmatrix{

    //database connection and table name
    private $conn;
    public $studyid;

    //constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    public function getConn(){
        return $this->conn;
    }

    //read quotes
    function read($id){

        //$result = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        $this->studyid = $id;

        //Pull the study
            //Pull all arms
                //For each arm, pull all visits
                    //For each visit, pull all forms / form templates
                        //For each form template, pull all section templates

        //Data provied will just be id and name for each object to avoid any special formatting, less looping

        $stmt = $this->runQuery('SELECT id, ciStudyProjectCode, protocolName 
                                    FROM study 
                                    WHERE id = "'.$this->studyid.'"');

        $num = $stmt->rowCount();

        if($num > 0){

            $output_arr = array();

            //retrive arm table conents
            while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    
                //extract row
                extract($row);
                $item = array(
                    "id" => $id,
                    "ciStudyProjectCode" => $ciStudyProjectCode,
                    "protocolName" => $protocolName,
                    "arms" => $this->armAssoc($id)
                );

                array_push($output_arr, $item);
            }

            if(sizeof($output_arr) > 0){
                return $output_arr;
            }
            else{
                return null;
            }
        }
        else {
            return null;
        }
    }

    function runQuery($query)
    {
        //prepare query statement
        $stmt = $this->conn->prepare($query);

        //execute query
        $stmt->execute();

        return $stmt;
    }

    function armAssoc($studyid)
    {
        $stmt =  $this->runQuery('SELECT id, name, armOrder, isRemovedFlag 
                                    FROM arm 
                                    WHERE studyid = "'.$studyid.'" 
                                    ORDER BY name asc');

        $output_arr = array();

        //retrive study table conents
        while($row2 = $stmt->fetch(PDO::FETCH_ASSOC)){
            //extract row
            extract($row2);
            $item = array(
                "id" => $id,
                "name" => $name,
                "armOrder" => $armOrder,
                "isRemovedFlag" => $isRemovedFlag,
                "visits" => $this->visitAssoc($id)
            );

            array_push($output_arr, $item);
        }

        if(sizeof($output_arr) > 0){
            return $output_arr;
        }
        else{
            return null;
        }
    }

    function visitAssoc($visitid)
    {
        $stmt =  $this->runQuery('SELECT id, name, visitOrder, visitType, isRemovedFlag 
                                    FROM visit 
                                    WHERE armid = "'.$visitid.'"
                                    ORDER BY visitOrder asc');

        $output_arr = array();

        //retrive study table conents
        while($row2 = $stmt->fetch(PDO::FETCH_ASSOC)){
            //extract row
            extract($row2);
            $item = array(
                "id" => $id,
                "name" => $name,
                "visitOrder" => $visitOrder,
                "visitType" => $visitType,
                "isRemovedFlag" => $isRemovedFlag,
                "forms" => $this->formAssoc($id)
            );

            array_push($output_arr, $item);
        }

        if(sizeof($output_arr) > 0){
            return $output_arr;
        }
        else{
            return null;
        }
    }

    function formAssoc($visitid)
    {
        $stmt =  $this->runQuery('SELECT id, name, orderInVisit, formTemplateid, isRemovedFlag
                                    FROM form 
                                    WHERE visitid = "'.$visitid.'"
                                    ORDER BY orderInVisit asc');

        $output_arr = array();

        //retrive study table conents
        while($row2 = $stmt->fetch(PDO::FETCH_ASSOC)){
            //extract row
            extract($row2);
            $item = array(
                "id" => $id,
                "name" => $name,
                "orderInVisit" => $orderInVisit,
                "isRemovedFlag" => $isRemovedFlag,
                "formTemplate" => $this->formTemplateAssoc($formTemplateid)
            );

            array_push($output_arr, $item);
        }

        if(sizeof($output_arr) > 0){
            return $output_arr;
        }
        else{
            return null;
        }
    }

    function formTemplateAssoc($formtemplateid)
    {
        $stmt =  $this->runQuery('SELECT id, name, isRemovedFlag
                                    FROM formtemplate 
                                    WHERE id = "'.$formtemplateid.'"');

        $output_arr = array();

        //retrive study table conents
        while($row2 = $stmt->fetch(PDO::FETCH_ASSOC)){
            //extract row
            extract($row2);
            $item = array(
                "id" => $id,
                "name" => $name,
                "isRemovedFlag" => $isRemovedFlag,
                "sectionTemplates" => $this->sectionTemplateAssoc($formtemplateid)
            );

            array_push($output_arr, $item);
        }

        if(sizeof($output_arr) > 0){
            return $output_arr;
        }
        else{
            return null;
        }
    }

    function sectionTemplateAssoc($formtemplateid)
    {
        $stmt =  $this->runQuery('SELECT sectiontemplate.id AS "id", 
                                         sectiontemplate.name AS "name",
                                         sectiontemplate.hasTriggerFlag,
                                         sectiontemplate.hasCrossFormFlag, 
                                         sectiontemplate.createdDate AS createdDate,
                                         sectiontemplate.lastModifiedDate AS lastModifiedDate,
                                         sectiontempformlink.orderInForm AS "orderInForm", 
                                         sectiontempformlink.id AS "sectiontempformlinkid",
                                         sectiontempformlink.isRemovedFlag AS "isRemovedFlag"
                                    FROM sectiontempformlink
                                    JOIN sectiontemplate ON sectiontemplate.id = sectiontempformlink.sectionTemplateid
                                    WHERE formtemplateid = "'.$formtemplateid.'"
                                    ORDER by sectiontempformlink.orderInForm asc');

        if($stmt->rowCount() > 0){
            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        }
        else{
            return null;
        }
    }
}

?>