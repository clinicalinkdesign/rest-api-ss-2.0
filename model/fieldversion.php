<?php

if(!defined('MyConst')) {
    die('Direct access not permitted');
}

include_once '../../functions/globalCUD.php';

class fieldversion{

    //database connection and table name
    private $conn;
    private $tableName = "fieldversion";

    //object properties
    public $id;
    
    private $globalCUD;

    //constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
        $this->globalCUD = new globalCUD();
    }

    public function getConn(){
        return $this->conn;
    }

    //read quotes
    function read($where){

        $query = '';
        
        //only id provided
        if($where != "")
        {
            $query = 'SELECT * 
                        FROM fieldversion 
                        WHERE '.$where.'
                        ORDER BY fieldId asc, createdDate asc';
        }
        //select all query 
        else{
            $query = 'SELECT *
                        FROM fieldversion 
                        ORDER BY fieldId asc, createdDate asc';                                  
        }

        //prepare query statement
        $stmt = $this->conn->prepare($query);

        //execute query
        $stmt->execute();
        $num = $stmt->rowCount();

        //check if more than 0 record found
        if($num>0){
            return $stmt->fetchAll(\PDO::FETCH_ASSOC);

        }
        else {
            return null;
        }
    }

    function create($array){
        
        //Check that your study actually exist!
        if(array_key_exists('fieldId', $array) && !id_Exists($array['fieldId'], "field", $this->getConn())){ 
                
            $currRet['success'] = "false";
            $currRet['message'] = "fieldId Not Found";

            return $currRet;

        }
        else{   
            return $this->globalCUD->CreateGlobal($array, $this->conn, $this->tableName, $this->id);
        }
        
    }

    function update($array){

        $this->id = $array['id'];
        

        //Check that your study actually exist!
        if(array_key_exists('fieldId', $array) && !id_Exists($array['fieldId'], "field", $this->getConn())){ 
                
            $currRet['success'] = "false";
            $currRet['message'] = "fieldId Not Found";

            return $currRet;

        }
        else{  
            return $this->globalCUD->UpdateGlobal($array, $this->conn, $this->tableName);
        }
    }

    function delete($array){

        $this->id = $array['id'];

        return $this->globalCUD->DeleteGlobal($array, $this->conn, $this->tableName);

    }
}

?>