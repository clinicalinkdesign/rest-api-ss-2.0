<?php
    if(!defined('MyConst')) {
        die('Direct access not permitted');
    }
       
    try{
        $history = new History($db);
        
        if(isset($_GET["table"])){

            if(isset($_GET["studyspecific"]) && isset($_GET["studyid"]))
            {
                $output = $history->read("", $_GET["table"], $_GET['studyid']);
            }
            else if($_GET["table"] == "fieldversion")
            {
                //set response code - 400 not found
                http_response_code(400);

                //tell the user no history found
                echo json_encode(array("message" => "Fielversion history only supported for individual studies"));
                exit();
            }
            else if($_GET["table"] == "apiusers")
            {
                //set response code - 400 not found
                http_response_code(400);

                //tell the user no history found
                echo json_encode(array('message' => "Not allowed"));
                exit();
            }
            else
            {
                $output = $history->read(readWhereClause($db), $_GET["table"]);
            }
            
            
            if($output != null) {
                //set response code - 200 OK
                http_response_code(200);
                echo json_encode($output);
            }
            else {
                //set response code - 404 not found
                http_response_code(404);

                //tell the user no history found
                echo json_encode(array("message" => "History search returned no results."));
            }
        }
        else {
            //set response code - 400 bad request
            http_response_code(400);
            echo json_encode(array('message' => "Table name not provided"));
        }
    } catch (Exception $e) {
        //set response code - 400 bad request
        http_response_code(400);
        echo json_encode(array('message' => $e->getMessage()));
    }
    exit();

?>