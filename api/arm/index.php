<?php
    //This serves as the arm controller

    define('MyConst', TRUE);

    try {
        include_once '../../model/arm.php';
        include_once '../../config/indexHeader.php';
    }
    catch (Exception $e) {
        logError($e, $requestMethod, $_SERVER['REQUEST_URI']);
        throw $e;
    }
?>