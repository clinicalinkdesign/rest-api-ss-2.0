<?php
    if(!defined('MyConst')) {
        die('Direct access not permitted');
    }
       
    try{
        $general = new General($db);

        if(isset($_GET["table"])){
            
            if($_GET["table"] != "apiusers")
            {
                if(isset($_GET["test"])) {
                    $output = $general->read(readWhereClause($db), $_GET["table"], $_GET["test"]);
                }
                else {
                    $output = $general->read(readWhereClause($db), $_GET["table"]);
                }

                if($output != null) {
                    //set response code - 200 OK
                    http_response_code(200);
                    echo json_encode($output);
                }
                else {
                    //set response code - 404 not found
                    http_response_code(404);

                    //tell the user no general found
                    echo json_encode(array("message" => "General search returned no results."));
                }
            }
            else {
                //set response code - 400 bad request
                http_response_code(400);
                echo json_encode(array('message' => "Not allowed"));
            }
        }
        else {
            //set response code - 400 bad request
            http_response_code(400);
            echo json_encode(array('message' => "Table name not provided"));
        }
    } catch (Exception $e) {
        //set response code - 400 bad request
        http_response_code(400);
        echo json_encode(array('message' => $e->getMessage()));
    }
    exit();

?>