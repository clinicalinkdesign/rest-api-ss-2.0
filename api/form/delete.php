<?php
    if(!defined('MyConst')) {
        die('Direct access not permitted');
    }
           
    if(array_key_exists('data', $data2) && sizeof($data2['data']) > 0)
    {
        $returnArray = array();

        for($i = 0; $i < sizeof($data2['data']); $i++)
        {
            $form = new form($db);

            //Add the delete request to the history table
            $history->write($requestMethod, $data2['data'][$i]['id'], "form", $globalUser); 
            // ^ this has to run before its actually deleted, but will itself error if there is no id found

            array_push($returnArray, $form->delete($data2['data'][$i]));
        }

        //set response code - 200 OK
        http_response_code(200);
        
        echo json_encode($returnArray);
    }
    else{
        //Tell the user the data is incomplete
        //set response code - 400 bad request
        http_response_code(400);
        
        //tell the user
        echo json_encode(array("message" => "Missing Required Parameters"));
    }
    exit();

    /*
    EXAMPLE OF EXPECTED INPUT:
    {
        "data" : [
            {
                "id": "uuid1"
            },
            {
                "id": "uuid2"
            },
            {
                "id": "uuid3"
            }
        ]
    }       
    */
?>