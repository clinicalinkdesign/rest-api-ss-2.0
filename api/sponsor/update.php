<?php

    if(!defined('MyConst')) {
        die('Direct access not permitted');
    }

    if(array_key_exists('data', $data2) && sizeof($data2['data']) > 0)
    {
        $returnArray = array();

        for($i = 0; $i < sizeof($data2['data']); $i++)
        {
            $sponsor = new sponsor($db);

            array_push($returnArray, $sponsor->update($data2['data'][$i]));

            if($returnArray[$i]['success'] == "true")
            {
                array_push($idList, $sponsor->id);
            }
        }

        //set response code - 200 OK
        http_response_code(200);
        
        echo json_encode($returnArray);
        
        //Call the list of ID's to send to the history table after update
        for($i = 0; $i < sizeof($idList); $i++){

            if(array_key_exists('action', $data2)){
                $requestMethod = strtoupper($data2['action']);
            }

            $history->write($requestMethod, $idList[$i], "sponsor", $globalUser);
        }
    }
    else{
        //Tell the user the data is incomplete
        //set response code - 400 bad request
        http_response_code(400);
        
        //tell the user
        echo json_encode(array("message" => "Missing Required Parameters"));
    }
    exit();

    /*
    EXAMPLE OF EXPECTED INPUT:
    {
        "data" : [
            {
                "id": "uuid1",
                "name": "new value1",
                "isRemovedFlag": "1"
            },
            {
                "id": "uuid2",
                "name": "new value1",
                "isRemovedFlag": "1"
            },
            {
                "id": "uuid3",
                "name": "new value1",
                "isRemovedFlag": "1"
            }
        ]
    }       
    */

?>