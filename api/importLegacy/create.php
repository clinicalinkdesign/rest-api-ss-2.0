<?php
    if(!defined('MyConst')) {
        die('Direct access not permitted');
    }
       
    $importLegacy = new importLegacy($db);

    $array = file_get_contents("php://input");
    $array = array_map("str_getcsv", explode("\n", $array));

    if($_GET['type'] == "visitmatrix"){

        reMapVisitMatrix($array);

        $importLegacy->importVisitMatrix($array);
    }
    else if($_GET['type'] == "fields"){

        reMapFields($array);

        $importLegacy->importFields($array);
    }
    else if($_GET['type'] == "crossform"){

        //This will REQUIRE another URL param, ciStudyProjectCode

        if(isset($_GET["ciStudyProjectCode"])) {
            reMapCrossForm($array);

            $importLegacy->importCrossForm($array);
        }
        else {
            http_response_code(400);
            echo json_encode(array('message' => "ciStudyProjectCode not provided"));
        }
    }
    else if($_GET['type'] == "trigger"){

        //This will REQUIRE another URL param, ciStudyProjectCode

        if(isset($_GET["ciStudyProjectCode"])) {
            reMapTriggers($array);

            $importLegacy->importTriggers($array);
        }
        else {
            http_response_code(400);
            echo json_encode(array('message' => "ciStudyProjectCode not provided"));
        }
    }
    else if($_GET['type'] == "sponsor"){

        $importLegacy->importSponsors($array);
    }
    else if($_GET['type'] == "study"){

        reMapStudies($array);

        $importLegacy->importStudies($array);
    }
    else if($_GET['type'] == "countOrphanedFields"){

        reMapFields($array);

        $importLegacy->countOrphanedFields($array);

    }

    exit();
?>