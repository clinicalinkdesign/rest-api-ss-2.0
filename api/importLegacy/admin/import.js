/*
    Hope and pray that you can do this correctly? JS sucks for this and does not execute one line at a time.
*/

let fieldsData = null;
let vmData = null;
let triggerData = null;
let cfData = null;
let errorOnImport = false;
let environment = null;


const openFields = function(event) {
  let input = event.target;
  let reader = new FileReader();
  reader.onload = function() {
    fieldsData = reader.result;
    console.log("<<---- FIELD IMPORT DATA CSV ---->>");
    console.log(fieldsData);
  };
  reader.readAsText(input.files[0]);
  document.getElementById("fieldsOutput").innerHTML = "FIELD CSV Data Loaded Successfully";
  document.getElementById('fieldsOutput').style.color = "green";
  document.getElementById('fieldsOutput').style.textAlign = "center";
}

const openVM = function(event) {
  let inputVM = event.target;
  let readerVM = new FileReader();
  readerVM.onload = function() {
    vmData = readerVM.result;
    console.log("<<---- VISIT MATRIX IMPORT DATA CSV ---->>");
    console.log(vmData);
  };
  readerVM.readAsText(inputVM.files[0]);
  document.getElementById("visitMatrixOutput").innerHTML = "VISIT MATRIX CSV Data Loaded Successfully";
  document.getElementById('visitMatrixOutput').style.color = "green";
  document.getElementById('visitMatrixOutput').style.textAlign = "center";
}

const openTRIGGER = function(event) {
  let inputTRIGGER = event.target;
  let readerTRIGGER = new FileReader();
  readerTRIGGER.onload = function() {
    triggerData = readerTRIGGER.result;
    console.log("<<---- TRIGGER IMPORT DATA CSV ---->>");
    console.log(triggerData);
  };
  readerTRIGGER.readAsText(inputTRIGGER.files[0]);
  document.getElementById("triggersOutput").innerHTML = "TRIGGER CSV Data Loaded Successfully";
  document.getElementById('triggersOutput').style.color = "green";
  document.getElementById('triggersOutput').style.textAlign = "center";
}

const openCF = function(event) {
  let inputCF = event.target;
  let readerCF = new FileReader();
  readerCF.onload = function() {
    cfData = readerCF.result;
    console.log("<<---- CROSS FORM IMPORT DATA CSV ---->>");
    console.log(cfData);
  };
  readerCF.readAsText(inputCF.files[0]);
  document.getElementById("crossFormOutput").innerHTML = "CROSS FORM CSV Data Loaded Successfully";
  document.getElementById('crossFormOutput').style.color = "green";
  document.getElementById('crossFormOutput').style.textAlign = "center";
}

document.getElementById("executeImport").onclick = function()
{   
  if(document.getElementById("useServer").checked)
  {
    environment = "10.11.193.188";
  }
  else
  {
    environment = "localhost";
  }

  main("LIONS", "TIGERS", "BEARS", "OHMY");
}

async function importFields(input){
  // has more functions that do other stuff
  console.log(input)

  document.getElementById('fieldsOutput').style.color = "purple";
  document.getElementById("fieldsOutput").innerHTML = "FIELD import in progress....";
  
  const url = "https://"+environment+":443/SureStart_2/api/importlegacy/?type=fields";

  await fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: fieldsData,
  })
  .then((response) => response.json())
  //Then with the data from the response in JSON...
  .then((data) => {
    console.log('Success:', data);

    //Determine if there are individual errors
    const text = JSON.stringify(data);

    if(text.includes("import failure")) 
    {
      errorOnImport = true;

      throw "API Accepted Import but returned errors. See data above."
    }
    else
    {
      document.getElementById('fieldsOutput').style.color = "green";
      document.getElementById("fieldsOutput").innerHTML = "FIELD import complete, no errors";
    }
  })
  //Then with the error genereted...
  .catch((error) => {
    console.error('Error:', error);
    document.getElementById('fieldsOutput').style.color = "red";
    document.getElementById("fieldsOutput").innerHTML = "FIELD import error, see console log.";
    errorOnImport = true;
  });
}

async function importVM(input){
  // has more functions that do other stuff
  console.log(input);
  
  document.getElementById('visitMatrixOutput').style.color = "purple";
  document.getElementById("visitMatrixOutput").innerHTML = "VISIT MATRIX import in progress....";

  const url = "https://"+environment+":443/SureStart_2/api/importlegacy/?type=visitmatrix";

  await fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: vmData,
  })
  .then((response) => response.json())
  //Then with the data from the response in JSON...
  .then((data) => {
    console.log('Success:', data);

    //Determine if there are individual errors
    const text = JSON.stringify(data);

    if(text.includes("import failure")) 
    {
      errorOnImport = true;

      throw "API Accepted Import but returned errors. See data above."
    }
    else if(text.includes("ERROR WITH VISIT MATRIX INPUT"))
    {
      errorOnImport = true;

      throw "API could not accept import. It is likely you have objects without an order number."
    }
    else
    {
      document.getElementById('visitMatrixOutput').style.color = "green";
      document.getElementById("visitMatrixOutput").innerHTML = "VISIT MATRIX import complete, no errors";
    }
  })
  //Then with the error genereted...
  .catch((error) => {
    console.error('Error:', error);
    document.getElementById('visitMatrixOutput').style.color = "red";
    document.getElementById("visitMatrixOutput").innerHTML = "VISIT MATRIX import error, see console log.";
    errorOnImport = true;
  });
}

async function importTrigger(input){
  // this code should only be executed after both task 1 and 2 finish
  console.log(input);
  
  document.getElementById('triggersOutput').style.color = "purple";
  document.getElementById("triggersOutput").innerHTML = "TRIGGER import in progress....";

  const url = "https://"+environment+":443/SureStart_2/api/importlegacy/?type=trigger&ciStudyProjectCode="+document.getElementById("intstudyid").value;

  await fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: triggerData,
  })
  .then((response) => response.json())
  //Then with the data from the response in JSON...
  .then((data) => {
    console.log('Success:', data);

    //Determine if there are individual errors
    const text = JSON.stringify(data);

    if(text.includes("import failure")) 
    {
      errorOnImport = true;

      throw "API Accepted Import but returned errors. See data above."
    }
    else
    {
      document.getElementById('triggersOutput').style.color = "green";
      document.getElementById("triggersOutput").innerHTML = "TRIGGER import complete, no errors";
    }
  })
  //Then with the error genereted...
  .catch((error) => {
    console.error('Error:', error);
    document.getElementById('triggersOutput').style.color = "red";
    document.getElementById("triggersOutput").innerHTML = "TRIGGER import error, see console log.";
    errorOnImport = true;
  });
}

async function importCrossForm(input){
  // this code should only be executed after both task 1 and 2 finish
  console.log(input);
  
  document.getElementById('crossFormOutput').style.color = "purple";
  document.getElementById("crossFormOutput").innerHTML = "CROSS FORM import in progress....";

  const url = "https://"+environment+":443/SureStart_2/api/importlegacy/?type=crossform&ciStudyProjectCode="+document.getElementById("intstudyid").value;

  await fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: cfData,
  })
  .then((response) => response.json())
  //Then with the data from the response in JSON...
  .then((data) => {
    console.log('Success:', data);

    //Determine if there are individual errors
    const text = JSON.stringify(data);

    if(text.includes("import failure")) 
    {
      errorOnImport = true;

      throw "API Accepted Import but returned errors. See data above."
    }
    else
    {
      document.getElementById('crossFormOutput').style.color = "green";
      document.getElementById("crossFormOutput").innerHTML = "CROSS FORM import complete, no errors";

      document.getElementById("hiddenstuff").style.display = "none";
    }
  })
  //Then with the error genereted...
  .catch((error) => {
    console.error('Error:', error);
    document.getElementById('crossFormOutput').style.color = "red";
    document.getElementById("crossFormOutput").innerHTML = "CROSS FORM import error, see console log.";
    errorOnImport = true;
  });
}

async function main(fields, vm, triggers, crossform){
  await Promise.all(
      [importFields(fields)]
  )
  
  if(!errorOnImport) 
  {
    await Promise.all(
        [importVM(vm)]
    )
  }

  if(!errorOnImport) 
  {
    await Promise.all(
        [importTrigger(triggers)]
    )
  }

  if(!errorOnImport) 
  {
    await Promise.all(
        [importCrossForm(crossform)]
    )
  }
}