/*
    Check and be sure that all inputs are good
    then enabled a button to handle the execution of import queries
*/
function init() {
    document.getElementById("FIELDS").reset;
    document.getElementById("VISITMATRIX").reset;
    document.getElementById("TRIGGERS").reset;
    document.getElementById("CROSSFORM").reset;
}
  
window.onload = init;
window.onbeforeunload = init;

function ClearForm(){
    document.reset();
}

let idGood = false;
let fieldsGood = false;
let vmGood = false;
let triggersGood = false;
let crossFormGood = false;

document.getElementById("checkInputs").onclick = function()
{
    inputsWork();
}

async function CheckForStudyid()
{
        //console.log("1");
        //1. Check that a valid internal study ID has been provided
        //2. Check that all four CSVs have been attached and are the expected format / structure

        const studyid = document.getElementById("intstudyid").value;

        // -- 1 --
        if(studyid != "") {
            document.getElementById('idOutput').innerHTML = "";

            const url = 'https://localhost:443/SureStart_2/api/study/?ciStudyProjectCode='+studyid;
            try
            {
                const response = await fetch(url);

                const theData = await response.json();

                if(theData[0].ciStudyProjectCode === studyid) {
                    document.getElementById('idOutput').innerHTML = "STUDY ID IS GOOD";
                    document.getElementById('idOutput').style.color = "green";
                    document.getElementById('idOutput').style.textAlign = "center";
                    idGood = true;
                }
            }
            catch(error)
            {
                document.getElementById('idOutput').innerHTML = "STUDY DOES NOT EXIST";
                document.getElementById('idOutput').style.color = "red";
                document.getElementById('idOutput').style.textAlign = "center";
            }
        }
        else {
            document.getElementById('idOutput').innerHTML = "ERROR!!! PLEASE PROVIDE STUDY ID";
            document.getElementById('idOutput').style.color = "red";
            document.getElementById('idOutput').style.textAlign = "center";
        }

        
}

async function fieldsCheck()
{
    //console.log("2");

    let file = document.getElementById("FIELDS");
    
    if(file.value != "" && fieldsData != null) 
    {
        // you have a file
        //console.log("FIELDS AVAILABLE!");
        document.getElementById('fieldsOutput').innerHTML = "Ready to import";
        document.getElementById('idOutput').style.color = "green";
        fieldsGood = true;
    }
    else
    {
        document.getElementById('fieldsOutput').innerHTML = "ERROR!!! PLEASE SELECT A FILE";
        document.getElementById('fieldsOutput').style.color = "red";
        document.getElementById('fieldsOutput').style.textAlign = "center";
    }
}

async function visitMatrixCheck()
{
    //console.log("3");

    let file = document.getElementById("VISITMATRIX");
    
    if(file.value != "" && vmData != null) 
    {
        // you have a file
        //console.log("VISIT MATRRIX AVAILABLE!");
        document.getElementById('visitMatrixOutput').innerHTML = "Ready to import";
        document.getElementById('idOutput').style.color = "green";
        vmGood = true;
    }
    else
    {
        document.getElementById('visitMatrixOutput').innerHTML = "ERROR!!! PLEASE SELECT A FILE";
        document.getElementById('visitMatrixOutput').style.color = "red";
        document.getElementById('visitMatrixOutput').style.textAlign = "center";
    }
}

async function triggerCheck()
{
    //console.log("4");

    let file = document.getElementById("TRIGGERS");
    
    if(file.value != "" && triggerData != null) 
    {
        // you have a file
        //console.log("TRIGGERS AVAILABLE!");
        document.getElementById('triggersOutput').innerHTML = "Ready to import";
        document.getElementById('idOutput').style.color = "green";
        triggersGood = true;
    }
    else
    {
        document.getElementById('triggersOutput').innerHTML = "ERROR!!! PLEASE SELECT A FILE";
        document.getElementById('triggersOutput').style.color = "red";
        document.getElementById('triggersOutput').style.textAlign = "center";
    }
}

async function crossformCheck()
{
    //console.log("5");

    let file = document.getElementById("CROSSFORM");
    
    if(file.value != "" && cfData != null) 
    {
        // you have a file
        //console.log("CROSS FORM AVAILABLE!");
        document.getElementById('crossFormOutput').innerHTML = "Ready to import";
        document.getElementById('idOutput').style.color = "green";
        crossFormGood = true;
    }
    else
    {
        document.getElementById('crossFormOutput').innerHTML = "ERROR!!! PLEASE SELECT A FILE";
        document.getElementById('crossFormOutput').style.color = "red";
        document.getElementById('crossFormOutput').style.textAlign = "center";
    }
}

function finishingUp()
{
    if(idGood && fieldsGood && vmGood && triggersGood /*&& crossFormGood*/) 
    {
        //console.log("Button should pop up");
        //Show the button
        document.getElementById("hiddenstuff").style.display = "block";

        console.log("Inputs successfully verified.");
    }
    else 
    {
        //console.log("ID      "+idGood);
        //console.log("FIELDS  "+fieldsGood);
        //console.log("VM      "+vmGood);
        //console.log("TRIGGER "+triggersGood);
        //console.log("CF      "+crossFormGood);
    }
}

async function inputsWork()
{
    //console.log("MAIN");

    await Promise.all(
        [CheckForStudyid(),
            fieldsCheck(),
            visitMatrixCheck(),
            triggerCheck(),
            crossformCheck()]
    )

    finishingUp();
 }