<?php
    if(!defined('MyConst')) {
        die('Direct access not permitted');
    }
       
    if(array_key_exists('data', $data2) && sizeof($data2['data']) > 0)
    {
        //$editqueueMany = array();
        $returnArray = array();

        for($i = 0; $i < sizeof($data2['data']); $i++)
        {
            $editqueue = new editqueue($db);

            array_push($returnArray, $editqueue->lock($data2['data'][$i]));
        }

        //set response code - 200 Created
        http_response_code(201);

        echo json_encode($returnArray);
    }
    else{
        //Tell the user the data is incomplete
        //set response code - 400 bad request
        http_response_code(400);
        
        //tell the user
        echo json_encode(array("message" => "Missing Required Parameters"));
    }
    exit();

    /*
    EXAMPLE OF EXPECTED INPUT:
    //Table names MUST match exactly what the database table is called, this should match the /endpoint url value
    {
        "data" : [
            {
                "tableName": "Table Name #1",
                "rowid": ""
            }
        ]
    }       
    */
?>
