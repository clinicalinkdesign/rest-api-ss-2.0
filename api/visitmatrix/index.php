<?php
    //This serves as the visit matrix controller
    //Meant to pull the entire visit matrix for a given study.
    //If a study is not provied, an error message is provided.

    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: GET,POST,PUT,DELETE");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    $requestMethod = $_SERVER["REQUEST_METHOD"];

    if ($requestMethod === 'OPTIONS') {
        header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
        header('Access-Control-Allow-Headers: Origin, Accept, Content-Type, X-Requested-With');
    }

    define('MyConst', TRUE);

    // include database and object files
    include_once '../../config/database.php';
    include_once '../../functions/myFunctions.php';
    include_once '../../config/errorLogs/errorLogs.php';
    
    include_once '../../model/visitmatrix.php';

    $database = new Database();
    $db = $database->connect();

    //API Auth Time
    require_once '../../config/authExecute.php';

    //USER MUST BE ADMIN TYPE
    if(!isAdmin($_SERVER['PHP_AUTH_USER'], $db)) {
        //set response code - 401 not authorized
        http_response_code(401);
    
        //tell the user no quotes found
        echo json_encode(array("message" => "Access denied."));

        exit();
    }

    $id = null;

    if ($requestMethod === 'GET') {
        $id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
    }

    // Parameters sent with POST, PUT, and DELETE methods
    $data = json_decode(file_get_contents("php://input"));
    if (!empty($data->id) && $requestMethod !== 'GET') { $id = $data->id; }
    
    //New array of decoded JSON data
    $data2 = json_decode(file_get_contents("php://input"),true);

    if($requestMethod == "GET"){
        include_once("read.php");
    }
    else if($requestMethod != "OPTIONS"){
        //set response code - 404 not found
        http_response_code(404);

        //tell the user no quotes found
        echo json_encode(array("message" => "Request type <".$requestMethod."> not found."));
    }
    else {
        //set response code - 404 not found
        http_response_code(404);

        //tell the user no quotes found
        echo json_encode(array("message" => "Request type <".$requestMethod."> not found."));
    }
?>