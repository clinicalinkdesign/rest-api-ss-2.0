<?php

    if(!defined('MyConst')) {
        die('Direct access not permitted');
    }

    if(array_key_exists('data', $data2) && sizeof($data2['data']) > 0)
    {
        $returnArray = array();
        $copied = array();

        for($i = 0; $i < sizeof($data2['data']); $i++)
        {
            $sectiontempformlink = new sectiontempformlink($db);

            array_push($returnArray, $sectiontempformlink->create($data2['data'][$i]));

            if($returnArray[$i]['success'] == "true")
            {
                array_push($idList, $sectiontempformlink->id);

                if (array_key_exists('copiedFromid', $data2['data'][$i])) {
                    array_push($copied, $data2['data'][$i]['copiedFromid']);
                }
                else {
                    array_push($copied, null);
                }
            }
        }

        //set response code - 200 Created
        http_response_code(201);

        echo json_encode($returnArray);

        //Call the list of ID's to send to the history table after creation
        for($i = 0; $i < sizeof($idList); $i++){

            if(array_key_exists('action', $data2)){
                $requestMethod = strtoupper($data2['action']);
            }

            $history->write($requestMethod, $idList[$i], "sectiontempformlink", $globalUser, $copied[$i]);
        }
    }
    else{
        //Tell the user the data is incomplete
        //set response code - 400 bad request
        http_response_code(400);
        
        //tell the user
        echo json_encode(array("message" => "Missing Required Parameters"));
    }
    exit();

    /*
    EXAMPLE OF EXPECTED INPUT: (ORDER MUST BE AN INTEGER)
    {
        "data" : [
            {
                "orderInForm ": "1",
                "sectionTemplateid ": "",
                "formTemplateid ": ""
            },
            {
                "orderInForm ": "2",
                "sectionTemplateid ": "",
                "formTemplateid ": ""
            }
        ]
    }       
    */
?>
