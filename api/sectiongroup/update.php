<?php

    if(!defined('MyConst')) {
        die('Direct access not permitted');
    }

    if(array_key_exists('data', $data2) && sizeof($data2['data']) > 0)
    {
        $returnArray = array();

        for($i = 0; $i < sizeof($data2['data']); $i++)
        {
            $sectiongroup = new sectiongroup($db);

            array_push($returnArray, $sectiongroup->update($data2['data'][$i]));

            if($returnArray[$i]['success'] == "true")
            {
                array_push($idList, $sectiongroup->id);
            }
        }

        //set response code - 200 OK
        http_response_code(200);
        
        echo json_encode($returnArray);

        //Call the list of ID's to send to the history table after creation
        for($i = 0; $i < sizeof($idList); $i++){

            if(array_key_exists('action', $data2)){
                $requestMethod = strtoupper($data2['action']);
            }

            $history->write($requestMethod, $idList[$i], "sectiongroup", $globalUser);
        }
    }
    else{
        //Tell the user the data is incomplete
        //set response code - 400 bad request
        http_response_code(400);
        
        //tell the user
        echo json_encode(array("message" => "Missing Required Parameters"));
    }
    exit();

    /*
    EXAMPLE OF EXPECTED INPUT:
    {
        "data" : [
            {
                "id": "uuid1",
                "name": "Name #1",
                "studyid": "",
                "sectionGroupPrefix": "1",
                "isRemovedFlag": ""
            }
        ]
    }       
    */

?>