<?php
    if(!defined('MyConst')) {
        die('Direct access not permitted');
    }
       
    if(array_key_exists('data', $data2) && sizeof($data2['data']) > 0)
    {
        $returnArray = array();

        for($i = 0; $i < sizeof($data2['data']); $i++)
        {
            $fieldversion = new fieldversion($db);

            array_push($returnArray, $fieldversion->create($data2['data'][$i]));

            if($returnArray[$i]['success'] == "true")
            {
                array_push($idList, $fieldversion->id);
            }
        }

        //set response code - 200 Created
        http_response_code(201);

        echo json_encode($returnArray);

        //Call the list of ID's to send to the history table after creation
        //for($i = 0; $i < sizeof($idList); $i++){
        //
        //    $history->write($requestMethod, $idList[$i], "fieldversion", $globalUser);
        //}
    }
    else{
        //Tell the user the data is incomplete
        //set response code - 400 bad request
        http_response_code(400);
        
        //tell the user
        echo json_encode(array("message" => "Missing Required Parameters"));
    }
    exit();

    /*
    EXAMPLE OF EXPECTED INPUT: (ORDER MUST BE AN INTEGER)
    {
        "data" : [
            {   
                "fieldid": "", 
                "ciFieldName": "", 
                "ciFieldType": "", 
                "createdByUserid": "", 
                "lastModifiedByUserid": "",
                "ciFriendlyName": "",  <- and below are OPTIONAL ON CREATE
                "ciMappingName": "", 
                "ciLabelText": "", 
                "ciValues": "", 
                "ciRange": "", 
                "ciLogic": "", 
                "isRequiredFlag": "", 
                "ciValidation": "", 
                "hasToolTipFlag": "", 
                "ciValidationMessage: "", 
                "cidesignInstructions": "", 
                "cidMInstructions": "", 
                "ciCustomField1": "", 
                "reqFail": ""
            }
        ]
    }       
    */
?>
