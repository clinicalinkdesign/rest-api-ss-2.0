<?php
    if(!defined('MyConst')) {
        die('Direct access not permitted');
    }
       
    http_response_code(400);
       
    //tell the user
    echo json_encode(array("message" => "Update is disabled for this endpoint. All changes are to be treated as a new row."));
    
    exit();

?>