<?php
    if(!defined('MyConst')) {
        die('Direct access not permitted');
    }
       
    try{
        $mappingdata = new mappingdata($db);
        
        if(isset($_GET["intstudyid"])){

            $output = $mappingdata->read($_GET["intstudyid"]);
            
            if($output != null) {
                //set response code - 200 OK
                http_response_code(200);
                echo json_encode($output);
            }
            else {
                //set response code - 404 not found
                http_response_code(404);

                //tell the user no mappingdata found
                echo json_encode(array("message" => "Mapping data search returned no results."));
            }
        }
        else {
            //set response code - 400 bad request
            http_response_code(400);
            echo json_encode(array('message' => "Internal study id not provided"));
        }
    } catch (Exception $e) {
        //set response code - 400 bad request
        http_response_code(400);
        echo json_encode(array('message' => $e->getMessage()));
    }
    exit();

?>