<?php

    if(!defined('MyConst')) {
        die('Direct access not permitted');
    }

    try{
        $sectiondata = new sectiondata($db);
        
        if(isset($_GET["studyid"])){

            if(isset($_GET["includehistory"])){ //This should only be true or false
                $output = $sectiondata->read($_GET["studyid"], $_GET["includehistory"]);
            }
            else{
                $output = $sectiondata->read($_GET["studyid"]);
            }
            
            if($output != null) {
                //set response code - 200 OK
                http_response_code(200);
                echo json_encode($output);
            }
            else {
                //set response code - 404 not found
                http_response_code(404);

                //tell the user no sectiondata found
                echo json_encode(array("message" => "Section data search returned no results."));
            }
        }
        else {
            //set response code - 400 bad request
            http_response_code(400);
            echo json_encode(array('message' => "study id not provided"));
        }
    } catch (Exception $e) {
        //set response code - 400 bad request
        http_response_code(400);
        echo json_encode(array('message' => $e->getMessage(), "trace" => $e->getTraceAsString()));
    }
    exit();

?>