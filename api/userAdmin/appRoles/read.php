<?php

    if(!defined('MyConst')) {
        die('Direct access not permitted');
    }

    try{
        $approle = new approles($db);
        
        $output = $approle->read(readWhereClause($db));

        if($output != null) {
            //set response code - 200 OK
            http_response_code(200);
            echo json_encode($output);
        }
        else {
            //set response code - 404 not found
            http_response_code(404);

            //tell the user no appuser found
            echo json_encode(array("message" => "approle not found"));
        }
    } catch (Exception $e) {
        //set response code - 400 bad request
        http_response_code(400);
        echo json_encode(array('message' => $e->getMessage()));
    }
    exit();

?>