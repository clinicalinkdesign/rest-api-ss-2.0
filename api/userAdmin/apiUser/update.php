<?php 

if(!defined('MyConst')) {
    die('Direct access not permitted');
}

if(array_key_exists('data', $data2) && sizeof($data2['data']) > 0)
{
    $returnArray = array();
    $returnItem = array();

    for($i = 0; $i < sizeof($data2['data']); $i++)
    {
        $apiUsers = new apiUsers($db);
            
        $returnItem = $apiUsers->update($data2['data'][$i]);

        if($returnItem != null && $returnItem['success'] == "true") {
            if(array_key_exists('passwordHash', $returnItem['data'])) unset($returnItem['data']['passwordHash']);
            if(array_key_exists('isRemovedFlag', $returnItem['data'])) unset($returnItem['data']['isRemovedFlag']);
            if(array_key_exists('hasAccessFlag', $returnItem['data'])) unset($returnItem['data']['hasAccessFlag']);
            if(array_key_exists('isAdminFlag', $returnItem['data'])) unset($returnItem['data']['isAdminFlag']);
            if(array_key_exists('dataServicesFlag', $returnItem['data'])) unset($returnItem['data']['dataServicesFlag']);
        }

        array_push($returnArray, $returnItem);
    }

    //set response code - 200 OK
    http_response_code(200);
    
    echo json_encode($returnArray);
}
else{
    //Tell the user the data is incomplete
    //set response code - 400 bad request
    http_response_code(400);
    
    //tell the user
    echo json_encode(array("message" => "Missing Required Parameters"));
}
exit();

    /*
    EXAMPLE OF EXPECTED INPUT:
    {
        "data" : [
            {
                "id": "uuid1",
                "userName" : "",
                "password" : "",
                "isRemovedFlag" : "",
                "hasAccessFlag" : "",
                "isAdminFlag" : ""
            }
        ]
    }   
    */
?>