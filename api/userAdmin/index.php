<?php

    //This acts as a file to house the global header included in ALL index.php files on all end points

    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: GET,POST,PUT,DELETE");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    $requestMethod = $_SERVER["REQUEST_METHOD"];

    if ($requestMethod === 'OPTIONS') {
        header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
        header('Access-Control-Allow-Headers: Origin, Accept, Content-Type, X-Requested-With');
    }

    define('MyConst', TRUE);
    
    // include database and object files
    include_once '../../config/database.php';
    include_once '../../functions/myFunctions.php';
    include_once '../../config/errorLogs/errorLogs.php';

    $database = new Database();
    $db = $database->connect();
    
    // Parameters sent with POST, PUT, and DELETE methods
    $data = json_decode(file_get_contents("php://input"));
    if ($requestMethod !== 'GET') { 

        if(!is_array($data->data)) {
            //set response code - 404 not found
            http_response_code(400);

            //tell the user no quotes found
            echo json_encode(array("message" => "data is not an array"));

            exit();
        }
    }

    // Parameters sent with POST, PUT, and DELETE methods
    $data2 = json_decode(file_get_contents("php://input"),true);

    //include_once 'register.php';

    // 0. Authorize login and only allow admins to use this endpoint
    // 1. Update to take in paramater of apiUsers OR appUsers
    // 2. Create instance of correct class
    // 3. Follow the CRUD method per usual

    require_once '../../config/authExecute.php';

    // Parameters sent with POST, PUT, and DELETE methods
    $data = json_decode(file_get_contents("php://input"));
    if ($requestMethod !== 'GET') { 

        if(!is_array($data->data)) {
            //set response code - 404 not found
            http_response_code(400);

            //tell the user no quotes found
            echo json_encode(array("message" => "data is not an array"));

            exit();
        }
    }

    //USER MUST BE ADMIN TYPE
    if(!isAdmin($_SERVER['PHP_AUTH_USER'], $db)) {
        //set response code - 401 not authorized
        http_response_code(401);
    
        //tell the user no quotes found
        echo json_encode(array("message" => "Access denied."));

        exit();
    }

    if(isset($_GET["userType"])) {
        //Go to apiUsers
        if($_GET["userType"] == "api") {

            include_once '../../model/apiUsers.php';

            if($requestMethod == "POST") {
            
                require_once("apiUser/create.php");
            }
            else if($requestMethod == "PUT") {
                require_once("apiUser/update.php");
            }
            else if($requestMethod != "OPTIONS") {
                //set response code - 404 not found
                http_response_code(404);
    
                //tell the user no quotes found
                echo json_encode(array("message" => "Request type <".$requestMethod."> not found."));
            }
        }
        //Go to appusers
        else if($_GET["userType"] == "app") {

            include_once '../../model/appusers.php';

            if($requestMethod == "GET"){
                require_once("appUser/read.php");
            }
            else if($requestMethod == "POST") {
                require_once("appUser/create.php");
            }
            else if($requestMethod == "PUT") {
                require_once("appUser/update.php");
            }
            else if($requestMethod == "DELETE") {
                require_once("appUser/delete.php");
            }
            else if($requestMethod != "OPTIONS") {
                //set response code - 404 not found
                http_response_code(404);
    
                //tell the user no quotes found
                echo json_encode(array("message" => "Request type <".$requestMethod."> not found."));
            }
        }
        else if($_GET["userType"] == "roles") {

            include_once '../../model/approles.php';

            if($requestMethod == "GET"){
                require_once("appRoles/read.php");
            }
            else if($requestMethod != "OPTIONS") {
                //set response code - 404 not found
                http_response_code(404);
    
                //tell the user no quotes found
                echo json_encode(array("message" => "Request type <".$requestMethod."> not found."));
            }
        }
        else {
            //set response code - 400 bad request
            http_response_code(400);
            echo json_encode(array('message' => "Not allowed"));
        }
    }
    else {
        //set response code - 400 bad request
        http_response_code(400);
        echo json_encode(array('message' => "Not allowed"));
    }