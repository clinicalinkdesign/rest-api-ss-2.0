<?php 

if(!defined('MyConst')) {
    die('Direct access not permitted');
}

class importCUD{

    private $requireParam = array(
        "arm" => array("name", "studyid", "armOrder"),
        "cro" => array("name"),
        "crossformlinking" => array("studyid", "parentFormid", "parentFieldVerid", "childFormid", "childFieldVerid"),
        "editqueue" => array("tableName", "rowid"),
        "field" => array("sectionTemplateid", "ciFieldOrderNum"),
        "fieldversion" => array("fieldid", "versionNum", "createdByAppUserid", "lastModifiedByAppUserid"),
        "form" => array("name", "visitid", "orderInVisit", "formTemplateid"),
        "formtemplate" => array("name", "studyid"),
        "formtrigger" => array("studyid", "triggerSourceType", "sourceFormVisitLinkid", "triggerFormid"),
        "qualityfinding" => array("fieldid", "qTestid"),
        "sectiongroup" => array("name", "studyid" , "sectionGroupPrefix"),
        "sectiongrouplink" => array("sectionTemplateid", "sectionGroupid"),
        "sectiontempformlink" => array("orderInForm", "sectionTemplateid", "formTemplateid"),
        "sectiontemplate" => array("name"),
        "sponsor" => array("name"),
        "study" => array("ciStudyProjectCode", "protocolName", "sponsorid"),
        "users" => array("userName", "passwordHash", "email", "userRole"),
        "visit" => array("name", "armid", "visitOrder", "visitType"),
        "visittrigger" => array("studyid", "triggerSourceType", "sourceFormVisitLinkid", "triggerVisitid")
    );
    
    private $optionalParam = array(
        "arm" => array("id", "isRemovedFlag"),
        "cro" => array("id", "isRemovedFlag"),
        "crossformlinking" => array("id", "isRemovedFlag"),
        "editqueue" => array("id", "userid"),
        "field" => array("id", "isRemovedFlag", "libraryStatus", "isIntegration"),
        "fieldversion" => array("id", "ciFieldType", "ciFieldName", "ciFriendlyName", "ciMappingName", "ciLabelText", "ciValues", 
                                "ciRange", "ciLogic", "isRequiredFlag", "ciValidation", "hasToolTipFlag", 
                                "ciValidationMessage", "cidesignInstructions", "cidMInstructions", 
                                "ciCustomField1", "reqFail", "latestFlag"),
        "form" => array ("id", "isRemovedFlag"),
        "formTemplate" => array("id", "isRemovedFlag"),
        "formtrigger" => array("id", "sourceTriggerFieldid", "isRemovedFlag"),
        "qualityfinding" => array("id", "failDescription", "failStatus"),
        "sectiongroup" => array("id", "uniqueSectionGroupHeader", "isRemovedFlag"),
        "sectiongrouplink" => array("id", "isRemovedFlag"),
        "sectiontempformlink" => array("id", "isRemovedFlag"),
        "sectiontemplate" => array("id", "uniqueSectionHeader", "copiedFlag", "copiedUserid", "copiedFromStudyid", "copiedFromSectionGroupid", 
                                   "copiedFromSectionTemplateid", "templateModifiedFlag", "dateTimeCopied", "sectionGeneratorConfig", 
                                   "uniqueFieldPrefix", "isRemovedFlag", "libraryStatus", "salesforceRecordid",
                                   "hasCrossFormFlag", "hasTriggerFlag"),
        "sponsor" => array("id", "isRemovedFlag"),
        "study" => array("croid", "id", "studyType", "isLegacyFlag", "libraryFlag", "isRemovedFlag", "studyPhaseFlag"),
        "users" => array("id", "firstName", "lastName"),
        "visit" => array("id", "isRemovedFlag", "filemakerVisitNote"),
        "visittrigger" => array("id", "sourceTriggerFieldid", "isRemovedFlag")
    );

    function CreateGlobal($array, $conn, $table)
    {
        $currRet = array(
            "success" => "",
            "message" => "",
            "data" => null,
        );
        
        if(array_key_exists($table, $this->requireParam)){

            $queryPieces = $this->requiredParametersCreation($table, $array, $conn);

            if($queryPieces != null)
                try {
                    $query = 'INSERT INTO '.$table.'
                                ('.$queryPieces[0].')
                            VALUES
                                ('.$queryPieces[1].')';

                    //prepare query statement
                    $stmt = $conn->prepare($query);

                    //execute query
                    $stmt->execute();
            
                    $currRet['success'] = "true";
                    $currRet['message'] = "Successfully created ".$table;
                    $currRet['data'] = array("id" => $array['id']) + $array;

                    return $currRet;

                } catch (Exception $e) {

                    logQueryError($query, $e->getMessage());

                    $currRet['success'] = "import failure";
                    $currRet['message'] = $e->getMessage();

                    return $currRet;

                }
            else{

                if(array_key_exists($table, $this->optionalParam)) {
                    $errAdd = " and OPTIONAL inputs: ".implode(", ", $this->optionalParam[$table]);
                }
                else {
                    $errAdd = "";
                }

                $currRet['success'] = "import failure";
                $currRet['message'] = "Incorrect provided paramters! For ".$table." REQUIRED inputs: ".implode(", ", $this->requireParam[$table]).$errAdd;
                $currRet['data'] = $array;

                echo(json_encode($array));

                return $currRet;

            }
        }
        else{

            $currRet['success'] = "import failure";
            $currRet['message'] = "Invalid table name provided: ".$table;

            return $currRet;

        }
    }

    function requiredParametersCreation($table, $data, $conn) // <-- used for CREATE
    {
        //Search through predefined k,v pairs for given table
        //Check that data provided has all required inputs for given table
        //Additionally verifies that no additional OPTIONAL params are provided
        
        $output = array();
        $requiredCount = 0; // <-- counts number of required fields and ensures they are all present
        $args0 = array();
        $args1 = array();

        foreach ($data as $key => $value)
        {
            if($value != "") {
                if(in_array($key, $this->requireParam[$table])){
                    $requiredCount++;
                }
                else if(array_key_exists($table, $this->optionalParam)){
                    if(!in_array($key, $this->optionalParam[$table])){
                        continue;
                    }
                }

                array_push($args0, $key);

                if(strpos($key, 'id') > 0 && $key != "cidesignInstructions" && $key != "cidMInstructions" && $key != "ciValidation" && $key != "ciValidationMessage") {
                    array_push($args1, $conn->quote($value));
                }
                else {
                    $value = htmlspecialchars($value);
                    array_push($args1, $conn->quote($value));
                }
            }
        }

        if(sizeof($this->requireParam[$table]) == $requiredCount){

            array_push($output, implode(', ', $args0));
            array_push($output, implode(', ', $args1));

            return $output;
        }
        else {
            return null;
        }
    }

    function id_used($id, $table, $column, $conn)
    {
        $query = 'SELECT '.$column.' FROM '.$table.' WHERE '.$column.' = '.$conn->quote($id);

        //prepare query statement
        $stmt = $conn->prepare($query);

        //execute query
        $stmt->execute();

        if($stmt->rowCount() > 0){
            return true;
        }
        else{
            return false;
        }
    }
}