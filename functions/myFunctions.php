<?php 

if(!defined('MyConst')) {
    die('Direct access not permitted');
}

//Grabs a UUID from the server and returns it, this is used for creating
function get_UUID($conn)
{
    //Generate UUID and return to object
    $query = 'SELECT UUID()';
    $stmt = $conn->prepare($query);
    $stmt->execute();

    $row = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_LAST);

    return $row[0];
}

//Used for checking if an ID exists within a table before attempting to add
function id_Exists($id, $table, $conn)
{
    //$id should be an int
    //$table should only be either 'category' or 'sponsor', for example

    $query = 'SELECT id FROM '.$table.' WHERE id = '.$conn->quote($id);

    //prepare query statement
    $stmt = $conn->prepare($query);

    //execute query
    $stmt->execute();

    if($stmt->rowCount() > 0){
        return true;
    }
    else{
        return false;
    }
}

//Clean data for update
function updateQueryCompile($data, $conn)
{
    $set= array();
    foreach ($data as $key => $value)
    {
        if($key != "id"){
            $set[] = $key.' = '.$conn->quote(strip_tags($value));
        }
    }

    $output = implode(', ', $set);

    return $output;
}

function readWhereClause($conn, $table = null)
{   
    //$table is an optional paramter that only needs to be provided when using . notation in the query with JOIN statements
    //exclRmvFlag is provided when the API request wants to exclude results where isRemovedFlag is NOT "1"

    //This is utilized for allowing custom filters by API when they exist as part of a join and not the default table
    $lookUp = array(
        "sectiontemplate" => array("studyid", "sectionGroupLinkid", "sectionGroupid"),
        "exclRmvFlag" => array("editQueue", "fieldVersion", "qualityFinding", "users")
    ); 

    $where_args = array();
    foreach ($_GET as $key=>$val) {
        if ($val != "" && $key != "table" && $key != "userType" && $key != "test") {
            if(!in_array($table, $lookUp["exclRmvFlag"]) && $key == "exclRmvFlag" && $val == "true") {
                $where_args[] = 'isRemovedFlag <=> "0" OR isRemovedFlag <=> NULL OR isRemovedFlag <=> ""';
            }
            else if($table == null) {
                $where_args[] = $key.'="'.$val.'"';
            }
            else if(array_key_exists($table, $lookUp)) {
                if(!in_array($key, $lookUp[$table])) {
                    $where_args[] = $table.'.'.$key.'='.$conn->quote($val);
                }
                else {
                    $where_args[] = $key.'='.$conn->quote($val);
                }
            }
            else {
                $where_args[] = $table.'.'.$key.'='.$conn->quote($val);
            }
       }
     } 

     $output = implode(' OR ', $where_args);

     return $output;
}

function idTableLookUp($id, $table, $conn) 
{
    //This is written to pull any table based on its id]

    //Write and prepare query
    $query = 'SELECT *
            FROM '.$table.'
            WHERE id = '.$conn->quote($id);

    //prepare query statement
    $stmt = $conn->prepare($query);

    //execute query
    $stmt->execute();
    
    if($stmt->rowCount() > 0)
    {
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }
    else{
        return null;
    }
}

function checkInputType($input)
{
    if($input != null){

        if(isJson($input)){
    
            return;
        }
        else {
            http_response_code(400);
    
            echo json_encode(array("message" => "Provided input is not JSON format. Ensure data is also of expected type array."));

            exit();
        }
    }
    else {
        http_response_code(400);
    
        echo json_encode(array("message" => "No input provided."));

        exit();
    }
}

function isJson($string) 
{
    json_decode($string);
    return json_last_error() === JSON_ERROR_NONE;
}

function isAdmin($userName, $conn) {
    $query = 'SELECT * 
                FROM apiusers 
                WHERE userName = '.$conn->quote($userName).' AND isAdminFlag = "1"';

    //prepare query statement
    $stmt = $conn->prepare($query);

    //execute query
    $stmt->execute();
    $num = $stmt->rowCount();

    if($num >0) {
        return true;
    }
    else {
        return false;
    }
}

function isDataServices($userName, $conn) {
    $query = 'SELECT * 
                FROM apiusers 
                WHERE userName = '.$conn->quote($userName).' AND dataServicesFlag = "1"';

    //prepare query statement
    $stmt = $conn->prepare($query);

    //execute query
    $stmt->execute();
    $num = $stmt->rowCount();

    if($num >0) {
        return true;
    }
    else {
        return false;
    }
}