<?php 

if(!defined('MyConst')) {
    die('Direct access not permitted');
}

class globalCUD{

    private $requireParam = array(
        "apiusers" => array("userName"),
        "appusers" => array("name", "email"),
        "arm" => array("name", "studyid", "armOrder"),
        "cro" => array("name"),
        "crossformlinking" => array("studyid", "childFieldVerid"), //array("studyid", "parentFormid", "parentFieldVerid", "childFormid", "childFieldVerid"),
        "editqueue" => array("tableName", "rowid"),
        "field" => array("sectionTemplateid", "ciFieldOrderNum"),
        "fieldversion" => array("fieldid", "versionNum", "ciFieldName", "ciFieldType", "createdByAppUserid", "lastModifiedByAppUserid"),
        "form" => array("name", "visitid", "orderInVisit"),
        "formtemplate" => array("name", "studyid"),
        "formtrigger" => array("studyid", "triggerSourceType"), //, "sourceFormVisitLinkid", "triggerFormid"),
        "qualityfinding" => array("fieldid", "qTestid"),
        "sectiongroup" => array("name", "studyid" , "sectionGroupPrefix"),
        "sectiongrouplink" => array("sectionTemplateid", "sectionGroupid"),
        "sectiontempformlink" => array("orderInForm", "sectionTemplateid", "formTemplateid"),
        "sectiontemplate" => array("name"),
        "sponsor" => array("name"),
        "study" => array("ciStudyProjectCode", "protocolName", "sponsorid"),
        "users" => array("userName", "passwordHash", "email", "userRole"),
        "visit" => array("name", "armid", "visitOrder", "visitType"),
        "visittrigger" => array("studyid", "triggerSourceType") //, "sourceFormVisitLinkid", "triggerVisitid")
    );
    
    private $optionalParam = array(
        "apiuser" => array("passwordHash", "isRemovedFlag", "hasAccessFlag", "isAdminFlag", "dataServicesFlag"),
        "appusers" => array("isActiveFlag", "readOnlyFlag", "userRole"),
        "arm" => array("isRemovedFlag"),
        "cro" => array("isRemovedFlag"),
        "crossformlinking" => array("parentFormid", "parentFieldVerid", "childFormid", "isRemovedFlag"),
        "editqueue" => array("userid"),
        "field" => array("isRemovedFlag", "libraryStatus", "isIntegration"),
        "fieldversion" => array("ciFriendlyName", "ciMappingName", "ciLabelText", "ciValues", 
                                "ciRange", "ciLogic", "isRequiredFlag", "ciValidation", "hasToolTipFlag", 
                                "ciValidationMessage", "cidesignInstructions", "cidMInstructions", 
                                "ciCustomField1", "reqFail", "latestFlag"),
        "form" => array ("isRemovedFlag", "formTemplateid"),
        "formtemplate" => array("isRemovedFlag"),
        "formtrigger" => array("sourceTriggerFieldid", "sourceFormVisitLinkid", "triggerFormid", "isRemovedFlag"),
        "qualityfinding" => array("failDescription", "failStatus"),
        "sectiongroup" => array("uniqueSectionGroupHeader", "isRemovedFlag"),
        "sectiongrouplink" => array("isRemovedFlag"),
        "sectiontempformlink" => array("isRemovedFlag"),
        "sectiontemplate" => array("uniqueSectionHeader", "copiedFlag", "copiedUserid", "copiedFromStudyid", "copiedFromSectionGroupid", 
                                   "copiedFromSectionTemplateid", "templateModifiedFlag", "dateTimeCopied", "sectionGeneratorConfig", 
                                   "uniqueFieldPrefix", "isRemovedFlag", "libraryStatus", "salesforceRecordid",
                                   "hasCrossFormFlag", "hasTriggerFlag"),
        "sponsor" => array("isRemovedFlag"),
        "study" => array("croid", "studyType", "isLegacyFlag", "libraryFlag", "isRemovedFlag", "studyPhaseFlag"),
        "users" => array("firstName", "lastName"),
        "visit" => array("isRemovedFlag", "filemakerVisitNote"),
        "visittrigger" => array("sourceTriggerFieldid", "sourceFormVisitLinkid", "triggerVisitid", "isRemovedFlag")
    );

    function CreateGlobal($array, $conn, $table, &$id)
    {
        $currRet = array(
            "success" => "",
            "message" => "",
            "data" => null,
        );
        
        if(array_key_exists($table, $this->requireParam)){
            $id = get_UUid($conn);

            $queryPieces = $this->requiredParametersCreation($table, $array, $conn);

            if($queryPieces != null)
                try {
                    $query = 'INSERT INTO '.$table.'
                                (id, '.$queryPieces[0].')
                            VALUES
                                ('.$conn->quote($id).', '.$queryPieces[1].')';

                    //prepare query statement
                    $stmt = $conn->prepare($query);

                    //execute query
                    $stmt->execute();
            
                    $currRet['success'] = "true";
                    $currRet['message'] = "Successfully created ".$table;
                    $currRet['data'] = array("id" => $id) + $array;

                    return $currRet;

                } catch (Exception $e) {

                    logQueryError($query, $e->getMessage());

                    $currRet['success'] = "false";
                    $currRet['message'] = $e->getMessage();

                    return $currRet;

                }
            else{

                if(array_key_exists($table, $this->optionalParam)) {
                    $errAdd = " and OPTIONAL inputs: ".implode(", ", $this->optionalParam[$table]);
                }
                else {
                    $errAdd = "";
                }

                $currRet['success'] = "false";
                $currRet['message'] = "Incorrect provided paramters! For ".$table." REQUIRED inputs: ".implode(", ", $this->requireParam[$table]).$errAdd;

                return $currRet;

            }
        }
        else{

            $currRet['success'] = "false";
            $currRet['message'] = "Invalid table name provided: ".$table;

            return $currRet;

        }
    }

    function requiredParametersCreation($table, $data, $conn) // <-- used for CREATE
    {
        //Search through predefined k,v pairs for given table
        //Check that data provided has all required inputs for given table
        //Additionally verifies that no additional OPTIONAL params are provided
        
        $output = array();
        $optPar = true;
        $requiredCount = 0; // <-- counts number of required fields and ensures they are all present
        $args0 = array();
        $args1 = array();

        foreach ($data as $key => $value)
        {
            if(in_array($key, $this->requireParam[$table])){
                $requiredCount++;
            }
            else if(array_key_exists($table, $this->optionalParam)){            
                if ($key == 'copiedFromid') {
                    continue;
                }
                else if(!in_array($key, $this->optionalParam[$table])){
                    $optPar = false;
                    break;
                }
            }

            array_push($args0, $key);

            if(strpos($key, 'id') > 0 && $key != "cidesignInstructions" && $key != "cidMInstructions" && $key != "ciValidation" && $key != "ciValidationMessage") {
                array_push($args1, $conn->quote($value));
            }
            else {
                $value = htmlspecialchars($value);
                array_push($args1, $conn->quote($value));
            }

            //I can try to build the output string here too to reduce overall looping
        }

        if(sizeof($this->requireParam[$table]) == $requiredCount && $optPar){

            array_push($output, implode(', ', $args0));
            array_push($output, implode(', ', $args1));

            return $output;
        }
        else {
            return null;
        }
    }

    function DeleteGlobal($array, $conn, $table)
    {
        $currRet = array(
            "success" => "",
            "message" => "",
            "data" => null,
        );

        if(array_key_exists('id', $array))
        {
            $id = $array['id'];

            if(id_Exists($id, $table, $conn))
            {
                try {
                        
                    $query = 'DELETE FROM '.$table.' WHERE id = '.$conn->quote($id);

                    //prepare query statement
                    $stmt = $conn->prepare($query);

                    //execute query
                    $stmt->execute();

                    $currRet['success'] = "true";
                    $currRet['message'] = "Successfully deleted ".$table;
                    $currRet['data'] = array("id" => $id);

                    return $currRet;
                        
                } catch (Exception $e) {

                    logQueryError($query, $e->getMessage());

                    $currRet['success'] = "false";
                    $currRet['message'] = $e->getMessage();

                    return $currRet;
                }
            }
            else{

                $currRet['success'] = "false";
                $currRet['message'] = $table."id not found";

                return $currRet;
            }
        }
        else {

            $currRet['success'] = "false";
            $currRet['message'] = "Missing Required Parameters. An id field is required.";

            return $currRet;
        }
    }

    function UpdateGlobal($array, $conn, $table)
    {
        $currRet = array(
            "success" => "",
            "message" => "",
            "data" => null,
        );

        if(array_key_exists('id', $array))
        {
            $id = $array['id'];

            if(id_Exists($id, $table, $conn)) {

                try {
                    $query = 'UPDATE '.$table.' 
                                SET '.updateQueryCompile($array, $conn).'
                                WHERE id = '.$conn->quote($id);

                    //prepare query statement
                    $stmt = $conn->prepare($query);

                    //execute query
                    $stmt->execute();
                    
                    $currRet['success'] = "true";
                    $currRet['message'] = "Successfully updated ".$table;
                    $currRet['data'] = $array;

                    return $currRet;

                } catch (Exception $e) {

                    logQueryError($query, $e->getMessage());
                    
                    $currRet['success'] = "false";
                    $currRet['message'] = $e->getMessage();

                    return $currRet;
                }
            }
            else {

                $currRet['success'] = "false";
                $currRet['message'] = $table."id not found";

                return $currRet;

            }
        }
        else {

            $currRet['success'] = "false";
            $currRet['message'] = "Missing Required Parameters. An id field is required.";

            return $currRet;
        }
    }

    function id_used($id, $table, $column, $conn)
    {
        $query = 'SELECT '.$column.' FROM '.$table.' WHERE '.$column.' = '.$conn->quote($id);

        //prepare query statement
        $stmt = $conn->prepare($query);

        //execute query
        $stmt->execute();

        if($stmt->rowCount() > 0){
            return true;
        }
        else{
            return false;
        }
    }
}