<?php

    if(!defined('MyConst')) {
        die('Direct access not permitted');
    }

    function reMapCrossForm(&$array)
    {
        $renameMap = [
            '0' => 'StudyName', 
            '1' => 'Environment', 
            '2' => 'ChildArm', 
            '3' => 'ChildVisit', 
            '4' => 'ChildForm', 
            '5' => 'ChildField', 
            '6' => 'ChildFriendly', 
            '7' => 'ChildUtilityField', //What is this even used for??
            '8' => 'ParentArm', 
            '9' => 'ParentVisit', 
            '10' => 'ParentForm', 
            '11' => 'ParentField', 
            '12' => 'ParentFriendly'
        ];

        mappingHelper($array, $renameMap);
    }

    function reMapStudies(&$array) 
    {
        $renameMap = [
            '0' => 'name', 
            '1' => 'sponsor'
        ];

        mappingHelper($array, $renameMap);
    }

    function reMapTriggers (&$array)
    {
        $renameMap = [
            '0' => 'StudyName', 
            '1' => 'Environment',
            '2' => 'TriggerType',
            '3' => 'ParentArm',
            '4' => 'ParentVisit',
            '5' => 'TriggerForm',
            '6' => 'ChildArm',
            '7' => 'ChildVisit',
            '8' => 'ConditionalForm',
            '9' => 'Trigger'
        ];

        mappingHelper($array, $renameMap);
    }

    function reMapVisitMatrix (&$array)
    {
        $renameMap = [
            '0' => 'Section_Order', 
            '1' => 'Section_Name', 
            '2' => 'Section_Template', 
            '3' => 'Section_Template_ID', 
            '4' => 'Arm_Name', 
            '5' => 'Visit_Name', 
            '6' => 'Visit_Notes', 
            '7' => 'Visit_Order', 
            '8' => 'Form_Template_Name', 
            '9' => 'Form_Name', 
            '10' => 'Form_Note', 
            '11' => 'Form_Order', 
            '12' => 'Study_Name', 
            '13' => 'cStatus', 
            '14' => 'Notes'
        ];

        mappingHelper($array, $renameMap);
    }

    function reMapFields (&$array)
    {
        $renameMap = [
            '0' => 'CI_Field_Name',   
            '1' => 'Order',   
            '2' => 'css_formatted_error_message',   
            '3' => 'css_formatted_friendly_name',   
            '4' => 'css_formatted_label',   
            '5' => 'css_formatted_logic',   
            '6' => 'css_formatted_values',  
            '7' => 'Design_Instructions',   
            '8' => 'DM_Instructions',   
            '9' => 'Error_Message',   
            '10' => 'Field_Type',   
            '11' => 'Friendly_Name',   
            '12' => 'Label',   
            '13' => 'Latest_Flag',   
            '14' => 'Logic',   
            '15' => 'Range',   
            '16' => 'Removed_Flag',  
            '17' => "fileMakerFieldid",
            '18' => "fileMakerFieldidLink",
            '19' => 'Status',   
            '20' => 'Validation',   
            '21' => 'Values',   
            '22' => 'Version',
            '23' => 'Custom_Field_Value_1',   
            '24' => 'Custom_Field_Value_2',    
            '25' => 'Custom_Field_Value_3',
            '26' => 'Custom_Field_Value_4',
            '27' => 'Custom_Field_Value_5',
            '28' => 'Custom_Field_Value_6',
            '29' => 'Custom_Field_Value_7',
            '30' => 'Custom_Field_Value_8',
            '31' => 'Custom_Field_Value_9',
            '32' => 'studyName',   
            '33' => 'sectionGroupName',   
            '34' => 'cStatus',   
            '35' => 'gCustom_Field_1',   
            '36' => 'gCustom_Field_2',
            '37' => 'gCustom_Field_3',
            '38' => 'gCustom_Field_4',
            '39' => 'gCustom_Field_5',
            '40' => 'gCustom_Field_6',
            '41' => 'gCustom_Field_7',
            '42' => 'gCustom_Field_8',
            '43' => 'gCustom_Field_9',
            '44' => 'sectionTemplateName',   
            '45' => 'sectionTemplateNameUnique',   
            '46' => 'sectionTemplateNotes',   
            '47' => 'sectionTemplateOrder',   
            '48' => 'sectionTemplateFMid'
        ];

        mappingHelper($array, $renameMap);
    }

    function mappingHelper(&$array, $renameMap)
    {
        arrayCLeanUp($array);

        for($i = 0; $i < sizeof($array); $i++){
            if(!empty($array[$i])) {
                $array[$i] = array_combine(array_map(function($el) use ($renameMap) {
                    return $renameMap[$el];
                }, array_keys($array[$i])), array_values($array[$i]));
            }
        }
    }

    function arrayCleanUp(&$array)
    {
        //This is used for cleaning up CSV files that come with an empty line at the end
        //Removes the empty array slot and calls again on the same array have seen files with multiple lines
        if(empty(end($array)['0']) && !array_key_exists('1', end($array)))
        {
            array_pop($array);
            arrayCleanUp($array);
        }
    }