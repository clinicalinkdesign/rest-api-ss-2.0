<?php

    if(!defined('MyConst')) {
        die('Direct access not permitted');
    }

    //This acts as a file to house the global header included in ALL index.php files on all end points

    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: GET,POST,PUT,DELETE");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    $requestMethod = $_SERVER["REQUEST_METHOD"];

    if ($requestMethod === 'OPTIONS') {
        header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
        header('Access-Control-Allow-Headers: Origin, Accept, Content-Type, X-Requested-With');
    }

    // include database and object files
    include_once '../../config/database.php';
    include_once '../../functions/myFunctions.php';
    include_once '../../config/errorLogs/errorLogs.php';
    
    include_once '../../model/history.php';

    if($requestMethod != "GET") {
        checkInputType(file_get_contents("php://input"));
    }

    $database = new Database();
    $db = $database->connect();

    //API Auth Time
    require_once 'authExecute.php';

    //USER MUST BE ADMIN TYPE
    if(!isAdmin($_SERVER['PHP_AUTH_USER'], $db)) {
        //set response code - 401 not authorized
        http_response_code(401);
    
        //tell the user no quotes found
        echo json_encode(array("message" => "Access denied."));

        exit();
    }

    $history = new History($db);
    $idList = array();

    $id = null;

    if ($requestMethod === 'GET') {
        $id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
    }

    // Parameters sent with POST, PUT, and DELETE methods
    $data = json_decode(file_get_contents("php://input"));
    if ($requestMethod !== 'GET') { 

        if(!is_array($data->data)) {
            //set response code - 404 not found
            http_response_code(400);

            //tell the user no quotes found
            echo json_encode(array("message" => "data is not an array"));

            exit();
        }
    }
    
    //New array of decoded JSON data
    $data2 = json_decode(file_get_contents("php://input"),true);


    if($requestMethod == "GET"){
        include_once("read.php");
    }
    else{
        if(array_key_exists('appUserid', $data2)) {
            $globalUser = $data2['appUserid'];
        }
        else{
            $globalUser = null;
        }

        //Continue

        if($requestMethod == "POST"){
            
            require_once("create.php");
        }
        else if($requestMethod == "PUT"){
            include_once("update.php");
        }
        else if($requestMethod == "DELETE"){
            include_once("delete.php");
        }
        else if($requestMethod != "OPTIONS"){
            //set response code - 404 not found
            http_response_code(404);

            //tell the user no quotes found
            echo json_encode(array("message" => "Request type <".$requestMethod."> not found."));
        }
    }