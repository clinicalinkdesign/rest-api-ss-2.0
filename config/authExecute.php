<?php

if(!defined('MyConst')) {
    die('Direct access not permitted');
}

if (!isset($_SERVER['PHP_AUTH_USER'])) {
    //set response code - 401 not authorized
    http_response_code(401);

    //tell the user no quotes found
    echo json_encode(array("message" => "Access denied."));
    exit;
}

//Password and UserName provided?
// https://www.php.net/manual/en/features.http-auth.php

// First check if a username was provided.
if ($_SERVER['PHP_AUTH_USER'] === '') {
    //set response code - 401 not authorized
    http_response_code(401);

    //tell the user no quotes found
    echo json_encode(array("message" => "Access denied. Incorrect Login information provided."));
    exit;
}

// If we get here, username was provided. Check password.
if ($_SERVER['PHP_AUTH_PW'] != '') {
    include_once 'auth.php';

    //New instance of auth class to authenticate user access to API
    $auth = new Auth($db, $_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']);

    try{
        //Authenticate the user
        if($auth->authenticate()) {
            //continue on!
        }
        else {
            //set response code - 401 not authorized
            http_response_code(401);
        
            //tell the user no quotes found
            echo json_encode(array("message" => "Access denied. Incorrect Login information provided."));
            exit;
        }
    }
    catch(Exception $e) {
        //set response code - 401 not authorized
        http_response_code(401);
    
        //tell the user no quotes found
        echo json_encode(array("message" => "Execption in authentication. Request terminated."));
        exit;

    }

} else {
    //set response code - 401 not authorized
    http_response_code(401);

    //tell the user no quotes found
    echo json_encode(array("message" => "Access denied. Incorrect Login information provided."));
    exit;
}

