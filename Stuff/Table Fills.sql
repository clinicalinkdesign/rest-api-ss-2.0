-- INSERTing 6 rows sponsor table.  
INSERT Sponsor  
(id, name)  
VALUES  
 (uuid(), 'Novartis'),
 (uuid(), 'Johnson & Johson'),
 (uuid(), 'Eli-Lilly'),
 (uuid(), 'Parexel'),
 (uuid(), 'Pfizer'),
 (uuid(), 'Clinical Ink');  


-- INSERTing 6 rows CRO table.  
INSERT CRO  
(id, name)  
VALUES  
 (uuid(), 'Cogstate'),
 (uuid(), 'ERT'),
 (uuid(), 'Clinipace'),
 (uuid(), 'CRO Group'),
 (uuid(), 'Endpoint'),
 (uuid(), 'CI - Outcome Solutions');  


-- INSERTing 6 rows Study table.  
-- I need the FKs for Sponsor and CRO once they are created here...
INSERT Study  
(id, ciStudyProjectCode, protocolName, sponsorid, croid, libraryFlag)  
VALUES  
 (uuid(), 'CI0019','eLAS v4 Validation Project',(SELECT id from sponsor WHERE name='Clinical Ink'),(SELECT id from cro WHERE name='CI - Outcome Solutions'), NULL),
 (uuid(), 'CI0020','Parkinsons Awareness Study',(SELECT id from sponsor WHERE name='Novartis'),(SELECT id from cro WHERE name='CI - Outcome Solutions'),NULL),
 (uuid(), 'CI0021','CI eCOA Library',(SELECT id from sponsor WHERE name='Clinical Ink'),(SELECT id from cro WHERE name='ERT'),'1'),
 (uuid(), 'CI0022','Filemaker Project',(SELECT id from sponsor WHERE name='Clinical Ink'),(SELECT id from cro WHERE name='CI - Outcome Solutions'), NULL),  
 (uuid(), 'CI0023','abc Project',(SELECT id from sponsor WHERE name='Parexel'),(SELECT id from cro WHERE name='Clinipace'), NULL), 
 (uuid(), 'CI0024','abcd Project',(SELECT id from sponsor WHERE name='Eli-Lilly'),(SELECT id from cro WHERE name='ERT'), NULL);  


-- INSERTing 3 rows Arm table
INSERT Arm
(id, name, studyid)
VALUES
 (uuid(), 'Group 1', (SELECT id from study WHERE ciStudyProjectCode='CI0022')),
 (uuid(), 'Group 2', (SELECT id from study WHERE ciStudyProjectCode='CI0022')),
 (uuid(), 'Group 3', (SELECT id from study WHERE ciStudyProjectCode='CI0022'));

 
-- INSERTing 18 rows Visit table
INSERT Visit
 (id, name, armid, visitOrder)
VALUES
 (uuid(), 'Screening Visit', (SELECT id from Arm WHERE name='Group 1'), 1),
 (uuid(), 'Baseline Visit', (SELECT id from Arm WHERE name='Group 1'), 2),
 (uuid(), 'Visit 1', (SELECT id from Arm WHERE name='Group 1'), 3),
 (uuid(), 'Visit 2', (SELECT id from Arm WHERE name='Group 1'), 4),
 (uuid(), 'Visit 3', (SELECT id from Arm WHERE name='Group 1'), 5),
 (uuid(), 'EOT Visit', (SELECT id from Arm WHERE name='Group 1'), 6),
 (uuid(), 'Screening Visit Rollover', (SELECT id from Arm WHERE name='Group 2'), 1),
 (uuid(), 'Baseline Visit Rollover', (SELECT id from Arm WHERE name='Group 2'), 2),
 (uuid(), 'Visit 1 Rollover', (SELECT id from Arm WHERE name='Group 2'), 3),
 (uuid(), 'Visit 2 Rollover', (SELECT id from Arm WHERE name='Group 2'), 4),
 (uuid(), 'Visit 3 Rollover', (SELECT id from Arm WHERE name='Group 2'), 5),
 (uuid(), 'EOT Visit Rollover', (SELECT id from Arm WHERE name='Group 2'), 6),
 (uuid(), 'Screening Visit Group 3', (SELECT id from Arm WHERE name='Group 3'), 1),
 (uuid(), 'Baseline Visit Group 3', (SELECT id from Arm WHERE name='Group 3'), 2),
 (uuid(), 'Visit 1 Group 3', (SELECT id from Arm WHERE name='Group 3'), 3),
 (uuid(), 'Visit 2 Group 3', (SELECT id from Arm WHERE name='Group 3'), 4),
 (uuid(), 'Visit 3 Group 3', (SELECT id from Arm WHERE name='Group 3'), 5),
 (uuid(), 'EOT Visit Group 3', (SELECT id from Arm WHERE name='Group 3'), 6);


-- INSERTing 10 rows Form Template
INSERT formtemplate
 (id, name, studyid)
VALUES
 (uuid(), '01_Visit Info Screening', (SELECT id from Study WHERE ciStudyProjectCode='CI0022')),
 (uuid(), '02_Visit Info Baseline', (SELECT id from Study WHERE ciStudyProjectCode='CI0022')),
 (uuid(), '03_Visit Info Scheduled', (SELECT id from Study WHERE ciStudyProjectCode='CI0022')),
 (uuid(), '05_Visit Info Screening RO', (SELECT id from Study WHERE ciStudyProjectCode='CI0022')),
 (uuid(), '06_Visit Info EOT', (SELECT id from Study WHERE ciStudyProjectCode='CI0022')),
 (uuid(), '07_Visit Info Screening G3', (SELECT id from Study WHERE ciStudyProjectCode='CI0022')),
 (uuid(), '900_CGI', (SELECT id from Study WHERE ciStudyProjectCode='CI0022')),
 (uuid(), '901_CGIS', (SELECT id from Study WHERE ciStudyProjectCode='CI0022')),
 (uuid(), '902_PGIC', (SELECT id from Study WHERE ciStudyProjectCode='CI0022')),
 (uuid(), '903_PGIS', (SELECT id from Study WHERE ciStudyProjectCode='CI0022'));


-- INSERTing 24 rows Form
INSERT Form
 (id, name, visitid, orderInVisit, formTemplateid)
VALUES
 (uuid(), 'Screening Form', (SELECT id from Visit WHERE name='Screening Visit'), 1, (SELECT id from FormTemplate WHERE name='01_Visit Info Screening')),
 (uuid(), 'Baseline Form', (SELECT id from Visit WHERE name='Baseline Visit'), 1, (SELECT id from FormTemplate WHERE name='02_Visit Info Baseline')),
 (uuid(), 'Week 1 Form', (SELECT id from Visit WHERE name='Visit 1'), 1, (SELECT id from FormTemplate WHERE name='03_Visit Info Scheduled')),
 (uuid(), 'Week 2 Form', (SELECT id from Visit WHERE name='Visit 2'), 1, (SELECT id from FormTemplate WHERE name='03_Visit Info Scheduled')),
 (uuid(), 'Week 3 Form', (SELECT id from Visit WHERE name='Visit 3'), 1, (SELECT id from FormTemplate WHERE name='03_Visit Info Scheduled')),
 (uuid(), 'EOT Form', (SELECT id from Visit WHERE name='EOT Visit'), 1, (SELECT id from FormTemplate WHERE name='06_Visit Info EOT')),
 (uuid(), 'CGI', (SELECT id from Visit WHERE name='Baseline Visit'), 2, (SELECT id from FormTemplate WHERE name='900_CGI')),
 (uuid(), 'CGI', (SELECT id from Visit WHERE name='Visit 2'), 2, (SELECT id from FormTemplate WHERE name='900_CGI')), 
 (uuid(), 'Screening Form Rollover', (SELECT id from Visit WHERE name='Screening Visit Rollover'), 1, (SELECT id from FormTemplate WHERE name='05_Visit Info Screening RO')),
 (uuid(), 'Baseline Form Rollover', (SELECT id from Visit WHERE name='Baseline Visit Rollover'), 1, (SELECT id from FormTemplate WHERE name='02_Visit Info Baseline')),
 (uuid(), 'Week 4 Form', (SELECT id from Visit WHERE name='Visit 1 Rollover'), 1, (SELECT id from FormTemplate WHERE name='03_Visit Info Scheduled')),
 (uuid(), 'Week 5 Form', (SELECT id from Visit WHERE name='Visit 2 Rollover'), 1, (SELECT id from FormTemplate WHERE name='03_Visit Info Scheduled')),
 (uuid(), 'Week 6 Form', (SELECT id from Visit WHERE name='Visit 3 Rollover'), 1, (SELECT id from FormTemplate WHERE name='03_Visit Info Scheduled')),
 (uuid(), 'EOT Form Rollover', (SELECT id from Visit WHERE name='EOT Visit Rollover'), 1, (SELECT id from FormTemplate WHERE name='06_Visit Info EOT')),
 (uuid(), 'CGIS', (SELECT id from Visit WHERE name='Baseline Visit Rollover'), 2, (SELECT id from FormTemplate WHERE name='901_CGIS')),
 (uuid(), 'CGIS', (SELECT id from Visit WHERE name='Visit 2 Rollover'), 2, (SELECT id from FormTemplate WHERE name='901_CGIS')), 
 (uuid(), 'Screening Form Group 3', (SELECT id from Visit WHERE name='Screening Visit Group 3'), 1, (SELECT id from FormTemplate WHERE name='07_Visit Info Screening G3')),
 (uuid(), 'Baseline Form Group 3', (SELECT id from Visit WHERE name='Baseline Visit Group 3'), 1, (SELECT id from FormTemplate WHERE name='02_Visit Info Baseline')),
 (uuid(), 'Week 7 Form', (SELECT id from Visit WHERE name='Visit 1 Group 3'), 1, (SELECT id from FormTemplate WHERE name='03_Visit Info Scheduled')),
 (uuid(), 'Week 8 Form', (SELECT id from Visit WHERE name='Visit 2 Group 3'), 1, (SELECT id from FormTemplate WHERE name='03_Visit Info Scheduled')),
 (uuid(), 'Week 9 Form', (SELECT id from Visit WHERE name='Visit 3 Group 3'), 1, (SELECT id from FormTemplate WHERE name='03_Visit Info Scheduled')),
 (uuid(), 'EOT Form Group 3', (SELECT id from Visit WHERE name='EOT Visit Group 3'), 1, (SELECT id from FormTemplate WHERE name='06_Visit Info EOT')),
 (uuid(), 'PGI-C', (SELECT id from Visit WHERE name='Baseline Visit'), 2, (SELECT id from FormTemplate WHERE name='902_PGIC')),
 (uuid(), 'PGI-S', (SELECT id from Visit WHERE name='Baseline Visit'), 2, (SELECT id from FormTemplate WHERE name='903_PGIS'));
 
 
 
-- INSERTing 7 rows SectionGroup
INSERT SectionGroup
 (id, name, studyid, sectionGroupPrefix)
VALUES
 (uuid(), 'Visit Information', (SELECT id from Study WHERE ciStudyProjectCode='CI0022'), "VI"),
 (uuid(), 'Vital Signs', (SELECT id from Study WHERE ciStudyProjectCode='CI0022'), "VS"),
 (uuid(), 'Body Measurements', (SELECT id from Study WHERE ciStudyProjectCode='CI0022'), "BM"),
 (uuid(), 'Next Visit Info', (SELECT id from Study WHERE ciStudyProjectCode='CI0022'), "NVI"),
 (uuid(), 'General Notes', (SELECT id from Study WHERE ciStudyProjectCode='CI0022'), "GN"),
 (uuid(), 'PGI', (SELECT id from Study WHERE ciStudyProjectCode='CI0022'), "PGI"),
 (uuid(), 'CGI', (SELECT id from Study WHERE ciStudyProjectCode='CI0022'), "CGI");


-- INSERTing 6 rows USERS
INSERT Users
 (id, userName, passwordHash, firstName, lastName, email, userRole)
VALUES
 (uuid(), 'jSmith', 'abc123', 'John', 'Smith', 'jsmith@clinicalink.com', 'CDM'),
 (uuid(), 'jDoe', 'abc1234', 'Jane', 'Doe', 'jdoe@clinicalink.com', 'Programming'),
 (uuid(), 'hAbbott', 'abc123', 'Hannah', 'Abbott', 'habbott.com', 'CDM'),
 (uuid(), 'ePlant', 'abc1234', 'Eliza', 'Plant', 'eplant@clinicalink.com', 'Programming'),
 (uuid(), 'aTest', 'abc1234', 'Abby', 'Test', 'atest@clinicalink.com', 'Tester'),
 (uuid(), 'dGeorge', 'abc1234', 'Dan', 'George', 'dgeorge@clinicalink.com', 'DataServices');


-- INSERTing 14 rows SectionTemplate
INSERT SectionTemplate
 (id, name, sectionGroupid, copiedFlag, copiedUserid, copiedFromStudyid, copiedFromSectionGroupid, copiedFromSectionTemplateid, templateModifiedFlag, uniqueFieldPrefix)
VALUES
(uuid(), 'Visit Information Screening', (SELECT id from SectionGroup WHERE name ='Visit Information'), 0, NULL, NULL, NULL, NULL, 0, 'VI'),
(uuid(), 'Visit Information Baseline', (SELECT id from SectionGroup WHERE name ='Visit Information'), 0, NULL, NULL, NULL, NULL, 0, 'VI'),
(uuid(), 'Visit Information Scheduled', (SELECT id from SectionGroup WHERE name ='Visit Information'), 0, NULL, NULL, NULL, NULL, 0, 'VI'),
(uuid(), 'Visit Information EOT', (SELECT id from SectionGroup WHERE name ='Visit Information'), 0, NULL, NULL, NULL, NULL, 0, 'VI'),
(uuid(), 'Vital Signs', (SELECT id from SectionGroup WHERE name ='Vital Signs'), 0, NULL, NULL, NULL, NULL, 0, 'VS'),
(uuid(), 'Body Measurements', (SELECT id from SectionGroup WHERE name ='Body Measurements'), 0, NULL, NULL, NULL, NULL, 0, 'BM'),
(uuid(), 'Next Visit Info', (SELECT id from SectionGroup WHERE name ='Next Visit Info'), 0, NULL, NULL, NULL, NULL, 0, 'NV'),
(uuid(), 'General Notes', (SELECT id from SectionGroup WHERE name ='General Notes'), 0, NULL, NULL, NULL, NULL, 0, 'NV'),
(uuid(), 'CGI', (SELECT id from SectionGroup WHERE name ='CGI'), 0, NULL, NULL, NULL, NULL, 0, 'CGI'),
(uuid(), 'CGI-S', (SELECT id from SectionGroup WHERE name ='CGI'), 0, NULL, NULL, NULL, NULL, 0, 'CGI'),
(uuid(), 'PGI-S', (SELECT id from SectionGroup WHERE name ='PGI'), 0, NULL, NULL, NULL, NULL, 0, 'PGI'),
(uuid(), 'PGI-C', (SELECT id from SectionGroup WHERE name ='PGI'), 0, NULL, NULL, NULL, NULL, 0, 'PGI');

INSERT SectionTemplate
 (id, name, sectionGroupid, copiedFlag, copiedUserid, copiedFromStudyid, copiedFromSectionGroupid, copiedFromSectionTemplateid, templateModifiedFlag, uniqueFieldPrefix)
VALUES
(uuid(), 'Visit Information Screening Rollover', (SELECT id from SectionGroup WHERE name ='Visit Information'), 1, (SELECT id from Users WHERE userName='jSmith'), (SELECT id from Study WHERE ciStudyProjectCode='CI0022'), (SELECT id from SectionGroup WHERE name='Visit Information'), <UUID>, 0,'VI'),
(uuid(), 'Visit Information Screening Group 3', (SELECT id from SectionGroup WHERE name ='Visit Information'), 1, (SELECT id from Users WHERE userName='jSmith'), (SELECT id from Study WHERE ciStudyProjectCode='CI0022'), (SELECT id from SectionGroup WHERE name='Visit Information'), <UUID>, 0,'VI');


-- INSERTing 33 Rows SectionTempFormLink
INSERT SectionTempFormLink
 (id, orderInForm, sectionTemplateid, formTemplateid)
VALUES
 (uuid(), 1, (SELECT id from SectionTemplate WHERE name ='Visit Information Screening'), (SELECT id from FormTemplate WHERE name ='01_Visit Info Screening')),
 (uuid(), 2, (SELECT id from SectionTemplate WHERE name ='Body Measurements'), (SELECT id from FormTemplate WHERE name ='01_Visit Info Screening')),
 (uuid(), 3, (SELECT id from SectionTemplate WHERE name ='Vital Signs'), (SELECT id from FormTemplate WHERE name ='01_Visit Info Screening')),
 (uuid(), 4, (SELECT id from SectionTemplate WHERE name ='Next Visit Info'), (SELECT id from FormTemplate WHERE name ='01_Visit Info Screening')),
 (uuid(), 5, (SELECT id from SectionTemplate WHERE name ='General Notes'), (SELECT id from FormTemplate WHERE name ='01_Visit Info Screening')),
 (uuid(), 1, (SELECT id from SectionTemplate WHERE name ='Visit Information Screening Rollover'), (SELECT id from FormTemplate WHERE name ='05_Visit Info Screening RO')),
 (uuid(), 2, (SELECT id from SectionTemplate WHERE name ='Body Measurements'), (SELECT id from FormTemplate WHERE name ='05_Visit Info Screening RO')),
 (uuid(), 3, (SELECT id from SectionTemplate WHERE name ='Vital Signs'), (SELECT id from FormTemplate WHERE name ='05_Visit Info Screening RO')),
 (uuid(), 4, (SELECT id from SectionTemplate WHERE name ='Next Visit Info'), (SELECT id from FormTemplate WHERE name ='05_Visit Info Screening RO')),
 (uuid(), 5, (SELECT id from SectionTemplate WHERE name ='General Notes'), (SELECT id from FormTemplate WHERE name ='05_Visit Info Screening RO')),
 (uuid(), 1, (SELECT id from SectionTemplate WHERE name ='Visit Information Screening Group 3'), (SELECT id from FormTemplate WHERE name ='07_Visit Info Screening G3')),
 (uuid(), 2, (SELECT id from SectionTemplate WHERE name ='Body Measurements'), (SELECT id from FormTemplate WHERE name ='07_Visit Info Screening G3')),
 (uuid(), 3, (SELECT id from SectionTemplate WHERE name ='Vital Signs'), (SELECT id from FormTemplate WHERE name ='07_Visit Info Screening G3')),
 (uuid(), 4, (SELECT id from SectionTemplate WHERE name ='Next Visit Info'), (SELECT id from FormTemplate WHERE name ='07_Visit Info Screening G3')),
 (uuid(), 5, (SELECT id from SectionTemplate WHERE name ='General Notes'), (SELECT id from FormTemplate WHERE name ='07_Visit Info Screening G3')),
 (uuid(), 1, (SELECT id from SectionTemplate WHERE name ='Visit Information Baseline'), (SELECT id from FormTemplate WHERE name ='02_Visit Info Baseline')),
 (uuid(), 2, (SELECT id from SectionTemplate WHERE name ='Body Measurements'), (SELECT id from FormTemplate WHERE name ='02_Visit Info Baseline')),
 (uuid(), 3, (SELECT id from SectionTemplate WHERE name ='Vital Signs'), (SELECT id from FormTemplate WHERE name ='02_Visit Info Baseline')),
 (uuid(), 4, (SELECT id from SectionTemplate WHERE name ='Next Visit Info'), (SELECT id from FormTemplate WHERE name ='02_Visit Info Baseline')),
 (uuid(), 5, (SELECT id from SectionTemplate WHERE name ='General Notes'), (SELECT id from FormTemplate WHERE name ='02_Visit Info Baseline')),
 (uuid(), 1, (SELECT id from SectionTemplate WHERE name ='Visit Information Scheduled'), (SELECT id from FormTemplate WHERE name ='03_Visit Info Scheduled')),
 (uuid(), 2, (SELECT id from SectionTemplate WHERE name ='Vital Signs'), (SELECT id from FormTemplate WHERE name ='03_Visit Info Scheduled')),
 (uuid(), 3, (SELECT id from SectionTemplate WHERE name ='Next Visit Info'), (SELECT id from FormTemplate WHERE name ='03_Visit Info Scheduled')),
 (uuid(), 4, (SELECT id from SectionTemplate WHERE name ='General Notes'), (SELECT id from FormTemplate WHERE name ='03_Visit Info Scheduled')),
 (uuid(), 1, (SELECT id from SectionTemplate WHERE name ='Visit Information EOT'), (SELECT id from FormTemplate WHERE name ='06_Visit Info EOT')),
 (uuid(), 2, (SELECT id from SectionTemplate WHERE name ='Body Measurements'), (SELECT id from FormTemplate WHERE name ='06_Visit Info EOT')),
 (uuid(), 3, (SELECT id from SectionTemplate WHERE name ='Vital Signs'), (SELECT id from FormTemplate WHERE name ='06_Visit Info EOT')),
 (uuid(), 4, (SELECT id from SectionTemplate WHERE name ='Next Visit Info'), (SELECT id from FormTemplate WHERE name ='06_Visit Info EOT')),
 (uuid(), 5, (SELECT id from SectionTemplate WHERE name ='General Notes'), (SELECT id from FormTemplate WHERE name ='06_Visit Info EOT')),
 (uuid(), 1, (SELECT id from SectionTemplate WHERE name ='CGI'), (SELECT id from FormTemplate WHERE name ='900_CGI')),
 (uuid(), 1, (SELECT id from SectionTemplate WHERE name ='CGI-S'), (SELECT id from FormTemplate WHERE name ='901_CGIS')),
 (uuid(), 1, (SELECT id from SectionTemplate WHERE name ='PGI-S'), (SELECT id from FormTemplate WHERE name ='903_PGIS')),
 (uuid(), 1, (SELECT id from SectionTemplate WHERE name ='PGI-C'), (SELECT id from FormTemplate WHERE name ='902_PGIC'));
 
 
 -- INSERTing 28 rows Field
 INSERT Field
  (id, sectionTemplateid, ciFieldOrderNum)
 VALUES
(uuid(), (SELECT id from SectionTemplate WHERE name ='Visit Information Screening'), 1),
(uuid(), (SELECT id from SectionTemplate WHERE name ='Visit Information Screening'), 2),
(uuid(), (SELECT id from SectionTemplate WHERE name ='Visit Information Baseline'), 1),
(uuid(), (SELECT id from SectionTemplate WHERE name ='Visit Information Baseline'), 2),
(uuid(), (SELECT id from SectionTemplate WHERE name ='Visit Information Scheduled'), 1),
(uuid(), (SELECT id from SectionTemplate WHERE name ='Visit Information Scheduled'), 2),
(uuid(), (SELECT id from SectionTemplate WHERE name ='Visit Information EOT'), 1),
(uuid(), (SELECT id from SectionTemplate WHERE name ='Visit Information EOT'), 2),
(uuid(), (SELECT id from SectionTemplate WHERE name ='Visit Information Screening Rollover'), 1),
(uuid(), (SELECT id from SectionTemplate WHERE name ='Visit Information Screening Rollover'), 2),
(uuid(), (SELECT id from SectionTemplate WHERE name ='Visit Information Screening Group 3'), 1),
(uuid(), (SELECT id from SectionTemplate WHERE name ='Visit Information Screening Group 3'), 2),
(uuid(), (SELECT id from SectionTemplate WHERE name ='Vital Signs'), 1),
(uuid(), (SELECT id from SectionTemplate WHERE name ='Vital Signs'), 2),
(uuid(), (SELECT id from SectionTemplate WHERE name ='Body Measurements'), 1),
(uuid(), (SELECT id from SectionTemplate WHERE name ='Body Measurements'), 2),
(uuid(), (SELECT id from SectionTemplate WHERE name ='Next Visit Info'), 1),
(uuid(), (SELECT id from SectionTemplate WHERE name ='Next Visit Info'), 2),
(uuid(), (SELECT id from SectionTemplate WHERE name ='General Notes'), 1),
(uuid(), (SELECT id from SectionTemplate WHERE name ='General Notes'), 2),
(uuid(), (SELECT id from SectionTemplate WHERE name ='CGI'), 1),
(uuid(), (SELECT id from SectionTemplate WHERE name ='CGI'), 2),
(uuid(), (SELECT id from SectionTemplate WHERE name ='CGI-S'), 1),
(uuid(), (SELECT id from SectionTemplate WHERE name ='CGI-S'), 2),
(uuid(), (SELECT id from SectionTemplate WHERE name ='PGI-S'), 1),
(uuid(), (SELECT id from SectionTemplate WHERE name ='PGI-S'), 2),
(uuid(), (SELECT id from SectionTemplate WHERE name ='PGI-C'), 1),
(uuid(), (SELECT id from SectionTemplate WHERE name ='PGI-C'), 2);


-- INSERTing 28 rows FieldVersion
INSERT FieldVersion
 (id, fieldid, ciFieldName, ciFriendlyName, ciMappingName, ciFieldType, isRequiredFlag, ciValidation, hasToolTipFlag, ciValidationMessage, cidesignInstructions, cidMInstructions, lastModifiedByUserid, createdByUserid)
VALUES
 (uuid(), "FIELD UUID GOES HERE", 'Vidate', 'Visit Date', 'VisitDate', 'DD/MMM/YYYY Date field', 1, 'Required', 1, 'Required: Visit date. Should be: > 01/OCT/2020. No future date.', NULL, NULL, (SELECT id from Users WHERE userName='jSmith'), (SELECT id from Users WHERE userName='jSmith')),
 (uuid(), "FIELD UUID GOES HERE", 'VINotes', 'Visit Information notes', 'VisitNotes', 'Ink area', 0, 'Optional', 0, NULL, '3-4 lines', NULL, (SELECT id from Users WHERE userName='jSmith'), (SELECT id from Users WHERE userName='jSmith')),
 (uuid(), "FIELD UUID GOES HERE", 'nc_Vidate', 'Screening Visit date', 'ScreeningVisitDate', 'Dynamic Label', NULL, 'NA', 0, NULL, 'Dependent Field Cross Section Functionality', NULL, (SELECT id from Users WHERE userName='jSmith'), (SELECT id from Users WHERE userName='jSmith')),
 (uuid(), "FIELD UUID GOES HERE", 'Vidate1', 'Visit Date', 'VisitDate', 'DD/MMM/YYYY Date field', 1, 'Required', 1, 'Required: Visit date. Should be: > Screening Visit date. No future date.', NULL, NULL, (SELECT id from Users WHERE userName='jSmith'), (SELECT id from Users WHERE userName='jSmith')),
 (uuid(), "FIELD UUID GOES HERE", 'nc_Vidate', 'Screening Visit date', 'ScreeningVisitDate', 'Dynamic Label', NULL, 'NA', 0, NULL, 'Dependent Field Cross Section Functionality', NULL, (SELECT id from Users WHERE userName='jSmith'), (SELECT id from Users WHERE userName='jSmith')),
 (uuid(), "FIELD UUID GOES HERE", 'Vidate2', 'Visit Date', 'VisitDate', 'DD/MMM/YYYY Date field', 1, 'Required', 1, 'Required: Visit date. Should be: Within test window. No future date.', NULL, NULL, (SELECT id from Users WHERE userName='jSmith'), (SELECT id from Users WHERE userName='jSmith')),
 (uuid(), "FIELD UUID GOES HERE", 'VIid', 'Visit type', 'VisitType', 'Single choice field', 1, 'Required', 0, 'Required: Visit type', NULL, NULL, (SELECT id from Users WHERE userName='jSmith'), (SELECT id from Users WHERE userName='jSmith')),
 (uuid(), "FIELD UUID GOES HERE", 'Vidate3', 'Visit Date', 'VisitDate', 'DD/MMM/YYYY Date field', 1, 'Required', 1, 'Required: Visit date. Should be: ≥ Baseline Visit date. No future date.', NULL, NULL, (SELECT id from Users WHERE userName='jSmith'), (SELECT id from Users WHERE userName='jSmith')),
 (uuid(), "FIELD UUID GOES HERE", 'Vidate5', 'Visit Date', 'VisitDate', 'DD/MMM/YYYY Date field', 1, 'Required', 1, 'Required: Visit date. Should be: > 01/OCT/2021. No future date.', NULL, NULL, (SELECT id from Users WHERE userName='jSmith'), (SELECT id from Users WHERE userName='jSmith')),
 (uuid(), "FIELD UUID GOES HERE", 'VINotes', 'Visit Information notes', 'VisitNotes', 'Ink area', 0, 'Optional', 0, NULL, '3-4 lines', NULL, (SELECT id from Users WHERE userName='jSmith')),
 (uuid(), "FIELD UUID GOES HERE", 'Vidate6', 'Visit Date', 'VisitDate', 'DD/MMM/YYYY Date field', 1, 'Required', 1, 'Required: Visit date. Should be: > 01/Jan/2022. No future date.', NULL, NULL, (SELECT id from Users WHERE userName='jSmith'), (SELECT id from Users WHERE userName='jSmith')),
 (uuid(), "FIELD UUID GOES HERE", 'VINotes', 'Visit Information notes', 'VisitNotes', 'Ink area', 0, 'Optional', 0, NULL, '3-4 lines', NULL, (SELECT id from Users WHERE userName='jSmith'), (SELECT id from Users WHERE userName='jSmith')),
 (uuid(), "FIELD UUID GOES HERE", 'VSSittingSystolicBP', 'Systolic blood pressure: sitting', NULL, 'Numeric ink field', 1, 'Field is required unless section is marked intentionally left blank', 1, 'Required: Systolic blood pressure: sitting. Should be: 90-190 mmHg. ≥ diastolic blood pressure.', NULL, NULL, (SELECT id from Users WHERE userName='jSmith'), (SELECT id from Users WHERE userName='jSmith')),
 (uuid(), "FIELD UUID GOES HERE", 'VSSittingDiastolicBP', 'Diastolic blood pressure: sitting', NULL, 'Numeric ink field', 1, 'Field is required unless section is marked intentionally left blank', 1, 'Required: Diastolic blood pressure: sitting. Should be: 60-110 mmHg. ≤ systolic blood pressure.', NULL, NULL, (SELECT id from Users WHERE userName='jSmith'), (SELECT id from Users WHERE userName='jSmith')),
 (uuid(), "FIELD UUID GOES HERE", 'BMWeight', 'Weight', 'Weight', 'Numeric ink field', 1, 'Required', 1, 'Required: Weight. Should be: 27.0 - 113.0 kg or 59.5 - 249.1 lb', NULL, NULL, (SELECT id from Users WHERE userName='jSmith'), (SELECT id from Users WHERE userName='jSmith')),
 (uuid(), "FIELD UUID GOES HERE", 'BMWeightUnit', 'Weight unit', 'WeightUnit', 'Single choice field', 1, 'Required', 0, 'Required: Weight unit', NULL, NULL, (SELECT id from Users WHERE userName='jSmith'), (SELECT id from Users WHERE userName='jSmith')), 
 (uuid(), "FIELD UUID GOES HERE", 'dv_NVWindowMin', 'Next visit min window', NULL, 'AC dynamic label', NULL, 'NA', 0, NULL, 'Dependent Field Cross Section Functionality', NULL, (SELECT id from Users WHERE userName='jSmith'), (SELECT id from Users WHERE userName='jSmith')),
 (uuid(), "FIELD UUID GOES HERE", 'dv_NVWindowMax', 'Next visit max window', NULL, 'AC dynamic label', NULL, 'NA', 0, NULL, 'Dependent Field Cross Section Functionality', NULL, (SELECT id from Users WHERE userName='jSmith'), (SELECT id from Users WHERE userName='jSmith')),
 (uuid(), "FIELD UUID GOES HERE", 'GNNotes', 'Notes', NULL, 'Ink area', 0, 'Optional', 0, NULL, '2 pages', NULL, (SELECT id from Users WHERE userName='jSmith'), (SELECT id from Users WHERE userName='jSmith')),
 (uuid(), "FIELD UUID GOES HERE", 'nc_Vidate_CF', 'Visit date', NULL, 'Child field', 0, 'Optional', 0, NULL, 'Child field-cross form functionality ', 'Vidate', (SELECT id from Users WHERE userName='jSmith'), (SELECT id from Users WHERE userName='jSmith')),
 (uuid(), "FIELD UUID GOES HERE", 'CGIQuestion1', 'CGI Question 1', 'Question1', 'Single choice field', 1, 'Required', 0, 'Required: CGI Question 1', NULL, NULL, (SELECT id from Users WHERE userName='jSmith'), (SELECT id from Users WHERE userName='jSmith')),
 (uuid(), "FIELD UUID GOES HERE", 'CGIQuestion2', 'CGI Question 2', 'Question2', 'Single choice field', 1, 'Required', 0, 'Required: CGI Question 2', NULL, NULL, (SELECT id from Users WHERE userName='jSmith'), (SELECT id from Users WHERE userName='jSmith')), 
 (uuid(), "FIELD UUID GOES HERE", 'CGISQuestion1', 'CGI-S Question 1', 'Question1', 'Single choice field', 1, 'Required', 0, 'Required: CGI-S Question 1', NULL, NULL, (SELECT id from Users WHERE userName='jSmith'), (SELECT id from Users WHERE userName='jSmith')),
 (uuid(), "FIELD UUID GOES HERE", 'CGISQuestion2', 'CGI-S Question 2', 'Question2', 'Single choice field', 1, 'Required', 0, 'Required: CGI-S Question 2', NULL, NULL, (SELECT id from Users WHERE userName='jSmith'), (SELECT id from Users WHERE userName='jSmith')),  
 (uuid(), "FIELD UUID GOES HERE", 'PGISQuestion1', 'PGI-S Question 1', 'Question1', 'Single choice field', 1, 'Required', 0, 'Required: PGI-S Question 1', NULL, NULL, (SELECT id from Users WHERE userName='jSmith'), (SELECT id from Users WHERE userName='jSmith')),
 (uuid(), "FIELD UUID GOES HERE", 'PGISQuestion2', 'PGI-S Question 2', 'Question2', 'Single choice field', 1, 'Required', 0, 'Required: PGI-S Question 2', NULL, NULL, (SELECT id from Users WHERE userName='jSmith'), (SELECT id from Users WHERE userName='jSmith')),  
 (uuid(), "FIELD UUID GOES HERE", 'PGICQuestion1', 'PGI-C Question 1', 'Question1', 'Single choice field', 1, 'Required', 0, 'Required: PGI-C Question 1', NULL, NULL, (SELECT id from Users WHERE userName='jSmith'), (SELECT id from Users WHERE userName='jSmith')),
 (uuid(), "FIELD UUID GOES HERE", 'PGICQuestion2', 'PGI-C Question 2', 'Question2', 'Single choice field', 1, 'Required', 0, 'Required: PGI-C Question 2', NULL, NULL, (SELECT id from Users WHERE userName='jSmith'), (SELECT id from Users WHERE userName='jSmith'));


-- INSERTing 6 rows QualityFinding
INSERT QualityFinding
 (id, fieldid, qTestid, failDescription, failStatus)
VALUES
 (uuid(), (SELECT id from FieldVersion WHERE ciFieldName ='Vidate'), 1, NULL, 'pass'),
 (uuid(), (SELECT id from FieldVersion WHERE ciFieldName ='CGIQuestion1'), 3, 'Label does not match', 'fail'),
 (uuid(), (SELECT id from FieldVersion WHERE ciFieldName ='VSSittingSystolicBP'), 5, NULL, 'pending'),
 (uuid(), (SELECT id from FieldVersion WHERE ciFieldName ='BMWeightUnit'), 7, 'Label misspelling', 'fail'),
 (uuid(), (SELECT id from FieldVersion WHERE ciFieldName ='CGISQuestion2'), 10, 'Values do not match scale', 'fail'),
 (uuid(), (SELECT id from FieldVersion WHERE ciFieldName ='dv_NVWindowMax'), 4, 'Value not displaying', 'fail');


-- INSERTing 6 rows CrossFormLinking
INSERT CrossFormLinking
 (id, studyid, parentFormid, parentFieldVerid, childFormid, childFieldVerid)
VALUES
 (uuid(), (SELECT id from Study WHERE ciStudyProjectCode='CI0022'), (SELECT id from Form WHERE name='Screening Form'), (SELECT id from FieldVersion WHERE ciFieldName ='Vidate'), (SELECT id from Form WHERE name='Baseline Form'), (SELECT id from FieldVersion WHERE ciFieldName ='nc_Vidate_CF')),
 (uuid(), (SELECT id from Study WHERE ciStudyProjectCode='CI0022'), (SELECT id from Form WHERE name='Screening Form'), (SELECT id from FieldVersion WHERE ciFieldName ='Vidate'), (SELECT id from Form WHERE name='Week 1 Form'), (SELECT id from FieldVersion WHERE ciFieldName ='nc_Vidate_CF')),
 (uuid(), (SELECT id from Study WHERE ciStudyProjectCode='CI0022'), (SELECT id from Form WHERE name='Screening Form'), (SELECT id from FieldVersion WHERE ciFieldName ='Vidate'), (SELECT id from Form WHERE name='Week 2 Form'), (SELECT id from FieldVersion WHERE ciFieldName ='nc_Vidate_CF')),
 (uuid(), (SELECT id from Study WHERE ciStudyProjectCode='CI0022'), (SELECT id from Form WHERE name='Screening Form'), (SELECT id from FieldVersion WHERE ciFieldName ='Vidate'), (SELECT id from Form WHERE name='Week 3 Form'), (SELECT id from FieldVersion WHERE ciFieldName ='nc_Vidate_CF')),
 (uuid(), (SELECT id from Study WHERE ciStudyProjectCode='CI0022'), (SELECT id from Form WHERE name='Screening Form Rollover'), (SELECT id from FieldVersion WHERE ciFieldName ='Vidate5'), (SELECT id from Form WHERE name='Week 4 Form'), (SELECT id from FieldVersion WHERE ciFieldName ='nc_Vidate_CF')),
 (uuid(), (SELECT id from Study WHERE ciStudyProjectCode='CI0022'), (SELECT id from Form WHERE name='Screening Form Rollover'), (SELECT id from FieldVersion WHERE ciFieldName ='Vidate5'), (SELECT id from Form WHERE name='Week 6 Form'), (SELECT id from FieldVersion WHERE ciFieldName ='nc_Vidate_CF'));


-- INSERTing 6 rows FormTrigger
INSERT FormTrigger
 (id, studyid, triggerSourceType, sourceFormVisitLinkid, sourceTriggerFieldid, triggerFormid)
VALUES
 (uuid(), (SELECT id from Study WHERE ciStudyProjectCode='CI0022'), 'form', (SELECT id from Form WHERE name='Baseline Form'), NULL, (SELECT id from Form WHERE name ='CGI'));


-- INSERTing 10 rows VisitTrigger
INSERT VisitTrigger 
 (id, studyid, triggerSourceType, sourceFormVisitLinkid, sourceTriggerFieldid, triggerVisitid)
VALUES
 (uuid(), (SELECT id from Study WHERE ciStudyProjectCode='CI0022'), 'form', (SELECT id from Form WHERE name='Screening Form'), NULL, (SELECT id from Visit WHERE name ='Baseline Visit')),
 (uuid(), (SELECT id from Study WHERE ciStudyProjectCode='CI0022'), 'form', (SELECT id from Form WHERE name='Screening Form'), NULL, (SELECT id from Visit WHERE name ='Visit 1')),
 (uuid(), (SELECT id from Study WHERE ciStudyProjectCode='CI0022'), 'form', (SELECT id from Form WHERE name='Screening Form'), NULL, (SELECT id from Visit WHERE name ='Visit 2')),
 (uuid(), (SELECT id from Study WHERE ciStudyProjectCode='CI0022'), 'form', (SELECT id from Form WHERE name='Screening Form'), NULL, (SELECT id from Visit WHERE name ='Visit 3')),
 (uuid(), (SELECT id from Study WHERE ciStudyProjectCode='CI0022'), 'form', (SELECT id from Form WHERE name='Screening Form'), NULL, (SELECT id from Visit WHERE name ='EOT Visit')),
 (uuid(), (SELECT id from Study WHERE ciStudyProjectCode='CI0022'), 'form', (SELECT id from Form WHERE name='Screening Form Rollover'), NULL, (SELECT id from Visit WHERE name ='Baseline Visit Rollover')),
 (uuid(), (SELECT id from Study WHERE ciStudyProjectCode='CI0022'), 'form', (SELECT id from Form WHERE name='Screening Form Rollover'), NULL, (SELECT id from Visit WHERE name ='EOT Visit')),
 (uuid(), (SELECT id from Study WHERE ciStudyProjectCode='CI0022'), 'form', (SELECT id from Form WHERE name='Screening Form Group 3'), NULL, (SELECT id from Visit WHERE name ='Baseline Visit Group 3')),
 (uuid(), (SELECT id from Study WHERE ciStudyProjectCode='CI0022'), 'field', (SELECT id from Form WHERE name='EOT Form'), (SELECT id from FieldVersion WHERE ciFieldName ='VIid'), (SELECT id from Visit WHERE name ='Screening Visit Rollover')),
 (uuid(), (SELECT id from Study WHERE ciStudyProjectCode='CI0022'), 'field', (SELECT id from Form WHERE name='EOT Form Rollover'), (SELECT id from FieldVersion WHERE ciFieldName ='VIid'), (SELECT id from Visit WHERE name ='Screening Visit Group 3'));