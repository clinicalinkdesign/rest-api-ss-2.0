-- TURN ON LOGGING
SET GLOBAL general_log = 'ON';

-- SPONSOR CREATION
CREATE TABLE Sponsor (
	id char(36) NOT NULL,
	name varchar(255) NOT NULL,
	createdDate datetime DEFAULT CURRENT_TIMESTAMP,
	lastModifiedDate datetime ON UPDATE CURRENT_TIMESTAMP,
	isRemovedFlag char(1) NULL,
	PRIMARY KEY (id)
);

CREATE TABLE sponsor_history (
	id char(36) NOT NULL,
	action char(8) NOT NULL,
	sponsorid char(36) NOT NULL,
	name varchar(255) NOT NULL,
	createdDate datetime NULL,
	lastModifiedDate datetime NULL,
	isRemovedFlag char(1) NULL,
	userid varchar(40) NULL, 
	userid char(36) NULL,
	PRIMARY KEY (id)
);
--

-- CRO CREATION
CREATE TABLE CRO (
	id char(36) NOT NULL,
	name varchar(255) NOT NULL,
	createdDate datetime DEFAULT CURRENT_TIMESTAMP,
	lastModifiedDate datetime ON UPDATE CURRENT_TIMESTAMP,
	isRemovedFlag char(1) NULL,
	PRIMARY KEY (id)
);

CREATE TABLE cro_history (
	id char(36) NOT NULL,
	action char(8) NOT NULL,
	croid char(36) NOT NULL,
	name varchar(255) NOT NULL,
	createdDate datetime NULL,
	lastModifiedDate datetime NULL,
	isRemovedFlag char(1) NULL,
	userid varchar(40) NULL, 
	userid char(36) NULL,
	PRIMARY KEY (id)
);
--

-- USERS CREATION
-- https://www.mssqltips.com/sqlservertip/4037/storing-passwords-in-a-secure-way-in-a-sql-server-database/ --
-- for later --
CREATE TABLE Users(
	id char(36) NOT NULL,
	userName varchar(40) NOT NULL,
    passwordHash BINARY(64) NOT NULL,
    firstName VARCHAR(40) NULL,
    lastName VARCHAR(40) NULL,
	email varChar(255) NOT NULL,
	userRole varChar(50) NOT NULL,
	createdDate datetime DEFAULT CURRENT_TIMESTAMP,
	lastModifiedDate datetime ON UPDATE CURRENT_TIMESTAMP,
	isRemovedFlag char(1) NULL,
	hasAccessFlag char(1) NULL,
	PRIMARY KEY (id)
);

CREATE TABLE users_history(
	id char(36) NOT NULL,
	action char(8) NOT NULL,
	userid char(36) NOT NULL,
	userName varchar(40) NOT NULL,
    passwordHash BINARY(64) NOT NULL,
    firstName VARCHAR(40) NULL,
    lastName VARCHAR(40) NULL,
	email varChar(255) NOT NULL,
	userRole varChar(50) NOT NULL,
	createdDate datetime NULL,
	lastModifiedDate datetime NULL,
	isRemovedFlag char(1) NULL,
	hasAccessFlag char(1) NULL,
	userid char(36) NULL,
	PRIMARY KEY (id)
);
--

-- STUDY CREATION
CREATE TABLE Study (
	id char(36) NOT NULL,
	ciStudyProjectCode char(7) NOT NULL,
	protocolName varchar(255) NOT NULL,
	sponsorid char(36) NULL,
	croid char(36) NULL,
	libraryFlag char(1) NULL,
	createdDate datetime DEFAULT CURRENT_TIMESTAMP,
	lastModifiedDate datetime ON UPDATE CURRENT_TIMESTAMP,
	studyType char(10) NULL,
	isLegacyFlag char(1) NULL,
	isRemovedFlag char(1) NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (sponsorid) REFERENCES Sponsor(id),
	FOREIGN KEY (croid) REFERENCES CRO(id)
);
CREATE TABLE study_history (
	id char(36) NOT NULL,
	action char(8) NOT NULL,
	studyid char(36) NOT NULL,
	ciStudyProjectCode char(7) NOT NULL,
	protocolName varchar(255) NOT NULL,
	sponsorid char(36) NOT NULL,
	croid char(36) NOT NULL,
	libraryFlag char(1) NULL,
	createdDate datetime NULL,
	lastModifiedDate datetime NULL,
	studyType char(10) NULL,
	isLegacyFlag char(1) NULL,
	isRemovedFlag char(1) NULL,
	userid char(36) NULL,
	PRIMARY KEY (id)
);
--

-- ARM CREATION
CREATE TABLE Arm (
	id char(36) NOT NULL,
	name varchar(255) NOT NULL,
	studyid char(36) NOT NULL,
	createdDate datetime DEFAULT CURRENT_TIMESTAMP,
	lastModifiedDate datetime ON UPDATE CURRENT_TIMESTAMP,
	isRemovedFlag char(1) NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (studyid) REFERENCES Study(id)
);

CREATE TABLE Arm_history (
	id char(36) NOT NULL,
	action char(8) NOT NULL,
	armid char(36) NOT NULL,
	name varchar(255) NOT NULL,
	studyid char(36) NOT NULL,
	createdDate datetime NOT NULL,
	lastModifiedDate datetime NOT NULL,
	isRemovedFlag char(1) NULL,
	userid char(36) NULL,
	PRIMARY KEY (id)
);
--

-- VISIT CREATION
CREATE TABLE Visit (
	id char(36) NOT NULL,
	name varchar(255) NOT NULL,
	armid char(36) NOT NULL,
	createdDate datetime DEFAULT CURRENT_TIMESTAMP,
	lastModifiedDate datetime ON UPDATE CURRENT_TIMESTAMP,
	visitOrder int NOT NULL,
	isRemovedFlag char(1) NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (armid) REFERENCES Arm(id)
);

CREATE TABLE Visit_history (
	id char(36) NOT NULL,
	action char(8) NOT NULL,
	visitid char(36) NOT NULL,
	name varchar(255) NOT NULL,
	armid char(36) NOT NULL,
	createdDate datetime NOT NULL,
	lastModifiedDate datetime NOT NULL,
	visitOrder int NOT NULL,
	isRemovedFlag char(1) NULL,
	userid char(36) NULL,
	PRIMARY KEY (id)
);
--

-- FORM TEMPALTE CREATION
CREATE TABLE FormTemplate (
	id char(36) NOT NULL,
	name varchar(40) NOT NULL,
	studyid char(36) NOT NULL,
	createdDate datetime DEFAULT CURRENT_TIMESTAMP,
	lastModifiedDate datetime ON UPDATE CURRENT_TIMESTAMP,
	isRemovedFlag char(1) NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (studyid) REFERENCES Study(id)
);

CREATE TABLE FormTemplate_history (
	id char(36) NOT NULL,
	action char(8) NOT NULL,
	formTemplateid char(36) NOT NULL,
	name varchar(40) NOT NULL,
	studyid char(36) NOT NULL,
	createdDate datetime NOT NULL,
	lastModifiedDate datetime NOT NULL,
	isRemovedFlag char(1) NULL,
	userid char(36) NULL,
	PRIMARY KEY (id)
);
--

-- FORM CREATION
CREATE TABLE Form (
	id char(36) NOT NULL,
	name varchar(255) NOT NULL,
	visitid char(36),
	orderInVisit int NOT NULL,
	formTemplateid char(36) NOT NULL,
	createdDate datetime DEFAULT CURRENT_TIMESTAMP,
	lastModifiedDate datetime ON UPDATE CURRENT_TIMESTAMP,
	isRemovedFlag char(1) NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (visitid) REFERENCES Visit(id),
	FOREIGN KEY (formTemplateid) REFERENCES FormTemplate(id)
);

CREATE TABLE Form_history (
	id char(36) NOT NULL,
	action char(8) NOT NULL,
	formid char(36) NOT NULL,
	name varchar(255) NOT NULL,
	visitid char(36),
	orderInVisit int NOT NULL,
	formTemplateid char(36) NOT NULL,
	createdDate datetime NOT NULL,
	lastModifiedDate datetime NOT NULL,
	isRemovedFlag char(1) NULL,
	userid char(36) NULL,
	PRIMARY KEY (id)
);
--

-- SECTION GROUP CREATION
CREATE TABLE SectionGroup (
	id char(36) NOT NULL,
	name varchar(50) NOT NULL,
	studyid char(36) NOT NULL,
	sectionGroupPrefix varchar(10) NOT NULL,
	createdDate datetime DEFAULT CURRENT_TIMESTAMP,
	lastModifiedDate datetime ON UPDATE CURRENT_TIMESTAMP,
	isRemovedFlag char(1) NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (studyid) REFERENCES Study(id)
);

CREATE TABLE SectionGroup_history (
	id char(36) NOT NULL,
	action char(8) NOT NULL,
	sectionGroupid char(36) NOT NULL,
	name varchar(50) NOT NULL,
	studyid char(36) NOT NULL,
	sectionGroupPrefix varchar(10) NOT NULL,
	createdDate datetime NOT NULL,
	lastModifiedDate datetime NOT NULL,
	isRemovedFlag char(1) NULL,
	userid char(36) NULL,
	PRIMARY KEY (id)
);
--

-- SECTION TEMPLATE CREATION
CREATE TABLE SectionTemplate (
	id char(36) NOT NULL,
	name varchar(255) NOT NULL,
	copiedFlag char(1) NULL,
	copiedUserid char(36) NULL,
	copiedFromStudyid char(36) NULL,
	copiedFromSectionGroupid char(36) NULL,
	copiedFromSectionTemplateid char(36) NULL,
	templateModifiedFlag char(1) NULL,
	dateTimeCopied datetime NULL,
	sectionGeneratorConfig TEXT NULL,
	uniqueFieldPrefix varchar(10) NOT NULL,
	createdDate datetime DEFAULT CURRENT_TIMESTAMP,
	lastModifiedDate datetime ON UPDATE CURRENT_TIMESTAMP,
	isRemovedFlag char(1) NULL,
	libraryStatus char(10) NULL,
	salesforceRecordid char(18) NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (sectionGroupid) REFERENCES SectionGroup(id),
	FOREIGN KEY (copiedUserid) REFERENCES Users(id)
);

CREATE TABLE SectionTemplate_history (
	id char(36) NOT NULL,
	action char(8) NOT NULL,
	sectionTemplateid char(36) NOT NULL,
	name varchar(255) NOT NULL,
	copiedFlag char(1) NULL,
	copiedUserid char(36) NOT NULL,
	copiedFromStudyid char(36) NULL,
	copiedFromSectionGroupid char(36) NULL,
	copiedFromSectionTemplateid char(36) NULL,
	templateModifiedFlag char(1) NULL,
	dateTimeCopied datetime NULL,
	sectionGeneratorConfig TEXT NULL,
	uniqueFieldPrefix varchar(10) NOT NULL,
	createdDate datetime NOT NULL,
	lastModifiedDate datetime NOT NULL,
	isRemovedFlag char(1) NULL,
	libraryStatus char(10) NULL,
	salesforceRecordid char(18) NULL,
	userid char(36) NULL,
	PRIMARY KEY (id)
);
--

-- SECTION GROUP LINK CREATION
CREATE TABLE SectionGroupLink (
	id char(36) NOT NULL,
	sectionTemplateid char(36) NOT NULL,
	sectionGroupid char(36) NOT NULL,
	createdDate datetime DEFAULT CURRENT_TIMESTAMP,
	lastModifiedDate datetime ON UPDATE CURRENT_TIMESTAMP,
	isRemovedFlag char(1) NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (sectionTemplateid) REFERENCES Sectiontemplate(id),
	FOREIGN KEY (sectionGroupid) REFERENCES SectionGroup(id)
)

CREATE TABLE SectionGroupLink_history (
	id char(36) NOT NULL,
	action char(8) NOT NULL,
	sectionGroupLinkid char(36) NOT NULL,
	sectionTemplateid char(36) NOT NULL,
	sectionGroupid char(36) NOT NULL,
	createdDate datetime DEFAULT CURRENT_TIMESTAMP,
	lastModifiedDate datetime ON UPDATE CURRENT_TIMESTAMP,
	isRemovedFlag char(1) NULL,
	PRIMARY KEY (id)
)
--

-- SECTION TEMPLATE FORM LINK CREATION
CREATE TABLE SectionTempFormLink (
	id char(36) NOT NULL,
	orderInForm int NOT NULL,
	sectionTemplateid char(36) NOT NULL,
	formTemplateid char(36) NOT NULL,
	createdDate datetime DEFAULT CURRENT_TIMESTAMP,
	lastModifiedDate datetime ON UPDATE CURRENT_TIMESTAMP,
	isRemovedFlag char(1) NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (sectionTemplateid) REFERENCES SectionTemplate(id),
	FOREIGN KEY (formTemplateid) REFERENCES FormTemplate(id)
);

CREATE TABLE SectionTempFormLink_history (
	id char(36) NOT NULL,
	action char(8) NOT NULL,
	sectionTempFormLinkid char(36) NOT NULL,
	orderInForm int NOT NULL,
	sectionTemplateid char(36) NOT NULL,
	formTemplateid char(36) NOT NULL,
	createdDate datetime NOT NULL,
	lastModifiedDate datetime NOT NULL,
	isRemovedFlag char(1) NULL,
	userid char(36) NULL,
	PRIMARY KEY (id)
);
--

-- FIELD CREATION
CREATE TABLE Field (
	id char(36) NOT NULL,
	sectionTemplateid char(36) NOT NULL,
	ciFieldOrderNum int NOT NULL,
	createdDate datetime DEFAULT CURRENT_TIMESTAMP,
	lastModifiedDate datetime ON UPDATE CURRENT_TIMESTAMP,
	isRemovedFlag char(1) NULL,
	libraryStatus char(10) NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (sectionTemplateid) REFERENCES SectionTemplate(id)
);

CREATE TABLE Field_history (
	id char(36) NOT NULL,
	action char(8) NOT NULL,
	fieldid char(36) NOT NULL,
	sectionTemplateid char(36) NOT NULL,
	ciFieldOrderNum int NOT NULL,
	ciRemovedFlag char(1),
	createdDate datetime NOT NULL,
	lastModifiedDate datetime NOT NULL,
	isRemovedFlag char(1) NULL,
	libraryStatus char(10) NULL,
	userid char(36) NULL,
	PRIMARY KEY (id)
);
--

-- FIELD VERSION CREATION
CREATE TABLE FieldVersion (
	id char(36) NOT NULL,
	fieldid char(36) NOT NULL,
	ciFieldName varchar(40) NOT NULL,
	ciFriendlyName varchar(255) NOT NULL,
	ciMappingName varchar(50) NULL,
	ciFieldType varchar(30) NOT NULL,
	ciLabelText TEXT NULL,
	ciValues TEXT NULL,
	ciRange TEXT NULL,
	ciLogic TEXT NULL,
	isRequiredFlag char(1) NULL,
	ciValidation varchar(255) NULL,
	hasToolTipFlag char(1) NULL,
	ciValidationMessage varchar(255) NULL,
	cidesignInstructions text NULL,
	cidMInstructions text NULL,
	ciCustomField1 text NULL,
	reqFail text NULL,
	createdDate datetime DEFAULT CURRENT_TIMESTAMP,
	createdByUserid char(36) NOT NULL,
	lastModifiedDate datetime ON UPDATE CURRENT_TIMESTAMP,
	lastModifiedByUserid char(36) NOT NULL,
	isRemovedFlag char(1) NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (fieldid) REFERENCES Field(id),
	FOREIGN KEY (createdByUserid) REFERENCES Users(id),
	FOREIGN KEY (lastModifiedByUserid) REFERENCES Users(id)
);
--

-- QUALITY FINDING CREATION
CREATE TABLE QualityFinding (
	id char(36) NOT NULL,
	fieldid char(36) NOT NULL,
	qTestid char(36) NOT NULL,
	failDescription text NULL,
	failStatus varchar(100),
	createdDate datetime DEFAULT CURRENT_TIMESTAMP,
	lastModifiedDate datetime ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (id),
	FOREIGN KEY (fieldid) REFERENCES FieldVersion(id)
);

CREATE TABLE QualityFinding_history (
	id char(36) NOT NULL,
	action char(8) NOT NULL,
	qualityFindingid char(36) NOT NULL,
	fieldid char(36) NOT NULL,
	qTestid char(36) NOT NULL,
	failDescription text NULL,
	failStatus varchar(100),
	createdDate datetime NOT NULL,
	lastModifiedDate datetime NOT NULL,
	userid char(36) NULL,
	PRIMARY KEY (id)
);
--

-- CROSS FORM LINKING CREATION
CREATE TABLE CrossFormLinking (
	id char(36) NOT NULL,
	studyid char(36) NOT NULL,
	parentFormid char(36) NOT NULL,
	parentFieldVerid char(36) NOT NULL,
	childFormid char(36) NOT NULL,
	childFieldVerid char(36) NOT NULL,
	createdDate datetime DEFAULT CURRENT_TIMESTAMP,
	lastModifiedDate datetime ON UPDATE CURRENT_TIMESTAMP,
	isRemovedFlag char(1) NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (studyid) REFERENCES Study(id),
	FOREIGN KEY (parentFormid) REFERENCES Form(id),
	FOREIGN KEY (parentFieldVerid) REFERENCES FieldVersion(id),
	FOREIGN KEY (childFormid) REFERENCES Form(id),
	FOREIGN KEY (childFieldVerid) REFERENCES FieldVersion(id)
);

CREATE TABLE CrossFormLinking_history (
	id char(36) NOT NULL,
	action char(8) NOT NULL,
	crossFormLinkingid char(36) NOT NULL,
	studyid char(36) NOT NULL,
	parentFormid char(36) NOT NULL,
	parentFieldVerid char(36) NOT NULL,
	childFormid char(36) NOT NULL,
	childFieldVerid char(36) NOT NULL,
	createdDate datetime NOT NULL,
	lastModifiedDate datetime NOT NULL,
	isRemovedFlag char(1) NULL,
	userid char(36) NULL,
	PRIMARY KEY (id)
);
--

-- FORM TRIGGER CREATION
CREATE TABLE FormTrigger (
	id char(36) NOT NULL,
	studyid char(36) NOT NULL,
	triggerSourceType varchar(5) NOT NULL,
	sourceFormVisitLinkid char(36) NOT NULL,
	sourceTriggerFieldid char(36) NULL,
	triggerFormid char(36)  NOT NULL,
	createdDate datetime DEFAULT CURRENT_TIMESTAMP,
	lastModifiedDate datetime ON UPDATE CURRENT_TIMESTAMP,
	isRemovedFlag char(1) NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (studyid) REFERENCES Study(id),
	FOREIGN KEY (sourceFormVisitLinkid) REFERENCES Form(id),
	FOREIGN KEY (sourceTriggerFieldid) REFERENCES FieldVersion(id),
	FOREIGN KEY (triggerFormid) REFERENCES Form(id)
);

CREATE TABLE FormTrigger_history (
	id char(36) NOT NULL,
	action char(8) NOT NULL,
	formTriggerid char(36) NOT NULL,
	studyid char(36) NOT NULL,
	triggerSourceType varchar(5) NOT NULL,
	sourceFormVisitLinkid char(36) NOT NULL,
	sourceTriggerFieldid char(36) NULL,
	triggerFormid char(36)  NOT NULL,
	createdDate datetime NOT NULL,
	lastModifiedDate datetime NOT NULL,
	isRemovedFlag char(1) NULL,
	userid char(36) NULL,
	PRIMARY KEY (id)
);
--

-- VISIT TRIGGER CREATION
CREATE TABLE VisitTrigger (
	id char(36) NOT NULL,
	studyid char(36) NOT NULL,
	triggerSourceType varchar(5) NOT NULL,
	sourceFormVisitLinkid char(36) NOT NULL,
	sourceTriggerFieldid char(36) NULL,
	triggerVisitid char(36) NOT NULL,
	createdDate datetime DEFAULT CURRENT_TIMESTAMP,
	lastModifiedDate datetime ON UPDATE CURRENT_TIMESTAMP,
	isRemovedFlag char(1) NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (studyid) REFERENCES Study(id),
	FOREIGN KEY (sourceFormVisitLinkid) REFERENCES Form(id),
	FOREIGN KEY (sourceTriggerFieldid) REFERENCES FieldVersion(id),
	FOREIGN KEY (triggerVisitid) REFERENCES Visit(id)
);

CREATE TABLE VisitTrigger_history (
	id char(36) NOT NULL,
	action char(8) NOT NULL,
	visitTriggerid char(36) NOT NULL,
	studyid char(36) NOT NULL,
	triggerSourceType varchar(5) NOT NULL,
	sourceFormVisitLinkid char(36) NOT NULL,
	sourceTriggerFieldid char(36) NULL,
	triggerVisitid char(36) NOT NULL,
	createdDate datetime NOT NULL,
	lastModifiedDate datetime NOT NULL,
	isRemovedFlag char(1) NULL,
	userid char(36) NULL,
	PRIMARY KEY (id)
);
--

-- EDIT QUEUE TABLE
CREATE TABLE editQueue (
	id char(36) NOT NULL,
	tableName char(20) NOT NULL,
	rowid char(36) NOT NULL,
	createdDate datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
	userid char(36) NULL,
	PRIMARY KEY (id)
)