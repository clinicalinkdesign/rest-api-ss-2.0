/* 
   Report Identifier : CI_SR19_Cross_Form_Field
   Created  by       : Bill Mitchell
   Created  on       : 2016-08-04
   Modified by       : Matt Williams
   Modified on       : 2022-08-25
   Reviewed by       : 
   Reviewed on       : 
   Version           : v1.1
   
   Revisions
   _______________________________________________________________________
   v1.0 - 08/04/2016 NM Initial version
   v1.1 - 08/25/2022 Added Parent and Child arm columns to report
		
//Uncomment the below 3 lines for queries to database

DECLARE @StudyId varchar(40), @Environment varchar(10)
SET @StudyId='48ba091a-b36d-4683-ae1f-26110b07b9f2'
SET @Environment='Build'*/

Declare @StudyName varchar(250) =NULL
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

DECLARE @SQL AS NVARCHAR(MAX) = NULL

IF OBJECT_ID('TEMPDB..#XFormFieldTable', 'U') IS NOT NULL
DROP TABLE #XFormFieldTable

Create table #XFormFieldTable (
    [Study Name] varchar(250),
    [Environment] varchar(10),
    [Child Arm Name] varchar(max),
    [Child Visit Name] varchar(max),
    [Child Form Name] varchar(max),
    [Child Field Name] varchar(max),
    [Child Friendly Name] varchar(max),
    [Child Utility Field Name] varchar(max),
    [Parent Arm Name] varchar(max),
    [Parent Visit Name] varchar(max),
    [Parent Form Name] varchar(max),
    [Parent Field Name] varchar(max),
    [Parent Friendly Name] varchar(max)
    )

Select @StudyName=Name from [ClinicalInkAdmin].dbo.Studies where id=@StudyId

IF OBJECT_ID('TEMPDB..#XFormFriend', 'U') IS NOT NULL
DROP TABLE #XFormFriend

Create table #XFormFriend (
    FormID varchar(40),
    fieldname varchar(200),
    [FriendlyName] varchar(max),

    )

Set @sql ='Select distinct ft.formid,ffd.fieldname,ffd.[FriendlyName] from ['+@StudyId+'].['+@Environment+'].formtemplates ft
	   join ( Select formid,max(version) maxV from ['+@StudyId+'].['+@Environment+'].formtemplates group by formid) FTMAX on FTMAX.formid=ft.formid and ft.version=FTMAX.maxV
	   join ['+@StudyId+'].['+@Environment+'].FormFieldDefinitions ffd on ffd.formtemplateid=ft.id
	   where fieldname in (Select Childfield from ['+@StudyId+'].['+@Environment+'].DependentVisitTemplateField)
	   or fieldname in (Select ParentField from ['+@StudyId+'].['+@Environment+'].DependentVisitTemplateField)'
	   
insert into #XFormFriend
exec (@SQL)

Set @sql =

'SELECT '''+
	@StudyName+''' as [Study Name],
	'''+@Environment+''' [Environment],
	childArm.Name as [Child Arm Name],
	folderChild.Name as [Child Visit Name], 
	vtChild.VisitName as [Child Form Name], 
	dvtf.ChildField as [Child Field Name], 
	cFFD.FriendlyName,
	'''' as [Child Utility Field Name],
	parentArm.Name as [Parent Arm Name],
	folderParent.Name as [Parent Visit Name], 
	vtParent.VisitName as [Parent Visit Name], 
	dvtf.ParentField as [Parent Field Name],
	pFFD.FriendlyName

FROM 
	['+@StudyId+'].['+@Environment+'].DependentVisitTemplateField dvtf

JOIN 
	['+@StudyId+'].['+@Environment+'].DependentVisitTemplate dvt ON dvt.Id = dvtf.DependentVisitTemplateId
JOIN 
	['+@StudyId+'].['+@Environment+'].VisitTemplate vtChild on vtChild.Id = dvt.VisitTemplateId
JOIN 
	#XFormFriend cFFD on cFFD.formid=vtChild.formid and dvtf.ChildField=cFFD.fieldname
JOIN 
	['+@StudyId+'].['+@Environment+'].VisitTemplate vtParent on vtParent.Id = dvt.DependentTemplateId
JOIN 
	#XFormFriend pFFD on pFFD.formid=vtParent.formid and dvtf.ParentField=pFFD.fieldname
LEFT JOIN 
	['+@StudyId+'].['+@Environment+'].VisitFolder folderChild ON folderChild.Id = vtChild.VisitFolderId
LEFT JOIN 
	['+@StudyId+'].['+@Environment+'].VisitFolder folderParent ON folderParent.Id = vtParent.VisitFolderId
JOIN
	['+@StudyId+'].['+@Environment+'].Arm parentArm ON vtParent.ArmId = parentArm.Id
JOIN
	['+@StudyId+'].['+@Environment+'].Arm childArm ON vtChild.ArmId = childArm.Id

ORDER BY
	folderchild.sequence, 
	vtChild.VisitSequenceNumber, 
	vtChild.VisitName'

insert into #XFormFieldTable
exec (@SQL)

Select * from #XFormFieldTable

drop table #XFormFieldTable
DROP TABLE #XFormFriend