<?php 

  if(!defined('MyConst')) {
      die('Direct access not permitted');
  }

  class Database {
    
    public $conn;

    // DB Connect
    public function connect() {

        $host = 'localhost';
        $db_name = 'SureStart2.0';
        $username = 'ss2_API_DB_conn'; //<- environment variable
        $password = 'QP#c*%Y3MLbuu0GBObdg'; //<- environment variable

        $this->conn = null;

        try { 
            $this->conn = new PDO('mysql:host=' . $host . ';dbname=' . $db_name, $username, $password);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch(PDOException $e) {
            echo 'Connection Error: ' . $e->getMessage();
        }

      return $this->conn;
    }
  }