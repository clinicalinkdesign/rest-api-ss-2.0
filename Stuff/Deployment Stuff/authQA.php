<?php

if(!defined('MyConst')) {
    die('Direct access not permitted');
}

class Auth {

    //database connection and table name
    private $conn;

    private $suppliedUserName;
    private $suppliedPassword;
    private $pepper = "CEuHApLAAJqAwLGIwnGDoIKrrKIvJAonKpyBEDKEDqMMIMCCtAABrxMtIGqo"; //<- environment variable

    //constructor with $db as database connection
    public function __construct($db, $un, $pw){
        $this->conn = $db;
        $this->suppliedUserName = $un;
        $this->suppliedPassword = $pw;
    }

    public function getConn(){
        return $this->conn;
    }

    //Functions
    function getUser(){
        //Query the database for the user provided and return the info needed to authenticate

        $query = 'SELECT * FROM apiusers WHERE userName = '.$this->conn->quote($this->suppliedUserName);
        
        //prepare query statement
        $stmt = $this->conn->prepare($query);

        //execute query
        $stmt->execute();
        $num = $stmt->rowCount();

        if($num > 0) {
            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        }
        else {
            return null;
        }
    }

    function authenticate(){
        //Returns a boolean,
        //True - continue, False, end instance

        $peppered_provided_pw = hash_hmac("sha256", $this->suppliedPassword, $this->pepper);

        $db_user_info = $this->getUser();

        if($db_user_info != null) {

            if($db_user_info[0]['passwordHash'] == crypt($peppered_provided_pw, $db_user_info[0]['passwordHash'])) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }
}