from imports.SureStart2 import *
from imports.SS2UnitTests import *
import sys

### Menu ###

def run_main():
    while True:
        print('\n')
        print('SureStart 2 Testing')
        print('*******************************************************')
        print('\t1: Test Case 1')
        print('\t2: BULK CREATE SPONSOR')
        print('\tQ: Quit')
        print('\n')

        choice = input('What would you like to do? ').upper()
        if choice == '1':
            testCase1()
            continue
        elif choice == '2':
            testCase2()
            continue
        elif choice == 'Q':
            sys.exit('Finished!')
            break
        else:
            print('Not valid input.')
            continue

run_main()