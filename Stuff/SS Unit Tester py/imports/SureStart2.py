# Necessary imports
import requests as r
import pandas as pd
import json
import warnings
warnings.filterwarnings("ignore")

# Set Endpoint Function

def Endpoint(port, table):
    
    endpoint = "https://localhost:"+str(port)+"/SureStart_2/api/"+table
    
    return endpoint

class apiSureStart():
    """
    A class to manage API calls to SureStart Requirements Database
    
    ...
    
    Attributes
    ----------
    
    required: dict
        Table: [Column Name1, Column Name2] key-value pairs where the key is a name of a table in the database
        and the value is a list of required columns.
        
    allCols: dict
        Table: [Column Name1, Column Name2] key-value pairs where the key is a name of a table in the database
        and the value is a list of all available columns.
        
    table: str
        name of database table targeted for DML operation
        
    values: list
        list of dictionaries with key:value pairs for attributes of records.  Each dictionary represents one record.
        Required fields must be provided for POST, Id must be provided for PUT, Id must be provided for DELETE.
        Duplicate column keys not allowed.
        [{attr1: attr1val, attr2: attr2val,...,attrX: attrXval},{attr1: attr1val, attr2: attr2val,...,attrX: attrXval},...]

    port: int
        port value for endpoint, defaults to 443
        
    filters: str
        filters intended for GET operation to be appended to endpoint - formatted "?attribute1=XXXXX&attribute2=XXXXX"
        
        
    headers: dict
        headers for REST call.  Default headers => self.headers = {
                                                        "Access-Control-Allow-Origin": "*",
                                                        "Content-Type": "application/json; charset=UTF-8",
                                                        "Access-Control-Allow-Methods": "GET,POST,PUT,DELETE",
                                                        "Access-Control-Max-Age": "3600",
                                                        "Access-Control-Allow-Headers": "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With"
                                                    }
                                                    
    endpoint: str
        default value: None
        endpoint is set on GET, PUT, POST, DELETE request and affected by override value
    
    override: bool
        setting override to true will switch the endpoint to a direct connection for testing purposes
        
    postBody: dict
        default value: None
        set when post() method is called
    
    putBody: dict
        default value: None
        set when put() method is called     
        
    deleteBody: dict
        default value: None
        set when delete() method is called  
        
    idsGet: list
        default value: []
        list of returned ids from successful GET call
        
    idsPOST: list
        default value: []
        list of created ids from successful POST call
        
    idsPUT: list
        default value: []
        list of updated ids from successful PUT call
    
    idsDELETE: list
        default value: []
        list of deleted ids from successful DELETE call
        
    getResult: str
        default value: None
        server response from GET call
    
    getResultJSON = dict
        default value: None
        response body from GET call
        
    postResult: str
        default value: None
        server response from POST call
    
    postResultJSON = dict
        default value: None
        response body from POST call
        
    putResult: str
        default value: None
        server response from PUT call
    
    putResultJSON = dict
        default value: None
        response body from PUT call
        
    deleteResult: str
        default value: None
        server response from DELETE call
    
    deleteResultJSON = dict
        default value: None
        response body from DELETE call
    
    
    Methods
    -------
    
    get():
        Sends GET request
        
    post():
        Sends POST request
        
    put():
        Sends PUT request
        
    delete():
        Sends DELETE request
    
    
    """
    def __init__(self, table, values=None, port=443, filters=None, headers=None, override=False):
        
        """
        Constructs all necessary attributes for apiSureStart object.
        
        Parameters
        ----------
            table (required): str
                name of database table targeted for DML operation

            values (optional): list
                list of tuples of attributes for records to POST, PUT, or DELETE.  Each tuple represents one record.
                    [(attribute1, attribute2,...,attributeX),(attribute1, attribute2,...,attributeX),...]

            port (optional): int
                port value for endpoint, defaults to 443

            filters (optional): str
                filters intended for GET operation to be appended to endpoint - formatted "?attribute1=XXXXX&attribute2=XXXXX"


            headers (optional): dict
                headers for REST call.  Default headers => self.headers = {
                                                                "Access-Control-Allow-Origin": "*",
                                                                "Content-Type": "application/json; charset=UTF-8",
                                                                "Access-Control-Allow-Methods": "GET,POST,PUT,DELETE",
                                                                "Access-Control-Max-Age": "3600",
                                                                "Access-Control-Allow-Headers": "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With"
                                                            }

            override (optional): bool
                setting override to true will switch the endpoint to a direct connection for testing purposes
                
        """
        
        self.required = {
            "arm": ["name", "studyid"],
            "cro": ["name"],
            "crossformlinking": ["studyid", "parentFormid", "parentFieldVerid", "childFormid", "childFieldVerid"],
            "field": ["sectionTemplateid", "ciFieldOrderNum"],
            "fieldversion": ["fieldid", "ciFieldName", "ciFIeldType", "createdByUserid", "lastModifiedByUserid"],
            "form": ["name", "visitid", "orderInVisit", "formTemplateid"],
            "formtemplate": ["name", "studyid"],
            "formtrigger": ["studyid", "triggerSourceType",  "sourceFormVisitLinkid",  "triggerFormid"],
            "qualityfinding": ["fieldid", "qTestid"],
            "sectiongroup": ["name", "studyid" , "sectionGroupPrefix"],
            "sectiontempformlink": ["orderInForm", "sectionTemplateid", "formTemplateid"],
            "sponsor": ["name"],
            "study": ["ciStudyProjectCode", "protocolName", "sponsorid", "croid", "studyType"],
            "users": ["userName", "passwordHash", "email", "userRole"],
            "visit": ["name", "armid", "visitOrder"],
            "visittrigger": ["studyid", "triggerSourceType", "sourceFormVisitLinkid", "triggerVisitid"]
        }
        
        self.allCols = {
            "arm": ["name", "studyid", "id"],
            "cro": ["name", "id"],
            "crossformlinking": ["studyid", "parentFormid", "parentFieldVerid", "childFormid", "childFieldVerid", "id"],
            "field": ["sectionTemplateid", "ciFieldOrderNum", "libraryStatus", "id"],
            "fieldversion": ["fieldid", "ciFieldName", "ciFIeldType", "createdByUserid", "lastModifiedByUserid", "ciFriendlyName", "ciMappingName", "ciLabelText", "ciValues", 
                                        "ciRange", "ciLogic", "isRequiredFlag", "ciValidation", "hasToolTipFlag", 
                                        "ciValidationMessage", "cidesignInstructions", "cidMInstructions", 
                                        "ciCustomField1", "reqFail", "id"],
            "form": ["name", "visitid", "orderInVisit", "formTemplateid", "id"],
            "formtemplate": ["name", "studyid", "id"],
            "formtrigger": ["studyid", "triggerSourceType",  "sourceFormVisitLinkid",  "triggerFormid", "sourceTriggerFieldid", "id"],
            "qualityfinding": ["fieldid", "qTestid", "failDescription", "failStatus", "id"],
            "sectiongroup": ["name", "studyid" , "sectionGroupPrefix", "id"],
            "sectiontempformlink": ["orderInForm", "sectionTemplateid", "formTemplateid", "id"],
            "sponsor": ["name", "id"],
            "study": ["ciStudyProjectCode", "protocolName", "sponsorid", "croid", "studyType", "libraryFlag", "id"],
            "users": ["userName", "passwordHash", "email", "userRole", "firstName", "lastName", "id"],
            "visit": ["name", "armid", "visitOrder", "id"],
            "visittrigger": ["studyid", "triggerSourceType", "sourceFormVisitLinkid", "triggerVisitid", "sourceTriggerFieldid", "id"]
        }

        
        if type(values) != list and values is not None:
            raise ValueError("values were entered incorrectly, values should be a list.")

        self.values = values
        self.table = table
        self.__table = table
        self.port = port
        self.override = override
        self.filters = filters
        
        self.postBody = None
        self.putBody = None
        self.deleteBody = None
        
        self.idsGET = []
        self.idsPOST = []
        self.idsPUT = []
        self.idsDELETE = []
        
        self.getResult = None
        self.getResultJSON = None
        
        self.postResult = None
        self.postResultJSON = None
        
        self.putResult = None
        self.putResultJSON = None
        
        self.deleteResult = None
        self.deleteResultJSON = None
        
        self.endpoint = None
        
        if headers is not None:
            self.headers = headers
        else:
            self.headers = {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json; charset=UTF-8",
                "Access-Control-Allow-Methods": "GET,POST,PUT,DELETE",
                "Access-Control-Max-Age": "3600",
                "Access-Control-Allow-Headers": "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With"
            }
        
    def __setEndpoint(self,method):
        if self.override == False:
            if method == "GET" and self.filters is not None:
                self.endpoint = Endpoint(self.port,self.__table)+"/"+self.filters
            else:
                self.endpoint = Endpoint(self.port,self.__table)+"/"

        elif self.override == True:
            if method == "GET" and self.filters is not None:
                self.endpoint = "https://192.168.50.84/SureStart_2/api/"+self.__table+"/"+self.filters
            else:
                self.endpoint = "https://192.168.50.84/SureStart_2/api/"+self.__table+"/"

    def __checkValues(self,method):
            if self.values is not None:
                for item in self.values:
                    if set(self.required[self.__table]).issubset(item.keys()) == False and method == "POST":
                        raise ValueError(f"Required column values for {self.__table} missing.")

                    if set(item.keys()).issubset(self.allCols[self.__table]) == False and method in ["POST","PUT","DELETE"]:
                        print(set(item.keys()))
                        print(self.allCols[self.__table])
                        raise ValueError(f"Columns passed for {self.__table} that do not exist!")

                    if len(set(item.keys())) < len(item.keys()) and method in ["POST","PUT","DELETE"]:
                        raise ValueError("Duplicate columns keys entered.")
            else:
                raise ValueError("Method requires method body.  Please set values property for object.")
                
        
    def get(self,filters=None):
        """
        Sends GET request to server.
        
        Parameters
        ----------
        filters (optional): str
            filters to be appended to endpoint - formatted "?attribute1=XXXXX&attribute2=XXXXX"
        
        Returns
        -------
        None
        """
        if filters is not None:
            self.filters = filters
        
        self.__setEndpoint("GET")

        try:
            self.getResult = r.get(self.endpoint, headers=self.headers, verify=False, allow_redirects=False)
            self.getResultJSON = self.getResult.json()
            self.idsGET = [i["id"] for i in self.getResultJSON]

        except r.exceptions.ConnectionError as e:
            print(e)
            self.postResult = "No response."
            
        
    def post(self,values=None):
        """
        Sends POST request to server.
        
        Parameters
        ----------
        values: list
            list of dictionaries with key:value pairs for attributes of records.  Each dictionary represents one record.
            Required fields must be provided for POST.
            Duplicate column keys not allowed.
            [{attr1: attr1val, attr2: attr2val,...,attrX: attrXval},{attr1: attr1val, attr2: attr2val,...,attrX: attrXval},...]
        
        Returns
        -------
        None
        """
        if values is not None:
            self.values = values
        
        self.__setEndpoint("POST")
        self.__checkValues("POST")
        self.postBody = json.dumps({"data":self.values})
        
        try:
            self.postResult = r.post(self.endpoint, headers=self.headers, data=self.postBody, verify=False, allow_redirects=False)
            self.postResultJSON = self.postResult.json()
            self.idsPOST = [i["data"]["id"] if i["success"] != "false" else "failed to create" for i in self.postResultJSON]

        except r.exceptions.ConnectionError as e:
            print(e)
            self.postResult = "No response."

        
    def put(self,values=None):
        """
        Sends PUT request to server.
        
        Parameters
        ----------
        values: list
            list of dictionaries with key:value pairs for attributes of records.  Each dictionary represents one record.
            Required fields must be provided for POST, Id must be provided for PUT, Id must be provided for DELETE.
            Duplicate column keys not allowed.
            [{attr1: attr1val, attr2: attr2val,...,attrX: attrXval},{attr1: attr1val, attr2: attr2val,...,attrX: attrXval},...]
        
        Returns
        -------
        None
        """
        if values is not None:
            self.values = values
        
        self.__setEndpoint("PUT")  
        self.__checkValues("PUT")
        self.putBody = json.dumps({"data":self.values})
        
        try:
            self.putResult = r.put(self.endpoint, headers=self.headers, data=self.putBody, verify=False, allow_redirects=False)
            self.putResultJSON = self.putResult.json()
            self.idsPUT = [i["data"]["id"] if i["success"] != "false" else "failed to create" for i in self.putResultJSON]

        except r.exceptions.ConnectionError as e:
            print(e)
            self.putResult = "No response."
            
            
    def delete(self,values=None):
        """
        Sends DELETE request to server.
        
        Parameters
        ----------
        None
        
        Returns
        -------
        None
        """
        if values is not None:
            self.values = values
        
        self.__setEndpoint("DELETE")
        self.__checkValues("DELETE")
        self.deleteBody = json.dumps({"data":self.values})
        
        try:
            self.deleteResult = r.delete(self.endpoint, headers=self.headers, data=self.deleteBody, verify=False, allow_redirects=False)
            try:
                self.deleteResultJSON = self.deleteResult.json()
                self.idsDELETE = [i["data"]["id"] if i["success"] != "false" else "failed to create" for i in self.deleteResultJSON]
            except:
                self.deleteResultJSON = self.deleteResult.text
        except r.exceptions.ConnectionError as e:
            print(e)
            self.putResult = "No response."
            
            
##################################################################################################################
##################################################################################################################

# Helper Functions to build out lists of dictionaries with table row entry data


def loadData():
    """
    Loads a CSV file and returns a list of dicts: [{col1: value, col2: value},{col1: value, col2: value},...]
    """
    #As of now, this is requiring the actual file to be in the same directory as this script file (the imports directory)
    filename = input("Please enter CSV filename including '.csv': ")
    return pd.read_csv('imports/'+filename+'.csv').to_dict(orient="records")

# POST doesn't need Id
# PUT and DELETE would require Id

# Follow your heart <3

def postArms(listTuples):
    """
    Expecting [(name1, studyid1),(name2, studyid2)]
    """
    armbody = []
    for i, item in enumerate(listTuples):
        armbody.append({"name": item[0], "studyid": item[1]}) 
    return armbody

def postSponsor(nameList):
    """
    Expecting [name1,name2]
    """
    sponsorbody = []
    for i, item in enumerate(nameList):
        sponsorbody.append({"name": item})
    return sponsorbody
