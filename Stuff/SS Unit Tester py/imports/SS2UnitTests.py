from .SureStart2 import *

###########################################################

def testCase1():
    """
    First Test Case for SureStart 2 API Requests
    
    Operations:
    - POST 10 Sponsors
    - Verify 10 were successfully posted
    - PUT to 10 posted sponsors
    - Verify 10 were successfully updated
    - DELETE 10 posted sponsors
    - Verify 10 were successfully deleted
    - Verify Sponsor History has 3 entries for each of the 10 Ids
    
    Returns:
        None
    """
    # Build Out Initial Post Sponsor Names, Expected Changed Names
    sponsornames = []
    sponsorassert = []
    
    for i in range(10):
        sponsornames.append("Sponsor TC "+str(i))
        sponsorassert.append("Sponsor TC "+str(i)+" Changed")
    x = postSponsor(sponsornames)
    
    # Make POST Request
    api = apiSureStart("sponsor",values=x,override=True)
    api.post()
    assert len(api.postResultJSON) == 10
    assert all([True if i["success"] == 'true' else False for i in api.postResultJSON])
    
    # Make PUT Request
    putValues = [{"id":i, "name":j} for i,j in zip(api.idsPOST,sponsorassert)]  
    api.put(values=putValues)
    assert len(api.putResultJSON) == 10
    assert all([True if i["success"] == 'true' else False for i in api.putResultJSON])
    assert [i["data"]["name"] for i in api.putResultJSON] == sponsorassert
    
    # Make Delete Request
    toDelete = [{"id": i["id"]} for i in putValues]
    toDeleteList = [i["id"] for i in putValues]
    api.delete(values=toDelete)
    assert len(api.deleteResultJSON) == 10
    assert all([True if i["success"] == 'true' else False for i in api.deleteResultJSON])
    assert [i["data"]["id"] for i in api.deleteResultJSON] == toDeleteList
    
    # For each item in toDeleteList
        # Verify it exists in history 3 times
        
    print("\nTest Case 1 Passed!")
    
    apiHistory = apiSureStart("history",filters="?table=sponsor",override=True)
    apiHistory.get()
    history = [i["sponsorid"] for i in apiHistory.getResultJSON]
    for item in toDeleteList:
        assert history.count(item) == 3
        
    print("Test Case 1 History Passed!")
    
def testCase2():
    """
    First Test Case for SureStart 2 API Requests
    
    Operations:
    - POST 10 Sponsors
    - Verify 10 were successfully posted
    - PUT to 10 posted sponsors
    - Verify 10 were successfully updated
    - DELETE 10 posted sponsors
    - Verify 10 were successfully deleted
    - Verify Sponsor History has 3 entries for each of the 10 Ids
    
    Returns:
        None
    """
    
    x = loadData()
    
    # Make POST Request
    api = apiSureStart("sponsor",values=x,override=True)
    api.post()
    assert len(api.postResultJSON) == len(x)
    assert all([True if i["success"] == 'true' else False for i in api.postResultJSON])

    print("\nTest Case 1 Passed!")
    

    apiHistory = apiSureStart("history",filters="?table=sponsor",override=True)
    apiHistory.get()
    history = [i["sponsorid"] for i in apiHistory.getResultJSON]
    for item in api.idsPOST:
        assert history.count(item) == 1
        
    print("Test Case 1 History Passed!")
    