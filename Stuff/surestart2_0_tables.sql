-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 19, 2023 at 06:22 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `surestart2.0`
--

-- --------------------------------------------------------

--
-- Table structure for table `apiusers`
--

CREATE TABLE `apiusers` (
  `id` char(36) NOT NULL,
  `userName` varchar(40) DEFAULT NULL,
  `passwordHash` text DEFAULT NULL,
  `createdDate` datetime DEFAULT current_timestamp(),
  `lastModifiedDate` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `isRemovedFlag` char(1) DEFAULT NULL,
  `hasAccessFlag` char(1) DEFAULT NULL,
  `isAdminFlag` char(1) DEFAULT NULL,
  `dataServicesFlag` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `apiusers`
--

INSERT INTO `apiusers` (`id`, `userName`, `passwordHash`, `createdDate`, `lastModifiedDate`, `isRemovedFlag`, `hasAccessFlag`, `isAdminFlag`, `dataServicesFlag`) VALUES
('0cdb9994-8df2-11ed-9b7c-244bfe7dd4fe', 'admin', '$2y$10$BirTiZCNkVAJ8g9D6fK6SeQx4nT1n282iJ03IsspggzTM7kkvcL9a', '2023-01-06 13:43:18', '2023-01-06 14:26:53', '', '1', '1', '1'),
('42c45ddc-8dfa-11ed-9b7c-244bfe7dd4fe', 'dataServices_INT', '$2y$10$8S..2A0S0ZeCCQj/I9t0Yur9CElV31.cQyiBLwfXTrUJnoe2mWggW', '2023-01-06 14:42:05', NULL, '', '1', '0', '1'),
('485a183d-8df8-11ed-9b7c-244bfe7dd4fe', 'app_SureStart2_0', '$2y$10$tOi2owfc9tvo2IqYAc1cV.OKmuMsfyWFw4rWME7QyyKP6sFSZG4P2', '2023-01-06 14:27:55', NULL, '', '1', '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `approles`
--

CREATE TABLE `approles` (
  `id` char(36) NOT NULL,
  `userRole` char(1) DEFAULT NULL,
  `roleName` varchar(255) DEFAULT NULL,
  `editUsers_01` char(1) DEFAULT NULL,
  `exportRecords_02` char(1) DEFAULT NULL,
  `editRequirements_03` char(1) DEFAULT NULL,
  `studyMilestones_04` char(1) DEFAULT NULL,
  `studyMilestonesQC_05` char(1) DEFAULT NULL,
  `editReqQCMilestone_06` char(1) DEFAULT NULL,
  `viewUserNamesHistory_07` char(1) DEFAULT NULL,
  `bypassStatusWorkflow_08` char(1) DEFAULT NULL,
  `editQueue_09` char(1) DEFAULT NULL,
  `createdDate` datetime DEFAULT current_timestamp(),
  `lastModifiedDate` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `approles`
--

INSERT INTO `approles` (`id`, `userRole`, `roleName`, `editUsers_01`, `exportRecords_02`, `editRequirements_03`, `studyMilestones_04`, `studyMilestonesQC_05`, `editReqQCMilestone_06`, `viewUserNamesHistory_07`, `bypassStatusWorkflow_08`, `editQueue_09`, `createdDate`, `lastModifiedDate`) VALUES
('14e1536e-9676-11ed-979a-244bfe7dd4fe', '0', 'admin', '1', '1', '1', '1', '1', '0', '1', '1', '1', '2023-01-17 09:48:54', '2023-01-17 09:50:56'),
('6e67d95a-9676-11ed-979a-244bfe7dd4fe', '1', 'Manager', '0', '1', '1', '1', '1', '0', '1', '1', '1', '2023-01-17 09:51:24', '2023-01-17 15:50:59'),
('7bbc2610-9676-11ed-979a-244bfe7dd4fe', '2', 'Design', '0', '1', '1', '1', '0', '0', '1', '0', '0', '2023-01-17 09:51:47', '2023-01-17 15:51:27'),
('8daaba8e-9676-11ed-979a-244bfe7dd4fe', '3', 'eSource Dev', '0', '1', '0', '0', '0', '0', '1', '0', '0', '2023-01-17 09:52:17', '2023-01-17 15:51:49'),
('9d04efee-9676-11ed-979a-244bfe7dd4fe', '4', 'QC', '0', '1', '0', '0', '1', '0', '1', '0', '0', '2023-01-17 09:52:43', '2023-01-17 15:52:19'),
('ac4d81c2-9676-11ed-979a-244bfe7dd4fe', '5', 'Guest', '0', '1', '0', '0', '0', '0', '1', '0', '0', '2023-01-17 09:53:08', '2023-01-17 15:52:46');

-- --------------------------------------------------------

--
-- Table structure for table `appusers`
--

CREATE TABLE `appusers` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `isActiveFlag` char(1) DEFAULT NULL,
  `readOnlyFlag` char(1) DEFAULT NULL,
  `userRole` char(1) DEFAULT NULL,
  `createdDate` datetime DEFAULT current_timestamp(),
  `lastModifiedDate` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `arm`
--

CREATE TABLE `arm` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `studyid` char(36) DEFAULT NULL,
  `armOrder` char(2) DEFAULT NULL,
  `createdDate` datetime DEFAULT current_timestamp(),
  `lastModifiedDate` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `isRemovedFlag` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `arm_history`
--

CREATE TABLE `arm_history` (
  `id` char(36) NOT NULL,
  `action` char(8) DEFAULT NULL,
  `modifiedByAppUserid` char(36) NOT NULL,
  `armid` char(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `studyid` char(36) DEFAULT NULL,
  `armOrder` char(2) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `isRemovedFlag` char(1) DEFAULT NULL,
  `copiedFromid` char(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `cro`
--

CREATE TABLE `cro` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `createdDate` datetime DEFAULT current_timestamp(),
  `lastModifiedDate` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `isRemovedFlag` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `cro_history`
--

CREATE TABLE `cro_history` (
  `id` char(36) NOT NULL,
  `action` char(8) DEFAULT NULL,
  `modifiedByAppUserid` char(36) NOT NULL,
  `croid` char(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `isRemovedFlag` char(1) DEFAULT NULL,
  `copiedFromid` char(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `crossformlinking`
--

CREATE TABLE `crossformlinking` (
  `id` char(36) NOT NULL,
  `studyid` char(36) DEFAULT NULL,
  `parentFormid` char(36) DEFAULT NULL,
  `parentFieldVerid` char(36) DEFAULT NULL,
  `childFormid` char(36) DEFAULT NULL,
  `childFieldVerid` char(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT current_timestamp(),
  `lastModifiedDate` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `isRemovedFlag` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `crossformlinking_history`
--

CREATE TABLE `crossformlinking_history` (
  `id` char(36) NOT NULL,
  `action` char(8) DEFAULT NULL,
  `modifiedByAppUserid` char(36) NOT NULL,
  `crossFormLinkingid` char(36) DEFAULT NULL,
  `studyid` char(36) DEFAULT NULL,
  `parentFormid` char(36) DEFAULT NULL,
  `parentFieldVerid` char(36) DEFAULT NULL,
  `childFormid` char(36) DEFAULT NULL,
  `childFieldVerid` char(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `isRemovedFlag` char(1) DEFAULT NULL,
  `copiedFromid` char(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `editqueue`
--

CREATE TABLE `editqueue` (
  `id` char(36) NOT NULL,
  `tableName` char(20) DEFAULT NULL,
  `rowid` char(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT current_timestamp(),
  `userid` char(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `field`
--

CREATE TABLE `field` (
  `id` char(36) NOT NULL,
  `sectionTemplateid` char(36) DEFAULT NULL,
  `ciFieldOrderNum` int(11) DEFAULT NULL,
  `createdDate` datetime DEFAULT current_timestamp(),
  `lastModifiedDate` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `isIntegration` char(1) DEFAULT NULL,
  `isRemovedFlag` char(1) DEFAULT NULL,
  `libraryStatus` char(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `fieldversion`
--

CREATE TABLE `fieldversion` (
  `id` char(36) NOT NULL,
  `fieldid` char(36) DEFAULT NULL,
  `ciFieldName` varchar(40) DEFAULT NULL,
  `versionNum` int(11) DEFAULT NULL,
  `ciFriendlyName` text DEFAULT NULL,
  `ciMappingName` varchar(255) DEFAULT NULL,
  `ciFieldType` varchar(255) DEFAULT NULL,
  `ciLabelText` text DEFAULT NULL,
  `ciValues` text DEFAULT NULL,
  `ciRange` text DEFAULT NULL,
  `ciLogic` text DEFAULT NULL,
  `isRequiredFlag` char(1) DEFAULT NULL,
  `ciValidation` text DEFAULT NULL,
  `hasToolTipFlag` char(1) DEFAULT NULL,
  `ciValidationMessage` text DEFAULT NULL,
  `cidesignInstructions` text DEFAULT NULL,
  `cidMInstructions` text DEFAULT NULL,
  `ciCustomField1` text DEFAULT NULL,
  `reqFail` text DEFAULT NULL,
  `createdDate` datetime DEFAULT current_timestamp(),
  `createdByAppUserid` char(36) NOT NULL,
  `lastModifiedDate` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `lastModifiedByAppUserid` char(36) NOT NULL,
  `latestFlag` char(1) DEFAULT NULL,
  `isRemovedFlag` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `field_history`
--

CREATE TABLE `field_history` (
  `id` char(36) NOT NULL,
  `action` char(8) DEFAULT NULL,
  `modifiedByAppUserid` char(36) NOT NULL,
  `fieldid` char(36) DEFAULT NULL,
  `sectionTemplateid` char(36) DEFAULT NULL,
  `ciFieldOrderNum` int(11) DEFAULT NULL,
  `ciRemovedFlag` char(1) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `isIntegration` char(1) DEFAULT NULL,
  `isRemovedFlag` char(1) DEFAULT NULL,
  `copiedFromid` char(36) DEFAULT NULL,
  `libraryStatus` char(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `form`
--

CREATE TABLE `form` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `visitid` char(36) DEFAULT NULL,
  `orderInVisit` int(11) DEFAULT NULL,
  `formTemplateid` char(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT current_timestamp(),
  `lastModifiedDate` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `isRemovedFlag` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `formtemplate`
--

CREATE TABLE `formtemplate` (
  `id` char(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `studyid` char(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT current_timestamp(),
  `lastModifiedDate` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `isRemovedFlag` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `formtemplate_history`
--

CREATE TABLE `formtemplate_history` (
  `id` char(36) NOT NULL,
  `action` char(8) DEFAULT NULL,
  `modifiedByAppUserid` char(36) NOT NULL,
  `formTemplateid` char(36) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `studyid` char(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `isRemovedFlag` char(1) DEFAULT NULL,
  `copiedFromid` char(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `formtrigger`
--

CREATE TABLE `formtrigger` (
  `id` char(36) NOT NULL,
  `studyid` char(36) DEFAULT NULL,
  `triggerSourceType` varchar(5) DEFAULT NULL,
  `sourceFormVisitLinkid` char(36) DEFAULT NULL,
  `sourceTriggerFieldid` char(36) DEFAULT NULL,
  `triggerFormid` char(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT current_timestamp(),
  `lastModifiedDate` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `isRemovedFlag` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `formtrigger_history`
--

CREATE TABLE `formtrigger_history` (
  `id` char(36) NOT NULL,
  `action` char(8) DEFAULT NULL,
  `modifiedByAppUserid` char(36) NOT NULL,
  `formTriggerid` char(36) DEFAULT NULL,
  `studyid` char(36) DEFAULT NULL,
  `triggerSourceType` varchar(5) DEFAULT NULL,
  `sourceFormVisitLinkid` char(36) DEFAULT NULL,
  `sourceTriggerFieldid` char(36) DEFAULT NULL,
  `triggerFormid` char(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `isRemovedFlag` char(1) DEFAULT NULL,
  `copiedFromid` char(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `form_history`
--

CREATE TABLE `form_history` (
  `id` char(36) NOT NULL,
  `action` char(8) DEFAULT NULL,
  `modifiedByAppUserid` char(36) NOT NULL,
  `formid` char(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `visitid` char(36) DEFAULT NULL,
  `orderInVisit` int(11) DEFAULT NULL,
  `formTemplateid` char(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `isRemovedFlag` char(1) DEFAULT NULL,
  `copiedFromid` char(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `qualityfinding`
--

CREATE TABLE `qualityfinding` (
  `id` char(36) NOT NULL,
  `fieldid` char(36) DEFAULT NULL,
  `qTestid` char(36) DEFAULT NULL,
  `failDescription` text DEFAULT NULL,
  `failStatus` varchar(100) DEFAULT NULL,
  `createdDate` datetime DEFAULT current_timestamp(),
  `lastModifiedDate` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `qualityfinding_history`
--

CREATE TABLE `qualityfinding_history` (
  `id` char(36) NOT NULL,
  `action` char(8) DEFAULT NULL,
  `modifiedByAppUserid` char(36) NOT NULL,
  `qualityFindingid` char(36) DEFAULT NULL,
  `fieldid` char(36) DEFAULT NULL,
  `qTestid` char(36) DEFAULT NULL,
  `failDescription` text DEFAULT NULL,
  `failStatus` varchar(100) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sectiongroup`
--

CREATE TABLE `sectiongroup` (
  `id` char(36) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `uniqueSectionGroupHeader` varchar(255) DEFAULT NULL,
  `studyid` char(36) DEFAULT NULL,
  `sectionGroupPrefix` varchar(10) DEFAULT NULL,
  `createdDate` datetime DEFAULT current_timestamp(),
  `lastModifiedDate` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `isRemovedFlag` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sectiongrouplink`
--

CREATE TABLE `sectiongrouplink` (
  `id` char(36) NOT NULL,
  `sectionTemplateid` char(36) DEFAULT NULL,
  `sectionGroupid` char(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT current_timestamp(),
  `lastModifiedDate` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `isRemovedFlag` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sectiongrouplink_history`
--

CREATE TABLE `sectiongrouplink_history` (
  `id` char(36) NOT NULL,
  `action` char(8) DEFAULT NULL,
  `modifiedByAppUserid` char(36) NOT NULL,
  `sectionGroupLinkid` char(36) DEFAULT NULL,
  `sectionTemplateid` char(36) DEFAULT NULL,
  `sectionGroupid` char(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT current_timestamp(),
  `lastModifiedDate` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `isRemovedFlag` char(1) DEFAULT NULL,
  `copiedFromid` char(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sectiongroup_history`
--

CREATE TABLE `sectiongroup_history` (
  `id` char(36) NOT NULL,
  `action` char(8) DEFAULT NULL,
  `modifiedByAppUserid` char(36) NOT NULL,
  `sectionGroupid` char(36) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `uniqueSectionGroupHeader` varchar(255) DEFAULT NULL,
  `studyid` char(36) DEFAULT NULL,
  `sectionGroupPrefix` varchar(10) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `isRemovedFlag` char(1) DEFAULT NULL,
  `copiedFromid` char(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sectiontempformlink`
--

CREATE TABLE `sectiontempformlink` (
  `id` char(36) NOT NULL,
  `orderInForm` int(11) DEFAULT NULL,
  `sectionTemplateid` char(36) DEFAULT NULL,
  `formTemplateid` char(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT current_timestamp(),
  `lastModifiedDate` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `isRemovedFlag` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sectiontempformlink_history`
--

CREATE TABLE `sectiontempformlink_history` (
  `id` char(36) NOT NULL,
  `action` char(8) DEFAULT NULL,
  `modifiedByAppUserid` char(36) NOT NULL,
  `sectionTempFormLinkid` char(36) DEFAULT NULL,
  `orderInForm` int(11) DEFAULT NULL,
  `sectionTemplateid` char(36) DEFAULT NULL,
  `formTemplateid` char(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `isRemovedFlag` char(1) DEFAULT NULL,
  `copiedFromid` char(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sectiontemplate`
--

CREATE TABLE `sectiontemplate` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `uniqueSectionHeader` varchar(255) DEFAULT NULL,
  `copiedFlag` char(1) DEFAULT NULL,
  `copiedUserid` char(36) DEFAULT NULL,
  `copiedFromStudyid` char(36) DEFAULT NULL,
  `copiedFromSectionGroupid` char(36) DEFAULT NULL,
  `copiedFromSectionTemplateid` char(36) DEFAULT NULL,
  `templateModifiedFlag` char(1) DEFAULT NULL,
  `dateTimeCopied` datetime DEFAULT NULL,
  `sectionGeneratorConfig` text DEFAULT NULL,
  `uniqueFieldPrefix` varchar(10) DEFAULT NULL,
  `createdDate` datetime DEFAULT current_timestamp(),
  `lastModifiedDate` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `hasCrossFormFlag` char(1) DEFAULT NULL,
  `hasTriggerFlag` char(1) DEFAULT NULL,
  `isRemovedFlag` char(1) DEFAULT NULL,
  `libraryStatus` char(10) DEFAULT NULL,
  `salesforceRecordid` char(18) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sectiontemplate_history`
--

CREATE TABLE `sectiontemplate_history` (
  `id` char(36) NOT NULL,
  `action` char(8) DEFAULT NULL,
  `modifiedByAppUserid` char(36) NOT NULL,
  `sectionTemplateid` char(36) DEFAULT NULL,
  `sectionGroupid` char(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `uniqueSectionHeader` varchar(255) DEFAULT NULL,
  `copiedFlag` char(1) DEFAULT NULL,
  `copiedUserid` char(36) DEFAULT NULL,
  `copiedFromStudyid` char(36) DEFAULT NULL,
  `copiedFromSectionGroupid` char(36) DEFAULT NULL,
  `copiedFromSectionTemplateid` char(36) DEFAULT NULL,
  `templateModifiedFlag` char(1) DEFAULT NULL,
  `dateTimeCopied` datetime DEFAULT NULL,
  `sectionGeneratorConfig` text DEFAULT NULL,
  `uniqueFieldPrefix` varchar(10) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `hasCrossFormFlag` char(1) DEFAULT NULL,
  `hasTriggerFlag` char(1) DEFAULT NULL,
  `isRemovedFlag` char(1) DEFAULT NULL,
  `copiedFromid` char(36) DEFAULT NULL,
  `libraryStatus` char(10) DEFAULT NULL,
  `salesforceRecordid` char(18) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sponsor`
--

CREATE TABLE `sponsor` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `createdDate` datetime DEFAULT current_timestamp(),
  `lastModifiedDate` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `isRemovedFlag` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sponsor_history`
--

CREATE TABLE `sponsor_history` (
  `id` char(36) NOT NULL,
  `action` char(8) DEFAULT NULL,
  `modifiedByAppUserid` char(36) NOT NULL,
  `sponsorid` char(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `isRemovedFlag` char(1) DEFAULT NULL,
  `copiedFromid` char(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `study`
--

CREATE TABLE `study` (
  `id` char(36) NOT NULL,
  `ciStudyProjectCode` char(20) DEFAULT NULL,
  `protocolName` varchar(255) DEFAULT NULL,
  `sponsorid` char(36) DEFAULT NULL,
  `croid` char(36) DEFAULT NULL,
  `libraryFlag` char(1) DEFAULT NULL,
  `createdDate` datetime DEFAULT current_timestamp(),
  `lastModifiedDate` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `studyType` char(10) DEFAULT NULL,
  `studyPhaseFlag` char(1) DEFAULT NULL,
  `isLegacyFlag` char(1) DEFAULT NULL,
  `isRemovedFlag` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `study_history`
--

CREATE TABLE `study_history` (
  `id` char(36) NOT NULL,
  `action` char(8) DEFAULT NULL,
  `modifiedByAppUserid` char(36) NOT NULL,
  `studyid` char(36) DEFAULT NULL,
  `ciStudyProjectCode` char(20) DEFAULT NULL,
  `protocolName` varchar(255) DEFAULT NULL,
  `sponsorid` char(36) DEFAULT NULL,
  `croid` char(36) DEFAULT NULL,
  `libraryFlag` char(1) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `studyType` char(10) DEFAULT NULL,
  `studyPhaseFlag` char(1) DEFAULT NULL,
  `isLegacyFlag` char(1) DEFAULT NULL,
  `isRemovedFlag` char(1) DEFAULT NULL,
  `copiedFromid` char(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `visit`
--

CREATE TABLE `visit` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `armid` char(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT current_timestamp(),
  `lastModifiedDate` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `visitOrder` int(11) DEFAULT NULL,
  `visitType` char(1) DEFAULT NULL,
  `filemakerVisitNote` text DEFAULT NULL,
  `isRemovedFlag` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `visittrigger`
--

CREATE TABLE `visittrigger` (
  `id` char(36) NOT NULL,
  `studyid` char(36) DEFAULT NULL,
  `triggerSourceType` varchar(5) DEFAULT NULL,
  `sourceFormVisitLinkid` char(36) DEFAULT NULL,
  `sourceTriggerFieldid` char(36) DEFAULT NULL,
  `triggerVisitid` char(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT current_timestamp(),
  `lastModifiedDate` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `isRemovedFlag` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `visittrigger_history`
--

CREATE TABLE `visittrigger_history` (
  `id` char(36) NOT NULL,
  `action` char(8) DEFAULT NULL,
  `modifiedByAppUserid` char(36) NOT NULL,
  `visitTriggerid` char(36) DEFAULT NULL,
  `studyid` char(36) DEFAULT NULL,
  `triggerSourceType` varchar(5) DEFAULT NULL,
  `sourceFormVisitLinkid` char(36) DEFAULT NULL,
  `sourceTriggerFieldid` char(36) DEFAULT NULL,
  `triggerVisitid` char(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `isRemovedFlag` char(1) DEFAULT NULL,
  `copiedFromid` char(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `visit_history`
--

CREATE TABLE `visit_history` (
  `id` char(36) NOT NULL,
  `action` char(8) DEFAULT NULL,
  `modifiedByAppUserid` char(36) NOT NULL,
  `visitid` char(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `armid` char(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `visitOrder` int(11) DEFAULT NULL,
  `visitType` char(1) DEFAULT NULL,
  `filemakerVisitNote` text DEFAULT NULL,
  `isRemovedFlag` char(1) DEFAULT NULL,
  `copiedFromid` char(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `apiusers`
--
ALTER TABLE `apiusers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `approles`
--
ALTER TABLE `approles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `appusers`
--
ALTER TABLE `appusers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `arm`
--
ALTER TABLE `arm`
  ADD PRIMARY KEY (`id`),
  ADD KEY `studyid` (`studyid`);

--
-- Indexes for table `arm_history`
--
ALTER TABLE `arm_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `arm_history_ibfk_1` (`modifiedByAppUserid`) USING BTREE,
  ADD KEY `armid` (`armid`),
  ADD KEY `studyid` (`studyid`);

--
-- Indexes for table `cro`
--
ALTER TABLE `cro`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crossformlinking`
--
ALTER TABLE `crossformlinking`
  ADD PRIMARY KEY (`id`),
  ADD KEY `studyid` (`studyid`),
  ADD KEY `parentFormid` (`parentFormid`),
  ADD KEY `parentFieldVerid` (`parentFieldVerid`),
  ADD KEY `childFormid` (`childFormid`),
  ADD KEY `childFieldVerid` (`childFieldVerid`);

--
-- Indexes for table `crossformlinking_history`
--
ALTER TABLE `crossformlinking_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `crossformlinking_history_ibfk_1` (`modifiedByAppUserid`),
  ADD KEY `crossFormLinkingid` (`crossFormLinkingid`),
  ADD KEY `parentFormid` (`parentFormid`),
  ADD KEY `parentFieldVerid` (`parentFieldVerid`),
  ADD KEY `childFormid` (`childFormid`),
  ADD KEY `childFieldVerid` (`childFieldVerid`),
  ADD KEY `studyid` (`studyid`);

--
-- Indexes for table `cro_history`
--
ALTER TABLE `cro_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cro_ibkf_1` (`modifiedByAppUserid`),
  ADD KEY `croid` (`croid`);

--
-- Indexes for table `editqueue`
--
ALTER TABLE `editqueue`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `field`
--
ALTER TABLE `field`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sectionTemplateid` (`sectionTemplateid`);

--
-- Indexes for table `fieldversion`
--
ALTER TABLE `fieldversion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fieldid` (`fieldid`),
  ADD KEY `fieldversion_ibfk_2` (`createdByAppUserid`),
  ADD KEY `fieldversion_ibfk_3` (`lastModifiedByAppUserid`);

--
-- Indexes for table `field_history`
--
ALTER TABLE `field_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `field_history_ibfk_1` (`modifiedByAppUserid`),
  ADD KEY `fieldid` (`fieldid`),
  ADD KEY `sectionTemplateid` (`sectionTemplateid`);

--
-- Indexes for table `form`
--
ALTER TABLE `form`
  ADD PRIMARY KEY (`id`),
  ADD KEY `visitid` (`visitid`),
  ADD KEY `formTemplateid` (`formTemplateid`);

--
-- Indexes for table `formtemplate`
--
ALTER TABLE `formtemplate`
  ADD PRIMARY KEY (`id`),
  ADD KEY `studyid` (`studyid`);

--
-- Indexes for table `formtemplate_history`
--
ALTER TABLE `formtemplate_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `formtemplate_history_ibfk_1` (`modifiedByAppUserid`),
  ADD KEY `studyid` (`studyid`),
  ADD KEY `formTemplateid` (`formTemplateid`);

--
-- Indexes for table `formtrigger`
--
ALTER TABLE `formtrigger`
  ADD PRIMARY KEY (`id`),
  ADD KEY `studyid` (`studyid`),
  ADD KEY `sourceFormVisitLinkid` (`sourceFormVisitLinkid`),
  ADD KEY `sourceTriggerFieldid` (`sourceTriggerFieldid`),
  ADD KEY `triggerFormid` (`triggerFormid`);

--
-- Indexes for table `formtrigger_history`
--
ALTER TABLE `formtrigger_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `formtrigger_history_ibfk_1` (`modifiedByAppUserid`),
  ADD KEY `studyid` (`studyid`),
  ADD KEY `sourceFormVisitLinkid` (`sourceFormVisitLinkid`),
  ADD KEY `sourceTriggerFieldid` (`sourceTriggerFieldid`),
  ADD KEY `triggerFormid` (`triggerFormid`),
  ADD KEY `formTriggerid` (`formTriggerid`);

--
-- Indexes for table `form_history`
--
ALTER TABLE `form_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `formid` (`formid`),
  ADD KEY `visitid` (`visitid`),
  ADD KEY `formTemplateid` (`formTemplateid`);

--
-- Indexes for table `qualityfinding`
--
ALTER TABLE `qualityfinding`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fieldid` (`fieldid`);

--
-- Indexes for table `qualityfinding_history`
--
ALTER TABLE `qualityfinding_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `qualityfinding_history_ibfk_1` (`modifiedByAppUserid`),
  ADD KEY `qualityFindingid` (`qualityFindingid`),
  ADD KEY `fieldid` (`fieldid`),
  ADD KEY `qTestid` (`qTestid`);

--
-- Indexes for table `sectiongroup`
--
ALTER TABLE `sectiongroup`
  ADD PRIMARY KEY (`id`),
  ADD KEY `studyid` (`studyid`);

--
-- Indexes for table `sectiongrouplink`
--
ALTER TABLE `sectiongrouplink`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sectionTemplateid` (`sectionTemplateid`),
  ADD KEY `sectionGroupid` (`sectionGroupid`);

--
-- Indexes for table `sectiongrouplink_history`
--
ALTER TABLE `sectiongrouplink_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sectiongrouplink_history_ibfk_1` (`modifiedByAppUserid`),
  ADD KEY `sectionGroupLinkid` (`sectionGroupLinkid`),
  ADD KEY `sectionTemplateid` (`sectionTemplateid`),
  ADD KEY `sectionGroupid` (`sectionGroupid`);

--
-- Indexes for table `sectiongroup_history`
--
ALTER TABLE `sectiongroup_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sectionGroupid` (`sectionGroupid`),
  ADD KEY `studyid` (`studyid`);

--
-- Indexes for table `sectiontempformlink`
--
ALTER TABLE `sectiontempformlink`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sectionTemplateid` (`sectionTemplateid`),
  ADD KEY `formTemplateid` (`formTemplateid`);

--
-- Indexes for table `sectiontempformlink_history`
--
ALTER TABLE `sectiontempformlink_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sectiontempformlink_history_ibfk_1` (`modifiedByAppUserid`),
  ADD KEY `sectionTempFormLinkid` (`sectionTempFormLinkid`),
  ADD KEY `sectionTemplateid` (`sectionTemplateid`),
  ADD KEY `formTemplateid` (`formTemplateid`);

--
-- Indexes for table `sectiontemplate`
--
ALTER TABLE `sectiontemplate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sectiontemplate_history`
--
ALTER TABLE `sectiontemplate_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sectiontemplate_history_ibfk_1` (`modifiedByAppUserid`),
  ADD KEY `sectionTemplateid` (`sectionTemplateid`);

--
-- Indexes for table `sponsor`
--
ALTER TABLE `sponsor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sponsor_history`
--
ALTER TABLE `sponsor_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sponsor_history_ibfk_1` (`modifiedByAppUserid`),
  ADD KEY `sponsorid` (`sponsorid`);

--
-- Indexes for table `study`
--
ALTER TABLE `study`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sponsorid` (`sponsorid`),
  ADD KEY `croid` (`croid`);

--
-- Indexes for table `study_history`
--
ALTER TABLE `study_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `study_history_ibfk_1` (`modifiedByAppUserid`),
  ADD KEY `studyid` (`studyid`),
  ADD KEY `sponsorid` (`sponsorid`),
  ADD KEY `croid` (`croid`);

--
-- Indexes for table `visit`
--
ALTER TABLE `visit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `armid` (`armid`);

--
-- Indexes for table `visittrigger`
--
ALTER TABLE `visittrigger`
  ADD PRIMARY KEY (`id`),
  ADD KEY `studyid` (`studyid`),
  ADD KEY `sourceFormVisitLinkid` (`sourceFormVisitLinkid`),
  ADD KEY `sourceTriggerFieldid` (`sourceTriggerFieldid`),
  ADD KEY `triggerVisitid` (`triggerVisitid`);

--
-- Indexes for table `visittrigger_history`
--
ALTER TABLE `visittrigger_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `visittrigger_history_ibfk_1` (`modifiedByAppUserid`),
  ADD KEY `visitTriggerid` (`visitTriggerid`),
  ADD KEY `studyid` (`studyid`),
  ADD KEY `sourceFormVisitLinkid` (`sourceFormVisitLinkid`),
  ADD KEY `sourceTriggerFieldid` (`sourceTriggerFieldid`),
  ADD KEY `triggerVisitid` (`triggerVisitid`);

--
-- Indexes for table `visit_history`
--
ALTER TABLE `visit_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `visit_history_ibfk_1` (`modifiedByAppUserid`),
  ADD KEY `visitid` (`visitid`),
  ADD KEY `armid` (`armid`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `arm`
--
ALTER TABLE `arm`
  ADD CONSTRAINT `arm_ibfk_1` FOREIGN KEY (`studyid`) REFERENCES `study` (`id`);

--
-- Constraints for table `arm_history`
--
ALTER TABLE `arm_history`
  ADD CONSTRAINT `arm_history_ibfk_1` FOREIGN KEY (`modifiedByAppUserid`) REFERENCES `appusers` (`id`);

--
-- Constraints for table `crossformlinking`
--
ALTER TABLE `crossformlinking`
  ADD CONSTRAINT `crossformlinking_ibfk_1` FOREIGN KEY (`studyid`) REFERENCES `study` (`id`),
  ADD CONSTRAINT `crossformlinking_ibfk_2` FOREIGN KEY (`parentFormid`) REFERENCES `form` (`id`),
  ADD CONSTRAINT `crossformlinking_ibfk_3` FOREIGN KEY (`parentFieldVerid`) REFERENCES `fieldversion` (`id`),
  ADD CONSTRAINT `crossformlinking_ibfk_4` FOREIGN KEY (`childFormid`) REFERENCES `form` (`id`),
  ADD CONSTRAINT `crossformlinking_ibfk_5` FOREIGN KEY (`childFieldVerid`) REFERENCES `fieldversion` (`id`);

--
-- Constraints for table `crossformlinking_history`
--
ALTER TABLE `crossformlinking_history`
  ADD CONSTRAINT `crossformlinking_history_ibfk_1` FOREIGN KEY (`modifiedByAppUserid`) REFERENCES `appusers` (`id`);

--
-- Constraints for table `cro_history`
--
ALTER TABLE `cro_history`
  ADD CONSTRAINT `cro_ibkf_1` FOREIGN KEY (`modifiedByAppUserid`) REFERENCES `appusers` (`id`);

--
-- Constraints for table `field`
--
ALTER TABLE `field`
  ADD CONSTRAINT `field_ibfk_1` FOREIGN KEY (`sectionTemplateid`) REFERENCES `sectiontemplate` (`id`);

--
-- Constraints for table `fieldversion`
--
ALTER TABLE `fieldversion`
  ADD CONSTRAINT `fieldversion_ibfk_1` FOREIGN KEY (`fieldid`) REFERENCES `field` (`id`),
  ADD CONSTRAINT `fieldversion_ibfk_2` FOREIGN KEY (`createdByAppUserid`) REFERENCES `appusers` (`id`),
  ADD CONSTRAINT `fieldversion_ibfk_3` FOREIGN KEY (`lastModifiedByAppUserid`) REFERENCES `appusers` (`id`);

--
-- Constraints for table `field_history`
--
ALTER TABLE `field_history`
  ADD CONSTRAINT `field_history_ibfk_1` FOREIGN KEY (`modifiedByAppUserid`) REFERENCES `appusers` (`id`);

--
-- Constraints for table `form`
--
ALTER TABLE `form`
  ADD CONSTRAINT `form_ibfk_1` FOREIGN KEY (`visitid`) REFERENCES `visit` (`id`),
  ADD CONSTRAINT `form_ibfk_2` FOREIGN KEY (`formTemplateid`) REFERENCES `formtemplate` (`id`);

--
-- Constraints for table `formtemplate`
--
ALTER TABLE `formtemplate`
  ADD CONSTRAINT `formtemplate_ibfk_1` FOREIGN KEY (`studyid`) REFERENCES `study` (`id`);

--
-- Constraints for table `formtemplate_history`
--
ALTER TABLE `formtemplate_history`
  ADD CONSTRAINT `formtemplate_history_ibfk_1` FOREIGN KEY (`modifiedByAppUserid`) REFERENCES `appusers` (`id`);

--
-- Constraints for table `formtrigger`
--
ALTER TABLE `formtrigger`
  ADD CONSTRAINT `formtrigger_ibfk_1` FOREIGN KEY (`studyid`) REFERENCES `study` (`id`),
  ADD CONSTRAINT `formtrigger_ibfk_2` FOREIGN KEY (`sourceFormVisitLinkid`) REFERENCES `form` (`id`),
  ADD CONSTRAINT `formtrigger_ibfk_3` FOREIGN KEY (`sourceTriggerFieldid`) REFERENCES `fieldversion` (`id`),
  ADD CONSTRAINT `formtrigger_ibfk_4` FOREIGN KEY (`triggerFormid`) REFERENCES `form` (`id`);

--
-- Constraints for table `formtrigger_history`
--
ALTER TABLE `formtrigger_history`
  ADD CONSTRAINT `formtrigger_history_ibfk_1` FOREIGN KEY (`modifiedByAppUserid`) REFERENCES `appusers` (`id`);

--
-- Constraints for table `qualityfinding`
--
ALTER TABLE `qualityfinding`
  ADD CONSTRAINT `qualityfinding_ibfk_1` FOREIGN KEY (`fieldid`) REFERENCES `fieldversion` (`id`);

--
-- Constraints for table `qualityfinding_history`
--
ALTER TABLE `qualityfinding_history`
  ADD CONSTRAINT `qualityfinding_history_ibfk_1` FOREIGN KEY (`modifiedByAppUserid`) REFERENCES `appusers` (`id`);

--
-- Constraints for table `sectiongroup`
--
ALTER TABLE `sectiongroup`
  ADD CONSTRAINT `sectiongroup_ibfk_1` FOREIGN KEY (`studyid`) REFERENCES `study` (`id`);

--
-- Constraints for table `sectiongrouplink`
--
ALTER TABLE `sectiongrouplink`
  ADD CONSTRAINT `sectiongrouplink_ibfk_1` FOREIGN KEY (`sectionTemplateid`) REFERENCES `sectiontemplate` (`id`),
  ADD CONSTRAINT `sectiongrouplink_ibfk_2` FOREIGN KEY (`sectionGroupid`) REFERENCES `sectiongroup` (`id`);

--
-- Constraints for table `sectiongrouplink_history`
--
ALTER TABLE `sectiongrouplink_history`
  ADD CONSTRAINT `sectiongrouplink_history_ibfk_1` FOREIGN KEY (`modifiedByAppUserid`) REFERENCES `appusers` (`id`);

--
-- Constraints for table `sectiontempformlink`
--
ALTER TABLE `sectiontempformlink`
  ADD CONSTRAINT `sectiontempformlink_ibfk_1` FOREIGN KEY (`sectionTemplateid`) REFERENCES `sectiontemplate` (`id`),
  ADD CONSTRAINT `sectiontempformlink_ibfk_2` FOREIGN KEY (`formTemplateid`) REFERENCES `formtemplate` (`id`);

--
-- Constraints for table `sectiontempformlink_history`
--
ALTER TABLE `sectiontempformlink_history`
  ADD CONSTRAINT `sectiontempformlink_history_ibfk_1` FOREIGN KEY (`modifiedByAppUserid`) REFERENCES `appusers` (`id`);

--
-- Constraints for table `sectiontemplate_history`
--
ALTER TABLE `sectiontemplate_history`
  ADD CONSTRAINT `sectiontemplate_history_ibfk_1` FOREIGN KEY (`modifiedByAppUserid`) REFERENCES `appusers` (`id`);

--
-- Constraints for table `sponsor_history`
--
ALTER TABLE `sponsor_history`
  ADD CONSTRAINT `sponsor_history_ibfk_1` FOREIGN KEY (`modifiedByAppUserid`) REFERENCES `appusers` (`id`);

--
-- Constraints for table `study`
--
ALTER TABLE `study`
  ADD CONSTRAINT `study_ibfk_1` FOREIGN KEY (`sponsorid`) REFERENCES `sponsor` (`id`),
  ADD CONSTRAINT `study_ibfk_2` FOREIGN KEY (`croid`) REFERENCES `cro` (`id`);

--
-- Constraints for table `study_history`
--
ALTER TABLE `study_history`
  ADD CONSTRAINT `study_history_ibfk_1` FOREIGN KEY (`modifiedByAppUserid`) REFERENCES `appusers` (`id`);

--
-- Constraints for table `visit`
--
ALTER TABLE `visit`
  ADD CONSTRAINT `visit_ibfk_1` FOREIGN KEY (`armid`) REFERENCES `arm` (`id`);

--
-- Constraints for table `visittrigger`
--
ALTER TABLE `visittrigger`
  ADD CONSTRAINT `visittrigger_ibfk_1` FOREIGN KEY (`studyid`) REFERENCES `study` (`id`),
  ADD CONSTRAINT `visittrigger_ibfk_2` FOREIGN KEY (`sourceFormVisitLinkid`) REFERENCES `form` (`id`),
  ADD CONSTRAINT `visittrigger_ibfk_3` FOREIGN KEY (`sourceTriggerFieldid`) REFERENCES `fieldversion` (`id`),
  ADD CONSTRAINT `visittrigger_ibfk_4` FOREIGN KEY (`triggerVisitid`) REFERENCES `visit` (`id`);

--
-- Constraints for table `visittrigger_history`
--
ALTER TABLE `visittrigger_history`
  ADD CONSTRAINT `visittrigger_history_ibfk_1` FOREIGN KEY (`modifiedByAppUserid`) REFERENCES `appusers` (`id`);

--
-- Constraints for table `visit_history`
--
ALTER TABLE `visit_history`
  ADD CONSTRAINT `visit_history_ibfk_1` FOREIGN KEY (`modifiedByAppUserid`) REFERENCES `appusers` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
