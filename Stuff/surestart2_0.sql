-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 19, 2023 at 06:22 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `surestart2.0`
--

-- --------------------------------------------------------

--
-- Table structure for table `apiusers`
--

CREATE TABLE `apiusers` (
  `id` char(36) NOT NULL,
  `userName` varchar(40) DEFAULT NULL,
  `passwordHash` text DEFAULT NULL,
  `createdDate` datetime DEFAULT current_timestamp(),
  `lastModifiedDate` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `isRemovedFlag` char(1) DEFAULT NULL,
  `hasAccessFlag` char(1) DEFAULT NULL,
  `isAdminFlag` char(1) DEFAULT NULL,
  `dataServicesFlag` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `apiusers`
--

INSERT INTO `apiusers` (`id`, `userName`, `passwordHash`, `createdDate`, `lastModifiedDate`, `isRemovedFlag`, `hasAccessFlag`, `isAdminFlag`, `dataServicesFlag`) VALUES
('0cdb9994-8df2-11ed-9b7c-244bfe7dd4fe', 'admin', '$2y$10$BirTiZCNkVAJ8g9D6fK6SeQx4nT1n282iJ03IsspggzTM7kkvcL9a', '2023-01-06 13:43:18', '2023-01-06 14:26:53', '', '1', '1', '1'),
('42c45ddc-8dfa-11ed-9b7c-244bfe7dd4fe', 'dataServices_INT', '$2y$10$8S..2A0S0ZeCCQj/I9t0Yur9CElV31.cQyiBLwfXTrUJnoe2mWggW', '2023-01-06 14:42:05', NULL, '', '1', '0', '1'),
('485a183d-8df8-11ed-9b7c-244bfe7dd4fe', 'app_SureStart2_0', '$2y$10$tOi2owfc9tvo2IqYAc1cV.OKmuMsfyWFw4rWME7QyyKP6sFSZG4P2', '2023-01-06 14:27:55', NULL, '', '1', '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `approles`
--

CREATE TABLE `approles` (
  `id` char(36) NOT NULL,
  `userRole` char(1) DEFAULT NULL,
  `roleName` varchar(255) DEFAULT NULL,
  `editUsers_01` char(1) DEFAULT NULL,
  `exportRecords_02` char(1) DEFAULT NULL,
  `editRequirements_03` char(1) DEFAULT NULL,
  `studyMilestones_04` char(1) DEFAULT NULL,
  `studyMilestonesQC_05` char(1) DEFAULT NULL,
  `editReqQCMilestone_06` char(1) DEFAULT NULL,
  `viewUserNamesHistory_07` char(1) DEFAULT NULL,
  `bypassStatusWorkflow_08` char(1) DEFAULT NULL,
  `editQueue_09` char(1) DEFAULT NULL,
  `createdDate` datetime DEFAULT current_timestamp(),
  `lastModifiedDate` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `approles`
--

INSERT INTO `approles` (`id`, `userRole`, `roleName`, `editUsers_01`, `exportRecords_02`, `editRequirements_03`, `studyMilestones_04`, `studyMilestonesQC_05`, `editReqQCMilestone_06`, `viewUserNamesHistory_07`, `bypassStatusWorkflow_08`, `editQueue_09`, `createdDate`, `lastModifiedDate`) VALUES
('14e1536e-9676-11ed-979a-244bfe7dd4fe', '0', 'admin', '1', '1', '1', '1', '1', '0', '1', '1', '1', '2023-01-17 09:48:54', '2023-01-17 09:50:56'),
('6e67d95a-9676-11ed-979a-244bfe7dd4fe', '1', 'Manager', '0', '1', '1', '1', '1', '0', '1', '1', '1', '2023-01-17 09:51:24', '2023-01-17 15:50:59'),
('7bbc2610-9676-11ed-979a-244bfe7dd4fe', '2', 'Design', '0', '1', '1', '1', '0', '0', '1', '0', '0', '2023-01-17 09:51:47', '2023-01-17 15:51:27'),
('8daaba8e-9676-11ed-979a-244bfe7dd4fe', '3', 'eSource Dev', '0', '1', '0', '0', '0', '0', '1', '0', '0', '2023-01-17 09:52:17', '2023-01-17 15:51:49'),
('9d04efee-9676-11ed-979a-244bfe7dd4fe', '4', 'QC', '0', '1', '0', '0', '1', '0', '1', '0', '0', '2023-01-17 09:52:43', '2023-01-17 15:52:19'),
('ac4d81c2-9676-11ed-979a-244bfe7dd4fe', '5', 'Guest', '0', '1', '0', '0', '0', '0', '1', '0', '0', '2023-01-17 09:53:08', '2023-01-17 15:52:46');

-- --------------------------------------------------------

--
-- Table structure for table `appusers`
--

CREATE TABLE `appusers` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `isActiveFlag` char(1) DEFAULT NULL,
  `readOnlyFlag` char(1) DEFAULT NULL,
  `userRole` char(1) DEFAULT NULL,
  `createdDate` datetime DEFAULT current_timestamp(),
  `lastModifiedDate` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `appusers`
--

INSERT INTO `appusers` (`id`, `name`, `email`, `isActiveFlag`, `readOnlyFlag`, `userRole`, `createdDate`, `lastModifiedDate`) VALUES
('26d0c045-9803-11ed-979a-244bfe7dd4fe', 'Import Admin', 'ss2ImportAdmin@clinicalink.com', '0', '1', '0', '2023-01-19 09:11:09', '2023-01-19 15:10:27'),
('29eef7a3-a17f-11ed-8ede-0eac330eef6b', 'General Demo', 'general.demoss2@clinicalink.com', '1', NULL, '0', '2023-01-31 15:51:45', '2023-01-31 15:53:28'),
('337cdad7-a17f-11ed-8ede-0eac330eef6b', 'APP QC', 'app.QCss2@clinicalink.com', '1', NULL, '0', '2023-01-31 15:52:01', '2023-01-31 15:53:08'),
('3d92d4b6-980a-11ed-979a-244bfe7dd4fe', 'Postman Test', 'ss2PostManTest@clinicalink.com', '0', '1', '0', '2023-01-19 10:01:54', '2023-01-19 16:01:29'),
('9b8dcd69-a17f-11ed-8ede-0eac330eef6b', 'ROLE admin', 'generictestingacct@clinicalink.com', '1', '0', '0', '2023-01-31 15:54:56', '2023-01-31 15:56:26'),
('b3c81587-a17f-11ed-8ede-0eac330eef6b', 'ROLE Guest', 'generictestingacct5@clinicalink.com', '1', '1', '5', '2023-01-31 15:55:36', '2023-01-31 15:56:40'),
('b7b28097-a17f-11ed-8ede-0eac330eef6b', 'ROLE QC', 'generictestingacct4@clinicalink.com', '1', '0', '4', '2023-01-31 15:55:43', '2023-01-31 15:56:53'),
('bd781718-a17f-11ed-8ede-0eac330eef6b', 'ROLE eSource Dev', 'generictestingacct3@clinicalink.com', '1', '0', '3', '2023-01-31 15:55:53', '2023-01-31 15:57:05'),
('c266aa0f-a17f-11ed-8ede-0eac330eef6b', 'ROLE Design', 'generictestingacct2@clinicalink.com', '1', '0', '2', '2023-01-31 15:56:01', '2023-01-31 15:57:13'),
('c64ae1e2-a17f-11ed-8ede-0eac330eef6b', 'ROLE Manager', 'generictestingacct1@clinicalink.com', '1', '0', '1', '2023-01-31 15:56:07', '2023-01-31 15:57:25');

-- --------------------------------------------------------

--
-- Table structure for table `arm`
--

CREATE TABLE `arm` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `studyid` char(36) DEFAULT NULL,
  `armOrder` char(2) DEFAULT NULL,
  `createdDate` datetime DEFAULT current_timestamp(),
  `lastModifiedDate` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `isRemovedFlag` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `arm_history`
--

CREATE TABLE `arm_history` (
  `id` char(36) NOT NULL,
  `action` char(8) DEFAULT NULL,
  `modifiedByAppUserid` char(36) NOT NULL,
  `armid` char(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `studyid` char(36) DEFAULT NULL,
  `armOrder` char(2) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `isRemovedFlag` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `cro`
--

CREATE TABLE `cro` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `createdDate` datetime DEFAULT current_timestamp(),
  `lastModifiedDate` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `isRemovedFlag` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `crossformlinking`
--

CREATE TABLE `crossformlinking` (
  `id` char(36) NOT NULL,
  `studyid` char(36) DEFAULT NULL,
  `parentFormid` char(36) DEFAULT NULL,
  `parentFieldVerid` char(36) DEFAULT NULL,
  `childFormid` char(36) DEFAULT NULL,
  `childFieldVerid` char(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT current_timestamp(),
  `lastModifiedDate` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `isRemovedFlag` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `crossformlinking_history`
--

CREATE TABLE `crossformlinking_history` (
  `id` char(36) NOT NULL,
  `action` char(8) DEFAULT NULL,
  `modifiedByAppUserid` char(36) NOT NULL,
  `crossFormLinkingid` char(36) DEFAULT NULL,
  `studyid` char(36) DEFAULT NULL,
  `parentFormid` char(36) DEFAULT NULL,
  `parentFieldVerid` char(36) DEFAULT NULL,
  `childFormid` char(36) DEFAULT NULL,
  `childFieldVerid` char(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `isRemovedFlag` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `cro_history`
--

CREATE TABLE `cro_history` (
  `id` char(36) NOT NULL,
  `action` char(8) DEFAULT NULL,
  `modifiedByAppUserid` char(36) NOT NULL,
  `croid` char(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `isRemovedFlag` char(1) DEFAULT NULL,
  `userid` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `editqueue`
--

CREATE TABLE `editqueue` (
  `id` char(36) NOT NULL,
  `tableName` char(20) DEFAULT NULL,
  `rowid` char(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT current_timestamp(),
  `userid` char(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `field`
--

CREATE TABLE `field` (
  `id` char(36) NOT NULL,
  `sectionTemplateid` char(36) DEFAULT NULL,
  `ciFieldOrderNum` int(11) DEFAULT NULL,
  `createdDate` datetime DEFAULT current_timestamp(),
  `lastModifiedDate` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `isIntegration` char(1) DEFAULT NULL,
  `isRemovedFlag` char(1) DEFAULT NULL,
  `libraryStatus` char(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `fieldversion`
--

CREATE TABLE `fieldversion` (
  `id` char(36) NOT NULL,
  `fieldid` char(36) DEFAULT NULL,
  `ciFieldName` varchar(40) DEFAULT NULL,
  `versionNum` int(11) DEFAULT NULL,
  `ciFriendlyName` text DEFAULT NULL,
  `ciMappingName` varchar(255) DEFAULT NULL,
  `ciFieldType` varchar(255) DEFAULT NULL,
  `ciLabelText` text DEFAULT NULL,
  `ciValues` text DEFAULT NULL,
  `ciRange` text DEFAULT NULL,
  `ciLogic` text DEFAULT NULL,
  `isRequiredFlag` char(1) DEFAULT NULL,
  `ciValidation` text DEFAULT NULL,
  `hasToolTipFlag` char(1) DEFAULT NULL,
  `ciValidationMessage` text DEFAULT NULL,
  `cidesignInstructions` text DEFAULT NULL,
  `cidMInstructions` text DEFAULT NULL,
  `ciCustomField1` text DEFAULT NULL,
  `reqFail` text DEFAULT NULL,
  `createdDate` datetime DEFAULT current_timestamp(),
  `createdByAppUserid` char(36) NOT NULL,
  `lastModifiedDate` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `lastModifiedByAppUserid` char(36) NOT NULL,
  `latestFlag` char(1) DEFAULT NULL,
  `isRemovedFlag` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `field_history`
--

CREATE TABLE `field_history` (
  `id` char(36) NOT NULL,
  `action` char(8) DEFAULT NULL,
  `modifiedByAppUserid` char(36) NOT NULL,
  `fieldid` char(36) DEFAULT NULL,
  `sectionTemplateid` char(36) DEFAULT NULL,
  `ciFieldOrderNum` int(11) DEFAULT NULL,
  `ciRemovedFlag` char(1) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `isIntegration` char(1) DEFAULT NULL,
  `isRemovedFlag` char(1) DEFAULT NULL,
  `libraryStatus` char(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `form`
--

CREATE TABLE `form` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `visitid` char(36) DEFAULT NULL,
  `orderInVisit` int(11) DEFAULT NULL,
  `formTemplateid` char(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT current_timestamp(),
  `lastModifiedDate` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `isRemovedFlag` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `formtemplate`
--

CREATE TABLE `formtemplate` (
  `id` char(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `studyid` char(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT current_timestamp(),
  `lastModifiedDate` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `isRemovedFlag` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `formtemplate_history`
--

CREATE TABLE `formtemplate_history` (
  `id` char(36) NOT NULL,
  `action` char(8) DEFAULT NULL,
  `modifiedByAppUserid` char(36) NOT NULL,
  `formTemplateid` char(36) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `studyid` char(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `isRemovedFlag` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `formtrigger`
--

CREATE TABLE `formtrigger` (
  `id` char(36) NOT NULL,
  `studyid` char(36) DEFAULT NULL,
  `triggerSourceType` varchar(5) DEFAULT NULL,
  `sourceFormVisitLinkid` char(36) DEFAULT NULL,
  `sourceTriggerFieldid` char(36) DEFAULT NULL,
  `triggerFormid` char(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT current_timestamp(),
  `lastModifiedDate` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `isRemovedFlag` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `formtrigger_history`
--

CREATE TABLE `formtrigger_history` (
  `id` char(36) NOT NULL,
  `action` char(8) DEFAULT NULL,
  `modifiedByAppUserid` char(36) NOT NULL,
  `formTriggerid` char(36) DEFAULT NULL,
  `studyid` char(36) DEFAULT NULL,
  `triggerSourceType` varchar(5) DEFAULT NULL,
  `sourceFormVisitLinkid` char(36) DEFAULT NULL,
  `sourceTriggerFieldid` char(36) DEFAULT NULL,
  `triggerFormid` char(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `isRemovedFlag` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `form_history`
--

CREATE TABLE `form_history` (
  `id` char(36) NOT NULL,
  `action` char(8) DEFAULT NULL,
  `modifiedByAppUserid` char(36) NOT NULL,
  `formid` char(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `visitid` char(36) DEFAULT NULL,
  `orderInVisit` int(11) DEFAULT NULL,
  `formTemplateid` char(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `isRemovedFlag` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `qualityfinding`
--

CREATE TABLE `qualityfinding` (
  `id` char(36) NOT NULL,
  `fieldid` char(36) DEFAULT NULL,
  `qTestid` char(36) DEFAULT NULL,
  `failDescription` text DEFAULT NULL,
  `failStatus` varchar(100) DEFAULT NULL,
  `createdDate` datetime DEFAULT current_timestamp(),
  `lastModifiedDate` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `qualityfinding_history`
--

CREATE TABLE `qualityfinding_history` (
  `id` char(36) NOT NULL,
  `action` char(8) DEFAULT NULL,
  `modifiedByAppUserid` char(36) NOT NULL,
  `qualityFindingid` char(36) DEFAULT NULL,
  `fieldid` char(36) DEFAULT NULL,
  `qTestid` char(36) DEFAULT NULL,
  `failDescription` text DEFAULT NULL,
  `failStatus` varchar(100) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sectiongroup`
--

CREATE TABLE `sectiongroup` (
  `id` char(36) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `uniqueSectionGroupHeader` varchar(255) DEFAULT NULL,
  `studyid` char(36) DEFAULT NULL,
  `sectionGroupPrefix` varchar(10) DEFAULT NULL,
  `createdDate` datetime DEFAULT current_timestamp(),
  `lastModifiedDate` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `isRemovedFlag` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sectiongrouplink`
--

CREATE TABLE `sectiongrouplink` (
  `id` char(36) NOT NULL,
  `sectionTemplateid` char(36) DEFAULT NULL,
  `sectionGroupid` char(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT current_timestamp(),
  `lastModifiedDate` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `isRemovedFlag` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sectiongrouplink_history`
--

CREATE TABLE `sectiongrouplink_history` (
  `id` char(36) NOT NULL,
  `action` char(8) DEFAULT NULL,
  `modifiedByAppUserid` char(36) NOT NULL,
  `sectionGroupLinkid` char(36) DEFAULT NULL,
  `sectionTemplateid` char(36) DEFAULT NULL,
  `sectionGroupid` char(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT current_timestamp(),
  `lastModifiedDate` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `isRemovedFlag` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sectiongroup_history`
--

CREATE TABLE `sectiongroup_history` (
  `id` char(36) NOT NULL,
  `action` char(8) DEFAULT NULL,
  `modifiedByAppUserid` char(36) NOT NULL,
  `sectionGroupid` char(36) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `uniqueSectionGroupHeader` varchar(255) DEFAULT NULL,
  `studyid` char(36) DEFAULT NULL,
  `sectionGroupPrefix` varchar(10) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `isRemovedFlag` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sectiontempformlink`
--

CREATE TABLE `sectiontempformlink` (
  `id` char(36) NOT NULL,
  `orderInForm` int(11) DEFAULT NULL,
  `sectionTemplateid` char(36) DEFAULT NULL,
  `formTemplateid` char(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT current_timestamp(),
  `lastModifiedDate` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `isRemovedFlag` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sectiontempformlink_history`
--

CREATE TABLE `sectiontempformlink_history` (
  `id` char(36) NOT NULL,
  `action` char(8) DEFAULT NULL,
  `modifiedByAppUserid` char(36) NOT NULL,
  `sectionTempFormLinkid` char(36) DEFAULT NULL,
  `orderInForm` int(11) DEFAULT NULL,
  `sectionTemplateid` char(36) DEFAULT NULL,
  `formTemplateid` char(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `isRemovedFlag` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sectiontemplate`
--

CREATE TABLE `sectiontemplate` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `uniqueSectionHeader` varchar(255) DEFAULT NULL,
  `copiedFlag` char(1) DEFAULT NULL,
  `copiedUserid` char(36) DEFAULT NULL,
  `copiedFromStudyid` char(36) DEFAULT NULL,
  `copiedFromSectionGroupid` char(36) DEFAULT NULL,
  `copiedFromSectionTemplateid` char(36) DEFAULT NULL,
  `templateModifiedFlag` char(1) DEFAULT NULL,
  `dateTimeCopied` datetime DEFAULT NULL,
  `sectionGeneratorConfig` text DEFAULT NULL,
  `uniqueFieldPrefix` varchar(10) DEFAULT NULL,
  `createdDate` datetime DEFAULT current_timestamp(),
  `lastModifiedDate` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `hasCrossFormFlag` char(1) DEFAULT NULL,
  `hasTriggerFlag` char(1) DEFAULT NULL,
  `isRemovedFlag` char(1) DEFAULT NULL,
  `libraryStatus` char(10) DEFAULT NULL,
  `salesforceRecordid` char(18) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sectiontemplate_history`
--

CREATE TABLE `sectiontemplate_history` (
  `id` char(36) NOT NULL,
  `action` char(8) DEFAULT NULL,
  `modifiedByAppUserid` char(36) NOT NULL,
  `sectionTemplateid` char(36) DEFAULT NULL,
  `sectionGroupid` char(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `uniqueSectionHeader` varchar(255) DEFAULT NULL,
  `copiedFlag` char(1) DEFAULT NULL,
  `copiedUserid` char(36) DEFAULT NULL,
  `copiedFromStudyid` char(36) DEFAULT NULL,
  `copiedFromSectionGroupid` char(36) DEFAULT NULL,
  `copiedFromSectionTemplateid` char(36) DEFAULT NULL,
  `templateModifiedFlag` char(1) DEFAULT NULL,
  `dateTimeCopied` datetime DEFAULT NULL,
  `sectionGeneratorConfig` text DEFAULT NULL,
  `uniqueFieldPrefix` varchar(10) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `hasCrossFormFlag` char(1) DEFAULT NULL,
  `hasTriggerFlag` char(1) DEFAULT NULL,
  `isRemovedFlag` char(1) DEFAULT NULL,
  `libraryStatus` char(10) DEFAULT NULL,
  `salesforceRecordid` char(18) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sponsor`
--

CREATE TABLE `sponsor` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `createdDate` datetime DEFAULT current_timestamp(),
  `lastModifiedDate` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `isRemovedFlag` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sponsor`
--

INSERT INTO `sponsor` (`id`, `name`, `createdDate`, `lastModifiedDate`, `isRemovedFlag`) VALUES
('ebd7748e-981d-11ed-979a-244bfe7dd4fe', '4D Pharma Plc', '2023-01-19 12:22:47', NULL, NULL),
('ebd8256a-981d-11ed-979a-244bfe7dd4fe', 'Abbott Labs', '2023-01-19 12:22:47', NULL, NULL),
('ebd922c0-981d-11ed-979a-244bfe7dd4fe', 'Actinogen Medical Ltd.', '2023-01-19 12:22:47', NULL, NULL),
('ebd9f15b-981d-11ed-979a-244bfe7dd4fe', 'Acumen Pharmaceuticals, Inc.', '2023-01-19 12:22:47', NULL, NULL),
('ebda59d8-981d-11ed-979a-244bfe7dd4fe', 'Alkermes, Inc.', '2023-01-19 12:22:47', NULL, NULL),
('ebdaca32-981d-11ed-979a-244bfe7dd4fe', 'Allergan Inc.', '2023-01-19 12:22:47', NULL, NULL),
('ebdb3167-981d-11ed-979a-244bfe7dd4fe', 'Alvotech Swiss AG', '2023-01-19 12:22:47', NULL, NULL),
('ebdb9a57-981d-11ed-979a-244bfe7dd4fe', 'Alyse&#039;s Training', '2023-01-19 12:22:47', NULL, NULL),
('ebdc02b9-981d-11ed-979a-244bfe7dd4fe', 'Amgen Inc.', '2023-01-19 12:22:47', NULL, NULL),
('ebdc6931-981d-11ed-979a-244bfe7dd4fe', 'AnaptysBio Inc.', '2023-01-19 12:22:47', NULL, NULL),
('ebdccf3c-981d-11ed-979a-244bfe7dd4fe', 'Angelini Pharma', '2023-01-19 12:22:47', NULL, NULL),
('ebdd3968-981d-11ed-979a-244bfe7dd4fe', 'AstraZeneca', '2023-01-19 12:22:47', NULL, NULL),
('ebdd9f29-981d-11ed-979a-244bfe7dd4fe', 'Avanir Pharmaceuticals, Inc.', '2023-01-19 12:22:47', NULL, NULL),
('ebde0d3a-981d-11ed-979a-244bfe7dd4fe', 'Baxalta', '2023-01-19 12:22:47', NULL, NULL),
('ebde7461-981d-11ed-979a-244bfe7dd4fe', 'Bayer', '2023-01-19 12:22:47', NULL, NULL),
('ebded62d-981d-11ed-979a-244bfe7dd4fe', 'Bionomics Ltd', '2023-01-19 12:22:47', NULL, NULL),
('ebdf4018-981d-11ed-979a-244bfe7dd4fe', 'BlackThorn Therapeutics, Inc.', '2023-01-19 12:22:47', NULL, NULL),
('ebdfa28d-981d-11ed-979a-244bfe7dd4fe', 'Boehringer Ingelheim', '2023-01-19 12:22:47', NULL, NULL),
('ebe0088e-981d-11ed-979a-244bfe7dd4fe', 'Calico Life Sciences LLC', '2023-01-19 12:22:47', NULL, NULL),
('ebe07010-981d-11ed-979a-244bfe7dd4fe', 'Camurus AB', '2023-01-19 12:22:47', NULL, NULL),
('ebe0d56d-981d-11ed-979a-244bfe7dd4fe', 'Celgene', '2023-01-19 12:22:47', NULL, NULL),
('ebe13e80-981d-11ed-979a-244bfe7dd4fe', 'Cerevance Beta, Inc.', '2023-01-19 12:22:47', NULL, NULL),
('ebe1a650-981d-11ed-979a-244bfe7dd4fe', 'Chong Kun Dang Pharmaceutical Corporation (CKD)', '2023-01-19 12:22:47', NULL, NULL),
('ebe207e3-981d-11ed-979a-244bfe7dd4fe', 'Cleveland Clinic', '2023-01-19 12:22:47', NULL, NULL),
('ebe26a18-981d-11ed-979a-244bfe7dd4fe', 'Clinical Ink', '2023-01-19 12:22:47', NULL, NULL),
('ebe2d009-981d-11ed-979a-244bfe7dd4fe', 'Clinical Ink - Data Management', '2023-01-19 12:22:47', NULL, NULL),
('ebe33130-981d-11ed-979a-244bfe7dd4fe', 'Clinical Ink Application Testing', '2023-01-19 12:22:47', NULL, NULL),
('ebe3921e-981d-11ed-979a-244bfe7dd4fe', 'Clinical Ink Demonstrations', '2023-01-19 12:22:47', NULL, NULL),
('ebe3f0bf-981d-11ed-979a-244bfe7dd4fe', 'Clinical Ink Protocol Services', '2023-01-19 12:22:47', NULL, NULL),
('ebe44c3a-981d-11ed-979a-244bfe7dd4fe', 'Clinical Ink Quality Department', '2023-01-19 12:22:47', NULL, NULL),
('ebe4a55f-981d-11ed-979a-244bfe7dd4fe', 'Clintara', '2023-01-19 12:22:47', NULL, NULL),
('ebe50803-981d-11ed-979a-244bfe7dd4fe', 'Cogstate', '2023-01-19 12:22:47', NULL, NULL),
('ebe56a02-981d-11ed-979a-244bfe7dd4fe', 'COMPASS Pathways, Ltd', '2023-01-19 12:22:47', NULL, NULL),
('ebe5c414-981d-11ed-979a-244bfe7dd4fe', 'Covis Pharma BV', '2023-01-19 12:22:47', NULL, NULL),
('ebe61cfb-981d-11ed-979a-244bfe7dd4fe', 'Cyclo Therapeutics', '2023-01-19 12:22:47', NULL, NULL),
('ebe677ed-981d-11ed-979a-244bfe7dd4fe', 'Danielle ImaginationLand Sponsor', '2023-01-19 12:22:47', NULL, NULL),
('ebe6d577-981d-11ed-979a-244bfe7dd4fe', 'Dart NeuroScience, LLC', '2023-01-19 12:22:47', NULL, NULL),
('ebe7307c-981d-11ed-979a-244bfe7dd4fe', 'Dartmouth', '2023-01-19 12:22:47', NULL, NULL),
('ebe79243-981d-11ed-979a-244bfe7dd4fe', 'Diamyd Medical AB', '2023-01-19 12:22:47', NULL, NULL),
('ebe7ef0d-981d-11ed-979a-244bfe7dd4fe', 'Dynacure', '2023-01-19 12:22:47', NULL, NULL),
('ebe850af-981d-11ed-979a-244bfe7dd4fe', 'EIP PHARMA, LLC', '2023-01-19 12:22:47', NULL, NULL),
('ebe8b1e8-981d-11ed-979a-244bfe7dd4fe', 'Eisai', '2023-01-19 12:22:47', NULL, NULL),
('ebe9144e-981d-11ed-979a-244bfe7dd4fe', 'Eli Lilly &amp; Company', '2023-01-19 12:22:47', NULL, NULL),
('ebe970f8-981d-11ed-979a-244bfe7dd4fe', 'Eliem Therapeutics (UK) Ltd', '2023-01-19 12:22:47', NULL, NULL),
('ebe9d9d8-981d-11ed-979a-244bfe7dd4fe', 'Emergo Therapeutics', '2023-01-19 12:22:47', NULL, NULL),
('ebea3c8b-981d-11ed-979a-244bfe7dd4fe', 'Endo Pharmaceuticals Inc.', '2023-01-19 12:22:47', NULL, NULL),
('ebea9a6d-981d-11ed-979a-244bfe7dd4fe', 'Evelo Biosciences', '2023-01-19 12:22:47', NULL, NULL),
('ebeafdec-981d-11ed-979a-244bfe7dd4fe', 'Gate Neurosciences, Inc', '2023-01-19 12:22:47', NULL, NULL),
('ebeb5932-981d-11ed-979a-244bfe7dd4fe', 'Genentech, Inc.', '2023-01-19 12:22:47', NULL, NULL),
('ebebb588-981d-11ed-979a-244bfe7dd4fe', 'GlaxoSmithKline Biologicals', '2023-01-19 12:22:47', NULL, NULL),
('ebec114c-981d-11ed-979a-244bfe7dd4fe', 'Glenmark Pharmaceuticals SA', '2023-01-19 12:22:47', NULL, NULL),
('ebec70b3-981d-11ed-979a-244bfe7dd4fe', 'Harmony Biosciences, LLC', '2023-01-19 12:22:47', NULL, NULL),
('ebeccfc0-981d-11ed-979a-244bfe7dd4fe', 'Hoffman La Roche', '2023-01-19 12:22:47', NULL, NULL),
('ebed2cc2-981d-11ed-979a-244bfe7dd4fe', 'Horizon Therapeutics', '2023-01-19 12:22:47', NULL, NULL),
('ebed85e2-981d-11ed-979a-244bfe7dd4fe', 'Incyte', '2023-01-19 12:22:47', NULL, NULL),
('ebede803-981d-11ed-979a-244bfe7dd4fe', 'Indivior', '2023-01-19 12:22:47', NULL, NULL),
('ebee412d-981d-11ed-979a-244bfe7dd4fe', 'InflaRX GmbH', '2023-01-19 12:22:47', NULL, NULL),
('ebeea55c-981d-11ed-979a-244bfe7dd4fe', 'INmune Bio, Inc.', '2023-01-19 12:22:47', NULL, NULL),
('ebef04f1-981d-11ed-979a-244bfe7dd4fe', 'Integra', '2023-01-19 12:22:47', NULL, NULL),
('ebef6734-981d-11ed-979a-244bfe7dd4fe', 'Janssen Research &amp; Development', '2023-01-19 12:22:47', NULL, NULL),
('ebefc64c-981d-11ed-979a-244bfe7dd4fe', 'Jodi&#039;s Sponsor Training', '2023-01-19 12:22:47', NULL, NULL),
('ebf027a5-981d-11ed-979a-244bfe7dd4fe', 'KellyMc Test Sponsor', '2023-01-19 12:22:47', NULL, NULL),
('ebf084e5-981d-11ed-979a-244bfe7dd4fe', 'KeraNetics', '2023-01-19 12:22:47', NULL, NULL),
('ebf0e909-981d-11ed-979a-244bfe7dd4fe', 'Kinevant Sciences GmbH', '2023-01-19 12:22:47', NULL, NULL),
('ebf147fc-981d-11ed-979a-244bfe7dd4fe', 'Kyowa Hakko Kirin Co., Ltd (KHK)', '2023-01-19 12:22:47', NULL, NULL),
('ebf1a543-981d-11ed-979a-244bfe7dd4fe', 'Laboratoires Thea', '2023-01-19 12:22:47', NULL, NULL),
('ebf20335-981d-11ed-979a-244bfe7dd4fe', 'Lakewood Amedex Inc.', '2023-01-19 12:22:47', NULL, NULL),
('ebf25df7-981d-11ed-979a-244bfe7dd4fe', 'Larimar Therapeutics, Inc.', '2023-01-19 12:22:47', NULL, NULL),
('ebf2bc9f-981d-11ed-979a-244bfe7dd4fe', 'Meghan&#039;s Pharma', '2023-01-19 12:22:47', NULL, NULL),
('ebf31a03-981d-11ed-979a-244bfe7dd4fe', 'Melissa&#039;s Sponser', '2023-01-19 12:22:47', NULL, NULL),
('ebf376f1-981d-11ed-979a-244bfe7dd4fe', 'Merck', '2023-01-19 12:22:47', NULL, NULL),
('ebf3d4bc-981d-11ed-979a-244bfe7dd4fe', 'MHS', '2023-01-19 12:22:47', NULL, NULL),
('ebf43311-981d-11ed-979a-244bfe7dd4fe', 'Miranda Training', '2023-01-19 12:22:47', NULL, NULL),
('ebf49178-981d-11ed-979a-244bfe7dd4fe', 'MoonLake Immunotherapeutics AG', '2023-01-19 12:22:47', NULL, NULL),
('ebf4f22a-981d-11ed-979a-244bfe7dd4fe', 'Mycovia', '2023-01-19 12:22:47', NULL, NULL),
('ebf55bfe-981d-11ed-979a-244bfe7dd4fe', 'Neothetics', '2023-01-19 12:22:47', NULL, NULL),
('ebf607b9-981d-11ed-979a-244bfe7dd4fe', 'Neurana Pharmaceuticals, Inc.', '2023-01-19 12:22:47', NULL, NULL),
('ebf6711b-981d-11ed-979a-244bfe7dd4fe', 'Novartis', '2023-01-19 12:22:47', NULL, NULL),
('ebf6d3ed-981d-11ed-979a-244bfe7dd4fe', 'Novartis / Oracle Library', '2023-01-19 12:22:47', NULL, NULL),
('ebf75702-981d-11ed-979a-244bfe7dd4fe', 'Novartis Oracle Clinical', '2023-01-19 12:22:47', NULL, NULL),
('ebf7b8f5-981d-11ed-979a-244bfe7dd4fe', 'Novavax', '2023-01-19 12:22:47', NULL, NULL),
('ebf81578-981d-11ed-979a-244bfe7dd4fe', 'NovoNordisk', '2023-01-19 12:22:47', NULL, NULL),
('ebf88b0d-981d-11ed-979a-244bfe7dd4fe', 'NuCana plc', '2023-01-19 12:22:47', NULL, NULL),
('ebf8ed7e-981d-11ed-979a-244bfe7dd4fe', 'Ono Pharmaceutical Co., LTD.', '2023-01-19 12:22:47', NULL, NULL),
('ebf95b50-981d-11ed-979a-244bfe7dd4fe', 'Ophirex, Inc.', '2023-01-19 12:22:47', NULL, NULL),
('ebf9bd84-981d-11ed-979a-244bfe7dd4fe', 'OPKO Biologics Ltd.', '2023-01-19 12:22:47', NULL, NULL),
('ebfa1cde-981d-11ed-979a-244bfe7dd4fe', 'Otsuka', '2023-01-19 12:22:47', NULL, NULL),
('ebfa7e93-981d-11ed-979a-244bfe7dd4fe', 'Pear Therapeutics, Inc.', '2023-01-19 12:22:47', NULL, NULL),
('ebfaef8c-981d-11ed-979a-244bfe7dd4fe', 'Pfizer', '2023-01-19 12:22:47', NULL, NULL),
('ebfb4cb7-981d-11ed-979a-244bfe7dd4fe', 'Priya Test', '2023-01-19 12:22:47', NULL, NULL),
('ebfba873-981d-11ed-979a-244bfe7dd4fe', 'Prometheus Labs', '2023-01-19 12:22:47', NULL, NULL),
('ebfc16c4-981d-11ed-979a-244bfe7dd4fe', 'Quintiles', '2023-01-19 12:22:47', NULL, NULL),
('ebfc72a7-981d-11ed-979a-244bfe7dd4fe', 'RAI Services Company', '2023-01-19 12:22:47', NULL, NULL),
('ebfcd53b-981d-11ed-979a-244bfe7dd4fe', 'Relmada', '2023-01-19 12:22:47', NULL, NULL),
('ebfd3c3b-981d-11ed-979a-244bfe7dd4fe', 'Retroscreen', '2023-01-19 12:22:47', NULL, NULL),
('ebfda1f4-981d-11ed-979a-244bfe7dd4fe', 'Revance Therapeutics, INC', '2023-01-19 12:22:47', NULL, NULL),
('ebfdfe0c-981d-11ed-979a-244bfe7dd4fe', 'Ritter Pharmaceuticals', '2023-01-19 12:22:47', NULL, NULL),
('ebfe61b2-981d-11ed-979a-244bfe7dd4fe', 'Romark', '2023-01-19 12:22:47', NULL, NULL),
('ebfec40e-981d-11ed-979a-244bfe7dd4fe', 'Sarepta Therapeutics', '2023-01-19 12:22:47', NULL, NULL),
('ebff26ab-981d-11ed-979a-244bfe7dd4fe', 'Sean&#039;s Training Sponsor', '2023-01-19 12:22:47', NULL, NULL),
('ebff83e8-981d-11ed-979a-244bfe7dd4fe', 'Shane Sponsor', '2023-01-19 12:22:47', NULL, NULL),
('ebffecb0-981d-11ed-979a-244bfe7dd4fe', 'Shire', '2023-01-19 12:22:47', NULL, NULL),
('ec004c54-981d-11ed-979a-244bfe7dd4fe', 'Sojournix, Inc', '2023-01-19 12:22:47', NULL, NULL),
('ec00b02f-981d-11ed-979a-244bfe7dd4fe', 'Statera Biopharma', '2023-01-19 12:22:47', NULL, NULL),
('ec010ed0-981d-11ed-979a-244bfe7dd4fe', 'Sunovion Pharmaceuticals Inc.', '2023-01-19 12:22:47', NULL, NULL),
('ec0177a2-981d-11ed-979a-244bfe7dd4fe', 'Takeda', '2023-01-19 12:22:47', NULL, NULL),
('ec01dae7-981d-11ed-979a-244bfe7dd4fe', 'Tasly Pharmaceuticals, Inc.', '2023-01-19 12:22:47', NULL, NULL),
('ec0241d3-981d-11ed-979a-244bfe7dd4fe', 'Tee&#039;s Training', '2023-01-19 12:22:47', NULL, NULL),
('ec02a55f-981d-11ed-979a-244bfe7dd4fe', 'The Learning Corp', '2023-01-19 12:22:47', NULL, NULL),
('ec0304c7-981d-11ed-979a-244bfe7dd4fe', 'Toyama Chemical Co., Ltd. [Fujifilm]', '2023-01-19 12:22:47', NULL, NULL),
('ec03653c-981d-11ed-979a-244bfe7dd4fe', 'UCB Biopharma SPRL', '2023-01-19 12:22:47', NULL, NULL),
('ec03d580-981d-11ed-979a-244bfe7dd4fe', 'UCB Biopharma SRL', '2023-01-19 12:22:47', NULL, NULL),
('ec043bdf-981d-11ed-979a-244bfe7dd4fe', 'Urovant Sciences, Inc.', '2023-01-19 12:22:47', NULL, NULL),
('ec049a2d-981d-11ed-979a-244bfe7dd4fe', 'US World Meds', '2023-01-19 12:22:47', NULL, NULL),
('ec050371-981d-11ed-979a-244bfe7dd4fe', 'Viela Bio, Inc.', '2023-01-19 12:22:47', NULL, NULL),
('ec057c2b-981d-11ed-979a-244bfe7dd4fe', 'Visus Therapeutics, Inc.', '2023-01-19 12:22:47', NULL, NULL),
('ec05df10-981d-11ed-979a-244bfe7dd4fe', 'Xynomic Pharmaceuticals', '2023-01-19 12:22:47', NULL, NULL),
('ec06398f-981d-11ed-979a-244bfe7dd4fe', 'Zenas BioPharma', '2023-01-19 12:22:47', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sponsor_history`
--

CREATE TABLE `sponsor_history` (
  `id` char(36) NOT NULL,
  `action` char(8) DEFAULT NULL,
  `modifiedByAppUserid` char(36) NOT NULL,
  `sponsorid` char(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `isRemovedFlag` char(1) DEFAULT NULL,
  `userid` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sponsor_history`
--

INSERT INTO `sponsor_history` (`id`, `action`, `modifiedByAppUserid`, `sponsorid`, `name`, `createdDate`, `lastModifiedDate`, `isRemovedFlag`, `userid`) VALUES
('ebd7bfc2-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebd7748e-981d-11ed-979a-244bfe7dd4fe', '4D Pharma Plc', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebd8c3e3-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebd8256a-981d-11ed-979a-244bfe7dd4fe', 'Abbott Labs', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebd9bf37-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebd922c0-981d-11ed-979a-244bfe7dd4fe', 'Actinogen Medical Ltd.', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebda25cb-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebd9f15b-981d-11ed-979a-244bfe7dd4fe', 'Acumen Pharmaceuticals, Inc.', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebda97a6-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebda59d8-981d-11ed-979a-244bfe7dd4fe', 'Alkermes, Inc.', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebdb0081-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebdaca32-981d-11ed-979a-244bfe7dd4fe', 'Allergan Inc.', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebdb6820-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebdb3167-981d-11ed-979a-244bfe7dd4fe', 'Alvotech Swiss AG', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebdbd3eb-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebdb9a57-981d-11ed-979a-244bfe7dd4fe', 'Alyse&#039;s Training', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebdc3898-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebdc02b9-981d-11ed-979a-244bfe7dd4fe', 'Amgen Inc.', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebdc9b20-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebdc6931-981d-11ed-979a-244bfe7dd4fe', 'AnaptysBio Inc.', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebdd06c4-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebdccf3c-981d-11ed-979a-244bfe7dd4fe', 'Angelini Pharma', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebdd6e5f-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebdd3968-981d-11ed-979a-244bfe7dd4fe', 'AstraZeneca', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebddd887-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebdd9f29-981d-11ed-979a-244bfe7dd4fe', 'Avanir Pharmaceuticals, Inc.', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebde4161-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebde0d3a-981d-11ed-979a-244bfe7dd4fe', 'Baxalta', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebdea37e-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebde7461-981d-11ed-979a-244bfe7dd4fe', 'Bayer', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebdf10f6-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebded62d-981d-11ed-979a-244bfe7dd4fe', 'Bionomics Ltd', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebdf7327-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebdf4018-981d-11ed-979a-244bfe7dd4fe', 'BlackThorn Therapeutics, Inc.', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebdfd556-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebdfa28d-981d-11ed-979a-244bfe7dd4fe', 'Boehringer Ingelheim', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebe03c7f-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebe0088e-981d-11ed-979a-244bfe7dd4fe', 'Calico Life Sciences LLC', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebe0a319-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebe07010-981d-11ed-979a-244bfe7dd4fe', 'Camurus AB', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebe10d43-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebe0d56d-981d-11ed-979a-244bfe7dd4fe', 'Celgene', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebe17396-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebe13e80-981d-11ed-979a-244bfe7dd4fe', 'Cerevance Beta, Inc.', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebe1da9e-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebe1a650-981d-11ed-979a-244bfe7dd4fe', 'Chong Kun Dang Pharmaceutical Corporation (CKD)', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebe23a75-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebe207e3-981d-11ed-979a-244bfe7dd4fe', 'Cleveland Clinic', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebe2a76e-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebe26a18-981d-11ed-979a-244bfe7dd4fe', 'Clinical Ink', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebe300fa-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebe2d009-981d-11ed-979a-244bfe7dd4fe', 'Clinical Ink - Data Management', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebe363c2-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebe33130-981d-11ed-979a-244bfe7dd4fe', 'Clinical Ink Application Testing', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebe3c328-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebe3921e-981d-11ed-979a-244bfe7dd4fe', 'Clinical Ink Demonstrations', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebe420c5-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebe3f0bf-981d-11ed-979a-244bfe7dd4fe', 'Clinical Ink Protocol Services', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebe47a2f-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebe44c3a-981d-11ed-979a-244bfe7dd4fe', 'Clinical Ink Quality Department', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebe4d475-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebe4a55f-981d-11ed-979a-244bfe7dd4fe', 'Clintara', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebe53a50-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebe50803-981d-11ed-979a-244bfe7dd4fe', 'Cogstate', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebe59882-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebe56a02-981d-11ed-979a-244bfe7dd4fe', 'COMPASS Pathways, Ltd', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebe5f19e-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebe5c414-981d-11ed-979a-244bfe7dd4fe', 'Covis Pharma BV', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebe64c78-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebe61cfb-981d-11ed-979a-244bfe7dd4fe', 'Cyclo Therapeutics', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebe6a6c5-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebe677ed-981d-11ed-979a-244bfe7dd4fe', 'Danielle ImaginationLand Sponsor', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebe705b8-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebe6d577-981d-11ed-979a-244bfe7dd4fe', 'Dart NeuroScience, LLC', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebe7647c-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebe7307c-981d-11ed-979a-244bfe7dd4fe', 'Dartmouth', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebe7c0c1-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebe79243-981d-11ed-979a-244bfe7dd4fe', 'Diamyd Medical AB', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebe82046-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebe7ef0d-981d-11ed-979a-244bfe7dd4fe', 'Dynacure', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebe88318-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebe850af-981d-11ed-979a-244bfe7dd4fe', 'EIP PHARMA, LLC', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebe8e353-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebe8b1e8-981d-11ed-979a-244bfe7dd4fe', 'Eisai', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebe94262-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebe9144e-981d-11ed-979a-244bfe7dd4fe', 'Eli Lilly &amp; Company', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebe9a4b0-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebe970f8-981d-11ed-979a-244bfe7dd4fe', 'Eliem Therapeutics (UK) Ltd', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebea0dd1-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebe9d9d8-981d-11ed-979a-244bfe7dd4fe', 'Emergo Therapeutics', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebea6c56-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebea3c8b-981d-11ed-979a-244bfe7dd4fe', 'Endo Pharmaceuticals Inc.', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebeace9e-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebea9a6d-981d-11ed-979a-244bfe7dd4fe', 'Evelo Biosciences', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebeb2f07-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebeafdec-981d-11ed-979a-244bfe7dd4fe', 'Gate Neurosciences, Inc', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebeb8885-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebeb5932-981d-11ed-979a-244bfe7dd4fe', 'Genentech, Inc.', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebebe5d5-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebebb588-981d-11ed-979a-244bfe7dd4fe', 'GlaxoSmithKline Biologicals', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebec4644-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebec114c-981d-11ed-979a-244bfe7dd4fe', 'Glenmark Pharmaceuticals SA', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebeca2cd-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebec70b3-981d-11ed-979a-244bfe7dd4fe', 'Harmony Biosciences, LLC', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebed00b4-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebeccfc0-981d-11ed-979a-244bfe7dd4fe', 'Hoffman La Roche', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebed5a6f-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebed2cc2-981d-11ed-979a-244bfe7dd4fe', 'Horizon Therapeutics', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebedb7b9-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebed85e2-981d-11ed-979a-244bfe7dd4fe', 'Incyte', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebee1667-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebede803-981d-11ed-979a-244bfe7dd4fe', 'Indivior', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebee7255-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebee412d-981d-11ed-979a-244bfe7dd4fe', 'InflaRX GmbH', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebeed7b6-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebeea55c-981d-11ed-979a-244bfe7dd4fe', 'INmune Bio, Inc.', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebef37c1-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebef04f1-981d-11ed-979a-244bfe7dd4fe', 'Integra', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebef9787-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebef6734-981d-11ed-979a-244bfe7dd4fe', 'Janssen Research &amp; Development', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebeff927-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebefc64c-981d-11ed-979a-244bfe7dd4fe', 'Jodi&#039;s Sponsor Training', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebf055c5-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebf027a5-981d-11ed-979a-244bfe7dd4fe', 'KellyMc Test Sponsor', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebf0ba4e-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebf084e5-981d-11ed-979a-244bfe7dd4fe', 'KeraNetics', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebf11986-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebf0e909-981d-11ed-979a-244bfe7dd4fe', 'Kinevant Sciences GmbH', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebf17833-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebf147fc-981d-11ed-979a-244bfe7dd4fe', 'Kyowa Hakko Kirin Co., Ltd (KHK)', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebf1d6b4-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebf1a543-981d-11ed-979a-244bfe7dd4fe', 'Laboratoires Thea', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebf232b0-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebf20335-981d-11ed-979a-244bfe7dd4fe', 'Lakewood Amedex Inc.', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebf28f74-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebf25df7-981d-11ed-979a-244bfe7dd4fe', 'Larimar Therapeutics, Inc.', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebf2edf2-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebf2bc9f-981d-11ed-979a-244bfe7dd4fe', 'Meghan&#039;s Pharma', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebf34a6e-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebf31a03-981d-11ed-979a-244bfe7dd4fe', 'Melissa&#039;s Sponser', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebf3a85c-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebf376f1-981d-11ed-979a-244bfe7dd4fe', 'Merck', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebf40550-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebf3d4bc-981d-11ed-979a-244bfe7dd4fe', 'MHS', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebf46435-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebf43311-981d-11ed-979a-244bfe7dd4fe', 'Miranda Training', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebf4bef2-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebf49178-981d-11ed-979a-244bfe7dd4fe', 'MoonLake Immunotherapeutics AG', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebf5278c-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebf4f22a-981d-11ed-979a-244bfe7dd4fe', 'Mycovia', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebf5d031-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebf55bfe-981d-11ed-979a-244bfe7dd4fe', 'Neothetics', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebf63de8-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebf607b9-981d-11ed-979a-244bfe7dd4fe', 'Neurana Pharmaceuticals, Inc.', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebf6a1e9-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebf6711b-981d-11ed-979a-244bfe7dd4fe', 'Novartis', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebf70550-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebf6d3ed-981d-11ed-979a-244bfe7dd4fe', 'Novartis / Oracle Library', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebf78be7-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebf75702-981d-11ed-979a-244bfe7dd4fe', 'Novartis Oracle Clinical', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebf7e8c5-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebf7b8f5-981d-11ed-979a-244bfe7dd4fe', 'Novavax', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebf84f9b-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebf81578-981d-11ed-979a-244bfe7dd4fe', 'NovoNordisk', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebf8bc2d-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebf88b0d-981d-11ed-979a-244bfe7dd4fe', 'NuCana plc', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebf92e4a-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebf8ed7e-981d-11ed-979a-244bfe7dd4fe', 'Ono Pharmaceutical Co., LTD.', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebf98e68-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebf95b50-981d-11ed-979a-244bfe7dd4fe', 'Ophirex, Inc.', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebf9eff5-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebf9bd84-981d-11ed-979a-244bfe7dd4fe', 'OPKO Biologics Ltd.', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebfa51b7-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', 'Otsuka', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebfabfee-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebfa7e93-981d-11ed-979a-244bfe7dd4fe', 'Pear Therapeutics, Inc.', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebfb21eb-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebfaef8c-981d-11ed-979a-244bfe7dd4fe', 'Pfizer', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebfb7bb4-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebfb4cb7-981d-11ed-979a-244bfe7dd4fe', 'Priya Test', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebfbea13-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebfba873-981d-11ed-979a-244bfe7dd4fe', 'Prometheus Labs', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebfc4626-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebfc16c4-981d-11ed-979a-244bfe7dd4fe', 'Quintiles', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebfca55d-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebfc72a7-981d-11ed-979a-244bfe7dd4fe', 'RAI Services Company', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebfd08db-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebfcd53b-981d-11ed-979a-244bfe7dd4fe', 'Relmada', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebfd74be-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebfd3c3b-981d-11ed-979a-244bfe7dd4fe', 'Retroscreen', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebfdd17d-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebfda1f4-981d-11ed-979a-244bfe7dd4fe', 'Revance Therapeutics, INC', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebfe2eb6-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebfdfe0c-981d-11ed-979a-244bfe7dd4fe', 'Ritter Pharmaceuticals', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebfe92fd-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebfe61b2-981d-11ed-979a-244bfe7dd4fe', 'Romark', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebfef7d4-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebfec40e-981d-11ed-979a-244bfe7dd4fe', 'Sarepta Therapeutics', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebff5790-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebff26ab-981d-11ed-979a-244bfe7dd4fe', 'Sean&#039;s Training Sponsor', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ebffb675-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebff83e8-981d-11ed-979a-244bfe7dd4fe', 'Shane Sponsor', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ec0020c3-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ebffecb0-981d-11ed-979a-244bfe7dd4fe', 'Shire', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ec00819c-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ec004c54-981d-11ed-979a-244bfe7dd4fe', 'Sojournix, Inc', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ec00de3b-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ec00b02f-981d-11ed-979a-244bfe7dd4fe', 'Statera Biopharma', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ec01460a-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ec010ed0-981d-11ed-979a-244bfe7dd4fe', 'Sunovion Pharmaceuticals Inc.', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ec01a6ec-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ec0177a2-981d-11ed-979a-244bfe7dd4fe', 'Takeda', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ec020cb2-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ec01dae7-981d-11ed-979a-244bfe7dd4fe', 'Tasly Pharmaceuticals, Inc.', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ec0277ee-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ec0241d3-981d-11ed-979a-244bfe7dd4fe', 'Tee&#039;s Training', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ec02d730-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ec02a55f-981d-11ed-979a-244bfe7dd4fe', 'The Learning Corp', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ec033639-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ec0304c7-981d-11ed-979a-244bfe7dd4fe', 'Toyama Chemical Co., Ltd. [Fujifilm]', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ec03a5f9-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ec03653c-981d-11ed-979a-244bfe7dd4fe', 'UCB Biopharma SPRL', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ec0409c8-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ec03d580-981d-11ed-979a-244bfe7dd4fe', 'UCB Biopharma SRL', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ec046c3a-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ec043bdf-981d-11ed-979a-244bfe7dd4fe', 'Urovant Sciences, Inc.', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ec04d63a-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ec049a2d-981d-11ed-979a-244bfe7dd4fe', 'US World Meds', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ec054d11-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ec050371-981d-11ed-979a-244bfe7dd4fe', 'Viela Bio, Inc.', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ec05ac55-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ec057c2b-981d-11ed-979a-244bfe7dd4fe', 'Visus Therapeutics, Inc.', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ec060d32-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ec05df10-981d-11ed-979a-244bfe7dd4fe', 'Xynomic Pharmaceuticals', '2023-01-19 12:22:47', NULL, NULL, NULL),
('ec0667d1-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ec06398f-981d-11ed-979a-244bfe7dd4fe', 'Zenas BioPharma', '2023-01-19 12:22:47', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `study`
--

CREATE TABLE `study` (
  `id` char(36) NOT NULL,
  `ciStudyProjectCode` char(7) DEFAULT NULL,
  `protocolName` varchar(255) DEFAULT NULL,
  `sponsorid` char(36) DEFAULT NULL,
  `croid` char(36) DEFAULT NULL,
  `libraryFlag` char(1) DEFAULT NULL,
  `createdDate` datetime DEFAULT current_timestamp(),
  `lastModifiedDate` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `studyType` char(10) DEFAULT NULL,
  `studyPhaseFlag` char(1) DEFAULT NULL,
  `isLegacyFlag` char(1) DEFAULT NULL,
  `isRemovedFlag` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `study`
--

INSERT INTO `study` (`id`, `ciStudyProjectCode`, `protocolName`, `sponsorid`, `croid`, `libraryFlag`, `createdDate`, `lastModifiedDate`, `studyType`, `studyPhaseFlag`, `isLegacyFlag`, `isRemovedFlag`) VALUES
('ed236b3c-981d-11ed-979a-244bfe7dd4fe', 'CI-0012', 'Standard Library', 'ebe26a18-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed2371d0-981d-11ed-979a-244bfe7dd4fe', 'ALK0002', 'Alkermes ALK21-029', 'ebda59d8-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed23785d-981d-11ed-979a-244bfe7dd4fe', 'ALK0004', 'ALKS 3831-A312', 'ebda59d8-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed237e18-981d-11ed-979a-244bfe7dd4fe', 'ALK0005', 'Alkermes ALKS 3831-A313', 'ebda59d8-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed238403-981d-11ed-979a-244bfe7dd4fe', 'ALK0001', 'Alkermes ALK5461-217 ', 'ebda59d8-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed238ad5-981d-11ed-979a-244bfe7dd4fe', 'ALL0001', 'Allergan 3143-101-012', 'ebdaca32-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed23912f-981d-11ed-979a-244bfe7dd4fe', 'AMG0002', 'Amgen 20180130', 'ebdc02b9-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed239b68-981d-11ed-979a-244bfe7dd4fe', 'AMG0000', 'Amgen eLAS', 'ebe26a18-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed23a05f-981d-11ed-979a-244bfe7dd4fe', 'AVA0001', '20-AVP-786-306', 'ebdd9f29-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed23a4f9-981d-11ed-979a-244bfe7dd4fe', 'AVA0002', '20-AVP-786-307', 'ebdd9f29-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed23a967-981d-11ed-979a-244bfe7dd4fe', 'CI-0011', 'General COVID Library', 'ebe26a18-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed23ade5-981d-11ed-979a-244bfe7dd4fe', 'CI-0013', 'Scleroderma', 'ebe26a18-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed23b24d-981d-11ed-979a-244bfe7dd4fe', 'CI-0005', 'Clinical Ink Demo Suite', 'ebe3921e-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed23b6dd-981d-11ed-979a-244bfe7dd4fe', 'CLL0001', 'Acumen ACU-001', 'ebd9f15b-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed23bd53-981d-11ed-979a-244bfe7dd4fe', 'CLP0002', 'Tasly T89-31-AMS', 'ec01dae7-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed23c2ad-981d-11ed-979a-244bfe7dd4fe', 'COG0004', 'Cogstate-Eli Lilly I5T-MC-AACG', 'ebe9144e-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed23c84a-981d-11ed-979a-244bfe7dd4fe', 'COG0005', 'Biogen 251AD201', 'ebe50803-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed23cce5-981d-11ed-979a-244bfe7dd4fe', 'COG0006', 'Cogstate AgeneBio AGB101 MCD', 'ebe50803-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed23d1c2-981d-11ed-979a-244bfe7dd4fe', 'COG0007', 'Cogstate I8G-MC-LMDC', 'ebe9144e-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed23d63b-981d-11ed-979a-244bfe7dd4fe', 'COG0008', 'NovoNordisk HAEM-4436', 'ebf81578-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed23db21-981d-11ed-979a-244bfe7dd4fe', 'COG0009', 'NovoNordisk P5', 'ebf81578-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed23df80-981d-11ed-979a-244bfe7dd4fe', 'COG0010', 'NovoNordisk P6', 'ebf81578-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed23e40d-981d-11ed-979a-244bfe7dd4fe', 'COG0011', 'NovoNordisk P5 English Sites', 'ebf81578-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed23e85f-981d-11ed-979a-244bfe7dd4fe', 'COG0012', 'NovoNordisk P6 English Sites', 'ebf81578-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed23ecb2-981d-11ed-979a-244bfe7dd4fe', 'COG0013', 'Toyama T817EU201', 'ec0304c7-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed23f0fa-981d-11ed-979a-244bfe7dd4fe', 'COG0015', 'Biogen 251AD201 LTE', 'ebe50803-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed23f554-981d-11ed-979a-244bfe7dd4fe', 'COG0016', 'Roche BP41316', 'ebeccfc0-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed23f9aa-981d-11ed-979a-244bfe7dd4fe', 'COG0017', 'Roche Library', 'ebeccfc0-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed23fde8-981d-11ed-979a-244bfe7dd4fe', 'COG0018', 'Eisai BAN2401', 'ebe8b1e8-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed24022a-981d-11ed-979a-244bfe7dd4fe', 'COG0019', 'Roche BP41674', 'ebeccfc0-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed2406e3-981d-11ed-979a-244bfe7dd4fe', 'COG0020', 'Eli Lilly 15T-MC-AACI', 'ebe9144e-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed240c68-981d-11ed-979a-244bfe7dd4fe', 'COG0021', 'Takeda TAK-071-2002', 'ec0177a2-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed2411be-981d-11ed-979a-244bfe7dd4fe', 'COG0022', 'Eli Lilly I5T-MC-AACH', 'ebe9144e-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed2416ad-981d-11ed-979a-244bfe7dd4fe', 'COG0023', 'INmune Bio XPROTM-AD-02', 'ebeea55c-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed241b2a-981d-11ed-979a-244bfe7dd4fe', 'COG0024', 'INmune Bio, Inc._XPro-AD-03', 'ebeea55c-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed242005-981d-11ed-979a-244bfe7dd4fe', 'EIS0002', 'Eisai-E2007-A001-408', 'ebe8b1e8-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed24247e-981d-11ed-979a-244bfe7dd4fe', 'eLAS001', 'eLAS v1', 'ebe26a18-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed2428f2-981d-11ed-979a-244bfe7dd4fe', 'eLAS002', 'eLAS v2', 'ebe26a18-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed242d4c-981d-11ed-979a-244bfe7dd4fe', 'eLAS003', 'eLAS v3', 'ebe26a18-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed24338f-981d-11ed-979a-244bfe7dd4fe', 'eLAS004', 'eLAS v4', 'ebe26a18-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed243de2-981d-11ed-979a-244bfe7dd4fe', 'EME0001', 'Emergo NKT-201', 'ebe9d9d8-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed2442d1-981d-11ed-979a-244bfe7dd4fe', 'EME0002', 'Emergo NKT-202', 'ebe9d9d8-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed244748-981d-11ed-979a-244bfe7dd4fe', 'EME0003', 'Emergo NKT-203', 'ebe9d9d8-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed244bf7-981d-11ed-979a-244bfe7dd4fe', 'END0002', 'EN3835-222 ', 'ebea3c8b-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed2451b6-981d-11ed-979a-244bfe7dd4fe', 'END0003', 'Endo EN3835-306', 'ebea3c8b-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed245765-981d-11ed-979a-244bfe7dd4fe', 'END0005', 'Endo EN3835-106', 'ebea3c8b-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed245cc8-981d-11ed-979a-244bfe7dd4fe', 'EVL0001', 'Evelo EDP1815-207', 'ebea9a6d-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed2462f6-981d-11ed-979a-244bfe7dd4fe', 'EVL0002', 'Evelo EDP1815-208', 'ebea9a6d-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed246ed6-981d-11ed-979a-244bfe7dd4fe', 'GEN0001', 'Genentech GA30044', 'ebeb5932-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed247390-981d-11ed-979a-244bfe7dd4fe', 'GEN0002', 'Genentech GA30066', 'ebeb5932-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed247873-981d-11ed-979a-244bfe7dd4fe', 'ICN0007', 'Indivior INDV-6000-401', 'ebede803-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed247cd8-981d-11ed-979a-244bfe7dd4fe', 'ICN0008', 'Diamyd DIAGNODE-3', 'ebe79243-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed248157-981d-11ed-979a-244bfe7dd4fe', 'ICN0010', 'Statera 103-301', 'ec00b02f-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed2486d7-981d-11ed-979a-244bfe7dd4fe', 'ICN0011', 'MoonLake M1095-PSA-201', 'ebf49178-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed248c6e-981d-11ed-979a-244bfe7dd4fe', 'ICN0012', 'MoonLake M1095-SPA-201', 'ebf49178-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed2491c6-981d-11ed-979a-244bfe7dd4fe', 'ICN0013', 'MoonLake Immunotherapeutics AG_M1095-HS-201', 'ebf49178-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed249682-981d-11ed-979a-244bfe7dd4fe', 'INC0002', 'Dart DNS-7801-201', 'ebe6d577-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed249b3a-981d-11ed-979a-244bfe7dd4fe', 'INC0003', 'Otsuka 331-14-213', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed24a408-981d-11ed-979a-244bfe7dd4fe', 'INC0005', 'Otsuka 405-201-00002', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed24aa81-981d-11ed-979a-244bfe7dd4fe', 'INC0006', 'Dart DNS-7801-201 Part B', 'ebe6d577-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed24b037-981d-11ed-979a-244bfe7dd4fe', 'INC0008', 'Otsuka 405-201-00013', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed24b52b-981d-11ed-979a-244bfe7dd4fe', 'INC0009', 'Otsuka 405-201-00014', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed24b9d2-981d-11ed-979a-244bfe7dd4fe', 'INC0010', 'Otsuka 405-201-00015', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed24be7f-981d-11ed-979a-244bfe7dd4fe', 'INC0011', 'Revance 1720302', 'ebfda1f4-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed24c2e4-981d-11ed-979a-244bfe7dd4fe', 'INC0013', 'Revance 1720304', 'ebfda1f4-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed24c74f-981d-11ed-979a-244bfe7dd4fe', 'INC0014', 'Otsuka 331-201-00182', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed24cc74-981d-11ed-979a-244bfe7dd4fe', 'INC0015', 'AnaptysBio ANB020-006', 'ebdc6931-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed24d0d3-981d-11ed-979a-244bfe7dd4fe', 'INC0016', 'Otsuka 331-201-00071', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed24d52f-981d-11ed-979a-244bfe7dd4fe', 'INC0017', 'Otsuka 331-201-00072', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed24d97a-981d-11ed-979a-244bfe7dd4fe', 'INC0018', 'Otsuka 331-201-00195', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed24ddc4-981d-11ed-979a-244bfe7dd4fe', 'INC0019', 'Relmada REL-1017-301', 'ebfcd53b-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed24e226-981d-11ed-979a-244bfe7dd4fe', 'INC0020', 'Relmada REL-1017-310', 'ebfcd53b-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed24e67b-981d-11ed-979a-244bfe7dd4fe', 'INC0022', 'Otsuka 331-201-00348', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed24eb33-981d-11ed-979a-244bfe7dd4fe', 'INC0023', 'Harmony Biosciences HBS-101-CL-010', 'ebec70b3-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed24efde-981d-11ed-979a-244bfe7dd4fe', 'INC0007', 'Otsuka 331-201-00079', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed24f522-981d-11ed-979a-244bfe7dd4fe', 'JAN0002', 'Janssen 80202135SLE2001', 'ebef6734-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed24fa5f-981d-11ed-979a-244bfe7dd4fe', 'JAN0003', 'Janssen 80202135LUN2001', 'ebef6734-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed24ff4c-981d-11ed-979a-244bfe7dd4fe', 'KER0001', 'KeraNetics KSGL-CRD-004', 'ebf084e5-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed2503e6-981d-11ed-979a-244bfe7dd4fe', 'LLY0001', 'I9X-MC-MTAE', 'ebe9144e-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed250868-981d-11ed-979a-244bfe7dd4fe', 'LLY0002', 'Eli Lilly J1V-MC-IMMA', 'ebe9144e-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed250cba-981d-11ed-979a-244bfe7dd4fe', 'LLY0004', 'Eli Lilly_15T-MC-AACM', 'ebe9144e-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed251124-981d-11ed-979a-244bfe7dd4fe', 'LLY0005', 'Eli Lilly_I5T-MC-AACO', 'ebe9144e-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed251573-981d-11ed-979a-244bfe7dd4fe', 'LLY9999', 'Eli Lilly eLAS v3.0', 'ebe9144e-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed2519c5-981d-11ed-979a-244bfe7dd4fe', 'MYC0001', 'VMT-VT-1161-CL-011', 'ebf4f22a-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed251e14-981d-11ed-979a-244bfe7dd4fe', 'MYC0002', 'VMT-VT-1161-CL-012', 'ebf4f22a-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed252262-981d-11ed-979a-244bfe7dd4fe', 'NEU0001', 'Neurana CLN-116', 'ebf607b9-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed2526ab-981d-11ed-979a-244bfe7dd4fe', 'NVS0041', 'Novartis COMB157GUS10', 'ebf6711b-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed252af7-981d-11ed-979a-244bfe7dd4fe', 'NVS0042', 'Novartis OMB157GUS09', 'ebf6711b-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed252f69-981d-11ed-979a-244bfe7dd4fe', 'NVS0043', 'Novartis CLNP023K12201', 'ebf6711b-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed2533ae-981d-11ed-979a-244bfe7dd4fe', 'NVS0044', 'Novartis CVAY736K12301', 'ebf6711b-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed253805-981d-11ed-979a-244bfe7dd4fe', 'OTS0016', 'Otsuka 331-201-00080', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed253c52-981d-11ed-979a-244bfe7dd4fe', 'OTS0017', 'Otsuka 331-201-00081', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed25416b-981d-11ed-979a-244bfe7dd4fe', 'OTS0018', 'Otsuka 277-201-00001', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed25468d-981d-11ed-979a-244bfe7dd4fe', 'OTS0021', 'Otsuka 331-201-00083', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed254b8e-981d-11ed-979a-244bfe7dd4fe', 'OTS0022', 'Otsuka 331-201-00103', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed255080-981d-11ed-979a-244bfe7dd4fe', 'OTS0023', 'Otsuka 405-201-00007', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed25555e-981d-11ed-979a-244bfe7dd4fe', 'OTS0024', 'Otsuka 303-201-00002', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed2559c1-981d-11ed-979a-244bfe7dd4fe', 'OTS0025', 'Otsuka 156-201-00183', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed255e1e-981d-11ed-979a-244bfe7dd4fe', 'OTS0026', 'Otsuka 31-14-204', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed2562bc-981d-11ed-979a-244bfe7dd4fe', 'OTS0027', 'Otsuka 031-201-00301', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed256764-981d-11ed-979a-244bfe7dd4fe', 'OTS0028', 'Otsuka 341-201-00004', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed256c83-981d-11ed-979a-244bfe7dd4fe', 'OTS0029', 'Otsuka 331-201-00084', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed25716a-981d-11ed-979a-244bfe7dd4fe', 'OTS0030', 'Otsuka 331-201-00148', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed257618-981d-11ed-979a-244bfe7dd4fe', 'OTS0031', 'Otsuka 331-201-00191', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed257c7c-981d-11ed-979a-244bfe7dd4fe', 'OTS0032', 'Otsuka 301-201-000181', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed2581fb-981d-11ed-979a-244bfe7dd4fe', 'OTS0033', 'Otsuka 331-201-00242', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed25869f-981d-11ed-979a-244bfe7dd4fe', 'OTS0035', 'Otsuka 331-201-00176', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed258b5c-981d-11ed-979a-244bfe7dd4fe', 'OTS0037', 'Otsuka 405-201-00016', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed25903f-981d-11ed-979a-244bfe7dd4fe', 'OTS0038', 'Otsuka 405-201-00017', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed25953a-981d-11ed-979a-244bfe7dd4fe', 'OTS0039', 'Otsuka 405-201-00021', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed2599dc-981d-11ed-979a-244bfe7dd4fe', 'OTS0042', 'Otsuka 325-201-00005', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed259e92-981d-11ed-979a-244bfe7dd4fe', 'PPD0001', 'Viela Bio VIB7734.P2.S1', 'ec050371-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed25a354-981d-11ed-979a-244bfe7dd4fe', 'PPD0003', 'Prometheus Labs PR200-104', 'ebfba873-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed25a7b0-981d-11ed-979a-244bfe7dd4fe', 'PPD0005', 'Zenas Biopharma ZB012-03-001', 'ec06398f-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed25ac03-981d-11ed-979a-244bfe7dd4fe', 'PPD0007', 'Horizon Therapeutics HZNP-DAX-204', 'ebed2cc2-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed25b06b-981d-11ed-979a-244bfe7dd4fe', 'PPD0008', 'Horizon Therapeutics HZNP-DAX-203', 'ebed2cc2-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed25b4c2-981d-11ed-979a-244bfe7dd4fe', 'PRG0001', 'Covis ALV-020-001', 'ebe5c414-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed25bbf6-981d-11ed-979a-244bfe7dd4fe', 'PRG0002', 'Larimar CLIN-1601-201', 'ebf25df7-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed25c0aa-981d-11ed-979a-244bfe7dd4fe', 'PRG0003', 'OPX-PR-01', 'ebf95b50-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed25c529-981d-11ed-979a-244bfe7dd4fe', 'PRG0004', 'Bionomics Ltd BNC210.012', 'ebded62d-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed25c9ab-981d-11ed-979a-244bfe7dd4fe', 'PRG0008', 'Premier Research 039(C)MD21046', 'ebdccf3c-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed25cdf4-981d-11ed-979a-244bfe7dd4fe', 'PRX0002', 'Camurus AB HS-17-585', 'ebe07010-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed25d23e-981d-11ed-979a-244bfe7dd4fe', 'PRX0003', 'Glenmark GBR830-204', 'ebec114c-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed25d6a0-981d-11ed-979a-244bfe7dd4fe', 'PRX0004', '4D Pharma BHT-II-002', 'ebd7748e-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed25dbf1-981d-11ed-979a-244bfe7dd4fe', 'PRX0005', 'Kyowa Hakko Kirin 4083-006', 'ebf147fc-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed25e116-981d-11ed-979a-244bfe7dd4fe', 'PRX0006', 'Xynomic Pharmaceuticals XYN-602', 'ec05df10-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed25e682-981d-11ed-979a-244bfe7dd4fe', 'PRX0007', '4D Pharma MRx0004', 'ebd7748e-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed25ec16-981d-11ed-979a-244bfe7dd4fe', 'PRX0008', 'Urovant URO-901-3005', 'ec043bdf-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed25f0ee-981d-11ed-979a-244bfe7dd4fe', 'PRX0011', 'UCB MG0003', 'ec03653c-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed25f590-981d-11ed-979a-244bfe7dd4fe', 'PRX0015', 'Urovant URO-901-3006', 'ec043bdf-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed25fa86-981d-11ed-979a-244bfe7dd4fe', 'PRX0017', 'Sarepta SRP-9001-301', 'ebf6711b-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed25fefa-981d-11ed-979a-244bfe7dd4fe', 'PRX0019', 'UCB SL0043', 'ec03653c-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed2603b1-981d-11ed-979a-244bfe7dd4fe', 'PRX0021', 'Kinevant KIN-1901-2001', 'ebf0e909-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed26082f-981d-11ed-979a-244bfe7dd4fe', 'PRX0024', 'UCB SL0044', 'ec03653c-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed260ca2-981d-11ed-979a-244bfe7dd4fe', 'PRX0025', 'UCB SL0046', 'ec03653c-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed2610ef-981d-11ed-979a-244bfe7dd4fe', 'PRX0026', 'UCB CD0003', 'ec03d580-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed261575-981d-11ed-979a-244bfe7dd4fe', 'PRX0029', 'Sarepta SRP-9001-103', 'ebfec40e-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed2619d0-981d-11ed-979a-244bfe7dd4fe', 'PRX0030', 'UCB AIE001', 'ec03d580-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed261e32-981d-11ed-979a-244bfe7dd4fe', 'PRX0033', 'Alpine AIS-A03', 'ebe26a18-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed26227f-981d-11ed-979a-244bfe7dd4fe', 'PRX0035', 'ONO-4059-09', 'ebf8ed7e-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed2626d6-981d-11ed-979a-244bfe7dd4fe', 'PRX0036', 'UCB PS0032 ', 'ec03653c-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed262bc3-981d-11ed-979a-244bfe7dd4fe', 'PRX0037', 'UCB EP0162', 'ec03d580-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed2630ce-981d-11ed-979a-244bfe7dd4fe', 'PRX0039', 'UCB EP0165', 'ec03d580-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed2635d1-981d-11ed-979a-244bfe7dd4fe', 'PRX0041', 'Sarepta SRP-9001-303', 'ebfec40e-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed263a49-981d-11ed-979a-244bfe7dd4fe', 'PRX0043', 'Boehringer Ingelheim 352-2159', 'ebdfa28d-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed263f1c-981d-11ed-979a-244bfe7dd4fe', 'QUI0010', 'Eli Lilly I4V-MC-JAHZ', 'ebe9144e-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed26438f-981d-11ed-979a-244bfe7dd4fe', 'QUI0011', 'Eli Lilly I4V-MC-JAIA', 'ebe9144e-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed2647e1-981d-11ed-979a-244bfe7dd4fe', 'QUI0002', 'InflaRx HS IFX-1-P2.4', 'ebee412d-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed264c49-981d-11ed-979a-244bfe7dd4fe', 'QUI0005', 'AnaptysBio ANB020-005', 'ebdc6931-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed2650a9-981d-11ed-979a-244bfe7dd4fe', 'QUI0006', 'Merck MS201781-0031', 'ebf376f1-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed2654fd-981d-11ed-979a-244bfe7dd4fe', 'QUI0013', 'Eli Lilly 14V-MC-JAIM', 'ebe9144e-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed265a9f-981d-11ed-979a-244bfe7dd4fe', 'QUI0014', 'Merck EMR700461-025', 'ebf376f1-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed265fc1-981d-11ed-979a-244bfe7dd4fe', 'QUI0017', 'Merck MS200647-0037', 'ebf376f1-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed2664aa-981d-11ed-979a-244bfe7dd4fe', 'QUI0018', 'CKD 182RA18009', 'ebe1a650-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed2669d8-981d-11ed-979a-244bfe7dd4fe', 'QUI0021', 'Merck MS201943-0029', 'ebf376f1-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed266e42-981d-11ed-979a-244bfe7dd4fe', 'QUI0022', 'Merck MS200647-0005', 'ebf376f1-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed267390-981d-11ed-979a-244bfe7dd4fe', 'QUI0023', 'Merck MS200647-0047', 'ebf376f1-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed2679b5-981d-11ed-979a-244bfe7dd4fe', 'QUI0024', 'Eli Lilly J1V-MC-IMMA', 'ebe9144e-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed267ee3-981d-11ed-979a-244bfe7dd4fe', 'QUI0026', 'Amgen AMG 592 20200234', 'ebdc02b9-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed26848a-981d-11ed-979a-244bfe7dd4fe', 'QUI0027', 'Amgen AMG 570 20170588', 'ebe26a18-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed268bcb-981d-11ed-979a-244bfe7dd4fe', 'QUI0028', 'NuCana NuTide 121', 'ebf88b0d-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed269106-981d-11ed-979a-244bfe7dd4fe', 'QUI0029', 'Pear-006-101', 'ebfa7e93-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed2695bc-981d-11ed-979a-244bfe7dd4fe', 'QUI0030', 'Novartis CBAF312AUS02', 'ebf6711b-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed269a16-981d-11ed-979a-244bfe7dd4fe', 'QUI0031', 'Merck MS200467-0055', 'ebf376f1-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed269e9a-981d-11ed-979a-244bfe7dd4fe', 'QUI0032', 'TLC-PCT-001', 'ec02a55f-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed26a322-981d-11ed-979a-244bfe7dd4fe', 'QUI0033', 'PEAR-006-101 Part 2', 'ebfa7e93-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed26a818-981d-11ed-979a-244bfe7dd4fe', 'QUI0034', 'Merck MS200647-0017', 'ebf376f1-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed26ac87-981d-11ed-979a-244bfe7dd4fe', 'QUI0035', '- Merck MS200569-0002', 'ebf376f1-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed26b182-981d-11ed-979a-244bfe7dd4fe', 'QUI0036', 'Eli Lilly J1P-MC-KFAJ', 'ebe9144e-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed26b633-981d-11ed-979a-244bfe7dd4fe', 'QUI0037', 'UCB N01269', 'ec03d580-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed26baf3-981d-11ed-979a-244bfe7dd4fe', 'QUI0038', 'AstraZeneca D3465C00001', 'ebdd3968-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed26c0c4-981d-11ed-979a-244bfe7dd4fe', 'QUI0041', 'UCB EP0132', 'ec03d580-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed26c5f4-981d-11ed-979a-244bfe7dd4fe', 'QUI0042', 'AstraZeneca D8850C00003', 'ebdd3968-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed26caee-981d-11ed-979a-244bfe7dd4fe', 'QUI0043', 'Merck MS201923-0050', 'ebf376f1-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed26cfc1-981d-11ed-979a-244bfe7dd4fe', 'QUI0044', 'Visus VT-001', 'ec057c2b-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed26d4e2-981d-11ed-979a-244bfe7dd4fe', 'QUI0046', 'Visus VT-002', 'ec057c2b-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed26d9b0-981d-11ed-979a-244bfe7dd4fe', 'QUI0047', 'Visus VT-003', 'ec057c2b-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed26de62-981d-11ed-979a-244bfe7dd4fe', 'QUI0049', 'Laboratoires Thea LT2764-001', 'ebf1a543-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed26e33e-981d-11ed-979a-244bfe7dd4fe', 'RIT0001', 'Ritter G28-006', 'ebfdfe0c-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed26e78d-981d-11ed-979a-244bfe7dd4fe', 'ROM0001', 'Romark RM08-2001', 'ebfe61b2-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed26ec25-981d-11ed-979a-244bfe7dd4fe', 'ROM0002', 'Romark RM08-3004', 'ebfe61b2-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed26f0ad-981d-11ed-979a-244bfe7dd4fe', 'ROM0003', 'Romark RM08-3005', 'ebfe61b2-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed26f4fd-981d-11ed-979a-244bfe7dd4fe', 'ROM0006', 'Romark RM08-3010', 'ebfe61b2-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed26f942-981d-11ed-979a-244bfe7dd4fe', 'ROM0007', 'Romark RM08-3011', 'ebfe61b2-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed26fe14-981d-11ed-979a-244bfe7dd4fe', 'SOJ0001', 'Sojournix SJX-653-006', 'ec004c54-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed2702b7-981d-11ed-979a-244bfe7dd4fe', 'SUN0002', 'Sunovion SUN101-401', 'ec010ed0-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed270705-981d-11ed-979a-244bfe7dd4fe', 'UCB0001', 'UCB MOG001', 'ec03d580-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed270b52-981d-11ed-979a-244bfe7dd4fe', 'UCB9999', 'UCB eLAS v1', 'ec03653c-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed270fc6-981d-11ed-979a-244bfe7dd4fe', 'VIE0001', 'Viela Bio Sjogren&#039;s Suite Pilot', 'ec050371-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed271484-981d-11ed-979a-244bfe7dd4fe', 'WCT0001', 'EIP-VX17-745-304', 'ebe850af-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed27196f-981d-11ed-979a-244bfe7dd4fe', 'WCT0003', 'Alvotech AVT02-GL-303', 'ebdb3167-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed271eee-981d-11ed-979a-244bfe7dd4fe', 'WCT0004', 'Alvotech AVT02-GL-302', 'ebdb3167-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed2723e1-981d-11ed-979a-244bfe7dd4fe', 'WCT0005', 'Cerevance CVN424-201', 'ebe13e80-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed272867-981d-11ed-979a-244bfe7dd4fe', 'WCT0007', 'BlackThorn K2-MDD-201', 'ebdf4018-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed272cbc-981d-11ed-979a-244bfe7dd4fe', 'WCT0012', 'Compass COMP 004', 'ebe56a02-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed273140-981d-11ed-979a-244bfe7dd4fe', 'WCT0013', 'Alvotech AVT04-GL-301', 'ebdb3167-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed2735aa-981d-11ed-979a-244bfe7dd4fe', 'WCT0014', 'Calico M20-405', 'ebe0088e-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed273a28-981d-11ed-979a-244bfe7dd4fe', 'WCT0015', 'Cyclo CTD-TCNPC-301', 'ebe61cfb-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed273e79-981d-11ed-979a-244bfe7dd4fe', 'WCT0016', 'GATE-251-C-201', 'ebeafdec-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed2742d4-981d-11ed-979a-244bfe7dd4fe', 'WCT0017', 'Dynacure DYN101-C102', 'ebe7ef0d-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed274733-981d-11ed-979a-244bfe7dd4fe', 'WCT0018', 'Dynacure NH-CNM-001', 'ebe7ef0d-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed274b80-981d-11ed-979a-244bfe7dd4fe', 'WCT0022', 'Eliem 020155-201', 'ebe970f8-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed274fd1-981d-11ed-979a-244bfe7dd4fe', 'WCT0023', 'Eliem 020155-202', 'ebe970f8-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed27543c-981d-11ed-979a-244bfe7dd4fe', 'WCT0024', 'Actinogen ACW0007', 'ebd922c0-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed275884-981d-11ed-979a-244bfe7dd4fe', 'WCT0025', 'Compass Pathways COMP 401', 'ebe56a02-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `study_history`
--

CREATE TABLE `study_history` (
  `id` char(36) NOT NULL,
  `action` char(8) DEFAULT NULL,
  `modifiedByAppUserid` char(36) NOT NULL,
  `studyid` char(36) DEFAULT NULL,
  `ciStudyProjectCode` char(7) DEFAULT NULL,
  `protocolName` varchar(255) DEFAULT NULL,
  `sponsorid` char(36) DEFAULT NULL,
  `croid` char(36) DEFAULT NULL,
  `libraryFlag` char(1) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `studyType` char(10) DEFAULT NULL,
  `studyPhaseFlag` char(1) DEFAULT NULL,
  `isLegacyFlag` char(1) DEFAULT NULL,
  `isRemovedFlag` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `study_history`
--

INSERT INTO `study_history` (`id`, `action`, `modifiedByAppUserid`, `studyid`, `ciStudyProjectCode`, `protocolName`, `sponsorid`, `croid`, `libraryFlag`, `createdDate`, `lastModifiedDate`, `studyType`, `studyPhaseFlag`, `isLegacyFlag`, `isRemovedFlag`) VALUES
('ed4ea323-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed236b3c-981d-11ed-979a-244bfe7dd4fe', 'CI-0012', 'Standard Library', 'ebe26a18-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed4f015c-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed2371d0-981d-11ed-979a-244bfe7dd4fe', 'ALK0002', 'Alkermes ALK21-029', 'ebda59d8-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed4f35a7-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed23785d-981d-11ed-979a-244bfe7dd4fe', 'ALK0004', 'ALKS 3831-A312', 'ebda59d8-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed4f6684-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed237e18-981d-11ed-979a-244bfe7dd4fe', 'ALK0005', 'Alkermes ALKS 3831-A313', 'ebda59d8-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed4f97a9-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed238403-981d-11ed-979a-244bfe7dd4fe', 'ALK0001', 'Alkermes ALK5461-217 ', 'ebda59d8-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed4fc8b5-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed238ad5-981d-11ed-979a-244bfe7dd4fe', 'ALL0001', 'Allergan 3143-101-012', 'ebdaca32-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed4ff866-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed23912f-981d-11ed-979a-244bfe7dd4fe', 'AMG0002', 'Amgen 20180130', 'ebdc02b9-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed5028ca-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed239b68-981d-11ed-979a-244bfe7dd4fe', 'AMG0000', 'Amgen eLAS', 'ebe26a18-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed50584c-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed23a05f-981d-11ed-979a-244bfe7dd4fe', 'AVA0001', '20-AVP-786-306', 'ebdd9f29-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed508b5a-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed23a4f9-981d-11ed-979a-244bfe7dd4fe', 'AVA0002', '20-AVP-786-307', 'ebdd9f29-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed50bc6c-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed23a967-981d-11ed-979a-244bfe7dd4fe', 'CI-0011', 'General COVID Library', 'ebe26a18-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed50f443-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed23ade5-981d-11ed-979a-244bfe7dd4fe', 'CI-0013', 'Scleroderma', 'ebe26a18-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed5127a9-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed23b24d-981d-11ed-979a-244bfe7dd4fe', 'CI-0005', 'Clinical Ink Demo Suite', 'ebe3921e-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed515ac0-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed23b6dd-981d-11ed-979a-244bfe7dd4fe', 'CLL0001', 'Acumen ACU-001', 'ebd9f15b-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed518eae-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed23bd53-981d-11ed-979a-244bfe7dd4fe', 'CLP0002', 'Tasly T89-31-AMS', 'ec01dae7-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed51c07a-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed23c2ad-981d-11ed-979a-244bfe7dd4fe', 'COG0004', 'Cogstate-Eli Lilly I5T-MC-AACG', 'ebe9144e-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed51f4d3-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed23c84a-981d-11ed-979a-244bfe7dd4fe', 'COG0005', 'Biogen 251AD201', 'ebe50803-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed52288b-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed23cce5-981d-11ed-979a-244bfe7dd4fe', 'COG0006', 'Cogstate AgeneBio AGB101 MCD', 'ebe50803-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed525baa-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed23d1c2-981d-11ed-979a-244bfe7dd4fe', 'COG0007', 'Cogstate I8G-MC-LMDC', 'ebe9144e-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed528e7b-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed23d63b-981d-11ed-979a-244bfe7dd4fe', 'COG0008', 'NovoNordisk HAEM-4436', 'ebf81578-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed52bbe6-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed23db21-981d-11ed-979a-244bfe7dd4fe', 'COG0009', 'NovoNordisk P5', 'ebf81578-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed53667c-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed23df80-981d-11ed-979a-244bfe7dd4fe', 'COG0010', 'NovoNordisk P6', 'ebf81578-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed539680-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed23e40d-981d-11ed-979a-244bfe7dd4fe', 'COG0011', 'NovoNordisk P5 English Sites', 'ebf81578-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed53c7b1-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed23e85f-981d-11ed-979a-244bfe7dd4fe', 'COG0012', 'NovoNordisk P6 English Sites', 'ebf81578-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed53f8e7-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed23ecb2-981d-11ed-979a-244bfe7dd4fe', 'COG0013', 'Toyama T817EU201', 'ec0304c7-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed542dc9-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed23f0fa-981d-11ed-979a-244bfe7dd4fe', 'COG0015', 'Biogen 251AD201 LTE', 'ebe50803-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed545d82-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed23f554-981d-11ed-979a-244bfe7dd4fe', 'COG0016', 'Roche BP41316', 'ebeccfc0-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed548e69-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed23f9aa-981d-11ed-979a-244bfe7dd4fe', 'COG0017', 'Roche Library', 'ebeccfc0-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed54bca4-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed23fde8-981d-11ed-979a-244bfe7dd4fe', 'COG0018', 'Eisai BAN2401', 'ebe8b1e8-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed54ea0e-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed24022a-981d-11ed-979a-244bfe7dd4fe', 'COG0019', 'Roche BP41674', 'ebeccfc0-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed551cc8-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed2406e3-981d-11ed-979a-244bfe7dd4fe', 'COG0020', 'Eli Lilly 15T-MC-AACI', 'ebe9144e-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed554af3-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed240c68-981d-11ed-979a-244bfe7dd4fe', 'COG0021', 'Takeda TAK-071-2002', 'ec0177a2-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed557b50-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed2411be-981d-11ed-979a-244bfe7dd4fe', 'COG0022', 'Eli Lilly I5T-MC-AACH', 'ebe9144e-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed55abaa-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed2416ad-981d-11ed-979a-244bfe7dd4fe', 'COG0023', 'INmune Bio XPROTM-AD-02', 'ebeea55c-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed55dbbe-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed241b2a-981d-11ed-979a-244bfe7dd4fe', 'COG0024', 'INmune Bio, Inc._XPro-AD-03', 'ebeea55c-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed560c95-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed242005-981d-11ed-979a-244bfe7dd4fe', 'EIS0002', 'Eisai-E2007-A001-408', 'ebe8b1e8-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed563b3b-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed24247e-981d-11ed-979a-244bfe7dd4fe', 'eLAS001', 'eLAS v1', 'ebe26a18-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed566d56-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed2428f2-981d-11ed-979a-244bfe7dd4fe', 'eLAS002', 'eLAS v2', 'ebe26a18-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed569ac3-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed242d4c-981d-11ed-979a-244bfe7dd4fe', 'eLAS003', 'eLAS v3', 'ebe26a18-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed56ca6d-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed24338f-981d-11ed-979a-244bfe7dd4fe', 'eLAS004', 'eLAS v4', 'ebe26a18-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed56fa63-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed243de2-981d-11ed-979a-244bfe7dd4fe', 'EME0001', 'Emergo NKT-201', 'ebe9d9d8-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed57321d-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed2442d1-981d-11ed-979a-244bfe7dd4fe', 'EME0002', 'Emergo NKT-202', 'ebe9d9d8-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed57658d-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed244748-981d-11ed-979a-244bfe7dd4fe', 'EME0003', 'Emergo NKT-203', 'ebe9d9d8-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed5798b0-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed244bf7-981d-11ed-979a-244bfe7dd4fe', 'END0002', 'EN3835-222 ', 'ebea3c8b-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed57cf5a-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed2451b6-981d-11ed-979a-244bfe7dd4fe', 'END0003', 'Endo EN3835-306', 'ebea3c8b-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed58065f-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed245765-981d-11ed-979a-244bfe7dd4fe', 'END0005', 'Endo EN3835-106', 'ebea3c8b-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed583b12-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed245cc8-981d-11ed-979a-244bfe7dd4fe', 'EVL0001', 'Evelo EDP1815-207', 'ebea9a6d-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed5872db-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed2462f6-981d-11ed-979a-244bfe7dd4fe', 'EVL0002', 'Evelo EDP1815-208', 'ebea9a6d-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed58a5f2-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed246ed6-981d-11ed-979a-244bfe7dd4fe', 'GEN0001', 'Genentech GA30044', 'ebeb5932-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed58defc-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed247390-981d-11ed-979a-244bfe7dd4fe', 'GEN0002', 'Genentech GA30066', 'ebeb5932-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed5910de-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed247873-981d-11ed-979a-244bfe7dd4fe', 'ICN0007', 'Indivior INDV-6000-401', 'ebede803-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed594253-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed247cd8-981d-11ed-979a-244bfe7dd4fe', 'ICN0008', 'Diamyd DIAGNODE-3', 'ebe79243-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed597539-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed248157-981d-11ed-979a-244bfe7dd4fe', 'ICN0010', 'Statera 103-301', 'ec00b02f-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed59a75f-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed2486d7-981d-11ed-979a-244bfe7dd4fe', 'ICN0011', 'MoonLake M1095-PSA-201', 'ebf49178-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed59d9c0-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed248c6e-981d-11ed-979a-244bfe7dd4fe', 'ICN0012', 'MoonLake M1095-SPA-201', 'ebf49178-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed5a0aaa-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed2491c6-981d-11ed-979a-244bfe7dd4fe', 'ICN0013', 'MoonLake Immunotherapeutics AG_M1095-HS-201', 'ebf49178-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed5a3e00-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed249682-981d-11ed-979a-244bfe7dd4fe', 'INC0002', 'Dart DNS-7801-201', 'ebe6d577-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed5a6ebf-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed249b3a-981d-11ed-979a-244bfe7dd4fe', 'INC0003', 'Otsuka 331-14-213', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed5aa0f7-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed24a408-981d-11ed-979a-244bfe7dd4fe', 'INC0005', 'Otsuka 405-201-00002', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed5ad280-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed24aa81-981d-11ed-979a-244bfe7dd4fe', 'INC0006', 'Dart DNS-7801-201 Part B', 'ebe6d577-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed5b0b2b-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed24b037-981d-11ed-979a-244bfe7dd4fe', 'INC0008', 'Otsuka 405-201-00013', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed5b3d67-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed24b52b-981d-11ed-979a-244bfe7dd4fe', 'INC0009', 'Otsuka 405-201-00014', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed5b6d70-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed24b9d2-981d-11ed-979a-244bfe7dd4fe', 'INC0010', 'Otsuka 405-201-00015', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed5ba09b-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed24be7f-981d-11ed-979a-244bfe7dd4fe', 'INC0011', 'Revance 1720302', 'ebfda1f4-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed5bd220-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed24c2e4-981d-11ed-979a-244bfe7dd4fe', 'INC0013', 'Revance 1720304', 'ebfda1f4-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed5c1502-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed24c74f-981d-11ed-979a-244bfe7dd4fe', 'INC0014', 'Otsuka 331-201-00182', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed5c4766-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed24cc74-981d-11ed-979a-244bfe7dd4fe', 'INC0015', 'AnaptysBio ANB020-006', 'ebdc6931-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed5c7a85-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed24d0d3-981d-11ed-979a-244bfe7dd4fe', 'INC0016', 'Otsuka 331-201-00071', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed5caadd-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed24d52f-981d-11ed-979a-244bfe7dd4fe', 'INC0017', 'Otsuka 331-201-00072', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed5cdff8-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed24d97a-981d-11ed-979a-244bfe7dd4fe', 'INC0018', 'Otsuka 331-201-00195', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed5d100b-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed24ddc4-981d-11ed-979a-244bfe7dd4fe', 'INC0019', 'Relmada REL-1017-301', 'ebfcd53b-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed5d40ab-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed24e226-981d-11ed-979a-244bfe7dd4fe', 'INC0020', 'Relmada REL-1017-310', 'ebfcd53b-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed5d710f-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed24e67b-981d-11ed-979a-244bfe7dd4fe', 'INC0022', 'Otsuka 331-201-00348', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed5dbd7d-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed24eb33-981d-11ed-979a-244bfe7dd4fe', 'INC0023', 'Harmony Biosciences HBS-101-CL-010', 'ebec70b3-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed5df529-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed24efde-981d-11ed-979a-244bfe7dd4fe', 'INC0007', 'Otsuka 331-201-00079', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed5e2428-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed24f522-981d-11ed-979a-244bfe7dd4fe', 'JAN0002', 'Janssen 80202135SLE2001', 'ebef6734-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed5e567a-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed24fa5f-981d-11ed-979a-244bfe7dd4fe', 'JAN0003', 'Janssen 80202135LUN2001', 'ebef6734-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed5e8dd0-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed24ff4c-981d-11ed-979a-244bfe7dd4fe', 'KER0001', 'KeraNetics KSGL-CRD-004', 'ebf084e5-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed5ebcf9-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed2503e6-981d-11ed-979a-244bfe7dd4fe', 'LLY0001', 'I9X-MC-MTAE', 'ebe9144e-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed5eedd4-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed250868-981d-11ed-979a-244bfe7dd4fe', 'LLY0002', 'Eli Lilly J1V-MC-IMMA', 'ebe9144e-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed5f222c-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed250cba-981d-11ed-979a-244bfe7dd4fe', 'LLY0004', 'Eli Lilly_15T-MC-AACM', 'ebe9144e-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed5f52b8-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed251124-981d-11ed-979a-244bfe7dd4fe', 'LLY0005', 'Eli Lilly_I5T-MC-AACO', 'ebe9144e-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed5f835e-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed251573-981d-11ed-979a-244bfe7dd4fe', 'LLY9999', 'Eli Lilly eLAS v3.0', 'ebe9144e-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed5fb2a1-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed2519c5-981d-11ed-979a-244bfe7dd4fe', 'MYC0001', 'VMT-VT-1161-CL-011', 'ebf4f22a-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed5fe936-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed251e14-981d-11ed-979a-244bfe7dd4fe', 'MYC0002', 'VMT-VT-1161-CL-012', 'ebf4f22a-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed60288d-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed252262-981d-11ed-979a-244bfe7dd4fe', 'NEU0001', 'Neurana CLN-116', 'ebf607b9-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed605932-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed2526ab-981d-11ed-979a-244bfe7dd4fe', 'NVS0041', 'Novartis COMB157GUS10', 'ebf6711b-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed608dab-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed252af7-981d-11ed-979a-244bfe7dd4fe', 'NVS0042', 'Novartis OMB157GUS09', 'ebf6711b-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed60c646-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed252f69-981d-11ed-979a-244bfe7dd4fe', 'NVS0043', 'Novartis CLNP023K12201', 'ebf6711b-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed60fa87-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed2533ae-981d-11ed-979a-244bfe7dd4fe', 'NVS0044', 'Novartis CVAY736K12301', 'ebf6711b-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed6138d1-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed253805-981d-11ed-979a-244bfe7dd4fe', 'OTS0016', 'Otsuka 331-201-00080', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed6179a8-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed253c52-981d-11ed-979a-244bfe7dd4fe', 'OTS0017', 'Otsuka 331-201-00081', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed61b024-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed25416b-981d-11ed-979a-244bfe7dd4fe', 'OTS0018', 'Otsuka 277-201-00001', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed61ebc2-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed25468d-981d-11ed-979a-244bfe7dd4fe', 'OTS0021', 'Otsuka 331-201-00083', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed6221e0-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed254b8e-981d-11ed-979a-244bfe7dd4fe', 'OTS0022', 'Otsuka 331-201-00103', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed6253b6-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed255080-981d-11ed-979a-244bfe7dd4fe', 'OTS0023', 'Otsuka 405-201-00007', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed6296a4-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed25555e-981d-11ed-979a-244bfe7dd4fe', 'OTS0024', 'Otsuka 303-201-00002', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed62d3e2-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed2559c1-981d-11ed-979a-244bfe7dd4fe', 'OTS0025', 'Otsuka 156-201-00183', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed6306f8-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed255e1e-981d-11ed-979a-244bfe7dd4fe', 'OTS0026', 'Otsuka 31-14-204', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed633e4c-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed2562bc-981d-11ed-979a-244bfe7dd4fe', 'OTS0027', 'Otsuka 031-201-00301', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed63705c-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed256764-981d-11ed-979a-244bfe7dd4fe', 'OTS0028', 'Otsuka 341-201-00004', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed63a797-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed256c83-981d-11ed-979a-244bfe7dd4fe', 'OTS0029', 'Otsuka 331-201-00084', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed63e5c0-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed25716a-981d-11ed-979a-244bfe7dd4fe', 'OTS0030', 'Otsuka 331-201-00148', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed641b57-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed257618-981d-11ed-979a-244bfe7dd4fe', 'OTS0031', 'Otsuka 331-201-00191', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed644f4e-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed257c7c-981d-11ed-979a-244bfe7dd4fe', 'OTS0032', 'Otsuka 301-201-000181', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed647f89-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed2581fb-981d-11ed-979a-244bfe7dd4fe', 'OTS0033', 'Otsuka 331-201-00242', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed64c344-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed25869f-981d-11ed-979a-244bfe7dd4fe', 'OTS0035', 'Otsuka 331-201-00176', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed64f15c-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed258b5c-981d-11ed-979a-244bfe7dd4fe', 'OTS0037', 'Otsuka 405-201-00016', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed6548ac-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed25903f-981d-11ed-979a-244bfe7dd4fe', 'OTS0038', 'Otsuka 405-201-00017', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed65798f-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed25953a-981d-11ed-979a-244bfe7dd4fe', 'OTS0039', 'Otsuka 405-201-00021', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed65ab8a-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed2599dc-981d-11ed-979a-244bfe7dd4fe', 'OTS0042', 'Otsuka 325-201-00005', 'ebfa1cde-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed65deab-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed259e92-981d-11ed-979a-244bfe7dd4fe', 'PPD0001', 'Viela Bio VIB7734.P2.S1', 'ec050371-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed66121b-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed25a354-981d-11ed-979a-244bfe7dd4fe', 'PPD0003', 'Prometheus Labs PR200-104', 'ebfba873-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed6643f8-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed25a7b0-981d-11ed-979a-244bfe7dd4fe', 'PPD0005', 'Zenas Biopharma ZB012-03-001', 'ec06398f-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed66760a-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed25ac03-981d-11ed-979a-244bfe7dd4fe', 'PPD0007', 'Horizon Therapeutics HZNP-DAX-204', 'ebed2cc2-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed66a5f3-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed25b06b-981d-11ed-979a-244bfe7dd4fe', 'PPD0008', 'Horizon Therapeutics HZNP-DAX-203', 'ebed2cc2-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed66d624-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed25b4c2-981d-11ed-979a-244bfe7dd4fe', 'PRG0001', 'Covis ALV-020-001', 'ebe5c414-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed670ac6-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed25bbf6-981d-11ed-979a-244bfe7dd4fe', 'PRG0002', 'Larimar CLIN-1601-201', 'ebf25df7-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed673ed2-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed25c0aa-981d-11ed-979a-244bfe7dd4fe', 'PRG0003', 'OPX-PR-01', 'ebf95b50-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed677210-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed25c529-981d-11ed-979a-244bfe7dd4fe', 'PRG0004', 'Bionomics Ltd BNC210.012', 'ebded62d-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed67a2d6-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed25c9ab-981d-11ed-979a-244bfe7dd4fe', 'PRG0008', 'Premier Research 039(C)MD21046', 'ebdccf3c-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed67d3f8-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed25cdf4-981d-11ed-979a-244bfe7dd4fe', 'PRX0002', 'Camurus AB HS-17-585', 'ebe07010-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed680538-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed25d23e-981d-11ed-979a-244bfe7dd4fe', 'PRX0003', 'Glenmark GBR830-204', 'ebec114c-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed6838da-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed25d6a0-981d-11ed-979a-244bfe7dd4fe', 'PRX0004', '4D Pharma BHT-II-002', 'ebd7748e-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed6867e7-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed25dbf1-981d-11ed-979a-244bfe7dd4fe', 'PRX0005', 'Kyowa Hakko Kirin 4083-006', 'ebf147fc-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed68a560-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed25e116-981d-11ed-979a-244bfe7dd4fe', 'PRX0006', 'Xynomic Pharmaceuticals XYN-602', 'ec05df10-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed68d735-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed25e682-981d-11ed-979a-244bfe7dd4fe', 'PRX0007', '4D Pharma MRx0004', 'ebd7748e-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed69064f-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed25ec16-981d-11ed-979a-244bfe7dd4fe', 'PRX0008', 'Urovant URO-901-3005', 'ec043bdf-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed6936ad-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed25f0ee-981d-11ed-979a-244bfe7dd4fe', 'PRX0011', 'UCB MG0003', 'ec03653c-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed696e2c-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed25f590-981d-11ed-979a-244bfe7dd4fe', 'PRX0015', 'Urovant URO-901-3006', 'ec043bdf-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed699c2e-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed25fa86-981d-11ed-979a-244bfe7dd4fe', 'PRX0017', 'Sarepta SRP-9001-301', 'ebf6711b-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed69cd5e-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed25fefa-981d-11ed-979a-244bfe7dd4fe', 'PRX0019', 'UCB SL0043', 'ec03653c-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed69ff56-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed2603b1-981d-11ed-979a-244bfe7dd4fe', 'PRX0021', 'Kinevant KIN-1901-2001', 'ebf0e909-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed6a3061-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed26082f-981d-11ed-979a-244bfe7dd4fe', 'PRX0024', 'UCB SL0044', 'ec03653c-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed6a64ad-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed260ca2-981d-11ed-979a-244bfe7dd4fe', 'PRX0025', 'UCB SL0046', 'ec03653c-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed6a93ad-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed2610ef-981d-11ed-979a-244bfe7dd4fe', 'PRX0026', 'UCB CD0003', 'ec03d580-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed6acbc9-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed261575-981d-11ed-979a-244bfe7dd4fe', 'PRX0029', 'Sarepta SRP-9001-103', 'ebfec40e-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed6b02b8-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed2619d0-981d-11ed-979a-244bfe7dd4fe', 'PRX0030', 'UCB AIE001', 'ec03d580-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed6b3794-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed261e32-981d-11ed-979a-244bfe7dd4fe', 'PRX0033', 'Alpine AIS-A03', 'ebe26a18-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed6b665a-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed26227f-981d-11ed-979a-244bfe7dd4fe', 'PRX0035', 'ONO-4059-09', 'ebf8ed7e-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed6b9603-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed2626d6-981d-11ed-979a-244bfe7dd4fe', 'PRX0036', 'UCB PS0032 ', 'ec03653c-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed6bd1cf-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed262bc3-981d-11ed-979a-244bfe7dd4fe', 'PRX0037', 'UCB EP0162', 'ec03d580-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed6c0224-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed2630ce-981d-11ed-979a-244bfe7dd4fe', 'PRX0039', 'UCB EP0165', 'ec03d580-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed6c4107-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed2635d1-981d-11ed-979a-244bfe7dd4fe', 'PRX0041', 'Sarepta SRP-9001-303', 'ebfec40e-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed6c72d0-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed263a49-981d-11ed-979a-244bfe7dd4fe', 'PRX0043', 'Boehringer Ingelheim 352-2159', 'ebdfa28d-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed6ca0ce-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed263f1c-981d-11ed-979a-244bfe7dd4fe', 'QUI0010', 'Eli Lilly I4V-MC-JAHZ', 'ebe9144e-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed6cd662-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed26438f-981d-11ed-979a-244bfe7dd4fe', 'QUI0011', 'Eli Lilly I4V-MC-JAIA', 'ebe9144e-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed6d0fc2-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed2647e1-981d-11ed-979a-244bfe7dd4fe', 'QUI0002', 'InflaRx HS IFX-1-P2.4', 'ebee412d-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed6d49ee-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed264c49-981d-11ed-979a-244bfe7dd4fe', 'QUI0005', 'AnaptysBio ANB020-005', 'ebdc6931-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed6d81f0-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed2650a9-981d-11ed-979a-244bfe7dd4fe', 'QUI0006', 'Merck MS201781-0031', 'ebf376f1-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed6db737-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed2654fd-981d-11ed-979a-244bfe7dd4fe', 'QUI0013', 'Eli Lilly 14V-MC-JAIM', 'ebe9144e-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed6ded98-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed265a9f-981d-11ed-979a-244bfe7dd4fe', 'QUI0014', 'Merck EMR700461-025', 'ebf376f1-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed6e21ba-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed265fc1-981d-11ed-979a-244bfe7dd4fe', 'QUI0017', 'Merck MS200647-0037', 'ebf376f1-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed6e553c-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed2664aa-981d-11ed-979a-244bfe7dd4fe', 'QUI0018', 'CKD 182RA18009', 'ebe1a650-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed6e8c74-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed2669d8-981d-11ed-979a-244bfe7dd4fe', 'QUI0021', 'Merck MS201943-0029', 'ebf376f1-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed6ebf8c-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed266e42-981d-11ed-979a-244bfe7dd4fe', 'QUI0022', 'Merck MS200647-0005', 'ebf376f1-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed6ef14a-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed267390-981d-11ed-979a-244bfe7dd4fe', 'QUI0023', 'Merck MS200647-0047', 'ebf376f1-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed6f293f-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed2679b5-981d-11ed-979a-244bfe7dd4fe', 'QUI0024', 'Eli Lilly J1V-MC-IMMA', 'ebe9144e-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed6f62ff-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed267ee3-981d-11ed-979a-244bfe7dd4fe', 'QUI0026', 'Amgen AMG 592 20200234', 'ebdc02b9-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed6f9509-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed26848a-981d-11ed-979a-244bfe7dd4fe', 'QUI0027', 'Amgen AMG 570 20170588', 'ebe26a18-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed6fc95b-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed268bcb-981d-11ed-979a-244bfe7dd4fe', 'QUI0028', 'NuCana NuTide 121', 'ebf88b0d-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed6fff82-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed269106-981d-11ed-979a-244bfe7dd4fe', 'QUI0029', 'Pear-006-101', 'ebfa7e93-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed703a02-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed2695bc-981d-11ed-979a-244bfe7dd4fe', 'QUI0030', 'Novartis CBAF312AUS02', 'ebf6711b-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed706c8b-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed269a16-981d-11ed-979a-244bfe7dd4fe', 'QUI0031', 'Merck MS200467-0055', 'ebf376f1-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed70a15d-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed269e9a-981d-11ed-979a-244bfe7dd4fe', 'QUI0032', 'TLC-PCT-001', 'ec02a55f-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed70d1a4-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed26a322-981d-11ed-979a-244bfe7dd4fe', 'QUI0033', 'PEAR-006-101 Part 2', 'ebfa7e93-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed710037-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed26a818-981d-11ed-979a-244bfe7dd4fe', 'QUI0034', 'Merck MS200647-0017', 'ebf376f1-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed7138c5-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed26ac87-981d-11ed-979a-244bfe7dd4fe', 'QUI0035', '- Merck MS200569-0002', 'ebf376f1-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed716c0f-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed26b182-981d-11ed-979a-244bfe7dd4fe', 'QUI0036', 'Eli Lilly J1P-MC-KFAJ', 'ebe9144e-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed719b76-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed26b633-981d-11ed-979a-244bfe7dd4fe', 'QUI0037', 'UCB N01269', 'ec03d580-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed71cf35-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed26baf3-981d-11ed-979a-244bfe7dd4fe', 'QUI0038', 'AstraZeneca D3465C00001', 'ebdd3968-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed7209cd-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed26c0c4-981d-11ed-979a-244bfe7dd4fe', 'QUI0041', 'UCB EP0132', 'ec03d580-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed723b4f-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed26c5f4-981d-11ed-979a-244bfe7dd4fe', 'QUI0042', 'AstraZeneca D8850C00003', 'ebdd3968-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed72829e-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed26caee-981d-11ed-979a-244bfe7dd4fe', 'QUI0043', 'Merck MS201923-0050', 'ebf376f1-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed72beb7-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed26cfc1-981d-11ed-979a-244bfe7dd4fe', 'QUI0044', 'Visus VT-001', 'ec057c2b-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed72f5c9-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed26d4e2-981d-11ed-979a-244bfe7dd4fe', 'QUI0046', 'Visus VT-002', 'ec057c2b-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed7329e2-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed26d9b0-981d-11ed-979a-244bfe7dd4fe', 'QUI0047', 'Visus VT-003', 'ec057c2b-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed735ac9-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed26de62-981d-11ed-979a-244bfe7dd4fe', 'QUI0049', 'Laboratoires Thea LT2764-001', 'ebf1a543-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed738f58-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed26e33e-981d-11ed-979a-244bfe7dd4fe', 'RIT0001', 'Ritter G28-006', 'ebfdfe0c-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed73edd7-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed26e78d-981d-11ed-979a-244bfe7dd4fe', 'ROM0001', 'Romark RM08-2001', 'ebfe61b2-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed74be81-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed26ec25-981d-11ed-979a-244bfe7dd4fe', 'ROM0002', 'Romark RM08-3004', 'ebfe61b2-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed75219d-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed26f0ad-981d-11ed-979a-244bfe7dd4fe', 'ROM0003', 'Romark RM08-3005', 'ebfe61b2-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed75b8d5-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed26f4fd-981d-11ed-979a-244bfe7dd4fe', 'ROM0006', 'Romark RM08-3010', 'ebfe61b2-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed763737-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed26f942-981d-11ed-979a-244bfe7dd4fe', 'ROM0007', 'Romark RM08-3011', 'ebfe61b2-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed768690-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed26fe14-981d-11ed-979a-244bfe7dd4fe', 'SOJ0001', 'Sojournix SJX-653-006', 'ec004c54-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL);
INSERT INTO `study_history` (`id`, `action`, `modifiedByAppUserid`, `studyid`, `ciStudyProjectCode`, `protocolName`, `sponsorid`, `croid`, `libraryFlag`, `createdDate`, `lastModifiedDate`, `studyType`, `studyPhaseFlag`, `isLegacyFlag`, `isRemovedFlag`) VALUES
('ed76e3e0-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed2702b7-981d-11ed-979a-244bfe7dd4fe', 'SUN0002', 'Sunovion SUN101-401', 'ec010ed0-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed771495-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed270705-981d-11ed-979a-244bfe7dd4fe', 'UCB0001', 'UCB MOG001', 'ec03d580-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed77500b-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed270b52-981d-11ed-979a-244bfe7dd4fe', 'UCB9999', 'UCB eLAS v1', 'ec03653c-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed77838b-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed270fc6-981d-11ed-979a-244bfe7dd4fe', 'VIE0001', 'Viela Bio Sjogren&#039;s Suite Pilot', 'ec050371-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed77b8c4-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed271484-981d-11ed-979a-244bfe7dd4fe', 'WCT0001', 'EIP-VX17-745-304', 'ebe850af-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed77f02e-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed27196f-981d-11ed-979a-244bfe7dd4fe', 'WCT0003', 'Alvotech AVT02-GL-303', 'ebdb3167-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed78243d-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed271eee-981d-11ed-979a-244bfe7dd4fe', 'WCT0004', 'Alvotech AVT02-GL-302', 'ebdb3167-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed785642-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed2723e1-981d-11ed-979a-244bfe7dd4fe', 'WCT0005', 'Cerevance CVN424-201', 'ebe13e80-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed78992f-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed272867-981d-11ed-979a-244bfe7dd4fe', 'WCT0007', 'BlackThorn K2-MDD-201', 'ebdf4018-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed78cf86-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed272cbc-981d-11ed-979a-244bfe7dd4fe', 'WCT0012', 'Compass COMP 004', 'ebe56a02-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed7900b1-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed273140-981d-11ed-979a-244bfe7dd4fe', 'WCT0013', 'Alvotech AVT04-GL-301', 'ebdb3167-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed7933d3-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed2735aa-981d-11ed-979a-244bfe7dd4fe', 'WCT0014', 'Calico M20-405', 'ebe0088e-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed796745-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed273a28-981d-11ed-979a-244bfe7dd4fe', 'WCT0015', 'Cyclo CTD-TCNPC-301', 'ebe61cfb-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed799b7a-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed273e79-981d-11ed-979a-244bfe7dd4fe', 'WCT0016', 'GATE-251-C-201', 'ebeafdec-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed7a48cb-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed2742d4-981d-11ed-979a-244bfe7dd4fe', 'WCT0017', 'Dynacure DYN101-C102', 'ebe7ef0d-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed7a7aa4-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed274733-981d-11ed-979a-244bfe7dd4fe', 'WCT0018', 'Dynacure NH-CNM-001', 'ebe7ef0d-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed7ac3c8-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed274b80-981d-11ed-979a-244bfe7dd4fe', 'WCT0022', 'Eliem 020155-201', 'ebe970f8-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed7affdd-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed274fd1-981d-11ed-979a-244bfe7dd4fe', 'WCT0023', 'Eliem 020155-202', 'ebe970f8-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed7b336c-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed27543c-981d-11ed-979a-244bfe7dd4fe', 'WCT0024', 'Actinogen ACW0007', 'ebd922c0-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL),
('ed7b7295-981d-11ed-979a-244bfe7dd4fe', 'IMPORT', '26d0c045-9803-11ed-979a-244bfe7dd4fe', 'ed275884-981d-11ed-979a-244bfe7dd4fe', 'WCT0025', 'Compass Pathways COMP 401', 'ebe56a02-981d-11ed-979a-244bfe7dd4fe', NULL, NULL, '2023-01-19 12:22:49', NULL, NULL, NULL, '1', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `visit`
--

CREATE TABLE `visit` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `armid` char(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT current_timestamp(),
  `lastModifiedDate` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `visitOrder` int(11) DEFAULT NULL,
  `visitType` char(1) DEFAULT NULL,
  `filemakerVisitNote` text DEFAULT NULL,
  `isRemovedFlag` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `visittrigger`
--

CREATE TABLE `visittrigger` (
  `id` char(36) NOT NULL,
  `studyid` char(36) DEFAULT NULL,
  `triggerSourceType` varchar(5) DEFAULT NULL,
  `sourceFormVisitLinkid` char(36) DEFAULT NULL,
  `sourceTriggerFieldid` char(36) DEFAULT NULL,
  `triggerVisitid` char(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT current_timestamp(),
  `lastModifiedDate` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `isRemovedFlag` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `visittrigger_history`
--

CREATE TABLE `visittrigger_history` (
  `id` char(36) NOT NULL,
  `action` char(8) DEFAULT NULL,
  `modifiedByAppUserid` char(36) NOT NULL,
  `visitTriggerid` char(36) DEFAULT NULL,
  `studyid` char(36) DEFAULT NULL,
  `triggerSourceType` varchar(5) DEFAULT NULL,
  `sourceFormVisitLinkid` char(36) DEFAULT NULL,
  `sourceTriggerFieldid` char(36) DEFAULT NULL,
  `triggerVisitid` char(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `isRemovedFlag` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `visit_history`
--

CREATE TABLE `visit_history` (
  `id` char(36) NOT NULL,
  `action` char(8) DEFAULT NULL,
  `modifiedByAppUserid` char(36) NOT NULL,
  `visitid` char(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `armid` char(36) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  `visitOrder` int(11) DEFAULT NULL,
  `visitType` char(1) DEFAULT NULL,
  `filemakerVisitNote` text DEFAULT NULL,
  `isRemovedFlag` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `apiusers`
--
ALTER TABLE `apiusers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `approles`
--
ALTER TABLE `approles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `appusers`
--
ALTER TABLE `appusers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `arm`
--
ALTER TABLE `arm`
  ADD PRIMARY KEY (`id`),
  ADD KEY `studyid` (`studyid`);

--
-- Indexes for table `arm_history`
--
ALTER TABLE `arm_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `arm_history_ibfk_1` (`modifiedByAppUserid`) USING BTREE,
  ADD KEY `armid` (`armid`),
  ADD KEY `studyid` (`studyid`);

--
-- Indexes for table `cro`
--
ALTER TABLE `cro`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `crossformlinking`
--
ALTER TABLE `crossformlinking`
  ADD PRIMARY KEY (`id`),
  ADD KEY `studyid` (`studyid`),
  ADD KEY `parentFormid` (`parentFormid`),
  ADD KEY `parentFieldVerid` (`parentFieldVerid`),
  ADD KEY `childFormid` (`childFormid`),
  ADD KEY `childFieldVerid` (`childFieldVerid`);

--
-- Indexes for table `crossformlinking_history`
--
ALTER TABLE `crossformlinking_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `crossformlinking_history_ibfk_1` (`modifiedByAppUserid`),
  ADD KEY `crossFormLinkingid` (`crossFormLinkingid`),
  ADD KEY `parentFormid` (`parentFormid`),
  ADD KEY `parentFieldVerid` (`parentFieldVerid`),
  ADD KEY `childFormid` (`childFormid`),
  ADD KEY `childFieldVerid` (`childFieldVerid`),
  ADD KEY `studyid` (`studyid`);

--
-- Indexes for table `cro_history`
--
ALTER TABLE `cro_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cro_ibkf_1` (`modifiedByAppUserid`),
  ADD KEY `croid` (`croid`);

--
-- Indexes for table `editqueue`
--
ALTER TABLE `editqueue`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `field`
--
ALTER TABLE `field`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sectionTemplateid` (`sectionTemplateid`);

--
-- Indexes for table `fieldversion`
--
ALTER TABLE `fieldversion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fieldid` (`fieldid`),
  ADD KEY `fieldversion_ibfk_2` (`createdByAppUserid`),
  ADD KEY `fieldversion_ibfk_3` (`lastModifiedByAppUserid`);

--
-- Indexes for table `field_history`
--
ALTER TABLE `field_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `field_history_ibfk_1` (`modifiedByAppUserid`),
  ADD KEY `fieldid` (`fieldid`),
  ADD KEY `sectionTemplateid` (`sectionTemplateid`);

--
-- Indexes for table `form`
--
ALTER TABLE `form`
  ADD PRIMARY KEY (`id`),
  ADD KEY `visitid` (`visitid`),
  ADD KEY `formTemplateid` (`formTemplateid`);

--
-- Indexes for table `formtemplate`
--
ALTER TABLE `formtemplate`
  ADD PRIMARY KEY (`id`),
  ADD KEY `studyid` (`studyid`);

--
-- Indexes for table `formtemplate_history`
--
ALTER TABLE `formtemplate_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `formtemplate_history_ibfk_1` (`modifiedByAppUserid`),
  ADD KEY `studyid` (`studyid`),
  ADD KEY `formTemplateid` (`formTemplateid`);

--
-- Indexes for table `formtrigger`
--
ALTER TABLE `formtrigger`
  ADD PRIMARY KEY (`id`),
  ADD KEY `studyid` (`studyid`),
  ADD KEY `sourceFormVisitLinkid` (`sourceFormVisitLinkid`),
  ADD KEY `sourceTriggerFieldid` (`sourceTriggerFieldid`),
  ADD KEY `triggerFormid` (`triggerFormid`);

--
-- Indexes for table `formtrigger_history`
--
ALTER TABLE `formtrigger_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `formtrigger_history_ibfk_1` (`modifiedByAppUserid`),
  ADD KEY `studyid` (`studyid`),
  ADD KEY `sourceFormVisitLinkid` (`sourceFormVisitLinkid`),
  ADD KEY `sourceTriggerFieldid` (`sourceTriggerFieldid`),
  ADD KEY `triggerFormid` (`triggerFormid`),
  ADD KEY `formTriggerid` (`formTriggerid`);

--
-- Indexes for table `form_history`
--
ALTER TABLE `form_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `formid` (`formid`),
  ADD KEY `visitid` (`visitid`),
  ADD KEY `formTemplateid` (`formTemplateid`);

--
-- Indexes for table `qualityfinding`
--
ALTER TABLE `qualityfinding`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fieldid` (`fieldid`);

--
-- Indexes for table `qualityfinding_history`
--
ALTER TABLE `qualityfinding_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `qualityfinding_history_ibfk_1` (`modifiedByAppUserid`),
  ADD KEY `qualityFindingid` (`qualityFindingid`),
  ADD KEY `fieldid` (`fieldid`),
  ADD KEY `qTestid` (`qTestid`);

--
-- Indexes for table `sectiongroup`
--
ALTER TABLE `sectiongroup`
  ADD PRIMARY KEY (`id`),
  ADD KEY `studyid` (`studyid`);

--
-- Indexes for table `sectiongrouplink`
--
ALTER TABLE `sectiongrouplink`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sectionTemplateid` (`sectionTemplateid`),
  ADD KEY `sectionGroupid` (`sectionGroupid`);

--
-- Indexes for table `sectiongrouplink_history`
--
ALTER TABLE `sectiongrouplink_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sectiongrouplink_history_ibfk_1` (`modifiedByAppUserid`),
  ADD KEY `sectionGroupLinkid` (`sectionGroupLinkid`),
  ADD KEY `sectionTemplateid` (`sectionTemplateid`),
  ADD KEY `sectionGroupid` (`sectionGroupid`);

--
-- Indexes for table `sectiongroup_history`
--
ALTER TABLE `sectiongroup_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sectionGroupid` (`sectionGroupid`),
  ADD KEY `studyid` (`studyid`);

--
-- Indexes for table `sectiontempformlink`
--
ALTER TABLE `sectiontempformlink`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sectionTemplateid` (`sectionTemplateid`),
  ADD KEY `formTemplateid` (`formTemplateid`);

--
-- Indexes for table `sectiontempformlink_history`
--
ALTER TABLE `sectiontempformlink_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sectiontempformlink_history_ibfk_1` (`modifiedByAppUserid`),
  ADD KEY `sectionTempFormLinkid` (`sectionTempFormLinkid`),
  ADD KEY `sectionTemplateid` (`sectionTemplateid`),
  ADD KEY `formTemplateid` (`formTemplateid`);

--
-- Indexes for table `sectiontemplate`
--
ALTER TABLE `sectiontemplate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sectiontemplate_history`
--
ALTER TABLE `sectiontemplate_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sectiontemplate_history_ibfk_1` (`modifiedByAppUserid`),
  ADD KEY `sectionTemplateid` (`sectionTemplateid`);

--
-- Indexes for table `sponsor`
--
ALTER TABLE `sponsor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sponsor_history`
--
ALTER TABLE `sponsor_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sponsor_history_ibfk_1` (`modifiedByAppUserid`),
  ADD KEY `sponsorid` (`sponsorid`);

--
-- Indexes for table `study`
--
ALTER TABLE `study`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sponsorid` (`sponsorid`),
  ADD KEY `croid` (`croid`);

--
-- Indexes for table `study_history`
--
ALTER TABLE `study_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `study_history_ibfk_1` (`modifiedByAppUserid`),
  ADD KEY `studyid` (`studyid`),
  ADD KEY `sponsorid` (`sponsorid`),
  ADD KEY `croid` (`croid`);

--
-- Indexes for table `visit`
--
ALTER TABLE `visit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `armid` (`armid`);

--
-- Indexes for table `visittrigger`
--
ALTER TABLE `visittrigger`
  ADD PRIMARY KEY (`id`),
  ADD KEY `studyid` (`studyid`),
  ADD KEY `sourceFormVisitLinkid` (`sourceFormVisitLinkid`),
  ADD KEY `sourceTriggerFieldid` (`sourceTriggerFieldid`),
  ADD KEY `triggerVisitid` (`triggerVisitid`);

--
-- Indexes for table `visittrigger_history`
--
ALTER TABLE `visittrigger_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `visittrigger_history_ibfk_1` (`modifiedByAppUserid`),
  ADD KEY `visitTriggerid` (`visitTriggerid`),
  ADD KEY `studyid` (`studyid`),
  ADD KEY `sourceFormVisitLinkid` (`sourceFormVisitLinkid`),
  ADD KEY `sourceTriggerFieldid` (`sourceTriggerFieldid`),
  ADD KEY `triggerVisitid` (`triggerVisitid`);

--
-- Indexes for table `visit_history`
--
ALTER TABLE `visit_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `visit_history_ibfk_1` (`modifiedByAppUserid`),
  ADD KEY `visitid` (`visitid`),
  ADD KEY `armid` (`armid`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `arm`
--
ALTER TABLE `arm`
  ADD CONSTRAINT `arm_ibfk_1` FOREIGN KEY (`studyid`) REFERENCES `study` (`id`);

--
-- Constraints for table `arm_history`
--
ALTER TABLE `arm_history`
  ADD CONSTRAINT `arm_history_ibfk_1` FOREIGN KEY (`modifiedByAppUserid`) REFERENCES `appusers` (`id`);

--
-- Constraints for table `crossformlinking`
--
ALTER TABLE `crossformlinking`
  ADD CONSTRAINT `crossformlinking_ibfk_1` FOREIGN KEY (`studyid`) REFERENCES `study` (`id`),
  ADD CONSTRAINT `crossformlinking_ibfk_2` FOREIGN KEY (`parentFormid`) REFERENCES `form` (`id`),
  ADD CONSTRAINT `crossformlinking_ibfk_3` FOREIGN KEY (`parentFieldVerid`) REFERENCES `fieldversion` (`id`),
  ADD CONSTRAINT `crossformlinking_ibfk_4` FOREIGN KEY (`childFormid`) REFERENCES `form` (`id`),
  ADD CONSTRAINT `crossformlinking_ibfk_5` FOREIGN KEY (`childFieldVerid`) REFERENCES `fieldversion` (`id`);

--
-- Constraints for table `crossformlinking_history`
--
ALTER TABLE `crossformlinking_history`
  ADD CONSTRAINT `crossformlinking_history_ibfk_1` FOREIGN KEY (`modifiedByAppUserid`) REFERENCES `appusers` (`id`);

--
-- Constraints for table `cro_history`
--
ALTER TABLE `cro_history`
  ADD CONSTRAINT `cro_ibkf_1` FOREIGN KEY (`modifiedByAppUserid`) REFERENCES `appusers` (`id`);

--
-- Constraints for table `field`
--
ALTER TABLE `field`
  ADD CONSTRAINT `field_ibfk_1` FOREIGN KEY (`sectionTemplateid`) REFERENCES `sectiontemplate` (`id`);

--
-- Constraints for table `fieldversion`
--
ALTER TABLE `fieldversion`
  ADD CONSTRAINT `fieldversion_ibfk_1` FOREIGN KEY (`fieldid`) REFERENCES `field` (`id`),
  ADD CONSTRAINT `fieldversion_ibfk_2` FOREIGN KEY (`createdByAppUserid`) REFERENCES `appusers` (`id`),
  ADD CONSTRAINT `fieldversion_ibfk_3` FOREIGN KEY (`lastModifiedByAppUserid`) REFERENCES `appusers` (`id`);

--
-- Constraints for table `field_history`
--
ALTER TABLE `field_history`
  ADD CONSTRAINT `field_history_ibfk_1` FOREIGN KEY (`modifiedByAppUserid`) REFERENCES `appusers` (`id`);

--
-- Constraints for table `form`
--
ALTER TABLE `form`
  ADD CONSTRAINT `form_ibfk_1` FOREIGN KEY (`visitid`) REFERENCES `visit` (`id`),
  ADD CONSTRAINT `form_ibfk_2` FOREIGN KEY (`formTemplateid`) REFERENCES `formtemplate` (`id`);

--
-- Constraints for table `formtemplate`
--
ALTER TABLE `formtemplate`
  ADD CONSTRAINT `formtemplate_ibfk_1` FOREIGN KEY (`studyid`) REFERENCES `study` (`id`);

--
-- Constraints for table `formtemplate_history`
--
ALTER TABLE `formtemplate_history`
  ADD CONSTRAINT `formtemplate_history_ibfk_1` FOREIGN KEY (`modifiedByAppUserid`) REFERENCES `appusers` (`id`);

--
-- Constraints for table `formtrigger`
--
ALTER TABLE `formtrigger`
  ADD CONSTRAINT `formtrigger_ibfk_1` FOREIGN KEY (`studyid`) REFERENCES `study` (`id`),
  ADD CONSTRAINT `formtrigger_ibfk_2` FOREIGN KEY (`sourceFormVisitLinkid`) REFERENCES `form` (`id`),
  ADD CONSTRAINT `formtrigger_ibfk_3` FOREIGN KEY (`sourceTriggerFieldid`) REFERENCES `fieldversion` (`id`),
  ADD CONSTRAINT `formtrigger_ibfk_4` FOREIGN KEY (`triggerFormid`) REFERENCES `form` (`id`);

--
-- Constraints for table `formtrigger_history`
--
ALTER TABLE `formtrigger_history`
  ADD CONSTRAINT `formtrigger_history_ibfk_1` FOREIGN KEY (`modifiedByAppUserid`) REFERENCES `appusers` (`id`);

--
-- Constraints for table `qualityfinding`
--
ALTER TABLE `qualityfinding`
  ADD CONSTRAINT `qualityfinding_ibfk_1` FOREIGN KEY (`fieldid`) REFERENCES `fieldversion` (`id`);

--
-- Constraints for table `qualityfinding_history`
--
ALTER TABLE `qualityfinding_history`
  ADD CONSTRAINT `qualityfinding_history_ibfk_1` FOREIGN KEY (`modifiedByAppUserid`) REFERENCES `appusers` (`id`);

--
-- Constraints for table `sectiongroup`
--
ALTER TABLE `sectiongroup`
  ADD CONSTRAINT `sectiongroup_ibfk_1` FOREIGN KEY (`studyid`) REFERENCES `study` (`id`);

--
-- Constraints for table `sectiongrouplink`
--
ALTER TABLE `sectiongrouplink`
  ADD CONSTRAINT `sectiongrouplink_ibfk_1` FOREIGN KEY (`sectionTemplateid`) REFERENCES `sectiontemplate` (`id`),
  ADD CONSTRAINT `sectiongrouplink_ibfk_2` FOREIGN KEY (`sectionGroupid`) REFERENCES `sectiongroup` (`id`);

--
-- Constraints for table `sectiongrouplink_history`
--
ALTER TABLE `sectiongrouplink_history`
  ADD CONSTRAINT `sectiongrouplink_history_ibfk_1` FOREIGN KEY (`modifiedByAppUserid`) REFERENCES `appusers` (`id`);

--
-- Constraints for table `sectiontempformlink`
--
ALTER TABLE `sectiontempformlink`
  ADD CONSTRAINT `sectiontempformlink_ibfk_1` FOREIGN KEY (`sectionTemplateid`) REFERENCES `sectiontemplate` (`id`),
  ADD CONSTRAINT `sectiontempformlink_ibfk_2` FOREIGN KEY (`formTemplateid`) REFERENCES `formtemplate` (`id`);

--
-- Constraints for table `sectiontempformlink_history`
--
ALTER TABLE `sectiontempformlink_history`
  ADD CONSTRAINT `sectiontempformlink_history_ibfk_1` FOREIGN KEY (`modifiedByAppUserid`) REFERENCES `appusers` (`id`);

--
-- Constraints for table `sectiontemplate_history`
--
ALTER TABLE `sectiontemplate_history`
  ADD CONSTRAINT `sectiontemplate_history_ibfk_1` FOREIGN KEY (`modifiedByAppUserid`) REFERENCES `appusers` (`id`);

--
-- Constraints for table `sponsor_history`
--
ALTER TABLE `sponsor_history`
  ADD CONSTRAINT `sponsor_history_ibfk_1` FOREIGN KEY (`modifiedByAppUserid`) REFERENCES `appusers` (`id`);

--
-- Constraints for table `study`
--
ALTER TABLE `study`
  ADD CONSTRAINT `study_ibfk_1` FOREIGN KEY (`sponsorid`) REFERENCES `sponsor` (`id`),
  ADD CONSTRAINT `study_ibfk_2` FOREIGN KEY (`croid`) REFERENCES `cro` (`id`);

--
-- Constraints for table `study_history`
--
ALTER TABLE `study_history`
  ADD CONSTRAINT `study_history_ibfk_1` FOREIGN KEY (`modifiedByAppUserid`) REFERENCES `appusers` (`id`);

--
-- Constraints for table `visit`
--
ALTER TABLE `visit`
  ADD CONSTRAINT `visit_ibfk_1` FOREIGN KEY (`armid`) REFERENCES `arm` (`id`);

--
-- Constraints for table `visittrigger`
--
ALTER TABLE `visittrigger`
  ADD CONSTRAINT `visittrigger_ibfk_1` FOREIGN KEY (`studyid`) REFERENCES `study` (`id`),
  ADD CONSTRAINT `visittrigger_ibfk_2` FOREIGN KEY (`sourceFormVisitLinkid`) REFERENCES `form` (`id`),
  ADD CONSTRAINT `visittrigger_ibfk_3` FOREIGN KEY (`sourceTriggerFieldid`) REFERENCES `fieldversion` (`id`),
  ADD CONSTRAINT `visittrigger_ibfk_4` FOREIGN KEY (`triggerVisitid`) REFERENCES `visit` (`id`);

--
-- Constraints for table `visittrigger_history`
--
ALTER TABLE `visittrigger_history`
  ADD CONSTRAINT `visittrigger_history_ibfk_1` FOREIGN KEY (`modifiedByAppUserid`) REFERENCES `appusers` (`id`);

--
-- Constraints for table `visit_history`
--
ALTER TABLE `visit_history`
  ADD CONSTRAINT `visit_history_ibfk_1` FOREIGN KEY (`modifiedByAppUserid`) REFERENCES `appusers` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
